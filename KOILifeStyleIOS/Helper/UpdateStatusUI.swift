//
//  UpdateStatusUI.swift
//  KOILifeStyleIOS
//
//  Created by Apple on 12/29/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
protocol UpdateStatusUI {
    func onCardChange(status: Bool)
}
