//
//  UIImage+Extension.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/9/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
extension UIImage {
 
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
    }
  
  func resize(width: CGFloat) -> UIImage {
    let height = (width/self.size.width)*self.size.height
    return self.resize(size: CGSize(width: width, height: height))
  }
  
  func resize(height: CGFloat) -> UIImage {
    let width = (height/self.size.height)*self.size.width
    return self.resize(size: CGSize(width: width, height: height))
  }
  
  func resize(size: CGSize) -> UIImage {
    let widthRatio  = size.width/self.size.width
    let heightRatio = size.height/self.size.height
    var updateSize = size
    if(widthRatio > heightRatio) {
      updateSize = CGSize(width:self.size.width*heightRatio, height:self.size.height*heightRatio)
    } else if heightRatio > widthRatio {
      updateSize = CGSize(width:self.size.width*widthRatio,  height:self.size.height*widthRatio)
    }
    UIGraphicsBeginImageContextWithOptions(updateSize, false, UIScreen.main.scale)
    self.draw(in: CGRect(origin: .zero, size: updateSize))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage!
  }
  
  public class func gifImageWithData(_ data: Data) -> UIImage? {
         guard let source = CGImageSourceCreateWithData(data as CFData, nil) else {
             print("image doesn't exist")
             return nil
         }
         
         return UIImage.animatedImageWithSource(source)
     }
     
     public class func gifImageWithURL(_ gifUrl:String) -> UIImage? {
         guard let bundleURL:URL? = URL(string: gifUrl)
             else {
                 print("image named \"\(gifUrl)\" doesn't exist")
                 return nil
         }
         guard let imageData = try? Data(contentsOf: bundleURL!) else {
             print("image named \"\(gifUrl)\" into NSData")
             return nil
         }
         
         return gifImageWithData(imageData)
     }
     
     public class func gifImageWithName(_ name: String) -> UIImage? {
         guard let bundleURL = Bundle.main
             .url(forResource: name, withExtension: "gif") else {
                 print("SwiftGif: This image named \"\(name)\" does not exist")
                 return nil
         }
         guard let imageData = try? Data(contentsOf: bundleURL) else {
             print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
             return nil
         }
         
         return gifImageWithData(imageData)
     }
     
     class func delayForImageAtIndex(_ index: Int, source: CGImageSource!) -> Double {
         var delay = 0.1
         
         let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
         let gifProperties: CFDictionary = unsafeBitCast(
             CFDictionaryGetValue(cfProperties,
                 Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()),
             to: CFDictionary.self)
         
         var delayObject: AnyObject = unsafeBitCast(
             CFDictionaryGetValue(gifProperties,
                 Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()),
             to: AnyObject.self)
         if delayObject.doubleValue == 0 {
             delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties,
                 Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
         }
         
         delay = delayObject as! Double
         
         if delay < 0.1 {
             delay = 0.1
         }
         
         return delay
     }
     
     class func gcdForPair(_ a: Int?, _ b: Int?) -> Int {
         var a = a
         var b = b
         if b == nil || a == nil {
             if b != nil {
                 return b!
             } else if a != nil {
                 return a!
             } else {
                 return 0
             }
         }
         
//         if a < b {
//             let c = a
//             a = b
//             b = c
//         }
         
         var rest: Int
         while true {
             rest = a! % b!
             
             if rest == 0 {
                 return b!
             } else {
                 a = b
                 b = rest
             }
         }
     }
     
     class func gcdForArray(_ array: Array<Int>) -> Int {
         if array.isEmpty {
             return 1
         }
         
         var gcd = array[0]
         
         for val in array {
             gcd = UIImage.gcdForPair(val, gcd)
         }
         
         return gcd
     }
     
     class func animatedImageWithSource(_ source: CGImageSource) -> UIImage? {
         let count = CGImageSourceGetCount(source)
         var images = [CGImage]()
         var delays = [Int]()
         
         for i in 0..<count {
             if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                 images.append(image)
             }
             
             let delaySeconds = UIImage.delayForImageAtIndex(Int(i),
                 source: source)
             delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
         }
         
         let duration: Int = {
             var sum = 0
             
             for val: Int in delays {
                 sum += val
             }
             
             return sum
         }()
         
         let gcd = gcdForArray(delays)
         var frames = [UIImage]()
         
         var frame: UIImage
         var frameCount: Int
         for i in 0..<count {
             frame = UIImage(cgImage: images[Int(i)])
             frameCount = Int(delays[Int(i)] / gcd)
             
             for _ in 0..<frameCount {
                 frames.append(frame)
             }
         }
         
         let animation = UIImage.animatedImage(with: frames,
             duration: Double(duration) / 1000.0)
         
         return animation
     }
}


