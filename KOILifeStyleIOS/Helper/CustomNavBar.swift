//
//  CustomNavBar.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/28/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class CustomNavBar: UINavigationBar {

  lazy var backBtn: UIButton = {
    let btn = UIButton(type: .system)
    btn.setImage(#imageLiteral(resourceName: "backhome").withRenderingMode(.alwaysOriginal), for: .normal)
    btn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    return btn
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
     let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
   navItem.leftBarButtonItem = leftBarButton
    
    navigationBar.items = [navItem]
    
    addSubview(navigationBar)
  
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
