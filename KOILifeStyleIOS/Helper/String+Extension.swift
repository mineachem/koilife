//
//  String+Extension.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/14/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import CommonCrypto

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
  

   public var sha512: String {
         let data = self.data(using: .utf8) ?? Data()
         var digest = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
         data.withUnsafeBytes {
             _ = CC_SHA512($0.baseAddress, CC_LONG(data.count), &digest)
         }
         return digest.map({ String(format: "%02hhx", $0) }).joined(separator: "")
     }
  
  
  func isValidEmail() -> Bool {
           // here, `try!` will always succeed because the pattern is valid
           let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
           return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
       }
  
  func replace(string:String, replacement:String) -> String {
      return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
  }

  func removeWhitespace() -> String {
      return self.replace(string: " ", replacement: "")
  }

}
