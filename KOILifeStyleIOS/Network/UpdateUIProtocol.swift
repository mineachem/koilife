//
//  UpdateUIProtocol.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
import Foundation
protocol UpdateUIProtocol {
    func onUpdateUI(customCardsModel: CustomCardsModel)
}
