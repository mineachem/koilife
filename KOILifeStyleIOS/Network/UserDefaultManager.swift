//
//  UserDefaultManager.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
import SDWebImage
class UserDefaultManager: NSObject {
    static var sharedInstance = UserDefaultManager()

    func saveData(key: String,value: Any)
    {
        UserDefaults.standard.set(value, forKey: key)
    }
    func getData(key: String) -> String
    {
        return UserDefaults.standard.string(forKey: key) ?? ""
    }
  
    func getInt(key: String) -> Int
    {
        return UserDefaults.standard.integer(forKey: key)
    }
   
}
