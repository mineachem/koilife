//
//  IGNetworkRequest.swift
//  TestProject
//
//  Created by Apple on 12/14/19.
//  Copyright © 2019 IG. All rights reserved.
//

//
//  IGNetworkRequest.swift
//  TestProject
//
//  Created by Apple on 12/14/19.
//  Copyright © 2019 IG. All rights reserved.
//

import UIKit
import Alamofire


class IGNetworkRequest: NSObject {
static var shareInstance = IGNetworkRequest()

  // Handle Authentication challenge
  func getCertificateAuthentication(){
   
    let delegate: Alamofire.SessionDelegate = ServiceConfigure.Manager.delegate
      delegate.sessionDidReceiveChallenge = { session, challenge in
           var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
           var credential: URLCredential?
           if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                disposition = URLSession.AuthChallengeDisposition.useCredential
                credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
           } else {
                if challenge.previousFailureCount > 0 {
                     disposition = .cancelAuthenticationChallenge
                } else {
                     credential = ServiceConfigure.Manager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                     if credential != nil {
                          disposition = .useCredential
                     }
                }
           }
           return (disposition, credential)
      }
  }
  

 
func requestPOSTURL<T:Codable>(_ strURL : String, params : [String : Any]?, success:@escaping (T) -> Void, failure:@escaping (String) -> Void){
    //getCertificateAuthentication()
  Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (responseObject) -> Void in
       
        if responseObject.result.isSuccess {
            guard let data = responseObject.data else { return }
          
            do {
                let recoment =  try JSONDecoder().decode(T.self, from: data)
                success(recoment)
                
            }catch {
                
                failure(responseObject.result.error?.localizedDescription ?? "")
            }
        }
        if responseObject.result.isFailure {
            failure(responseObject.error?.localizedDescription ?? "Error")
        }
     }
    }
    
  
  func requestPUTURL<T:Codable>(_ strURL : String, params : [String : Any]?, success:@escaping (T) -> Void, failure:@escaping (String) -> Void){
   
  Alamofire.request(strURL, method: .put, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (responseObject) -> Void in
       
        if responseObject.result.isSuccess {
            guard let data = responseObject.data else { return }
          
            do {
                let recoment =  try JSONDecoder().decode(T.self, from: data)
                success(recoment)
                
            }catch {
                
                failure(responseObject.result.error?.localizedDescription ?? "")
            }
        }
        if responseObject.result.isFailure {
            failure(responseObject.error?.localizedDescription ?? "Error")
        }
     }
    }
  
    func requestPOSTBODYURL<T:Codable>(_ strURL : String, params : String,header: [String: String]?, success:@escaping (T) -> Void, failure:@escaping (String) -> Void){
        
       
               Alamofire.request(strURL, method: .post, parameters: nil, encoding: params , headers: header).responseJSON { (responseObject) -> Void in
                   if responseObject.result.isSuccess {
                       guard let data = responseObject.data else { return }
                       do {
                           let response =  try JSONDecoder().decode(T.self, from: data)
                        print(response)
                           success(response)
                       }catch {
                           failure(responseObject.result.error?.localizedDescription ?? "")
                       }
                   }else {
                       failure(responseObject.error?.localizedDescription ?? "Error")
                   }
                  
               }
        }
    
      func requestGET<T:Codable>(_ strURL: String, success:@escaping (T) -> Void, failure:@escaping (Error) -> Void)
      {
       // getCertificateAuthentication()
        Alamofire.request(strURL,encoding: JSONEncoding.default).responseJSON { (responseObject) -> Void in
              
              if responseObject.result.isSuccess {
                  guard let data = responseObject.data else { return }
                  
                  do {
                      let recoment =  try JSONDecoder().decode(T.self, from: data)
                      success(recoment)
                      
                  }catch {
                      print(error)
                  }
              }
              if responseObject.result.isFailure {
                  let error : Error = responseObject.result.error!
                  failure(error)
              }
          }
      }
  
}
extension String: ParameterEncoding {

    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }

}
