//
//  KoiApi.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/25/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import CoreLocation
class KoiService {
  static let share = KoiService()
  //static let baseUrl = "http://192.168.4.226:8000/api/v1"
 // static let baseUrl = "http://192.168.5.247:8000/api/v1"
// static let baseUrl = "http://192.168.4.244:8000/api/v1"
   static let baseUrl = "http://uat-api.karanakkoithe.com/api/v1"
  
  var SLIDESHOW = "SLIDESHOW"
  var HOMEPAGE = "HOMEPAGE"
    
  var BASE_SLIDE_IMAGE = baseUrl+"/back-office/slideShow"
  var BASE_MENU_IMAGE = baseUrl+"/back-office/life-style/product"
  var BASE_IMAGE = baseUrl + "/life-style/background-image-app/images/"
  var BASE_CARD_IMAGE = baseUrl + "/life-style/image-card-type/images/"
  var BRANCH_BASE_IMAGE = baseUrl + "/back-office/branch/images/"
  var REWARD_BASE_IMAGE = baseUrl + "/life-style/reward/images/"
  //MARK:Customer URL
  let customerInfoUrl = baseUrl + "/life-style/customer/all"
  let presignupUrl = baseUrl + "/life-style/customer/pre-sign-up"
  let signupUrl = baseUrl + "/life-style/customer/sign-up"
  let loginUrl = baseUrl + "/life-style/customer/login"
  let signupWithFacebookUrl = baseUrl + "/life-style/customer/sign-up"
  let checkFacebookIdUrl = baseUrl + "/life-style/customer/check-facebook-id"
  let checkFacebookIdiosUrl = baseUrl + "/life-style/customer/ios-check-facebook-id"
  let loginFbUrl = baseUrl + "/life-style/customer/fb-sign-up"
  let inboxUrl = baseUrl + "/back-office/life-style/inbox/notification/list"
  let editprofileUrl = baseUrl + "/life-style/customer/edit-profile-customer/"
  let uploadUrl = baseUrl + "/life-style/customer/upload"
  let displayImgUrl = baseUrl + "/back-office/life-style/promotion/images/"
  let linkToEmailUrl = baseUrl + "/life-style/customer/link-to-email"
  let changePasswordUrl = baseUrl + "/life-style/customer/change-password/"
  let signoutUrl = baseUrl + "/life-style/customer/logout"
  let getCustomerInfoURL = baseUrl + "/life-style/customer/ios-customer-info"
  //MARK:REWARD POINT
  let rewardpointAllUrl = baseUrl + "/life-style/reward/all"
  let resetpasswordUrl = baseUrl + "/life-style/customer/email/send-reset-code"
  
  //MARK:Slideshow
  let getAllBackgroundURL = baseUrl + "/life-style/background-image-app/all"

  let resetpasswordByPhoneUrl =  baseUrl + "/life-style/customer/send-reset-code"
  let customerResetPasswordUrl = baseUrl + "/life-style/customer/reset-password"
    
    
  //MARK: Promotion
  let promotionAllUrl = baseUrl + "/back-office/life-style/promotion/customer-promotion/list"
 
  //MARK:Outlet
  let outletnearbyUrl = baseUrl + "/life-style/outlet/findNearBy"
  let getAllOutlet = baseUrl + "/life-style/outlet/all"
  //MARK:Categories
  let categoriesUrl = baseUrl + "/back-office/category/mobile/list?offset=0&max=1000"
  let categoryUrl = baseUrl + "/back-office/category"
  //MARK:Slideshow
  let slideshow = baseUrl + "/back-office/slideShow"
  let slideShowList = baseUrl + "/back-office/slideShow/list?size=10&page=0"
  
  //MARK:Product
  let productUrl = baseUrl + "/back-office/life-style/product"
  let productListUrl = baseUrl + "/back-office/life-style/product/product-branch/list?size=10&page=0"
  let getProductAllCategory = baseUrl + "/back-office/product/list?size=10&page="
  let getProductByCategory = baseUrl + "/back-office/life-style/product/category/list?"

  //MARK:Customer Card
  let customerCardListUrl = baseUrl + "/life-style/customer-card/card"
    let customerAddCard = baseUrl + "/life-style/customer-card/add-card"
      let customerUpdateCardStatus = baseUrl + "/life-style/customer-card/cardStatus/0"

  
  //MARK:Customer History
  
  let customerHistoryListUrl = baseUrl + "/life-style/customer-history/list/"
  let customerHistoryDetailUrl = baseUrl + "/life-style/customer-history/detail/"
  
  //MARK:Helper
  
  let helperUrl = baseUrl + "/back-office/life-style/help/all"
  
  //MARK:Term & conditon
  let termconditionUrl = baseUrl + "/back-office/life-style/term-condition/list"
  
  //MARK:Pravacy
  let privacyUrl = baseUrl + "/back-office/life-style/privacy-policy/list"
  
  //MARK:Deactivate
  let deactivateUrl = baseUrl + "/life-style/customer-card/cardStatus"
  
  
    static var mLate = 0.0
    static var mLng = 0.0
}
