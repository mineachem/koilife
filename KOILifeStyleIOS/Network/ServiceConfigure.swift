//
//  ServiceConfigure.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/18/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Alamofire

class ServiceConfigure {
   static var Manager: Alamofire.SessionManager = {

       // Create the server trust policies
       let serverTrustPolicies: [String: ServerTrustPolicy] = [

            "http://uat-api.karanakkoithe.com/api/v1": .disableEvaluation
       ]

       // Create custom manager
       let configuration = URLSessionConfiguration.default
       configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
       let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
       )

       return manager
  }()
  

}

