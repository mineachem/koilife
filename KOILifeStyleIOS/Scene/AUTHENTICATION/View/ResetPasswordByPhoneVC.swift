//
//  ResetPasswordByPhoneVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/31/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ResetPasswordByPhoneVC: UIViewController {

  lazy var resetPasswordByPhoneView = ResetPasswordByPhoneView()
  let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
  
  var phoneNumber:String?
  var count:Int = 2
  var timer:Timer?
  var totalTime = 120
  
  override func loadView() {
    super.loadView()
    self.view = resetPasswordByPhoneView
  }
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
      setupView()
      setupEvent()
      setupIndicator()
    }
  
  fileprivate func setupIndicator(){
     
        loading.translatesAutoresizingMaskIntoConstraints = false
         
        view.addSubview(loading)
        NSLayoutConstraint.activate([
          loading.widthAnchor.constraint(equalToConstant: 40),
          loading.heightAnchor.constraint(equalToConstant: 40),
          loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
          loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
     
   }
  
  func showLoading()
    {
      loading.startAnimating()
    }

    func hideLoading()
    {
      loading.stopAnimating()
    }
    
  fileprivate func setupView(){
    resetPasswordByPhoneView.pincodeTextField.delegate = self
    resetPasswordByPhoneView.newPasswordTextField.delegate = self
    resetPasswordByPhoneView.confirmPasswordTextField.delegate = self
  }

  fileprivate func setupEvent(){
    resetPasswordByPhoneView.submitBtn.addTarget(self, action: #selector(handleResetPasswTapped), for: .touchUpInside)
    resetPasswordByPhoneView.pincodeBtn.addTarget(self, action: #selector(handlePincodeTapped), for: .touchUpInside)
  }
  
  @objc private func handleResetPasswTapped(){
    
    let pincode = resetPasswordByPhoneView.pincodeTextField.text!
    let newpassword = resetPasswordByPhoneView.newPasswordTextField.text!
    let confirmPassword = resetPasswordByPhoneView.confirmPasswordTextField.text!

    if pincode.isEmpty {
      resetPasswordByPhoneView.requirementLabel.isHidden = false
      resetPasswordByPhoneView.requirementLabel.text = "your pincode empty"
      resetPasswordByPhoneView.pincodeTextField.becomeFirstResponder()
    }else if newpassword.isEmpty {
      resetPasswordByPhoneView.requirementLabel.isHidden = false
      resetPasswordByPhoneView.requirementLabel.text = "your new password empty"
      resetPasswordByPhoneView.newPasswordTextField.becomeFirstResponder()
    }else if confirmPassword.isEmpty {
      resetPasswordByPhoneView.requirementLabel.isHidden = false
      resetPasswordByPhoneView.requirementLabel.text = "your confirm password empty"
      resetPasswordByPhoneView.confirmPasswordTextField.becomeFirstResponder()
    }else if newpassword != confirmPassword {
      resetPasswordByPhoneView.requirementLabel.isHidden = false
      resetPasswordByPhoneView.requirementLabel.text = "your new password and confirm password not match"
      resetPasswordByPhoneView.confirmPasswordTextField.becomeFirstResponder()
    }


      let KEY = "KoiTheCambodia0123456789@Pass0rd"
      let IV = "UjXn2r5u8x/A?D(G"

      let encryptin = AESUtils.instance.encryptionAES(value: newpassword, key256: KEY, iv: IV)

    if (!pincode.isEmpty) && (!newpassword.isEmpty) && (!confirmPassword.isEmpty){
      let param:[String:Any] = [
        "phoneNumber": "+855" + phoneNumber!.dropFirst(),
         "password": encryptin,
         "confirmCode": pincode
      ]
      self.showLoading()
      IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.customerResetPasswordUrl, params: param, success: { (resetupPasswd:SignupUser) in
        if resetupPasswd.response?.code == 200 {
          self.hideLoading()
          let alertResetController = UIAlertController(title: "Reset Password By Phone", message: resetupPasswd.response!.message, preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                          }
                      alertResetController.addAction(okAction)
                self.present(alertResetController, animated: true)
        }else {

        }
      }) { (failure) in
        print(failure)
      }
    }
  }
  
  @objc private func handlePincodeTapped(){
     let param:[String:Any] = [
           "phoneNumber":"+855\(String(describing: phoneNumber!.dropFirst()))",
           "termcondit":true
         ]
     
           IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.presignupUrl, params: param, success: { (jsonResponse:Presignup) in
                if jsonResponse.response?.code == 200 {
                 }else {
                  self.hideLoading()
                 }
              }) { (failure) in
                 self.hideLoading()
                print(failure)
              }
  }
  
//  @objc private func updateTimer(){
//    resetPasswordByPhoneView.pincodeBtn.setTitle(self.timeFormatted(self.totalTime), for: .normal)  // will show timer
//     //signupFormView.timerLabel.isHidden = false
//      if totalTime != 0 {
//                        totalTime -= 1  // decrease counter timer
//
//        resetPasswordByPhoneView.pincodeBtn.isUserInteractionEnabled = false
//                    } else {
//                  if let timers = self.timer {
//                            timers.invalidate()
//                            self.timer = nil
//
//       resetPasswordByPhoneView.pincodeBtn.isUserInteractionEnabled = true
//       resetPasswordByPhoneView.pincodeBtn.setTitle("resent", for: .normal)
//            }
//        }
//  }
//
//  func timeFormatted(_ totalSeconds: Int) -> String {
//      let seconds: Int = totalSeconds % 60
//      let minutes: Int = (totalSeconds / 60) % 60
//      return String(format: "%02d:%02d", minutes, seconds)
//  }
}

extension ResetPasswordByPhoneVC:UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
     textField.resignFirstResponder()
     return true
   }
}
