//
//  SignupFormFacebookCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/23/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

protocol GetValueFromDatePickerDelegate {
  func getGenderValue(value: String)
  func getDobValue(value:String)
}

protocol CheckSentPinDelegate {
  func getSentPin()
}
class SignupFormFacebookCell: UITableViewCell {
  
  let genderString = ["Male", "Female"]
  lazy var genderPicker = UIPickerView()
  lazy var datePicker = UIDatePicker()
  var getvalueFromPickerDelegate:GetValueFromDatePickerDelegate?
  var checkSentPinDelegate:CheckSentPinDelegate?
  
  lazy var descritionTitleLabel: UILabel = {
    let label = UILabel()
    label.text = "To complete the SIGN UP precedure please fill the requre fields. We will not share your info with others"
    label.textAlignment = .center
    label.numberOfLines = 0
    label.textColor = .black
    label.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  lazy var titleFirstNameLabel: UILabel = {
          let label = UILabel()
       
        label.text = "First Name<sup>*</sup>".htmlToString
       
          label.textAlignment = .left
          label.font = UIFont(name: "Montserrat-Regular", size: 12)
          label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          label.translatesAutoresizingMaskIntoConstraints = false
          return label

      }()

      lazy var firstnametextField: UITextField = {
          let textfield = UITextField()
          textfield.placeholder = "First Name"
          textfield.tag = 0
          textfield.textColor = .lightGray
          textfield.tintColor = .clear
          textfield.autocapitalizationType = .none
          textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
          textfield.translatesAutoresizingMaskIntoConstraints = false
          return textfield
      }()
    
    lazy var bottomLineFirstnameView: UIView = {
          let line = UIView()
          line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          line.translatesAutoresizingMaskIntoConstraints = false
          return line
      }()
    
    lazy var titleLastNameLabel: UILabel = {
          let label = UILabel()
      label.text = "Last Name<sup>*</sup>".htmlToString
          label.textAlignment = .left
          label.font = UIFont(name: "Montserrat-Regular", size: 12)
          label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          label.translatesAutoresizingMaskIntoConstraints = false
          return label

      }()

      lazy var lastnametextField: UITextField = {
          let textfield = UITextField()
          textfield.placeholder = "Last Name"
          textfield.autocapitalizationType = .none
          textfield.tag = 1
          textfield.textColor = .lightGray
          textfield.tintColor = .clear
          textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
          textfield.translatesAutoresizingMaskIntoConstraints = false
          return textfield
      }()
    
    lazy var bottomLineLastnameView: UIView = {
          let line = UIView()
          line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          line.translatesAutoresizingMaskIntoConstraints = false
          return line
      }()
    
    lazy var titleEmailLabel: UILabel = {
           let label = UILabel()
           label.text = "Email"
           label.textAlignment = .left
           label.font = UIFont(name: "Montserrat-Regular", size: 12)
           label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
           label.translatesAutoresizingMaskIntoConstraints = false
           return label

       }()

      lazy var emailtextField: UITextField = {
           let textfield = UITextField()
           textfield.placeholder = "Email"
           textfield.tag = 2
           textfield.textColor = .lightGray
           textfield.tintColor = .clear
           textfield.autocapitalizationType = .none
           textfield.font = UIFont.systemFont(ofSize: 14, weight: .medium)
           textfield.translatesAutoresizingMaskIntoConstraints = false
           return textfield
       }()
    
    lazy var bottomLineEmailView: UIView = {
          let line = UIView()
          line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          line.translatesAutoresizingMaskIntoConstraints = false
          return line
      }()
    
  
  lazy var titleaddressLabel: UILabel = {
          let label = UILabel()
          label.text = "Address"
          label.textAlignment = .left
          label.font = UIFont(name: "Montserrat-Regular", size: 12)
          label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          label.translatesAutoresizingMaskIntoConstraints = false
          return label

      }()

     lazy var addresstextField: UITextField = {
          let textfield = UITextField()
          textfield.placeholder = "Address"
          textfield.autocapitalizationType = .none
          textfield.tag = 3
          textfield.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
          textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
          textfield.translatesAutoresizingMaskIntoConstraints = false
          return textfield
      }()
     
     lazy var bottomLineAddressView: UIView = {
           let line = UIView()
           line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
           line.translatesAutoresizingMaskIntoConstraints = false
           return line
       }()
  
    lazy var titlePhoneNumLabel: UILabel = {
         let label = UILabel()
         label.text = "Phone Number<sup>*</sup>".htmlToString
         label.textAlignment = .left
         label.font = UIFont(name: "Montserrat-Regular", size: 12)
         label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
         label.translatesAutoresizingMaskIntoConstraints = false
         return label

     }()

    lazy var phoneNumtextField: UITextField = {
         let textfield = UITextField()
         textfield.placeholder = "Phone Number"
         textfield.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
         textfield.tag = 4
         textfield.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
         textfield.autocapitalizationType = .none
         textfield.keyboardType = .numberPad
         textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
         textfield.translatesAutoresizingMaskIntoConstraints = false
         return textfield
     }()
    
  lazy var sentPinBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("Send Pin", for: .normal)
    button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 12)!
    button.setTitleColor(#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), for: .normal)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
    lazy var bottomLinephoneNumView: UIView = {
          let line = UIView()
          line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          line.translatesAutoresizingMaskIntoConstraints = false
          return line
      }()
  
  
    lazy var titlePinLabel: UILabel = {
         let label = UILabel()
         label.text = "Pin Code<sup>*</sup>".htmlToString
         label.textAlignment = .left
         label.font = UIFont(name: "Montserrat-Regular", size: 12)
         label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
         label.translatesAutoresizingMaskIntoConstraints = false
         return label

     }()

    lazy var pintextField: UITextField = {
         let textfield = UITextField()
         textfield.placeholder = "Pin Code"
         textfield.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
         textfield.tag = 5
         textfield.keyboardType = .numberPad
         textfield.autocapitalizationType = .none
         textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
         textfield.translatesAutoresizingMaskIntoConstraints = false
         return textfield
     }()
    
  
    lazy var bottomLinepinView: UIView = {
          let line = UIView()
          line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          line.translatesAutoresizingMaskIntoConstraints = false
          return line
      }()
  
    
    lazy var titleGenderLabel: UILabel = {
          let label = UILabel()
          label.text = "Gender<sup>*</sup>".htmlToString
          label.textAlignment = .left
          label.font = UIFont(name: "Montserrat-Regular", size: 12)
          label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          label.translatesAutoresizingMaskIntoConstraints = false
          return label

      }()

    
     lazy var gendertextField: UITextField = {
          let textfield = UITextField()
          textfield.placeholder = "Gender"
          textfield.tag = 6
          textfield.tintColor = .clear
           textfield.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
          textfield.autocapitalizationType = .none
          textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
          textfield.translatesAutoresizingMaskIntoConstraints = false
          return textfield
      }()
    
    lazy var bottomLinegenderView: UIView = {
          let line = UIView()
          line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          line.translatesAutoresizingMaskIntoConstraints = false
          return line
      }()
  
    
    lazy var titledobLabel: UILabel = {
         let label = UILabel()
         label.text = "Date Of birth"
         label.textAlignment = .left
         label.font = UIFont(name: "Montserrat-Regular", size: 12)
         label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
         label.translatesAutoresizingMaskIntoConstraints = false
         return label

     }()

    lazy var dobtextField: UITextField = {
         let textfield = UITextField()
       textfield.placeholder = "choose your birthday"
       textfield.autocapitalizationType = .none
       textfield.tag = 7
        textfield.tintColor = .clear
        textfield.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
       textfield.translatesAutoresizingMaskIntoConstraints = false
         return textfield
     }()
    
    lazy var bottomLinedobView: UIView = {
          let line = UIView()
          line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          line.translatesAutoresizingMaskIntoConstraints = false
          return line
      }()
    
   
    

   lazy var titlepasswordLabel: UILabel = {
        let label = UILabel()
        label.text = "Password<sup>*</sup>".htmlToString
        label.textAlignment = .left
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label

    }()

   lazy var passwordtextField: UITextField = {
        let textfield = UITextField()
       textfield.placeholder = "Password"
        textfield.autocapitalizationType = .none
        textfield.tag = 8
        textfield.isSecureTextEntry = true
        textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
   
   lazy var bottomLinePasswordView: UIView = {
         let line = UIView()
         line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
         line.translatesAutoresizingMaskIntoConstraints = false
         return line
     }()
    
  lazy var titleConfirmpasswordLabel: UILabel = {
       let label = UILabel()
       label.text = "Confirm Password<sup>*</sup>".htmlToString
       label.textAlignment = .left
       label.font = UIFont(name: "Montserrat-Regular", size: 12)
       label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label

   }()

  lazy var confirmPasswordtextField: UITextField = {
       let textfield = UITextField()
       textfield.placeholder = "Confirm Password"
       textfield.autocapitalizationType = .none
       textfield.tag = 9
       textfield.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       textfield.isSecureTextEntry = true
       textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
       textfield.translatesAutoresizingMaskIntoConstraints = false
       return textfield
   }()
  
  lazy var bottomLineConfirmPasswordView: UIView = {
        let line = UIView()
        line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
  
    lazy var requirementLabel: UILabel = {
      let label = UILabel()
      label.text = "fklllkslkslsllsdf"
      label.textAlignment = .center
      label.textColor = .red
      label.numberOfLines = 0
      label.isHidden = true
      label.font = UIFont(name: "Montserrat-Regular", size: 14)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    setupView()
    setupEvent()
    setupPicker()
  }
  
  fileprivate func setupEvent(){
//    gendertextField.addTarget(self, action: #selector(handleGenderTapped), for: .touchDown)
    dobtextField.addTarget(self, action: #selector(handleDobTapped), for: .touchDown)
    sentPinBtn.addTarget(self, action: #selector(handleSendPinTapped), for: .touchUpInside)
  }
  
  
  @objc private func handleSendPinTapped(){
    checkSentPinDelegate?.getSentPin()
  }
  
//  @objc private func handleGenderTapped(){
//    getvalueFromPickerDelegate?.getGenderValue()
//  }
  
  
  @objc private func handleDobTapped(){
      let datePickerView = UIDatePicker()
      datePickerView.datePickerMode = .date
      dobtextField.inputView = datePickerView
      
      let toolbar = UIToolbar()
      toolbar.sizeToFit()
      
      let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneClicked))
      toolbar.setItems([doneButton], animated: true)
      
      dobtextField.inputAccessoryView = toolbar
      
      datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
  
  }
  
  @objc func doneClicked(){
    self.endEditing(true)
  }
  
  
  @objc func handleDatePicker(sender:UIDatePicker){
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MMM-yyyy"
    if dateFormatter.string(from: sender.date) != ""{
        dobtextField.text = dateFormatter.string(from: sender.date)
        getvalueFromPickerDelegate?.getDobValue(value: dobtextField.text!)
      
    }else {
      dobtextField.text = ""
      getvalueFromPickerDelegate?.getDobValue(value: dobtextField.text!)
    }
  }
  
  fileprivate func setupView(){
         
          let contentFirstNameHStack = UIStackView(arrangedSubviews: [titleFirstNameLabel, firstnametextField])
              contentFirstNameHStack.axis = .horizontal
              contentFirstNameHStack.alignment = .fill
              contentFirstNameHStack.distribution = .equalSpacing
              contentFirstNameHStack.translatesAutoresizingMaskIntoConstraints = false

         
         let contentLastNameHStack = UIStackView(arrangedSubviews: [titleLastNameLabel, lastnametextField])
                   contentLastNameHStack.axis = .horizontal
                   contentLastNameHStack.alignment = .fill
                   contentLastNameHStack.distribution = .equalSpacing
                   contentLastNameHStack.translatesAutoresizingMaskIntoConstraints = false

         let contentEmailHStack = UIStackView(arrangedSubviews: [titleEmailLabel, emailtextField])
                   contentEmailHStack.axis = .horizontal
                   contentEmailHStack.alignment = .fill
                   contentEmailHStack.distribution = .equalSpacing
                   contentEmailHStack.translatesAutoresizingMaskIntoConstraints = false

         
         let contentPhoneNumHStack = UIStackView(arrangedSubviews: [titlePhoneNumLabel, phoneNumtextField,sentPinBtn])
                   contentPhoneNumHStack.axis = .horizontal
                   contentPhoneNumHStack.alignment = .fill
                   contentPhoneNumHStack.distribution = .equalSpacing
                   contentPhoneNumHStack.translatesAutoresizingMaskIntoConstraints = false

    let contentPinHStack = UIStackView(arrangedSubviews: [titlePinLabel, pintextField])
                      contentPinHStack.axis = .horizontal
                      contentPinHStack.alignment = .fill
                      contentPinHStack.distribution = .equalSpacing
                      contentPinHStack.translatesAutoresizingMaskIntoConstraints = false
    

         let contentGenderHStack = UIStackView(arrangedSubviews: [titleGenderLabel, gendertextField])
                   contentGenderHStack.axis = .horizontal
                   contentGenderHStack.alignment = .fill
                   contentGenderHStack.distribution = .equalSpacing
                   contentFirstNameHStack.translatesAutoresizingMaskIntoConstraints = false


    
    
         let contentdobHStack = UIStackView(arrangedSubviews: [titledobLabel, dobtextField])
                   contentdobHStack.axis = .horizontal
                   contentdobHStack.alignment = .fill
                   contentdobHStack.distribution = .equalSpacing
                   contentdobHStack.translatesAutoresizingMaskIntoConstraints = false



         let contentaddressHStack = UIStackView(arrangedSubviews: [titleaddressLabel, addresstextField])
                   contentaddressHStack.axis = .horizontal
                   contentaddressHStack.alignment = .fill
                   contentaddressHStack.distribution = .equalSpacing
                   contentaddressHStack.translatesAutoresizingMaskIntoConstraints = false
     
     let contentpasswordHStack = UIStackView(arrangedSubviews: [titlepasswordLabel, passwordtextField])
     contentpasswordHStack.axis = .horizontal
     contentpasswordHStack.alignment = .fill
     contentpasswordHStack.distribution = .equalSpacing
     contentpasswordHStack.translatesAutoresizingMaskIntoConstraints = false
     
     let contentconfirmPasswordHStack = UIStackView(arrangedSubviews: [titleConfirmpasswordLabel, confirmPasswordtextField])
        contentconfirmPasswordHStack.axis = .horizontal
        contentconfirmPasswordHStack.alignment = .fill
        contentconfirmPasswordHStack.distribution = .equalSpacing
        contentconfirmPasswordHStack.translatesAutoresizingMaskIntoConstraints = false
  

         let contentFirstNameVStack = UIStackView(arrangedSubviews: [contentFirstNameHStack,bottomLineFirstnameView])
         contentFirstNameVStack.axis = .vertical
         contentFirstNameVStack.alignment = .fill
         contentFirstNameVStack.distribution = .fill
         
         contentFirstNameVStack.translatesAutoresizingMaskIntoConstraints = false
         
     let contentLastNameVStack = UIStackView(arrangedSubviews: [contentLastNameHStack,bottomLineLastnameView])
          contentLastNameVStack.axis = .vertical
          contentLastNameVStack.alignment = .fill
          contentLastNameVStack.distribution = .fill
         
          contentLastNameVStack.translatesAutoresizingMaskIntoConstraints = false
         
     let contentEmailVStack = UIStackView(arrangedSubviews: [contentEmailHStack,bottomLineEmailView])
           contentEmailVStack.axis = .vertical
           contentEmailVStack.alignment = .fill
           contentEmailVStack.distribution = .fill
           
           contentEmailVStack.translatesAutoresizingMaskIntoConstraints = false
         
     let contentPhoneNumVStack = UIStackView(arrangedSubviews: [contentPhoneNumHStack,bottomLinephoneNumView])
     contentPhoneNumVStack.axis = .vertical
     contentPhoneNumVStack.alignment = .fill
     contentPhoneNumVStack.distribution = .fill
     contentPhoneNumVStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentPinVStack = UIStackView(arrangedSubviews: [contentPinHStack,bottomLinepinView])
        contentPinVStack.axis = .vertical
        contentPinVStack.alignment = .fill
        contentPinVStack.distribution = .fill
        contentPinVStack.translatesAutoresizingMaskIntoConstraints = false
         
    let contentGenderVStack = UIStackView(arrangedSubviews: [contentGenderHStack,bottomLinegenderView])
    contentGenderVStack.axis = .vertical
    contentGenderVStack.alignment = .fill
    contentGenderVStack.distribution = .fill
    
    contentGenderVStack.translatesAutoresizingMaskIntoConstraints = false
         
   let contentdobVStack = UIStackView(arrangedSubviews: [contentdobHStack,bottomLinedobView])
   contentdobVStack.axis = .vertical
   contentdobVStack.alignment = .fill
   contentdobVStack.distribution = .fill
   contentdobVStack.translatesAutoresizingMaskIntoConstraints = false
         
     let contentaddressVStack = UIStackView(arrangedSubviews: [contentaddressHStack,bottomLineAddressView])
     contentaddressVStack.axis = .vertical
     contentaddressVStack.alignment = .fill
     contentaddressVStack.distribution = .fill
     contentaddressVStack.translatesAutoresizingMaskIntoConstraints = false
         
     
     let contentpasswordVStack = UIStackView(arrangedSubviews: [contentpasswordHStack,bottomLinePasswordView])
        contentpasswordVStack.axis = .vertical
        contentpasswordVStack.alignment = .fill
        contentpasswordVStack.distribution = .fill
        contentpasswordVStack.translatesAutoresizingMaskIntoConstraints = false
     
     let contentConfirmPasswordVStack = UIStackView(arrangedSubviews: [contentconfirmPasswordHStack,bottomLineConfirmPasswordView])
           contentConfirmPasswordVStack.axis = .vertical
           contentConfirmPasswordVStack.alignment = .fill
           contentConfirmPasswordVStack.distribution = .fill
           contentConfirmPasswordVStack.translatesAutoresizingMaskIntoConstraints = false
       
     let contentEditProfileFormVStack = UIStackView(arrangedSubviews: [descritionTitleLabel,contentFirstNameVStack,contentLastNameVStack,contentEmailVStack,contentConfirmPasswordVStack,contentPhoneNumVStack,contentPinVStack,contentGenderVStack,contentdobVStack,contentpasswordVStack,contentConfirmPasswordVStack,requirementLabel])
         contentEditProfileFormVStack.axis = .vertical
         contentEditProfileFormVStack.alignment = .fill
         contentEditProfileFormVStack.distribution = .equalSpacing
         contentEditProfileFormVStack.translatesAutoresizingMaskIntoConstraints = false
          
         addSubview(contentEditProfileFormVStack)
         
         NSLayoutConstraint.activate([
           contentEditProfileFormVStack.topAnchor.constraint(equalTo: topAnchor,constant: 20),
           contentEditProfileFormVStack.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
           contentEditProfileFormVStack.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
           contentEditProfileFormVStack.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10),
           firstnametextField.leadingAnchor.constraint(equalTo: titleFirstNameLabel.leadingAnchor,constant: 90),
           lastnametextField.leadingAnchor.constraint(equalTo: titleLastNameLabel.leadingAnchor,constant: 90),
           pintextField.leadingAnchor.constraint(equalTo: titlePinLabel.leadingAnchor,constant: 70),
           emailtextField.leadingAnchor.constraint(equalTo: titleEmailLabel.leadingAnchor,constant: 50),
           gendertextField.leadingAnchor.constraint(equalTo: titleGenderLabel.leadingAnchor,constant: 70),
           dobtextField.leadingAnchor.constraint(equalTo: titledobLabel.leadingAnchor,constant: 90),
           addresstextField.leadingAnchor.constraint(equalTo: titleaddressLabel.leadingAnchor,constant: 60),
           passwordtextField.leadingAnchor.constraint(equalTo: titlepasswordLabel.leadingAnchor,constant: 90),
           confirmPasswordtextField.leadingAnchor.constraint(equalTo: titleConfirmpasswordLabel.leadingAnchor,constant: 130),
           bottomLineFirstnameView.heightAnchor.constraint(equalToConstant: 0.5),
           bottomLinephoneNumView.heightAnchor.constraint(equalToConstant: 0.5),
           bottomLineLastnameView.heightAnchor.constraint(equalToConstant: 0.5),
           bottomLineEmailView.heightAnchor.constraint(equalToConstant: 0.5),
           bottomLinegenderView.heightAnchor.constraint(equalToConstant: 0.5),
           bottomLinedobView.heightAnchor.constraint(equalToConstant: 0.5),
           bottomLineAddressView.heightAnchor.constraint(equalToConstant: 0.5),
           bottomLinePasswordView.heightAnchor.constraint(equalToConstant: 0.5),
           bottomLineConfirmPasswordView.heightAnchor.constraint(equalToConstant: 0.5),
           bottomLinepinView.heightAnchor.constraint(equalToConstant: 0.5)
         ])
         
       }
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

extension SignupFormFacebookCell: UIPickerViewDataSource, UIPickerViewDelegate{
    
     func setupPicker(){
         genderPicker.backgroundColor = .white
         genderPicker.delegate = self
         genderPicker.dataSource = self
         gendertextField.inputView = genderPicker
        
         datePicker.datePickerMode = .date
         datePicker.backgroundColor = .white
         datePicker.addTarget(self, action: #selector(pickDate), for: .valueChanged)
         dobtextField.inputView = datePicker
     }
    
    @objc func pickDate(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dobtextField.text = dateFormatter.string(from: datePicker.date)
    }

     func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
     }

     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return genderString.count
     }

     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return genderString[row]
     }

     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         gendertextField.text = genderString[row]
         getvalueFromPickerDelegate?.getGenderValue(value:gendertextField.text!)
     }
     
 }
