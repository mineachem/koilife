//
//  SignupWithFacebookView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/23/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class SignupWithFacebookView: UIView {

  lazy var bgEditProfile: UIView = {
      let subView = UIView()
      subView.backgroundColor = .none
      subView.translatesAutoresizingMaskIntoConstraints = false
      return subView
    }()
    
    lazy var bgEditProfileImage: UIImageView = {
       let imageView = UIImageView()
       imageView.image = #imageLiteral(resourceName: "bgsign-in")
       imageView.contentMode = .scaleAspectFill
       imageView.layer.masksToBounds = true
       imageView.translatesAutoresizingMaskIntoConstraints = false
       return imageView
     }()
      lazy var cardEditProfileView: UIView = {
        let subView = UIView()
        subView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        subView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        subView.layer.shadowOpacity = 1.0
        subView.layer.shadowRadius = 0.0
        subView.layer.masksToBounds = false
        subView.layer.cornerRadius = 10.0
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
      }()

      lazy var tableView: UITableView = {
          let tableView = UITableView()
          tableView.backgroundColor = .white
          tableView.layer.cornerRadius = 20
          tableView.layer.masksToBounds = true
          tableView.separatorStyle = .none
          tableView.backgroundColor = .none
          tableView.showsVerticalScrollIndicator = false
          tableView.showsHorizontalScrollIndicator = false
          tableView.register(SignupFormFacebookCell.self, forCellReuseIdentifier: "signupFormfacebookCell")
          tableView.translatesAutoresizingMaskIntoConstraints = false
          return tableView

      }()
      
    
    lazy var doneBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("DONE", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
        button.layer.cornerRadius = 15
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
     }()
  
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .white
    setupView()
  }
  
  func setupView(){
        
         
       // bgEditProfile.
        addSubview(bgEditProfileImage)
        NSLayoutConstraint.activate([
          bgEditProfileImage.topAnchor.constraint(equalTo: topAnchor),
          bgEditProfileImage.leadingAnchor.constraint(equalTo: leadingAnchor),
          bgEditProfileImage.trailingAnchor.constraint(equalTo: trailingAnchor),
          bgEditProfileImage.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        addSubview(cardEditProfileView)
         
         NSLayoutConstraint.activate([

           cardEditProfileView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 44),
           cardEditProfileView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 30),
           cardEditProfileView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -30),
           cardEditProfileView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
         ])
        
  //      addSubview(crosImagView)
  //           NSLayoutConstraint.activate([
  //             crosImagView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 30),
  //             crosImagView.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor,constant: -20),
  //             crosImagView.widthAnchor.constraint(equalToConstant: 25),
  //             crosImagView.heightAnchor.constraint(equalToConstant: 25),
  //           ])
        
          cardEditProfileView.addSubview(tableView)
        
          NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo:  cardEditProfileView.topAnchor,constant: 5),
            tableView.leadingAnchor.constraint(equalTo: cardEditProfileView.leadingAnchor, constant: 10),
            tableView.trailingAnchor.constraint(equalTo: cardEditProfileView.trailingAnchor, constant: -10),
            tableView.heightAnchor.constraint(equalToConstant: 480)
          
          ])
        
        cardEditProfileView.addSubview(doneBtn)
        
        NSLayoutConstraint.activate([
          doneBtn.topAnchor.constraint(equalTo: tableView.bottomAnchor),
          doneBtn.leadingAnchor.constraint(equalTo: cardEditProfileView.leadingAnchor,constant: 50),
          doneBtn.trailingAnchor.constraint(equalTo: cardEditProfileView.trailingAnchor,constant: -50),
          doneBtn.bottomAnchor.constraint(equalTo: cardEditProfileView.bottomAnchor,constant: -40),
          doneBtn.heightAnchor.constraint(equalToConstant: 35)
        ])
        
      }
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
