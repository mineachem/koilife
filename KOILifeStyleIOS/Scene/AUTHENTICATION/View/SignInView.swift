//
//  SignInView.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit



class SignInView: UIView {
  
  lazy var descriptionLabel: UILabel = {
    let label = UILabel()
    label.text = "Don't have an account?"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.font = UIFont(name: "Montserrat-Regular", size: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var phoneNumTitleLabel: UILabel = {
    let label = UILabel()
    label.text = "Email/Tel"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont(name: "Montserrat-Regular", size: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var bottomLinePhoneView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var phoneNumberTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Email/Tel"
    textField.autocapitalizationType = .none
    textField.font = UIFont(name: "Montserrat-Medium", size: 14)
    textField.keyboardType = .emailAddress
    textField.tag = 0
    textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  
  lazy var passwordTitleLabel: UILabel = {
    let label = UILabel()
    label.text = "Password"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont(name: "Montserrat-Regular", size: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var passwordTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Password"
    textField.font = UIFont(name: "Montserrat-Medium", size: 14)
    textField.keyboardType = .default
    textField.tag = 1
    textField.isSecureTextEntry = true
    textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  lazy var bottomLinePasswordView: UIView = {
     let subView = UIView()
     subView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
  
  lazy var signinBtn: UIButton = {
      let button = UIButton(type: .system)
      button.setTitle("SIGN IN", for: .normal)
      button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
      button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
      button.layer.cornerRadius = 15
      button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
       button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
           button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
           button.layer.shadowOpacity = 1.0
           button.layer.shadowRadius = 0.0
           button.layer.masksToBounds = false
           
      button.translatesAutoresizingMaskIntoConstraints = false
      return button
    }()
  
  lazy var forgetPasswordBtn: UIButton = {
       let button = UIButton(type: .system)
       button.setTitle("Forget your password?", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 14)
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
    }()
  
  lazy var createOneBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Create One", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
     }()
  
  lazy var requirementLabel: UILabel = {
     let label = UILabel()
     label.isHidden = true
    label.numberOfLines = 0
     label.textAlignment = .center
     label.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
     label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var bgSignImagView: UIImageView = {
     let imageView = UIImageView()
       imageView.image = #imageLiteral(resourceName: "bgsign-in")
       imageView.contentMode = .scaleAspectFill
       imageView.layer.masksToBounds = true
       imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  override init(frame: CGRect) {
    super.init(frame: frame)
    //backgroundColor = .brown
    
    setupUI()
  }
  

  fileprivate func setupUI(){
    addSubview(bgSignImagView)
    
    NSLayoutConstraint.activate([
      bgSignImagView.topAnchor.constraint(equalTo: topAnchor),
      bgSignImagView.leadingAnchor.constraint(equalTo: leadingAnchor),
      bgSignImagView.trailingAnchor.constraint(equalTo: trailingAnchor),
      bgSignImagView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
    
    let contentSignInPhoneNumHStack = UIStackView(arrangedSubviews: [phoneNumTitleLabel,phoneNumberTextField])
    contentSignInPhoneNumHStack.axis = .horizontal
    contentSignInPhoneNumHStack.alignment = .fill
    contentSignInPhoneNumHStack.distribution = .fill
    contentSignInPhoneNumHStack.spacing = 10
    contentSignInPhoneNumHStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentSignInPasswordHStack = UIStackView(arrangedSubviews: [passwordTitleLabel,passwordTextField])
    contentSignInPasswordHStack.axis = .horizontal
    contentSignInPasswordHStack.alignment = .fill
    contentSignInPasswordHStack.distribution = .fill
    contentSignInPasswordHStack.spacing = 10
    contentSignInPasswordHStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentBottomPhoneNumVStack = UIStackView(arrangedSubviews: [contentSignInPhoneNumHStack,bottomLinePhoneView])
    contentBottomPhoneNumVStack.axis = .vertical
    contentBottomPhoneNumVStack.alignment = .fill
    contentBottomPhoneNumVStack.distribution = .fill
    contentBottomPhoneNumVStack.spacing = 10
    contentBottomPhoneNumVStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentBottomPaswVStack = UIStackView(arrangedSubviews: [contentSignInPasswordHStack,bottomLinePasswordView,requirementLabel])
        contentBottomPaswVStack.axis = .vertical
       contentBottomPaswVStack.alignment = .fill
       contentBottomPaswVStack.distribution = .fill
      contentBottomPaswVStack.spacing = 10
       contentBottomPaswVStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentSigninStackView = UIStackView(arrangedSubviews: [contentBottomPhoneNumVStack,contentBottomPaswVStack])
    contentSigninStackView.axis = .vertical
    contentSigninStackView.alignment = .fill
    contentSigninStackView.distribution = .fill
    contentSigninStackView.spacing = 20
    contentSigninStackView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(contentSigninStackView)
    
    NSLayoutConstraint.activate([
      contentSigninStackView.centerXAnchor.constraint(equalTo: centerXAnchor),
      contentSigninStackView.topAnchor.constraint(equalTo:safeAreaLayoutGuide.topAnchor,constant: 100),
      contentSigninStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 40),
      contentSigninStackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -40),
      bottomLinePhoneView.heightAnchor.constraint(equalToConstant: 0.5),
      bottomLinePasswordView.heightAnchor.constraint(equalToConstant: 0.5),
      phoneNumberTextField.leadingAnchor.constraint(equalTo: bottomLinePhoneView.leadingAnchor,constant: 90),
      passwordTextField.leadingAnchor.constraint(equalTo: bottomLinePasswordView.leadingAnchor,constant: 90)
    ])
    
    addSubview(signinBtn)
    
    NSLayoutConstraint.activate([
      signinBtn.topAnchor.constraint(equalTo: contentSigninStackView.bottomAnchor,constant: 30),
      signinBtn.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 70),
      signinBtn.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -70),
      signinBtn.heightAnchor.constraint(equalToConstant: 35)
    ])
    
    
    let contentCreateOneHStack = UIStackView(arrangedSubviews: [descriptionLabel,createOneBtn])
    contentCreateOneHStack.axis = .horizontal
    contentCreateOneHStack.alignment = .fill
    contentCreateOneHStack.distribution = .equalSpacing
    contentCreateOneHStack.spacing = 5
    contentCreateOneHStack.translatesAutoresizingMaskIntoConstraints = false
   
    
    let contentCreateOnVStack = UIStackView(arrangedSubviews: [forgetPasswordBtn,contentCreateOneHStack])
    contentCreateOnVStack.axis = .vertical
    contentCreateOnVStack.alignment = .fill
    contentCreateOnVStack.distribution = .equalSpacing
    contentCreateOnVStack.spacing = -5
    contentCreateOnVStack.translatesAutoresizingMaskIntoConstraints = false
    addSubview(contentCreateOnVStack)
    
    NSLayoutConstraint.activate([
      contentCreateOnVStack.topAnchor.constraint(equalTo: signinBtn.bottomAnchor,constant: 5),
      contentCreateOnVStack.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}



