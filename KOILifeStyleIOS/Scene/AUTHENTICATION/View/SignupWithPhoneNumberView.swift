//
//  SignupView.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class SignupWithPhoneNumberView: UIView {

  
  lazy var cardsignupView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .white
    subView.layer.cornerRadius = 10
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var descriptionLabel: UILabel = {
     let label = UILabel()
     label.text = "To complete this sign up procedure you must fill in all the required fields. We will not share your information with the others"
     label.textAlignment = .center
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.numberOfLines = 0
    label.font = UIFont(name: "Montserrat-Medium", size: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var phoneNumTitleLabel: UILabel = {
     let label = UILabel()
     label.text = "Phone Number"
     label.textAlignment = .left
     label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     label.font = UIFont.systemFont(ofSize: 14, weight: .thin)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var phoneNumberTextField: UITextField = {
     let textField = UITextField()
     textField.placeholder = "Phone Number"
     textField.font = UIFont(name: "Montserrat-Medium", size: 14)
    textField.keyboardType = .numberPad
     textField.translatesAutoresizingMaskIntoConstraints = false
     return textField
   }()
  
  lazy var bottomLinePhoneView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var requireMessageLabel: UILabel = {
    let label = UILabel()
    label.isHidden = true
    label.textColor = .red
    label.textAlignment = .center
   label.numberOfLines = 0
    label.font = UIFont.systemFont(ofSize: 12)
   label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var checkBoxBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "unchecked-box").withRenderingMode(.alwaysOriginal), for: .normal)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
   
  lazy var alreadyAlltermLabel: UILabel = {
    let label = UILabel()
    label.text = "I agree all the terms and conditons"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
    label.font = UIFont(name: "Montserrat-Medium", size: 12)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
   lazy var signupBtn: UIButton = {
       let button = UIButton(type: .system)
       button.setTitle("SIGN UP", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
       button.layer.cornerRadius = 15
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
  
  lazy var loginBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("Login", for: .normal)
    button.setTitleColor(.black, for: .normal)
    button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var alreadyLabel: UILabel = {
    let label = UILabel()
    label.text = "Already have an account?"
    label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var bgSignUpImagView: UIImageView = {
     let imageView = UIImageView()
       imageView.image = #imageLiteral(resourceName: "bgsign-in")
       imageView.contentMode = .scaleToFill
       imageView.layer.masksToBounds = true
       imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .brown
    setupUI()
  }
  
  fileprivate func setupUI(){
    
    addSubview(bgSignUpImagView)
       
       NSLayoutConstraint.activate([
         bgSignUpImagView.topAnchor.constraint(equalTo: topAnchor),
         bgSignUpImagView.leadingAnchor.constraint(equalTo: leadingAnchor),
         bgSignUpImagView.trailingAnchor.constraint(equalTo: trailingAnchor),
         bgSignUpImagView.bottomAnchor.constraint(equalTo: bottomAnchor)
       ])
    
    addSubview(cardsignupView)
  
    cardsignupView.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor,constant: 10).isActive = true
    cardsignupView.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor,constant: -10).isActive = true
    cardsignupView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor).isActive = true
    cardsignupView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor).isActive = true
    
         let contentCheckBoxStackView = UIStackView(arrangedSubviews: [checkBoxBtn,alreadyAlltermLabel])
        contentCheckBoxStackView.axis = .horizontal
        contentCheckBoxStackView.alignment = .center
        contentCheckBoxStackView.distribution = .equalSpacing
        contentCheckBoxStackView.translatesAutoresizingMaskIntoConstraints = false
    
        let contentPhoneNumHStack = UIStackView(arrangedSubviews: [phoneNumTitleLabel,phoneNumberTextField])
        contentPhoneNumHStack.axis = .horizontal
        contentPhoneNumHStack.alignment = .fill
        contentPhoneNumHStack.distribution = .fill
        contentPhoneNumHStack.spacing = 5
    
        let contentSignupStackView = UIStackView(arrangedSubviews: [descriptionLabel,contentPhoneNumHStack,bottomLinePhoneView,requireMessageLabel,contentCheckBoxStackView,signupBtn])
        contentSignupStackView.axis = .vertical
        contentSignupStackView.alignment = .fill
        contentSignupStackView.distribution = .fillProportionally
        contentSignupStackView.spacing = 20
        contentSignupStackView.translatesAutoresizingMaskIntoConstraints = false
        cardsignupView.addSubview(contentSignupStackView)
    
    
    let contentloginStackView = UIStackView(arrangedSubviews: [alreadyLabel,loginBtn])
     contentloginStackView.axis = .horizontal
     contentloginStackView.alignment = .center
     contentloginStackView.distribution = .equalSpacing
     contentloginStackView.spacing = 5
    contentloginStackView.translatesAutoresizingMaskIntoConstraints = false
    cardsignupView.addSubview(contentloginStackView)
    
    NSLayoutConstraint.activate([
      contentSignupStackView.leftAnchor.constraint(equalTo: cardsignupView.leftAnchor,constant: 40),
      contentSignupStackView.rightAnchor.constraint(equalTo: cardsignupView.rightAnchor,constant: -40),
      contentSignupStackView.topAnchor.constraint(equalTo: cardsignupView.topAnchor,constant: 15),
      contentloginStackView.topAnchor.constraint(equalTo: contentSignupStackView.bottomAnchor,constant: 5),
      contentloginStackView.centerXAnchor.constraint(equalTo: cardsignupView.centerXAnchor),
      requireMessageLabel.heightAnchor.constraint(equalToConstant: 15),
      contentloginStackView.bottomAnchor.constraint(equalTo: cardsignupView.bottomAnchor,constant: -15),
      bottomLinePhoneView.heightAnchor.constraint(equalToConstant: 1),
      checkBoxBtn.widthAnchor.constraint(equalToConstant: 25),
      checkBoxBtn.heightAnchor.constraint(equalToConstant: 25),
      signupBtn.heightAnchor.constraint(equalToConstant: 40),
      alreadyAlltermLabel.leadingAnchor.constraint(equalTo: checkBoxBtn.leadingAnchor,constant: 30),
    ])
   
   
  }
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
