//
//  SignupFormView.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/27/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class SignupFormView: UIView {
  
  lazy var bgSignupImagView: UIImageView = {
      let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "bgsign-in")
        imageView.contentMode = .scaleToFill
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
     return imageView
   }()
  
  lazy var cardsignupView: UIView = {
     let subView = UIView()
     subView.backgroundColor = .white
     subView.layer.cornerRadius = 10
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
  
  lazy var timerLabel: UILabel = {
    let label = UILabel()
    label.isHidden = true
    //label.text = "00:00"
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  lazy var phoneNumberLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  ////
  lazy var pincodeTitleLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.text = "Pin Code<sup>*</sup>".htmlToString
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont(name: "Montserrat-Regular", size: 12)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var pincodeTextField: UITextField = {
      let textField = UITextField()
      textField.placeholder = "Pin Code"
      textField.font = UIFont(name: "Montserrat-Medium", size: 14)
     textField.keyboardType = .namePhonePad
      textField.translatesAutoresizingMaskIntoConstraints = false
      return textField
    }()
  
  lazy var pincodeBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("sent pin", for: .normal)
    button.setTitleColor(#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), for: .normal)
    button.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 12)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var bottomPinCodeView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  ////
  lazy var passwordTitleLabel: UILabel = {
     let label = UILabel()
     label.numberOfLines = 0
     label.text = "Password<sup>*</sup>".htmlToString
     label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
     label.font = UIFont(name: "Montserrat-Regular", size: 12)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var passwordTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Password"
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.keyboardType = .namePhonePad
    textField.isSecureTextEntry = true
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  lazy var bottomPasswordView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  
  
  lazy var confirmTitleLabel: UILabel = {
      let label = UILabel()
      label.numberOfLines = 0
      label.text = "Confirm Password<sup>*</sup>".htmlToString
      label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
      label.font = UIFont(name: "Montserrat-Regular", size: 12)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
  
  
  lazy var confirmPasswordTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Confirm Password"
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.keyboardType = .namePhonePad
    textField.isSecureTextEntry = true
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  lazy var bottomConfirmPassView: UIView = {
     let subView = UIView()
     subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
  
  //
  
  lazy var profileInfoLabel: UILabel = {
     let label = UILabel()
     label.text = "Profile Info"
     label.textAlignment = .left
    label.font = UIFont.boldSystemFont(ofSize: 14)
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  
  //
  lazy var firstNameTitleLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.text = "First Name<sup>*</sup>".htmlToString
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont(name: "Montserrat-Regular", size: 12)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var firstNameTextField: UITextField = {
     let textField = UITextField()
     textField.placeholder = "First Name"
     textField.font = UIFont.systemFont(ofSize: 14)
     textField.keyboardType = .namePhonePad
     textField.translatesAutoresizingMaskIntoConstraints = false
     return textField
   }()
  
  
  lazy var bottomFirstnameView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  
  lazy var requireLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.font = UIFont.systemFont(ofSize: 10)
    label.textColor = .red
    label.textAlignment = .center
    label.isHidden = true
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  ////
  
  lazy var lastNameTitleLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.text = "Last Name<sup>*</sup>".htmlToString
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont(name: "Montserrat-Regular", size: 12)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  
  lazy var lastNameTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Last Name"
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.keyboardType = .namePhonePad
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  lazy var bottomLastnameView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  
  
  lazy var genderTitleLabel: UILabel = {
     let label = UILabel()
     label.numberOfLines = 0
     label.text = "Gender<sup>*</sup>".htmlToString
     label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
     label.font = UIFont(name: "Montserrat-Regular", size: 12)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var genderTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Gender"
    textField.tintColor = .clear
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  
  lazy var bottomGenderView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
 
  
  lazy var emailTitleLabel: UILabel = {
      let label = UILabel()
      label.numberOfLines = 0
      label.text = "Email"
      label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
      label.font = UIFont(name: "Montserrat-Regular", size: 12)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
  
  lazy var emailTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Email"
    textField.autocapitalizationType = .none
    textField.font = UIFont(name: "Montserrat-Medium", size: 14)
    textField.keyboardType = .emailAddress
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  
  lazy var bottomEmailView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  
  
  
  lazy var birthdayTitleLabel: UILabel = {
       let label = UILabel()
       label.numberOfLines = 0
       label.text = "Birthday"
       label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
       label.font = UIFont(name: "Montserrat-Regular", size: 12)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
     }()
  
  lazy var birthdayTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Free gift on your birthday"
    textField.tintColor = .clear
    textField.font = UIFont(name: "Montserrat-Medium", size: 14)
    textField.keyboardType = .namePhonePad
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  lazy var bottomBirthdayView: UIView = {
     let subView = UIView()
     subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
  
 
  lazy var submitBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("SUBMIT", for: .normal)
    button.setTitleColor(.black, for: .normal)
    button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
    button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    button.layer.cornerRadius = 15
    button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(bgSignupImagView)
    
    NSLayoutConstraint.activate([
      bgSignupImagView.topAnchor.constraint(equalTo: topAnchor),
      bgSignupImagView.leadingAnchor.constraint(equalTo: leadingAnchor),
      bgSignupImagView.trailingAnchor.constraint(equalTo: trailingAnchor),
      bgSignupImagView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
    
    addSubview(cardsignupView)
    
    NSLayoutConstraint.activate([
      cardsignupView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 70),
      cardsignupView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
      cardsignupView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20),
    ])
    
//    cardsignupView.addSubview(phoneNumberLabel)
//
//    NSLayoutConstraint.activate([
//         phoneNumberLabel.topAnchor.constraint(equalTo: cardsignupView.topAnchor,constant: 10),
//         phoneNumberLabel.leadingAnchor.constraint(equalTo: cardsignupView.leadingAnchor,constant: 10),
//         phoneNumberLabel.trailingAnchor.constraint(equalTo: cardsignupView.trailingAnchor,constant: -10),
//       ])
//
    let contentPhonePinHStack = UIStackView(arrangedSubviews: [phoneNumberLabel,timerLabel])
    contentPhonePinHStack.axis = .horizontal
    contentPhonePinHStack.alignment = .fill
    contentPhonePinHStack.distribution = .equalSpacing
    contentPhonePinHStack.translatesAutoresizingMaskIntoConstraints = false
    
    cardsignupView.addSubview(contentPhonePinHStack)
    
    NSLayoutConstraint.activate([
      contentPhonePinHStack.topAnchor.constraint(equalTo: cardsignupView.topAnchor,constant: 10),
      contentPhonePinHStack.leadingAnchor.constraint(equalTo: cardsignupView.leadingAnchor,constant: 10),
      contentPhonePinHStack.trailingAnchor.constraint(equalTo: cardsignupView.trailingAnchor,constant: -10),
    ])
 
    let contentPinCodeHStackView = UIStackView(arrangedSubviews: [pincodeTitleLabel,pincodeTextField,pincodeBtn])
    contentPinCodeHStackView.axis = .horizontal
    contentPinCodeHStackView.alignment = .center
    contentPinCodeHStackView.distribution = .equalSpacing
    contentPinCodeHStackView.translatesAutoresizingMaskIntoConstraints = false
    
    let contentPasswordHStackView = UIStackView(arrangedSubviews: [passwordTitleLabel,passwordTextField])
       contentPasswordHStackView.axis = .horizontal
       contentPasswordHStackView.alignment = .center
       contentPasswordHStackView.distribution = .equalSpacing
       contentPasswordHStackView.translatesAutoresizingMaskIntoConstraints = false

    let contentConfirmPassHStackView = UIStackView(arrangedSubviews: [confirmTitleLabel,confirmPasswordTextField])
    contentConfirmPassHStackView.axis = .horizontal
    contentConfirmPassHStackView.alignment = .center
    contentConfirmPassHStackView.distribution = .equalSpacing
    contentConfirmPassHStackView.translatesAutoresizingMaskIntoConstraints = false
    
    
    let contentPasswordFormVStackView = UIStackView(arrangedSubviews: [contentPinCodeHStackView,bottomPinCodeView,contentPasswordHStackView,bottomPasswordView,contentConfirmPassHStackView,bottomConfirmPassView])
    contentPasswordFormVStackView.axis = .vertical
    contentPasswordFormVStackView.alignment = .fill
    contentPasswordFormVStackView.distribution = .fill
    contentPasswordFormVStackView.spacing = 10
    contentPasswordFormVStackView.translatesAutoresizingMaskIntoConstraints = false
    cardsignupView.addSubview(contentPasswordFormVStackView)
    

    NSLayoutConstraint.activate([
      contentPasswordFormVStackView.topAnchor.constraint(equalTo: phoneNumberLabel.bottomAnchor,constant: 30),
      contentPasswordFormVStackView.leadingAnchor.constraint(equalTo: cardsignupView.leadingAnchor,constant: 20),
      contentPasswordFormVStackView.trailingAnchor.constraint(equalTo: cardsignupView.trailingAnchor,constant: -20),
      pincodeTextField.leadingAnchor.constraint(equalTo: pincodeTitleLabel.leadingAnchor,constant: 70),
      passwordTextField.leadingAnchor.constraint(equalTo: passwordTitleLabel.leadingAnchor,constant: 70),
      confirmPasswordTextField.leadingAnchor.constraint(equalTo: confirmTitleLabel.leadingAnchor,constant: 70),
      bottomPinCodeView.heightAnchor.constraint(equalToConstant: 0.3),
      bottomPasswordView.heightAnchor.constraint(equalToConstant: 0.3),
      bottomConfirmPassView.heightAnchor.constraint(equalToConstant: 0.3),
    ])
//
    cardsignupView.addSubview(profileInfoLabel)
//
    NSLayoutConstraint.activate([
      profileInfoLabel.topAnchor.constraint(equalTo: contentPasswordFormVStackView.bottomAnchor,constant: 30),
      profileInfoLabel.leadingAnchor.constraint(equalTo: cardsignupView.leadingAnchor,constant: 20),
      profileInfoLabel.trailingAnchor.constraint(equalTo: cardsignupView.trailingAnchor,constant: -20)
    ])

    let contentFirstnameHStackView = UIStackView(arrangedSubviews: [firstNameTitleLabel,firstNameTextField])
       contentFirstnameHStackView.axis = .horizontal
       contentFirstnameHStackView.alignment = .fill
       contentFirstnameHStackView.distribution = .equalSpacing
       contentFirstnameHStackView.translatesAutoresizingMaskIntoConstraints = false

    let contentLastNameHStackView = UIStackView(arrangedSubviews: [lastNameTitleLabel,lastNameTextField])
    contentLastNameHStackView.axis = .horizontal
    contentLastNameHStackView.alignment = .fill
    contentLastNameHStackView.distribution = .equalSpacing
    contentLastNameHStackView.translatesAutoresizingMaskIntoConstraints = false

    let contentEmailHStackView = UIStackView(arrangedSubviews: [emailTitleLabel,emailTextField])
    contentEmailHStackView.axis = .horizontal
    contentEmailHStackView.alignment = .fill
    contentEmailHStackView.distribution = .equalSpacing
    contentEmailHStackView.translatesAutoresizingMaskIntoConstraints = false

    let contentGenderHStackView = UIStackView(arrangedSubviews: [genderTitleLabel,genderTextField])
       contentGenderHStackView.axis = .horizontal
       contentGenderHStackView.alignment = .fill
       contentGenderHStackView.distribution = .equalSpacing
       contentGenderHStackView.translatesAutoresizingMaskIntoConstraints = false

    let contentBirthdayHStackView = UIStackView(arrangedSubviews: [birthdayTitleLabel,birthdayTextField])
          contentBirthdayHStackView.axis = .horizontal
          contentBirthdayHStackView.alignment = .fill
          contentBirthdayHStackView.distribution = .equalSpacing
          contentBirthdayHStackView.translatesAutoresizingMaskIntoConstraints = false


    let contentProfileInfoVStackView = UIStackView(arrangedSubviews: [contentFirstnameHStackView,bottomFirstnameView,contentLastNameHStackView,bottomLastnameView,contentEmailHStackView,bottomEmailView,contentGenderHStackView,bottomGenderView,contentBirthdayHStackView,bottomBirthdayView,requireLabel,submitBtn])
    contentProfileInfoVStackView.axis = .vertical
    contentProfileInfoVStackView.alignment = .fill
    contentProfileInfoVStackView.distribution = .equalSpacing
    contentProfileInfoVStackView.spacing = 10
    contentProfileInfoVStackView.translatesAutoresizingMaskIntoConstraints = false

    cardsignupView.addSubview(contentProfileInfoVStackView)

    NSLayoutConstraint.activate([
      contentProfileInfoVStackView.topAnchor.constraint(equalTo: profileInfoLabel.bottomAnchor,constant: 20),
      contentProfileInfoVStackView.leadingAnchor.constraint(equalTo: cardsignupView.leadingAnchor,constant: 20),
      contentProfileInfoVStackView.trailingAnchor.constraint(equalTo: cardsignupView.trailingAnchor,constant: -20),
      contentProfileInfoVStackView.bottomAnchor.constraint(equalTo: cardsignupView.bottomAnchor,constant: -10),
      firstNameTextField.leadingAnchor.constraint(equalTo: firstNameTitleLabel.leadingAnchor,constant: 70),
      lastNameTextField.leadingAnchor.constraint(equalTo: lastNameTitleLabel.leadingAnchor,constant: 80),
      emailTextField.leadingAnchor.constraint(equalTo: emailTitleLabel.leadingAnchor,constant: 70),
      genderTextField.leadingAnchor.constraint(equalTo: genderTitleLabel.leadingAnchor,constant: 70),
      birthdayTextField.leadingAnchor.constraint(equalTo: birthdayTitleLabel.leadingAnchor,constant: 70),
      bottomFirstnameView.heightAnchor.constraint(equalToConstant: 0.3),
      bottomLastnameView.heightAnchor.constraint(equalToConstant: 0.3),
      bottomEmailView.heightAnchor.constraint(equalToConstant: 0.5),
      bottomGenderView.heightAnchor.constraint(equalToConstant: 0.3),
      bottomBirthdayView.heightAnchor.constraint(equalToConstant: 0.3)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
