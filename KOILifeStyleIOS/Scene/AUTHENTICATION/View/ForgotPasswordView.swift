//
//  ForgotPasswordView.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/27/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class ForgotPasswordView: UIView {

  lazy var cardforgotPasswordView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .white
    subView.layer.cornerRadius = 10
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var bgImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "bgsign-in")
    imageView.contentMode = .scaleToFill
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  lazy var descriptionLabel: UILabel = {
     let label = UILabel()
     label.text = "If you have forgotten your password. it is not a problem. Fill in your email address you have signed up with, and we will send you login information right away."
     label.textAlignment = .center
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var resetBtn: UIButton = {
     let button = UIButton(type: .system)
     button.setTitle("RESET", for: .normal)
     button.setTitleColor(.black, for: .normal)
     button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
     button.layer.cornerRadius = 15
     button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
     button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
     button.translatesAutoresizingMaskIntoConstraints = false
     return button
   }()
  
  lazy var titlePhoneNumberLabel: UILabel = {
    let label = UILabel()
    label.text = "Email/Tel:"
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  lazy var phoneNumberTextField: UITextField = {
     let textField = UITextField()
     textField.placeholder = "Email"
     textField.tag = 0
     textField.autocapitalizationType = .none
     textField.font = UIFont.systemFont(ofSize: 14)
    textField.keyboardType = .emailAddress
     textField.translatesAutoresizingMaskIntoConstraints = false
     return textField
   }()
  
  lazy var bottomLineView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var requirementLabel: UILabel = {
       let label = UILabel()
       label.textAlignment = .center
       label.textColor = .red
       label.numberOfLines = 0
       label.isHidden = true
       label.font = UIFont(name: "Montserrat-Regular", size: 14)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
     }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  fileprivate func setupUI(){
    addSubview(bgImageView)
    NSLayoutConstraint.activate([
      bgImageView.topAnchor.constraint(equalTo: topAnchor),
      bgImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
      bgImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
      bgImageView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
    addSubview(cardforgotPasswordView)
    
      cardforgotPasswordView.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor,constant: 40).isActive = true
      cardforgotPasswordView.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor,constant: -40).isActive = true
      cardforgotPasswordView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor).isActive = true
      cardforgotPasswordView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor).isActive = true
    
    cardforgotPasswordView.addSubview(descriptionLabel)
    
    NSLayoutConstraint.activate([
      descriptionLabel.topAnchor.constraint(equalTo: cardforgotPasswordView.topAnchor,constant: 10),
      descriptionLabel.leadingAnchor.constraint(equalTo: cardforgotPasswordView.leadingAnchor,constant: 20),
      descriptionLabel.trailingAnchor.constraint(equalTo: cardforgotPasswordView.trailingAnchor,constant: -20)
    ])
    
    
    let contentResetHStack = UIStackView(arrangedSubviews: [titlePhoneNumberLabel,phoneNumberTextField])
    contentResetHStack.axis = .horizontal
    contentResetHStack.alignment = .fill
    contentResetHStack.distribution = .equalSpacing
    contentResetHStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentResetBottomVStack = UIStackView(arrangedSubviews: [contentResetHStack,bottomLineView])
    contentResetBottomVStack.axis = .vertical
    contentResetBottomVStack.alignment = .fill
    contentResetBottomVStack.distribution = .fill
    contentResetBottomVStack.spacing = 5
    contentResetBottomVStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentResetStackView = UIStackView(arrangedSubviews: [contentResetBottomVStack,requirementLabel])
    contentResetStackView.axis = .vertical
    contentResetStackView.alignment = .fill
    contentResetStackView.distribution = .fillEqually
    contentResetStackView.spacing = 5
    contentResetStackView.translatesAutoresizingMaskIntoConstraints = false
    cardforgotPasswordView.addSubview(contentResetStackView)
    
    NSLayoutConstraint.activate([
      contentResetStackView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor,constant: 30),
      contentResetStackView.leadingAnchor.constraint(equalTo: cardforgotPasswordView.leadingAnchor,constant: 10),
      contentResetStackView.trailingAnchor.constraint(equalTo: cardforgotPasswordView.trailingAnchor,constant: -10),
      //contentResetStackView.bottomAnchor.constraint(equalTo: cardforgotPasswordView.bottomAnchor,constant: -10),
      phoneNumberTextField.leadingAnchor.constraint(equalTo: titlePhoneNumberLabel.leadingAnchor,constant: 80),
      bottomLineView.heightAnchor.constraint(equalToConstant: 0.5)
     
    ])
    
    cardforgotPasswordView.addSubview(resetBtn)
    NSLayoutConstraint.activate([
      resetBtn.topAnchor.constraint(equalTo: contentResetStackView.bottomAnchor,constant: 40),
      resetBtn.leadingAnchor.constraint(equalTo: cardforgotPasswordView.leadingAnchor,constant: 40),
      resetBtn.trailingAnchor.constraint(equalTo: cardforgotPasswordView.trailingAnchor,constant: -40),
      resetBtn.heightAnchor.constraint(equalToConstant: 40),
      resetBtn.bottomAnchor.constraint(equalTo: cardforgotPasswordView.bottomAnchor,constant: -10)
    ])
  }
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
