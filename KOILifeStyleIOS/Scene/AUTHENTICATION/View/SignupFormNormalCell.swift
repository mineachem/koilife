//
//  SignupFormNormalCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class SignupFormNormalCell: UITableViewCell {

  lazy var bgSignupView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .white
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  lazy var titleHeaderLabel: UILabel = {
      let label = UILabel()
    label.numberOfLines = 0
    label.text = "To complete the SIGN UP precedure please fill the required fields. We will not share your info with others. We will not share your info with others"
    label.font = UIFont(name: "Montserrat-Regular", size: 12)
    label.translatesAutoresizingMaskIntoConstraints = false
      return label

  }()
  
   
        lazy var titleNameLabel: UILabel = {
            let label = UILabel()
            label.text = "Name<sup>*</sup>".htmlToString
            label.textAlignment = .left
            label.font = UIFont(name: "Montserrat-Regular", size: 12)
            label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
            return label

        }()

        lazy var nametextField: UITextField = {
            let textfield = UITextField()
            textfield.placeholder = "Name"
            textfield.tag = 0
            textfield.autocapitalizationType = .none
            textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
            textfield.translatesAutoresizingMaskIntoConstraints = false
            return textfield
        }()
      
  lazy var bottomLineNameView: UIView = {
      let line = UIView()
      line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
      line.translatesAutoresizingMaskIntoConstraints = false
      return line
  }()
    
      lazy var titleEmailLabel: UILabel = {
             let label = UILabel()
             label.text = "Email"
             label.textAlignment = .left
             label.font = UIFont(name: "Montserrat-Regular", size: 12)
             label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
             label.translatesAutoresizingMaskIntoConstraints = false
             return label

         }()

        lazy var emailtextField: UITextField = {
             let textfield = UITextField()
             textfield.placeholder = "Email"
             textfield.tag = 2
             textfield.autocapitalizationType = .none
             textfield.font = UIFont.systemFont(ofSize: 14, weight: .medium)
             textfield.translatesAutoresizingMaskIntoConstraints = false
             return textfield
         }()
      
      lazy var bottomLineEmailView: UIView = {
            let line = UIView()
            line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            line.translatesAutoresizingMaskIntoConstraints = false
            return line
        }()
      
  lazy var titleGenderLabel: UILabel = {
             let label = UILabel()
             label.text = "Gender"
             label.textAlignment = .left
             label.font = UIFont(name: "Montserrat-Regular", size: 12)
             label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
             label.translatesAutoresizingMaskIntoConstraints = false
             return label

         }()

       
        lazy var gendertextField: UITextField = {
             let textfield = UITextField()
             textfield.placeholder = "Gender"
             textfield.tag = 5
             textfield.autocapitalizationType = .none
             textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
             textfield.translatesAutoresizingMaskIntoConstraints = false
             return textfield
         }()
       
       lazy var bottomLinegenderView: UIView = {
             let line = UIView()
             line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
             line.translatesAutoresizingMaskIntoConstraints = false
             return line
         }()
  
  lazy var titledobLabel: UILabel = {
         let label = UILabel()
         label.text = "Date Of birth"
         label.textAlignment = .left
         label.font = UIFont(name: "Montserrat-Regular", size: 12)
         label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
         label.translatesAutoresizingMaskIntoConstraints = false
         return label

     }()

    lazy var dobtextField: UITextField = {
         let textfield = UITextField()
       textfield.placeholder = "Date Of birth"
       textfield.autocapitalizationType = .none
       textfield.isEnabled = false
       textfield.tag = 4
       textfield.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
       textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
       textfield.translatesAutoresizingMaskIntoConstraints = false
         return textfield
     }()
    
    lazy var bottomLinedobView: UIView = {
          let line = UIView()
          line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
          line.translatesAutoresizingMaskIntoConstraints = false
          return line
      }()
   
  

      lazy var titleaddressLabel: UILabel = {
            let label = UILabel()
            label.text = "City/Province"
            label.textAlignment = .left
            label.font = UIFont(name: "Montserrat-Regular", size: 12)
            label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
            return label

        }()

       lazy var addresstextField: UITextField = {
            let textfield = UITextField()
           textfield.placeholder = "City/Province"
            textfield.autocapitalizationType = .none
            textfield.tag = 3
            textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
            textfield.translatesAutoresizingMaskIntoConstraints = false
            return textfield
        }()
       
       
       lazy var bottomLineAddressView: UIView = {
             let line = UIView()
             line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
             line.translatesAutoresizingMaskIntoConstraints = false
             return line
         }()
      
      
      
      lazy var titlePhoneNumLabel: UILabel = {
           let label = UILabel()
           label.text = "Phone Number"
           label.textAlignment = .left
           label.font = UIFont(name: "Montserrat-Regular", size: 12)
           label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
           label.translatesAutoresizingMaskIntoConstraints = false
           return label

       }()

      lazy var phoneNumtextField: UITextField = {
           let textfield = UITextField()
           textfield.placeholder = "Phone Number"
           textfield.isEnabled = false
           textfield.tag = 6
           textfield.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
           textfield.autocapitalizationType = .none
           textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
           textfield.translatesAutoresizingMaskIntoConstraints = false
           return textfield
       }()
      
      lazy var bottomLinephoneNumView: UIView = {
            let line = UIView()
            line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            line.translatesAutoresizingMaskIntoConstraints = false
            return line
        }()
    
  
  lazy var titlePasswordLabel: UILabel = {
           let label = UILabel()
           label.text = "Password"
           label.textAlignment = .left
           label.font = UIFont(name: "Montserrat-Regular", size: 12)
           label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
           label.translatesAutoresizingMaskIntoConstraints = false
           return label

       }()

      lazy var passwordtextField: UITextField = {
           let textfield = UITextField()
           textfield.placeholder = "Password"
           textfield.isEnabled = false
           textfield.tag = 6
           textfield.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
           textfield.autocapitalizationType = .none
           textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
           textfield.translatesAutoresizingMaskIntoConstraints = false
           return textfield
       }()
      
      lazy var bottomLinepasswordView: UIView = {
            let line = UIView()
            line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            line.translatesAutoresizingMaskIntoConstraints = false
            return line
        }()
    
  
      
  lazy var titlePConfirmPasswLabel: UILabel = {
           let label = UILabel()
           label.text = "Confirm Password"
           label.textAlignment = .left
           label.font = UIFont(name: "Montserrat-Regular", size: 12)
           label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
           label.translatesAutoresizingMaskIntoConstraints = false
           return label

       }()

      lazy var confirmPasswtextField: UITextField = {
           let textfield = UITextField()
           textfield.placeholder = "Confirm Password"
           textfield.isEnabled = false
           textfield.tag = 6
           textfield.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
           textfield.autocapitalizationType = .none
           textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
           textfield.translatesAutoresizingMaskIntoConstraints = false
           return textfield
       }()
      
      lazy var bottomLineconfirmPasswView: UIView = {
            let line = UIView()
            line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            line.translatesAutoresizingMaskIntoConstraints = false
            return line
        }()
      
      lazy var requirementLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .red
        label.isHidden = true
        label.font = UIFont(name: "Montserrat-Regular", size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
      }()
      
  
  //
  lazy var maleLabel: UILabel = {
    let label = UILabel()
    label.text = "Male"
    label.textAlignment = .left
    label.font = UIFont(name: "Montserrat-Regular", size: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var maleBtn: UIButton = {
    let button = UIButton()
    button.setImage(#imageLiteral(resourceName: "circle").withRenderingMode(.alwaysOriginal), for:.normal)
    button.contentMode = .scaleAspectFit
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var famaleLabel: UILabel = {
    let label = UILabel()
    label.text = "Female"
    label.textAlignment = .left
    label.font = UIFont(name: "Montserrat-Regular", size: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var famaleBtn: UIButton = {
     let button = UIButton()
     button.setImage(#imageLiteral(resourceName: "circle").withRenderingMode(.alwaysOriginal), for:.normal)
     button.contentMode = .scaleAspectFit
     button.translatesAutoresizingMaskIntoConstraints = false
     return button
   }()
      
  lazy var circleBtn: UIButton = {
      let button = UIButton()
      button.setImage(#imageLiteral(resourceName: "circle").withRenderingMode(.alwaysOriginal), for:.normal)
      button.contentMode = .scaleAspectFit
      button.translatesAutoresizingMaskIntoConstraints = false
      return button
    }()
  
  lazy var conditionLabel: UILabel = {
    let label = UILabel()
    label.text = "I agree with all the terms and conditions"
    label.textAlignment = .left
    label.font = UIFont(name: "Montserrat-Regular", size: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
          selectionStyle = .none
           
          setupView()
        }
        
  
      fileprivate func setupView(){
         addSubview(titleHeaderLabel)
        NSLayoutConstraint.activate([
          titleHeaderLabel.topAnchor.constraint(equalTo: topAnchor),
          titleHeaderLabel.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 50),
          titleHeaderLabel.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -50),
        ])
        
        let contentNameHStack = UIStackView(arrangedSubviews: [titleNameLabel,nametextField])
        contentNameHStack.axis = .horizontal
        contentNameHStack.alignment = .fill
        contentNameHStack.distribution = .equalSpacing
        contentNameHStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentEmailHStack = UIStackView(arrangedSubviews: [titleEmailLabel,emailtextField])
        contentEmailHStack.axis = .horizontal
        contentEmailHStack.alignment = .fill
        contentEmailHStack.distribution = .equalSpacing
        contentEmailHStack.translatesAutoresizingMaskIntoConstraints = false
        
        
        let contentGenderHStack = UIStackView(arrangedSubviews: [titleGenderLabel,gendertextField])
        contentGenderHStack.axis = .horizontal
        contentGenderHStack.alignment = .fill
        contentGenderHStack.distribution = .equalSpacing
        contentGenderHStack.translatesAutoresizingMaskIntoConstraints = false
        
        
        let contentBirthdayHStack = UIStackView(arrangedSubviews: [titledobLabel,dobtextField])
        contentBirthdayHStack.axis = .horizontal
        contentBirthdayHStack.alignment = .fill
        contentBirthdayHStack.distribution = .equalSpacing
        contentBirthdayHStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentAddressHStack = UIStackView(arrangedSubviews: [titleaddressLabel,addresstextField])
        contentAddressHStack.axis = .horizontal
        contentAddressHStack.alignment = .fill
        contentAddressHStack.distribution = .equalSpacing
        contentAddressHStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentPhoneNumHStack = UIStackView(arrangedSubviews: [titlePhoneNumLabel,phoneNumtextField])
        contentPhoneNumHStack.axis = .horizontal
        contentPhoneNumHStack.alignment = .fill
        contentPhoneNumHStack.distribution = .equalSpacing
        contentPhoneNumHStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentPasswdHStack = UIStackView(arrangedSubviews: [titlePasswordLabel,passwordtextField])
        contentPasswdHStack.axis = .horizontal
         contentPasswdHStack.alignment = .fill
         contentPasswdHStack.distribution = .equalSpacing
         contentPasswdHStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentConfirmPasswdHStack = UIStackView(arrangedSubviews: [titlePConfirmPasswLabel,passwordtextField])
        contentConfirmPasswdHStack.axis = .horizontal
        contentConfirmPasswdHStack.alignment = .fill
        contentConfirmPasswdHStack.distribution = .equalSpacing
        contentConfirmPasswdHStack.translatesAutoresizingMaskIntoConstraints = false
        
        /////////
        let contentBottomNameVStack = UIStackView(arrangedSubviews: [contentNameHStack,bottomLineNameView])
        contentBottomNameVStack.axis = .vertical
        contentBottomNameVStack.alignment = .fill
        contentBottomNameVStack.distribution = .fill
        contentBottomNameVStack.spacing = 10
        contentBottomNameVStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentBottomEmailVStack = UIStackView(arrangedSubviews: [contentNameHStack,bottomLineNameView])
        contentBottomEmailVStack.axis = .vertical
        contentBottomEmailVStack.alignment = .fill
        contentBottomEmailVStack.distribution = .fill
        contentBottomEmailVStack.spacing = 10
        contentBottomEmailVStack.translatesAutoresizingMaskIntoConstraints = false
        
        
        let contentBottomGenderVStack = UIStackView(arrangedSubviews: [contentNameHStack,bottomLineNameView])
        contentBottomGenderVStack.axis = .vertical
        contentBottomGenderVStack.alignment = .fill
        contentBottomGenderVStack.distribution = .fill
        contentBottomGenderVStack.spacing = 10
        contentBottomGenderVStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentBottomDobVStack = UIStackView(arrangedSubviews: [contentNameHStack,bottomLineNameView])
        contentBottomDobVStack.axis = .vertical
       contentBottomDobVStack.alignment = .fill
       contentBottomDobVStack.distribution = .fill
       contentBottomDobVStack.spacing = 10
       contentBottomDobVStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentBottomAddressVStack = UIStackView(arrangedSubviews: [contentNameHStack,bottomLineNameView])
        contentBottomAddressVStack.axis = .vertical
        contentBottomAddressVStack.alignment = .fill
        contentBottomAddressVStack.distribution = .fill
        contentBottomAddressVStack.spacing = 10
        contentBottomAddressVStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentBottomPhoneNumVStack = UIStackView(arrangedSubviews: [contentNameHStack,bottomLineNameView])
        contentBottomPhoneNumVStack.axis = .vertical
        contentBottomPhoneNumVStack.alignment = .fill
        contentBottomPhoneNumVStack.distribution = .fill
        contentBottomPhoneNumVStack.spacing = 10
        contentBottomPhoneNumVStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentBottomPasswordVStack = UIStackView(arrangedSubviews: [contentNameHStack,bottomLineNameView])
        contentBottomPasswordVStack.axis = .vertical
        contentBottomPasswordVStack.alignment = .fill
        contentBottomPasswordVStack.distribution = .fill
        contentBottomPasswordVStack.spacing = 10
        contentBottomPasswordVStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentBottomConfirmPasswVStack = UIStackView(arrangedSubviews: [contentNameHStack,bottomLineNameView])
        contentBottomConfirmPasswVStack.axis = .vertical
        contentBottomConfirmPasswVStack.alignment = .fill
        contentBottomConfirmPasswVStack.distribution = .fill
        contentBottomConfirmPasswVStack.spacing = 10
        contentBottomConfirmPasswVStack.translatesAutoresizingMaskIntoConstraints = false
        
        let contentSignupFormVStack = UIStackView(arrangedSubviews: [contentBottomNameVStack,contentBottomEmailVStack,contentBottomGenderVStack,contentBottomDobVStack])
       
      }
      
     
      
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
}
