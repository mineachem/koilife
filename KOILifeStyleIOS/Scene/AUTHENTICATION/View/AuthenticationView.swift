//
//  AuthenticationView.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AuthenticationView: UIView {

  lazy var welcomeScreenLabel: UILabel = {
    let label = UILabel()
    let attributedString = NSMutableAttributedString(string: "Welcome \n Sign up KOI The Memebership program to recive updates,collect points and get exclusive membership benefits.")
    let paragraphStyle = NSMutableParagraphStyle()
    let paragraphstyleSub = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 1
    paragraphstyleSub.lineSpacing = 1
    // *** Apply attribute to string ***
    
    
    attributedString.addAttribute(.font, value:UIFont(name: "Montserrat-Medium", size: 35)!, range:NSRange(location: 0, length: 7))
    attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: 7))
    
    
    attributedString.addAttribute(.font, value:UIFont(name: "Montserrat-Regular", size: 12)!, range:NSRange(location: 8, length: 108))
    attributedString.addAttribute(.paragraphStyle, value: paragraphstyleSub, range: NSRange(location: 8, length: 108))
    label.attributedText = attributedString
    label.numberOfLines = 0
    label.textAlignment = .center
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var signinBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("SIGN IN", for: .normal)
    button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
    button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    button.layer.cornerRadius = 15
    button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
         button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
         button.layer.shadowOpacity = 1.0
         button.layer.shadowRadius = 0.0
         button.layer.masksToBounds = false
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var signupBtn: UIButton = {
     let button = UIButton(type: .system)
     button.setTitle("SIGN UP", for: .normal)
     button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
     button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
     button.layer.cornerRadius = 15
     button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
    button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    button.layer.shadowOpacity = 1.0
    button.layer.shadowRadius = 0.0
    button.layer.masksToBounds = false
     button.translatesAutoresizingMaskIntoConstraints = false
     return button
   }()
  
  lazy var signinFbBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("CONNECT WITH FACEBOOK", for: .normal)
    //button.set
    button.setTitleColor(#colorLiteral(red: 0.1205270067, green: 0.3759053349, blue: 0.5918573141, alpha: 1), for: .normal)
    button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
    button.layer.cornerRadius = 15
    button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
    button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    button.layer.shadowOpacity = 1.0
    button.layer.shadowRadius = 0.0
    button.layer.masksToBounds = false
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  
  lazy var bgLoginImgView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "bg-authentication")
    imageView.contentMode = .scaleAspectFill
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .white
     setupUI()
  }
  
  private func setupUI(){
    addSubview(bgLoginImgView)
    
    NSLayoutConstraint.activate([
      bgLoginImgView.topAnchor.constraint(equalTo: topAnchor),
      bgLoginImgView.leadingAnchor.constraint(equalTo: leadingAnchor),
      bgLoginImgView.trailingAnchor.constraint(equalTo: trailingAnchor),
      bgLoginImgView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
    
    addSubview(welcomeScreenLabel)
    
    NSLayoutConstraint.activate([
      welcomeScreenLabel.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      welcomeScreenLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
      welcomeScreenLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10)
    ])
    let contentAuthenticationStackView = UIStackView(arrangedSubviews: [signinBtn,signupBtn,signinFbBtn])
    contentAuthenticationStackView.axis = .vertical
    contentAuthenticationStackView.alignment = .fill
    contentAuthenticationStackView.distribution = .equalSpacing
    contentAuthenticationStackView.spacing = 10
    contentAuthenticationStackView.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentAuthenticationStackView)
    
  NSLayoutConstraint.activate([
    contentAuthenticationStackView.topAnchor.constraint(equalTo: welcomeScreenLabel.bottomAnchor,constant: 30),
    contentAuthenticationStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
    contentAuthenticationStackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
    contentAuthenticationStackView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
    contentAuthenticationStackView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
    signinBtn.heightAnchor.constraint(equalToConstant: 40),
    signupBtn.heightAnchor.constraint(equalTo: signinBtn.heightAnchor, multiplier: 1.0),
    signinFbBtn.heightAnchor.constraint(equalTo: signinBtn.heightAnchor, multiplier: 1.0)
    ])
    
  
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  

}
