//
//  SignupWithFacebookViewController.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/23/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class SignupWithFacebookViewController: BaseViewController {

  lazy var signupWithFacebookView = SignupWithFacebookView()
    var count:Int = 3
    var timer:Timer?
    var totalTime = 120
  var resultFb: [String : String]?
  var facebookId:String? = ""
  var firstNameFb:String? = ""
  var lastNameFb:String? = ""
  var emailFb:String? = ""
  var photoFb:URL? = nil
  
 var addressValue:String? = ""
 var phoneNumValue:String? = ""
 var genderValue:String? = ""
 var dateOfBirthValue:String? = ""
 var passwordValue:String? = ""
 var confirmPassValue:String? = ""
  var pincode:String? = ""
  
  
  lazy var genderView: GenderFbView = {
    let genderView = GenderFbView()
    genderView.signupFacebookVC = self
    genderView.getDataGenderDelegate = self
    
    return genderView
  }()
  
  override func setupLoadView() {
    super.setupLoadView()
    self.view = signupWithFacebookView
  }
  
  
  override func setupNavigationBar() {
     super.setupNavigationBar()
       let label = UILabel()
       label.text = "SIGNUP FORM WITH FACEBOOK"
       label.font = UIFont(name: "Montserrat-Medium", size: 14)
     titleNavigationBar.topItem?.titleView = label
   }
  
  override func setupUI() {
    super.setupUI()
    signupWithFacebookView.tableView.delegate = self
    signupWithFacebookView.tableView.dataSource = self
    
  }
  
   //MARK:SETUP EVENT
    override func setupEvent(){
      super.setupEvent()
      
      signupWithFacebookView.doneBtn.addTarget(self, action: #selector(handleDoneTapped), for: .touchUpInside)
    }

  @objc fileprivate func handleDoneTapped(){
    let signupCell = signupWithFacebookView.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! SignupFormFacebookCell
    
    if (pincode!.isEmpty) && (firstNameFb!.isEmpty) && (lastNameFb!.isEmpty) && (emailFb!.isEmpty) && (addressValue!.isEmpty) && (phoneNumValue!.isEmpty) && (passwordValue!.isEmpty) && (confirmPassValue!.isEmpty){

      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "please complete your fields"
      signupCell.firstnametextField.becomeFirstResponder()
    }else if (firstNameFb!.isEmpty){
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your first name empty"
      signupCell.firstnametextField.becomeFirstResponder()
    }else if (lastNameFb!.isEmpty){
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your last name empty"
      signupCell.lastnametextField.becomeFirstResponder()
    }else if (!emailFb!.isValidEmail()){
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your email invalid"
      signupCell.emailtextField.becomeFirstResponder()
    }else if (genderValue!.isEmpty){
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your phone gender empty"
      signupCell.gendertextField.becomeFirstResponder()
    }else if (phoneNumValue!.isEmpty){
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your phone number empty"
      signupCell.phoneNumtextField.becomeFirstResponder()
    }else if (pincode!.isEmpty){
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your pincode empty"
      signupCell.pintextField.becomeFirstResponder()
      
    }else if (passwordValue!.isEmpty){
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your password empty"
      signupCell.passwordtextField.becomeFirstResponder()
    }else if (confirmPassValue!.isEmpty){
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your confirmpassword empty"
      signupCell.confirmPasswordtextField.becomeFirstResponder()
    }else if (passwordValue != confirmPassValue){
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your password and confirmpassword not match"
      signupCell.confirmPasswordtextField.becomeFirstResponder()
    }
    
    if(!pincode!.isEmpty) && (!firstNameFb!.isEmpty) && (!lastNameFb!.isEmpty) && (!phoneNumValue!.isEmpty) && (!genderValue!.isEmpty) && (!passwordValue!.isEmpty) && (!confirmPassValue!.isEmpty) && (passwordValue == confirmPassValue){
      
      signupCell.requirementLabel.isHidden = true
      
       let KEY = "KoiTheCambodia0123456789@Pass0rd"
       let IV = "UjXn2r5u8x/A?D(G"
       
      UserDefaults.standard.set(passwordValue, forKey: "password")
       let encryptin = AESUtils.instance.encryptionAES(value: passwordValue!, key256: KEY, iv: IV)
      
      let formatterDate = DateFormatter()
      formatterDate.dateFormat = "dd-MMM-yyyy"
      
      let formatterDateString = DateFormatter()
           formatterDateString.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
      
      var dateStringFormat = ""
      if dateOfBirthValue != "" {
        let dateFormat = formatterDate.date(from: dateOfBirthValue!)
         dateStringFormat = formatterDateString.string(from: dateFormat!)
      }else {
        dateStringFormat = ""
      }
      
    
     
      let param:[String:Any] = [
        "phoneNumber":"+855" + phoneNumValue!.dropFirst(),
      "password": encryptin,
      "gender": genderValue!,
      "last_name": lastNameFb!,
      "first_name": firstNameFb!,
      "dob": dateStringFormat,
      "confirmCode": pincode!,
      "email": emailFb!,
      "photo": photoFb?.absoluteString ?? "",
      "address": addressValue!,
      "firebaseToken": "",
      "facebookId": facebookId!
      ]
     
      self.showLoading()
      IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.signupWithFacebookUrl, params: param, success: { (signupWithFb:SignupUser) in

        if signupWithFb.response?.code == 200 {
            UserDefaults.standard.set(true,forKey: "login")
          UserDefaults.standard.set(signupWithFb.data?.firstName, forKey: "firstname")
          UserDefaults.standard.set(signupWithFb.data?.lastName, forKey: "lastname")
            UserDefaults.standard.set(signupWithFb.data?.id!, forKey: "id")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.id, forKey: "customerWalletId")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.balance, forKey: "balance")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.accName, forKey: "accName")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.score, forKey: "score")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.membershipDate, forKey: "membershipDate")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.rewardScore, forKey: "rewardscore")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.endYear, forKey: "endyear")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.accNumber, forKey: "accNumber")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.qty, forKey: "qty")

            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.memberType?.id, forKey: "memberTypeId")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.memberType?.maxQty, forKey: "memberTypeMaxqty")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.memberType?.minQty, forKey: "memberTypeMinqty")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.memberType?.type, forKey: "memberType")
            UserDefaults.standard.set(signupWithFb.data?.customerWallet?.memberType?.status, forKey: "memberTypeStatus")
          self.hideLoading()
            let rootMenuVC = MenuRootVC()
            rootMenuVC.modalPresentationStyle = .overCurrentContext
            rootMenuVC.modalTransitionStyle = .crossDissolve
            self.present(rootMenuVC, animated: true)
          
          
        }else {
           self.hideLoading()
          signupCell.requirementLabel.isHidden = false
          signupCell.requirementLabel.text = signupWithFb.response?.message ?? ""
          
        }
        
      }) { (failure) in
         self.hideLoading()
        signupCell.requirementLabel.isHidden = false
        signupCell.requirementLabel.text = "No internet"
        print(failure)
      }
      
    }
    
  }

}

extension SignupWithFacebookViewController:UITableViewDelegate,UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let facebookcell = tableView.dequeueReusableCell(withIdentifier: "signupFormfacebookCell", for: indexPath) as! SignupFormFacebookCell
     
    facebookcell.firstnametextField.text = firstNameFb
    facebookcell.lastnametextField.text = lastNameFb
    facebookcell.emailtextField.text = emailFb
    facebookcell.getvalueFromPickerDelegate = self
    facebookcell.checkSentPinDelegate = self
    
    facebookcell.firstnametextField.delegate = self
    facebookcell.lastnametextField.delegate = self
    facebookcell.emailtextField.delegate = self
    facebookcell.addresstextField.delegate = self
    facebookcell.pintextField.delegate = self
    facebookcell.dobtextField.delegate = self
    facebookcell.gendertextField.delegate = self
    facebookcell.phoneNumtextField.delegate = self
    facebookcell.passwordtextField.delegate = self
    facebookcell.confirmPasswordtextField.delegate = self
    return facebookcell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 450
    }
}


extension SignupWithFacebookViewController:GetDataGenderFbDelegate,GetValueFromDatePickerDelegate,CheckSentPinDelegate{
 
    func getGenderValue(value: String) {
        genderValue = value
    }
    
    private func startOtpTimer() {
        self.totalTime = 120
        if count > 0 {  self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        count -= 1}
    }
    
    @objc private func updateTimer(){
        
        let signupCell = signupWithFacebookView.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! SignupFormFacebookCell
        signupCell.sentPinBtn.setTitle(self.timeFormatted(self.totalTime), for: .normal)
        if totalTime != 0 {
          totalTime -= 1  // decrease counter timer
             
          signupCell.sentPinBtn.isUserInteractionEnabled = false
                      } else {
                    if let timers = self.timer {
                              timers.invalidate()
                              self.timer = nil
                           totalTime = 5
          signupCell.sentPinBtn.isUserInteractionEnabled = true
          signupCell.sentPinBtn.setTitle("Send Pin", for: .normal)
              }
          }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
  
  func getSentPin() {
    
    let signupCell = signupWithFacebookView.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! SignupFormFacebookCell
    
    if phoneNumValue!.isEmpty {
      signupCell.requirementLabel.isHidden = false
      signupCell.requirementLabel.text = "your phone number is empty"
      signupCell.phoneNumtextField.becomeFirstResponder()

    }else {

      signupCell.requirementLabel.isHidden = true
       let param:[String:Any] = [
        "phoneNumber":"+855\(phoneNumValue!.dropFirst())",
         "termcondit":true
       ]

      print(param)
       IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.presignupUrl, params: param, success: { (jsonResponse:Presignup) in
  print("sign facebook",jsonResponse)
         if jsonResponse.response?.code == 200 {
            self.startOtpTimer()
          }else {
          signupCell.requirementLabel.isHidden = false
          signupCell.requirementLabel.text = jsonResponse.response?.message ?? ""
          }
       }) { (failure) in
          signupCell.requirementLabel.isHidden = false
          signupCell.requirementLabel.text = "No internet"
         print(failure)
       }
    }
  }
  
  func getDobValue(value: String) {
    dateOfBirthValue = value
  }
  
  func getGenderValue() {
    genderView.showGender()
  }
  
  
  func getDataGender(value: String) {
    
    let genderCell = signupWithFacebookView.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! SignupFormFacebookCell
    genderCell.gendertextField.text = value
    self.genderValue = value
  }
  
  
}

extension SignupWithFacebookViewController:UITextFieldDelegate {
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//     if textField.tag == 6 {
//       textField.resignFirstResponder()
//       return false
//     }
     return true
   }
   
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
     textField.resignFirstResponder()
     return true
   }
   
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     //let indexOf = controls.index(of: )
    
       switch textField.tag {
       case 0:
         guard let currentText = textField.text else { return true }
         let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
         
         self.firstNameFb = finalText
       case 1:
         
         guard let currentText = textField.text else { return true }
         let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
         self.lastNameFb = finalText
         
       case 2:
         guard let currentText = textField.text else { return true }
         let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
         self.emailFb = finalText
     
       case 3:
       guard let currentText = textField.text else { return true }
       let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
       self.addressValue = finalText
       
       case 4:
       guard let currentText = textField.text else { return true }
       let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
       
       self.phoneNumValue = finalText
       
       case 5:
       guard let currentText = textField.text else { return true }
       let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
       self.pincode = finalText
       case 6:
       guard let currentText = textField.text else { return true }
       let _ = (currentText as NSString).replacingCharacters(in: range, with: string)
      // self.addressValue = finalText
      case 7:
          guard let currentText = textField.text else { return true }
          let _ = (currentText as NSString).replacingCharacters(in: range, with: string)
          //self.passwordValue = finalText
   
      case 8:
          guard let currentText = textField.text else { return true }
          let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
          self.passwordValue = finalText
        
      case 9:
      guard let currentText = textField.text else { return true }
      let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
      self.confirmPassValue = finalText
        
       default:
         print("nothing")
       }
       return true
     }
}
