//
//  ViewController.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/25/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import FBSDKCoreKit


class AuthenticationVC: BaseViewController {
  
  //MARK:Properties
lazy private var authenticationView = AuthenticationView()
 fileprivate let readPermissions: [Permission] = [ .publicProfile, .email]
  
  
  //MARK:Life Cycle
  
  override func setupLoadView() {
    super.setupLoadView()
     self.view = authenticationView
  }
  
  
  override func setupNavigationBar() {
    super.setupNavigationBar()
    titleNavigationBar.isHidden = true
  }

  
  //MARK: SETUP EVENT
  
  override func setupEvent() {
    super.setupEvent()
    
      authenticationView.signinBtn.addTarget(self, action: #selector(setupSignInTapped), for: .touchUpInside)
      authenticationView.signupBtn.addTarget(self, action: #selector(setupSignUpTapped), for: .touchUpInside)
      authenticationView.signinFbBtn.addTarget(self, action: #selector(signinFBTapped), for: .touchUpInside)
  }

  //MARK: SIGNIN EVENT
 
  @objc private func setupSignInTapped(){
    let signInVC = SigninFormVC()
    signInVC.modalTransitionStyle = .crossDissolve
    signInVC.modalPresentationStyle = .overCurrentContext
    self.present(signInVC, animated: true)
  }
  
  //MARK: SIGNUP EVENT
 
  @objc private func setupSignUpTapped(){
    let signupVC = SignupWithPhoneNumberVC()
    signupVC.modalPresentationStyle = .overCurrentContext
    signupVC.modalTransitionStyle = .crossDissolve
    self.present(signupVC, animated: true)
  }
  
  //MARK:SIGNIN FACEBOOK
  @objc private func signinFBTapped(){
    UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
      self.authenticationView.signinFbBtn.transform = CGAffineTransform(scaleX: 0.92, y: 0.92)
       }) { (_) in
         UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
           self.authenticationView.signinFbBtn.transform = CGAffineTransform(scaleX: 1, y: 1)
         }){(_) in
           
           let loginManager = LoginManager()
          
          loginManager.logIn(permissions: self.readPermissions, viewController: self, completion: self.didReceiveFacebookLoginResult)
         }
    }
   }
  
  //MARK: RECEIVE FACEBOOK
  private func didReceiveFacebookLoginResult(loginResult: LoginResult){
    switch loginResult{
    case .success(_,_,let token):
      
      didLoginWithFacebook(token: token)
      break
    case .cancelled:
      
      print("cancelled")
    case .failed(_):
      print("failed")
    }
  }
  
  //MARK: DIDLOGIN WITH FACEBOOK
  fileprivate func didLoginWithFacebook(token:AccessToken){
    
    let connection = GraphRequestConnection()
    connection.add(GraphRequest(graphPath: "/me", parameters: ["fields":"id,name,email"], httpMethod: .get)) { (requestconnct, result, error) in
      
      if error != nil {
             NSLog(error.debugDescription)
             return
         }
      
      // Handle vars
       let result = result as? [String:String]
        let email: String = result?["email"] ?? ""
        let name:String = result?["name"] ?? ""
        let fbId: String = result?["id"] ?? ""
        let imgURLString = URL(string: "http://graph.facebook.com/\(result!["id"]!)"  + "/picture?type=large")!
        
    
        self.postDataFacebookToServer(name: name, email: email, fbId: fbId, image: imgURLString)
        
    }
   connection.start()
  }
  
  
  //MARK: POST DATA FACEBOOK TO SERVER
  fileprivate func postDataFacebookToServer(name:String,email:String,fbId:String,image:URL){
    
    
    IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.checkFacebookIdiosUrl + "?facebookId=\(fbId)", params: nil, success: { (signup:SignupUser) in

      if signup.response?.code == 200 {
        
       let homeVC = MenuRootVC()
        homeVC.modalPresentationStyle = .overCurrentContext
        homeVC.modalTransitionStyle = .crossDissolve
        self.present(homeVC, animated: true)
      }else {

        let signupVC = SignupWithFacebookViewController()
               signupVC.facebookId = fbId
               let userFb = name.components(separatedBy: " ")
               signupVC.firstNameFb = userFb[0]
               signupVC.lastNameFb = userFb[1]
               signupVC.emailFb = email
               signupVC.photoFb = image
               signupVC.photoFb = image
                signupVC.modalPresentationStyle = .overCurrentContext
                signupVC.modalTransitionStyle = .crossDissolve
                self.present(signupVC, animated: true)
      }
    }) { (failure) in
      print(failure)
    }
    
     

  
  }
}

