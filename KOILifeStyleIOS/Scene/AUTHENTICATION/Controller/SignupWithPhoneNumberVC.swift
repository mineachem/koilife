//
//  SignupVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import Alamofire

class SignupWithPhoneNumberVC: BaseViewController {

 
  lazy private var signupView = SignupWithPhoneNumberView()
  
  
  var termcondit:Bool = false
  
  override func setupLoadView() {
    super.setupLoadView()
    self.view = signupView
  }
  
// MARK: SETUP NAVIGATION
  override func setupNavigationBar() {
    super.setupNavigationBar()
    
      let label = UILabel()
      label.text = "SIGN UP With Phone"
       label.font = UIFont(name: "Montserrat-Medium", size: 14)
    titleNavigationBar.topItem?.titleView = label
  }
  
  //MARK: SETUP VIEW
  override func setupUI() {
    super.setupUI()
    signupView.phoneNumberTextField.delegate = self
  }
  
//  MARK:SETUP EVENT
  override func setupEvent(){
    super.setupEvent()
    
    signupView.checkBoxBtn.addTarget(self, action: #selector(handleCheckboxTapped), for: .touchUpInside)
    signupView.signupBtn.addTarget(self, action: #selector(handleSignupTapped), for: .touchUpInside)
    signupView.loginBtn.addTarget(self, action: #selector(handleLoginTapped), for: .touchUpInside)
  
    let tap = UITapGestureRecognizer(target: self, action: #selector(handleHideKeyboard))
    view.addGestureRecognizer(tap)
  }
  
  @objc private func handleHideKeyboard(){
        view.endEditing(true)
      }
  
  @objc private func handleCheckboxTapped(){
    termcondit = false
    if signupView.checkBoxBtn.currentImage == #imageLiteral(resourceName: "unchecked-box").withRenderingMode(.alwaysOriginal) {
      termcondit = true
      signupView.checkBoxBtn.setImage(#imageLiteral(resourceName: "check-box").withRenderingMode(.alwaysOriginal), for: .normal)
    }else {
      termcondit = false
      signupView.checkBoxBtn.setImage(#imageLiteral(resourceName: "unchecked-box").withRenderingMode(.alwaysOriginal), for: .normal)
    }
  }
  
  @objc private func handleSignupTapped(){
    
    let phoneNumber = signupView.phoneNumberTextField.text!
    signupView.phoneNumberTextField.resignFirstResponder()
    if (!phoneNumber.isEmpty) && termcondit == true{
     self.signupView.requireMessageLabel.isHidden = true
      
      
      let param:[String:Any] = [
        "phoneNumber":"+855\(phoneNumber.dropFirst())",
        "termcondit":termcondit
      ]
      
      //self.showLoading()
      IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.presignupUrl, params: param, success: { (jsonResponse:Presignup) in
          
        if jsonResponse.response?.code == 200 {
          
         // self.hideLoading()
        //  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                   let signupFormVC = SignupFormVC()
                   signupFormVC.modalPresentationStyle = .overCurrentContext
                   signupFormVC.modalTransitionStyle = .crossDissolve
                 //  let stringArray = jsonResponse.data!.message!.components(separatedBy: CharacterSet.decimalDigits.inverted)
                   signupFormVC.phonenumber = phoneNumber
            
//                   stringArray.forEach { (item) in
//                     if let number = Int(item){ signupFormVC.getCodeVerify = number }
//                   }
            self.present(signupFormVC, animated: true)
                
         // }
            }else {
          self.hideLoading()
          
             self.signupView.requireMessageLabel.isHidden = false
             self.signupView.requireMessageLabel.text = jsonResponse.response?.message ?? ""

                 }
      }) { (failure) in
        self.hideLoading()
        print(failure)
      }
    }else if termcondit == false {
      self.signupView.requireMessageLabel.isHidden = false
      self.signupView.requireMessageLabel.text = "you must agree term and condition"

    }else if (phoneNumber.isEmpty) {
      self.signupView.requireMessageLabel.isHidden = false
      self.signupView.requireMessageLabel.text = "please enter your phone number"

    }else if phoneNumber.count < 9 {
      self.signupView.requireMessageLabel.isHidden = false
      self.signupView.requireMessageLabel.text = "phone number is invalid!"
    }
  
  }
  
  
  @objc private func handleLoginTapped(){
    let signinVC = SigninFormVC()
    signinVC.modalPresentationStyle = .overCurrentContext
    signinVC.modalTransitionStyle = .crossDissolve
    self.present(signinVC, animated: true)
  }
  
}


extension SignupWithPhoneNumberVC:UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
}
