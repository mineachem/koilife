//
//  SignupFormVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/27/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit


class SignupFormVC: BaseViewController {
  
  lazy var signupFormView = SignupFormView()
  
  let signinWithPhone = SignupWithPhoneNumberVC()
  //var getCodeVerify:Int? = 0
  var phonenumber:String? = ""
  var dateOfBirth:String? = ""
  var genderArray = ["male","female"]
  
  var count:Int = 2
  var timer:Timer?
  var totalTime = 120
  override func setupLoadView() {
    super.setupLoadView()
     self.view = signupFormView
  }

  
  //MARK: SETUP NAVIGATIONBAR
  override func setupNavigationBar() {
    super.setupNavigationBar()
    
    let label = UILabel()
         label.text = "SIGN UP"
         label.font = UIFont(name: "Montserrat-Medium", size: 14)
       titleNavigationBar.topItem?.titleView = label
  }
  
  //MARK:SETUP VIEW
  
  override func setupUI() {
  
    signupFormView.pincodeTextField.delegate = self
    signupFormView.passwordTextField.delegate = self
    signupFormView.confirmPasswordTextField.delegate = self
    signupFormView.firstNameTextField.delegate = self
    signupFormView.lastNameTextField.delegate = self
    signupFormView.emailTextField.delegate = self
    signupFormView.genderTextField.delegate = self
    signupFormView.birthdayTextField.delegate = self
    
    
    let intLetters = phonenumber!.prefix(0)
    let endLetters = phonenumber!.suffix(2)
    let stars = String(repeating: "x", count: phonenumber!.count)
    let result = intLetters + stars + endLetters
    signupFormView.phoneNumberLabel.text = result.description
    //signupFormView.pincodeTextField.text = getCodeVerify!.description
  }
  
  //  MARK:SETUP EVENT
  
  override func setupEvent(){
    super.setupEvent()
    
    signupFormView.submitBtn.addTarget(self, action: #selector(handleSubmitTapped), for: .touchUpInside)
    signupFormView.genderTextField.addTarget(self, action: #selector(handleGenderPickerTouch), for: .touchDown)
    signupFormView.birthdayTextField.addTarget(self, action: #selector(handleDateOfbirthPickerToch), for: .touchDown)
    let tap = UITapGestureRecognizer(target: self, action: #selector(handleHideKeyboard))
    view.addGestureRecognizer(tap)
    
    signupFormView.pincodeBtn.addTarget(self, action: #selector(handlePinCodeTapped), for: .touchUpInside)
  }
  
  
  @objc private func handlePinCodeTapped(){
    self.totalTime = 120

    if count > 0 {
      
            let param:[String:Any] = [
              "phoneNumber":"+855\(String(describing: phonenumber!.dropFirst()))",
            "termcondit":true
          ]
      
            IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.presignupUrl, params: param, success: { (jsonResponse:Presignup) in
                 if jsonResponse.response?.code == 200 {
                  self.signupFormView.requireLabel.isHidden = false
                  self.signupFormView.requireLabel.text = jsonResponse.response!.message
                  
                  }else {
                  
                  self.signupFormView.requireLabel.isHidden = false
                  self.signupFormView.requireLabel.text = jsonResponse.response!.message
                  
                  }
               }) { (failure) in
                self.signupFormView.requireLabel.isHidden = false
                self.signupFormView.requireLabel.text = "No internet"
      
                 print(failure)
               }
      
      self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
      count -= 1
    }else {
      signupFormView.timerLabel.isHidden = true
      signupFormView.pincodeBtn.isUserInteractionEnabled = false
     // signupFormView.pincodeBtn.isHidden = true
      print("nothing")
    }
  }
  
  @objc private func updateTimer(){
    signupFormView.timerLabel.text = self.timeFormatted(self.totalTime) // will show timer
     signupFormView.timerLabel.isHidden = false
      if totalTime != 0 {
                        totalTime -= 1  // decrease counter timer
           
        signupFormView.pincodeBtn.isUserInteractionEnabled = false
                    } else {
                  if let timers = self.timer {
                            timers.invalidate()
                            self.timer = nil
                         
        signupFormView.pincodeBtn.isUserInteractionEnabled = true
        signupFormView.timerLabel.isHidden = true
            }
        }
  }
  @objc private func handleHideKeyboard(){
    view.endEditing(true)
  }
  
  func timeFormatted(_ totalSeconds: Int) -> String {
      let seconds: Int = totalSeconds % 60
      let minutes: Int = (totalSeconds / 60) % 60
      return String(format: "%02d:%02d", minutes, seconds)
  }
  
  @objc private func handleGenderPickerTouch(){
    let genderPickerView = UIPickerView()
    genderPickerView.delegate = self
    genderPickerView.dataSource = self

    let toolbar = UIToolbar()
    toolbar.sizeToFit()
    
    
    let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
    toolbar.setItems([doneButton], animated: true)
    
    signupFormView.genderTextField.inputView = genderPickerView
    signupFormView.genderTextField.inputAccessoryView = toolbar
    
  }
  
  @objc private func handleDateOfbirthPickerToch(){
    let datePickerView = UIDatePicker()
    datePickerView.datePickerMode = .date
    signupFormView.birthdayTextField.inputView = datePickerView
    
    let toolbar = UIToolbar()
    toolbar.sizeToFit()
    
    let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
    toolbar.setItems([doneButton], animated: true)
    
    signupFormView.birthdayTextField.inputAccessoryView = toolbar
    
    datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
  }
  
  @objc func doneClicked(){
    self.view.endEditing(true)
  }
  
  
  @objc func handleDatePicker(sender:UIDatePicker){
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MMM-yyyy"
    if dateFormatter.string(from: sender.date) != ""{
      signupFormView.birthdayTextField.text = dateFormatter.string(from: sender.date)
    }else {
      print("your date empty")
    }
    
  }
  
 
  
  @objc private func handleSubmitTapped(){
    
    //let phoneNumber = signupFormView.phoneNumberLabel.text!
    let password = signupFormView.passwordTextField.text!
    
//    let KEY = "KoiTheCambodia0123456789@Pass0rd"
//    let IV = "UjXn2r5u8x/A?D(G"
//    
    // let encryptin = AESUtils.instance.encryptionAES(value: password, key256: KEY, iv: IV)
    
    let confirmPass = signupFormView.confirmPasswordTextField.text!
    let pincode = signupFormView.pincodeTextField.text!
    let gender = signupFormView.genderTextField.text!
    let lastname = signupFormView.lastNameTextField.text!
    let firstname = signupFormView.firstNameTextField.text!
    let dob = signupFormView.birthdayTextField.text!
    let confirmCode = signupFormView.pincodeTextField.text!
    let email = signupFormView.emailTextField.text!
    
  
    signupFormView.requireLabel.isHidden = true
    
   
//    let formatterDateString = DateFormatter()
//    formatterDateString.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    if (pincode.isEmpty) && (password.isEmpty) && (firstname.isEmpty) && (lastname.isEmpty) && (gender.isEmpty){
      signupFormView.requireLabel.isHidden = false
      signupFormView.requireLabel.text = "plase enter all data in form sign up"
      signupFormView.pincodeTextField.becomeFirstResponder()
    }else if (pincode.isEmpty) {
      signupFormView.requireLabel.isHidden = false
      signupFormView.requireLabel.text = "your pincode is empty"
      signupFormView.pincodeTextField.becomeFirstResponder()
    }else if (password.isEmpty){
      signupFormView.requireLabel.isHidden = false
      signupFormView.requireLabel.text = "your password is empty"
      signupFormView.passwordTextField.becomeFirstResponder()
    }else if (confirmPass.isEmpty){
      signupFormView.requireLabel.isHidden = false
      signupFormView.requireLabel.text = "your confirm password is empty"
      signupFormView.confirmPasswordTextField.becomeFirstResponder()
    }else if (password != confirmPass){
      signupFormView.requireLabel.isHidden = false
      signupFormView.requireLabel.text = "password and confirm password not match"
      signupFormView.confirmPasswordTextField.becomeFirstResponder()
    }else if (firstname.isEmpty){
      signupFormView.requireLabel.isHidden = false
      signupFormView.requireLabel.text = "your firstname is empty"
      signupFormView.firstNameTextField.becomeFirstResponder()
    }else if (lastname.isEmpty){
      signupFormView.requireLabel.isHidden = false
      signupFormView.requireLabel.text = "your lastname is empty"
      signupFormView.lastNameTextField.becomeFirstResponder()
    }else if (!email.isValidEmail()){
      signupFormView.requireLabel.isHidden = false
      signupFormView.requireLabel.text = "your email invalid"
      signupFormView.emailTextField.becomeFirstResponder()
    }else if (gender.isEmpty){
      signupFormView.requireLabel.isHidden = false
      signupFormView.requireLabel.text = "your gender is empty"
      signupFormView.genderTextField.becomeFirstResponder()
    }
    
    //dateOfBirth
   
    if (!pincode.isEmpty) && (!password.isEmpty) && (!gender.isEmpty) && (!lastname.isEmpty) && (!firstname.isEmpty){
       signupFormView.requireLabel.isHidden = true
      
      
     let formatterDate = DateFormatter()
     formatterDate.dateFormat = "dd-MMM-yyyy"
     
     let formatterDateString = DateFormatter()
      formatterDateString.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
      
      var dateStringFormat = ""
      if dob != "" {
        let dateCurrent = formatterDate.string(from: Date())
        let dateCurrentNow = formatterDate.date(from: dateCurrent)
        let dateUser = formatterDate.date(from: dob)
        
        let now = dateCurrentNow!
        let birthday: Date = dateUser!
        let calendar = Calendar.current

        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        let age = ageComponents.year!
        
        if age < 10 {
          
          signupFormView.requireLabel.isHidden = false
          signupFormView.requireLabel.text = "your birthday is smaller than 10"
        }else {
          
         dateStringFormat = formatterDateString.string(from: birthday)
          self.signupUser(dateOfBirth: dateStringFormat, phone: phonenumber!, password: password, gender: gender, lastname: lastname, firstname: firstname, confirmCode: confirmCode, email: email)
        }
       
      }else {
        dateStringFormat = ""
        self.signupUser(dateOfBirth: dateStringFormat, phone: phonenumber!, password: password, gender: gender, lastname: lastname, firstname: firstname, confirmCode: confirmCode, email: email)
      }
      
    }
  }
  
  func signupUser(dateOfBirth:String,phone:String,password:String,gender:String,lastname:String,firstname:String,confirmCode:String,email:String){
   // UserDefaults.standard.set(password, forKey: "password")
             let param:[String:Any] = [
               "phoneNumber":"+855" + phone.dropFirst(),
                 "password": password,
                 "gender": gender,
                 "last_name": lastname,
                 "first_name": firstname,
                 "dob": dateOfBirth,
                 "confirmCode": confirmCode,
                 "email": email
             ]
             
             self.showLoading()
             IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.signupUrl, params: param, success: { (signupResponse:SignupUser) in
               
               if signupResponse.response!.code == 200 {
                 
                 self.hideLoading()
                 
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                    let signinVC = SigninFormVC()
                        signinVC.modalTransitionStyle = .crossDissolve
                        signinVC.modalPresentationStyle = .overCurrentContext
                    self.present(signinVC, animated: true)
                 }
                   }else {
                 self.hideLoading()
                 
                 self.signupFormView.requireLabel.isHidden = false
                 self.signupFormView.requireLabel.text = signupResponse.response!.message
                       print(signupResponse.response!.message!)
                   }
             }) { (failure) in
               print(failure)
               self.signupFormView.requireLabel.isHidden = false
               self.signupFormView.requireLabel.text = "Internet not connection"
             }
  }
}




extension SignupFormVC:UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}



extension SignupFormVC:UIPickerViewDelegate,UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return genderArray.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return genderArray[row]
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    signupFormView.genderTextField.text = genderArray[row]
  }
}
