//
//  ForgotPasswordVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/27/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ForgotPasswordVC: UIViewController {
let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    let label = UILabel()
    label.text = "RESET PASSWORD"
    label.font = UIFont(name: "Montserrat-Medium", size: 14)
    navItem.titleView = label
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
  
  lazy var forgotPasswordView = ForgotPasswordView()
 // var phoneNumber:String?
  
  override func loadView() {
    super.loadView()
    self.view = forgotPasswordView
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
       setupNavigationBar()
       setupView()
       setupIndicator()
       setupEvent()
    }
    
  fileprivate func setupIndicator(){
     
        loading.translatesAutoresizingMaskIntoConstraints = false
         
        view.addSubview(loading)
        NSLayoutConstraint.activate([
          loading.widthAnchor.constraint(equalToConstant: 40),
          loading.heightAnchor.constraint(equalToConstant: 40),
          loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
          loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
     
   }
  
  func showLoading()
    {
      loading.startAnimating()
    }

    func hideLoading()
    {
      loading.stopAnimating()
    }
  
  fileprivate func setupNavigationBar(){
    view.addSubview(titleNavigationBar)
    
    NSLayoutConstraint.activate([
         titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
         titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
         titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
       ])
  }
  
  
  fileprivate func setupView(){
    forgotPasswordView.phoneNumberTextField.delegate = self
  }
  
  fileprivate func setupEvent(){
    forgotPasswordView.resetBtn.addTarget(self, action: #selector(handleResetPassword), for: .touchUpInside)
  }
  
  @objc private func handleResetPassword(){
    let phoneNumber = forgotPasswordView.phoneNumberTextField.text!
    if phoneNumber.isEmpty {
      forgotPasswordView.requirementLabel.isHidden = false
      forgotPasswordView.requirementLabel.text = "your email or password is empty"
      forgotPasswordView.phoneNumberTextField.becomeFirstResponder()
    }
    
    var param:[String:Any] = [:]
    
    if phoneNumber.isValidEmail(){
      
       param = ["email":phoneNumber]
      
      self.showLoading()
      IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.resetpasswordUrl, params: param, success: { (resetupResponse:ResetPassword) in
              
              if resetupResponse.response?.code == 200 {
                self.hideLoading()
                let alertResetController = UIAlertController(title: "Reset Password", message: resetupResponse.response!.message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                  self.dismiss(animated: true)
                  }
                alertResetController.addAction(okAction)
                self.present(alertResetController, animated: true)
              }else {
                self.hideLoading()
                self.forgotPasswordView.requirementLabel.isHidden = false
                self.forgotPasswordView.requirementLabel.text = resetupResponse.response!.message
              }
            }) { (failure) in
              self.hideLoading()
              self.forgotPasswordView.requirementLabel.isHidden = false
              self.forgotPasswordView.requirementLabel.text = "No Internet"
            }
      
    }else {
      param = [ "phoneNumber": "+855" + phoneNumber.dropFirst() ,
                "termcondit": true]
      
      self.showLoading()
      IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.resetpasswordByPhoneUrl, params: param, success: { (resetpasswordByPhone:ResetPasswordByPhone) in
        if resetpasswordByPhone.response?.code == 200 {
          self.hideLoading()
           let alertResetController = UIAlertController(title: "Reset Password By Phone", message: resetpasswordByPhone.response!.message, preferredStyle: .alert)
               let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                   let resetPasswByPhoneVC = ResetPasswordByPhoneVC()
                resetPasswByPhoneVC.phoneNumber = phoneNumber
                resetPasswByPhoneVC.modalPresentationStyle = .overCurrentContext
                resetPasswByPhoneVC.modalTransitionStyle = .crossDissolve
                self.present(resetPasswByPhoneVC, animated: true)
                 }
               alertResetController.addAction(okAction)
              self.present(alertResetController, animated: true)
        }else {
           self.hideLoading()
          self.forgotPasswordView.requirementLabel.isHidden = false
          self.forgotPasswordView.requirementLabel.text = resetpasswordByPhone.response!.message
        }
      }) { (failure) in
         self.hideLoading()
        self.forgotPasswordView.requirementLabel.isHidden = false
        self.forgotPasswordView.requirementLabel.text = "No internet connection"
      }
    }
    
  }
  
  @objc private func handleBackTapped(){
     view.endEditing(true)
    self.dismiss(animated: true)
    
  }
}


extension ForgotPasswordVC:UITextFieldDelegate {
//  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//    textField.resignFirstResponder()
//      return true
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
    }
  
}
