//
//  SigninVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import Firebase

struct SignIn:Codable{
  var phoneNumber:String
  var password:String
}
class SigninFormVC: BaseViewController {
  
  //MARK: PROPERTIES
  
  lazy var signinView = SignInView()
  var phoneNumber:String? = ""
  var password:String? = ""
     
  //MARK:Life Cycle
  
  override func setupLoadView() {
    super.setupLoadView()
     self.view = signinView
  }

  
  override func setupNavigationBar() {
    super.setupNavigationBar()
      let label = UILabel()
      label.text = "SIGN IN"
      label.font = UIFont(name: "Montserrat-Medium", size: 14)
    titleNavigationBar.topItem?.titleView = label
  }
  
  //MARK: SETUP VIEW
//  fileprivate func setupNavigation(){
//
//  }

  override func setupUI() {
    super.setupUI()
    signinView.phoneNumberTextField.delegate = self
    signinView.passwordTextField.delegate = self
  }
  
     
  
  //MARK:SETUP EVENT
  
  override func setupEvent(){
    super.setupEvent()
    
    signinView.forgetPasswordBtn.addTarget(self, action: #selector(handleForgotPasswordTapped), for: .touchUpInside)
    signinView.signinBtn.addTarget(self, action: #selector(handleSigninTapped), for: .touchUpInside)
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(handleHideKeyboard))
    view.addGestureRecognizer(tap)
    
    signinView.createOneBtn.addTarget(self, action: #selector(handleCreateOneTapped), for: .touchUpInside)
   }
  
  
  @objc private func handleHideKeyboard(){
           view.endEditing(true)
    }
  
 
  
  
  @objc private func handleForgotPasswordTapped(){
     let forgotPasswordVC = ForgotPasswordVC()
     forgotPasswordVC.modalPresentationStyle = .overCurrentContext
     forgotPasswordVC.modalTransitionStyle = .crossDissolve
     self.present(forgotPasswordVC, animated: true)
   }
  
  @objc private func handleCreateOneTapped(){
     let signupWithPhoneNumVC = SignupWithPhoneNumberVC()
     signupWithPhoneNumVC.modalPresentationStyle = .overCurrentContext
     signupWithPhoneNumVC.modalTransitionStyle = .crossDissolve
     self.present(signupWithPhoneNumVC, animated: true)
   }
  
  @objc private func handleSigninTapped(){
    if phoneNumber!.isEmpty &&  password!.isEmpty  {
      signinView.requirementLabel.isHidden = false
      signinView.requirementLabel.text = "please enter your phone number/email and password"
      signinView.phoneNumberTextField.becomeFirstResponder()
    }else if phoneNumber!.isEmpty {
      signinView.requirementLabel.isHidden = false
      signinView.requirementLabel.text = "please enter your phone number or email"
      signinView.phoneNumberTextField.becomeFirstResponder()
    }else if password!.isEmpty {
      signinView.requirementLabel.isHidden = false
      signinView.requirementLabel.text = "please enter your password"
      signinView.passwordTextField.becomeFirstResponder()
    }
    
    

    if (!phoneNumber!.isEmpty) && (!password!.isEmpty){
      
      signinView.requirementLabel.isHidden = true
      
      let KEY = "KoiTheCambodia0123456789@Pass0rd"
      let IV = "UjXn2r5u8x/A?D(G"
      
      UserDefaults.standard.set(password, forKey: "password")
      let encryptin = AESUtils.instance.encryptionAES(value: password!, key256: KEY, iv: IV)
      
      print("image: \(encryptin)")
      var param:[String:Any] = [:]
      
      if phoneNumber!.isValidEmail() {
         param = [
          "phoneNumber":phoneNumber!,
          "password": encryptin
        ]
      }else {
         param = [
                 "phoneNumber":"+855"+phoneNumber!.dropFirst(),
                 "password": encryptin
            ]
      }
      
      
        
      self.showLoading()
      IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.loginUrl, params: param, success: { (signinResponse:SignupUser) in

        if signinResponse.response?.code == 200 {

              UserDefaults.standard.set(true,forKey: "login")
              UserDefaults.standard.set(signinResponse.data?.firstName, forKey: "firstname")
              UserDefaults.standard.set(signinResponse.data?.lastName, forKey: "lastname")
              UserDefaults.standard.set(signinResponse.data?.id!, forKey: "id")

              UserDefaults.standard.set(signinResponse.data?.customerWallet?.id, forKey: "customerWalletId")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.balance, forKey: "balance")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.accName, forKey: "accName")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.score, forKey: "score")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.membershipDate, forKey: "membershipDate")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.rewardScore, forKey: "rewardscore")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.endYear, forKey: "endyear")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.accNumber, forKey: "accNumber")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.qty, forKey: "qty")

              UserDefaults.standard.set(signinResponse.data?.customerWallet?.memberType?.id, forKey: "memberTypeId")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.memberType?.maxQty, forKey: "memberTypeMaxqty")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.memberType?.minQty, forKey: "memberTypeMinqty")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.memberType?.type, forKey: "memberType")
              UserDefaults.standard.set(signinResponse.data?.customerWallet?.memberType?.status, forKey: "memberTypeStatus")

          self.hideLoading()
          /// subsribe topic
          let trimmedMemberType = signinResponse.data?.customerWallet?.memberType?.type?.removeWhitespace()
          guard let memberType = trimmedMemberType else {return}
          
          Messaging.messaging().subscribe(toTopic: memberType) { error in
              print("Subscribed to \(memberType) topic")
            }
          Messaging.messaging().subscribe(toTopic: "Promotion") { error in
             print("Subscribed to Promotion topic")
           }
            let homeVC = MenuRootVC()
            homeVC.modalPresentationStyle = .overCurrentContext
             homeVC.modalTransitionStyle = .crossDissolve
             self.present(homeVC, animated: true)
          


        }else{
             self.hideLoading()
          self.signinView.requirementLabel.isHidden = false
          self.signinView.requirementLabel.text = signinResponse.response?.message ?? ""
          self.signinView.phoneNumberTextField.becomeFirstResponder()

        }
      }) { (failure) in
        self.hideLoading()
        print(failure)
        self.signinView.requirementLabel.isHidden = false
        self.signinView.requirementLabel.text = "No Internet Connection"
        self.signinView.phoneNumberTextField.becomeFirstResponder()
        
      }
      
    }
  }
}



extension SigninFormVC: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
  
      switch textField.tag {
      case 0:
        guard let currentText = textField.text else { return true }
        let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        self.phoneNumber = finalText
      case 1:
        
        guard let currentText = textField.text else { return true }
        let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        self.password = finalText
        
      default:
        print("nothing")
      }
      return true
    }
}
