//
//  SignupUser.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/28/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation

// MARK: - SignupUser
struct SignupUser: Codable {
    let data: SignUpUserData?
    let response: ResponseDataUser?
}

// MARK: - DataClass
struct SignUpUserData: Codable {
    let id: Int?
    let firstName, lastName, userType, phoneNo: String?
    let email, address: String?
    let dob: Int?
    let gender, password: String?
    let customerWallet: CustomerWallet?
    let confirmCode: String?
    let photo: String?
    let memberType, accNumber, passcodeLock: String?
    let passcodeLockOn, confirmCodeExpired: Bool?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case userType, phoneNo, email, address, dob, gender, password, customerWallet, confirmCode, photo, memberType, accNumber, passcodeLock, passcodeLockOn, confirmCodeExpired
    }
}

// MARK: - CustomerWallet
struct CustomerWallet: Codable {
    let id: Int?
    let accNumber, accName: String?
    let balance:Double?
    let membershipDate, score, rewardScore: Int?
    let qty: Int?
    let memberType: MemberType?
    let endYear: Int?
}

// MARK: - MemberType
struct MemberType: Codable {
    let id: Int?
    let type: String?
    let minQty, maxQty: Int?
    let status: Bool?
    let createById, updatedById: Int?
}

// MARK: - Response
struct ResponseDataUser: Codable {
    let code: Int?
    let message: String?
}
