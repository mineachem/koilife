//
//  Presignup.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/27/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
// MARK: - Presignup
struct Presignup: Codable {
    let data, response: DataPreSignup?
}

// MARK: - DataClass
struct DataPreSignup: Codable {
    let code: Int?
    let message: String?
}
