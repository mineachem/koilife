//
//  ResetPassword.swift
//  KOILifeStyleIOS
//
//  Created by Apple on 12/31/19.
//  Copyright © 2019 Minea. All rights reserved.
//


import Foundation
// MARK: - ResetPassword
struct ResetPassword: Codable {
    let response: Response?
}

// MARK: - Response
struct Response: Codable {
    let code: Int?
    let message: String?
}
