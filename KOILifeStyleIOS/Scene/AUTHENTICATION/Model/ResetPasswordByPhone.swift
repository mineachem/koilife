//
//  ResetpasswordByPhone.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/31/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
// MARK: - ResetPasswordByPhone
struct ResetPasswordByPhone: Codable {
    let data, response: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    let code: Int?
    let message: String?
}
