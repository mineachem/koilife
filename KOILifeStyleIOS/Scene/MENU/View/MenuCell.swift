//
//  MenuCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MenuCell: UICollectionViewCell {
    override init(frame: CGRect) {
           super.init(frame: frame)
           setupViews()
       }
       
       required init?(coder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }
       
       func setupViews()
       {
            addSubview(shadowView)
            NSLayoutConstraint.activate([
              shadowView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
              shadowView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
              shadowView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
              shadowView.heightAnchor.constraint(equalToConstant: self.frame.width)
            ])
        
           shadowView.addSubview(imageView)
           imageView.topAnchor.constraint(equalTo: shadowView.topAnchor, constant: 10).isActive = true
           imageView.leadingAnchor.constraint(equalTo: shadowView.leadingAnchor, constant: 5).isActive = true
           imageView.trailingAnchor.constraint(equalTo: shadowView.trailingAnchor, constant: -5).isActive = true
           imageView.bottomAnchor.constraint(equalTo: shadowView.bottomAnchor).isActive = true
           
           addSubview(titleLabel)
           titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 5).isActive = true
           titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
           titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5).isActive = true
       }
    
        lazy var shadowView: UIView = {
            
            let containerView = UIView()
            containerView.layer.shadowColor = UIColor.black.cgColor
            containerView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            containerView.layer.shadowRadius = 3.0
            containerView.layer.shadowOpacity = 0.2
            containerView.translatesAutoresizingMaskIntoConstraints = false
            return containerView
            
        }()
       lazy var imageView: CustomImageView = {
           let iv = CustomImageView()
           iv.translatesAutoresizingMaskIntoConstraints = false
           iv.image = UIImage(named: "img_placeholder")
           iv.contentMode = .scaleToFill
           iv.layer.masksToBounds = true
           iv.layer.cornerRadius = 25
           return iv
       }()
       
       lazy var titleLabel: UILabel = {
           let label = UILabel()
           label.translatesAutoresizingMaskIntoConstraints = false
           label.text = "DRINK MENU"
           label.font = label.font.withSize(15)
           label.textColor = .black
           label.numberOfLines = 2
           return label
       }()
}
