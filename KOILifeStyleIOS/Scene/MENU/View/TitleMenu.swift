//
//  TitleMenu.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
class TitleMenu: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews()
    {
    
        addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        addSubview(categoryName)
        categoryName.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 2).isActive = true
        categoryName.bottomAnchor.constraint(equalTo: bottomAnchor.self, constant: -5).isActive = true
        categoryName.leftAnchor.constraint(equalTo: leftAnchor.self, constant: 5).isActive = true
        categoryName.rightAnchor.constraint(equalTo: rightAnchor.self, constant: -5).isActive = true
        
      
    }
    lazy var categoryName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    lazy var imageView: CustomImageView = {
              let iv = CustomImageView()
              iv.translatesAutoresizingMaskIntoConstraints = false
              iv.image = UIImage(named: "img_placeholder")
              iv.contentMode = .scaleAspectFill
              iv.layer.masksToBounds = true
              iv.layer.cornerRadius = 25
              return iv
          }()
}

