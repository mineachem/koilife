//
//  MenuBarView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MenuBarView: UIView {

    let menuCellId = "menuCellId"
        
    var menuVC: MenuVC?
        var menuCategory: [CategoryModel]? {
            didSet{
                collectionView.reloadData()
            }
        }
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setupViews()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
       func setupViews()
          {
              addSubview(titleLabel)
              titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
              titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
              addSubview(collectionView)
              collectionView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5).isActive = true
              collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
              collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
              collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
          }
          lazy var titleLabel: UILabel = {
              let label = UILabel()
              label.translatesAutoresizingMaskIntoConstraints = false
              label.text = "Select Category"
              label.font = UIFont.boldSystemFont(ofSize: 15)
              label.textColor = .black
              return label
          }()
          
          lazy var collectionView: UICollectionView = {
              let layout = UICollectionViewFlowLayout()
              layout.scrollDirection = .horizontal
              let vc = UICollectionView(frame: .zero, collectionViewLayout: layout)
              vc.translatesAutoresizingMaskIntoConstraints = false
              vc.backgroundColor = .white
              vc.showsHorizontalScrollIndicator = false
              vc.contentInset = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
              vc.delegate = self
              vc.dataSource = self
          
              vc.register(MenuCell.self, forCellWithReuseIdentifier: menuCellId)
          
              return vc
          }()
    }
    extension MenuBarView: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return menuCategory?.count ?? 0
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuCellId, for: indexPath) as! MenuCell
            let category = menuCategory?[indexPath.row]
            cell.titleLabel.text = category?.name
            if let imageUrl = category?.photo {
                let fullImageUrl = "\(KoiService.share.BASE_IMAGE)\(imageUrl)"
                cell.imageView.loadImageUsingUrlString(urlString: fullImageUrl)
            }
            return cell
            
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            menuVC?.showCategory(index: indexPath.row)
        }
      
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.frame.width / 4 , height: collectionView.frame.height)
        }

}
