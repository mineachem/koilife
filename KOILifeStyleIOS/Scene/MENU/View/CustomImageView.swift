//
//  CustomImageView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import SDWebImage

class CustomImageView: UIImageView {

    func loadImageUsingUrlString(urlString: String) {
        guard let url = URL(string: urlString) else {return}
        self.sd_setImage(with: url, placeholderImage: UIImage(named: "img_placeholder"))
    }

}
