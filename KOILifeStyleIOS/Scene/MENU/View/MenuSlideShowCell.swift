//
//  MenuSlideShowCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import FSPagerView

class MenuSlideShowCell: UICollectionViewCell {
    
    let cellId = "cellId"
    
      var slideShows: [Data]? {
             didSet{
              pagerView.reloadData()
                indecatorView.numberOfPages = slideShows?.count ?? 0
             }
         }
      
    
    
      override init(frame: CGRect) {
          super.init(frame: frame)
          setupViews()
       
      }
      
      required init?(coder: NSCoder) {
          fatalError("init(coder:) has not been implemented")
      }
      
      func setupViews()
      {
          contentView.addSubview(pagerView)
          pagerView.frame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 180)
        
          contentView.addSubview(indecatorView)
          indecatorView.topAnchor.constraint(equalTo: pagerView.bottomAnchor, constant: -5).isActive = true
          indecatorView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
      }
      
      lazy var pagerView: FSPagerView = {
           let page = FSPagerView()
           page.itemSize = CGSize.init(width: self.frame.width-5, height: 180)
           page.dataSource = self
           page.delegate = self
           page.automaticSlidingInterval = 5.0
           page.transformer = FSPagerViewTransformer(type: .linear)
           page.register(FSPagerViewCell.self, forCellWithReuseIdentifier: cellId)
           return page
       }()
    
    lazy var indecatorView: UIPageControl = {
          let pager = UIPageControl()
          pager.translatesAutoresizingMaskIntoConstraints = false
          pager.currentPage = 0
          pager.currentPageIndicatorTintColor = #colorLiteral(red: 0.8559798598, green: 0.6504896879, blue: 0.2624273896, alpha: 1)
          pager.pageIndicatorTintColor = .gray
          return pager
      }()
  }

  extension MenuSlideShowCell: FSPagerViewDelegate,FSPagerViewDataSource {
      
      func numberOfItems(in pagerView: FSPagerView) -> Int {
        return slideShows?.count ?? 0
      }
      func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
          let cell = pagerView.dequeueReusableCell(withReuseIdentifier: cellId, at: index)
          let slide = slideShows?[index]
       
        if let image = UIImage(data: slide!) {
            cell.imageView?.image = image
           }
          cell.isSelected = false
          cell.imageView?.contentMode = .scaleToFill
        indecatorView.currentPage = index
          return cell
      }
      func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
             indecatorView.currentPage = targetIndex
         }

    
}
