//
//  ItemCardCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class ItemCardCell: UICollectionViewCell {
    
  override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews()
    {
        addSubview(shadowView)
        shadowView.topAnchor.constraint(equalTo: topAnchor.self, constant: 0).isActive = true
        shadowView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        shadowView.widthAnchor.constraint(equalToConstant: self.frame.width * 0.90).isActive = true
        shadowView.heightAnchor.constraint(equalToConstant: self.frame.width).isActive = true
        
        shadowView.addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: shadowView.topAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: shadowView.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: shadowView.widthAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: shadowView.widthAnchor).isActive = true
        
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 5).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor.self, constant: 15).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor.self, constant: -15).isActive = true
        
        addSubview(mainStackView)
        mainStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15).isActive = true
        mainStackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5).isActive = true
  
        sizeIcon.widthAnchor.constraint(equalToConstant: 22).isActive = true
        sizeIcon.heightAnchor.constraint(equalToConstant: 22).isActive = true
    }
    
    lazy var shadowView: UIView = {
        
        let containerView = UIView()
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        containerView.layer.shadowRadius = 3.0
        containerView.layer.shadowOpacity = 0.2
        containerView.translatesAutoresizingMaskIntoConstraints = false
        return containerView
        
    }()
    
     lazy var imageView: CustomImageView = {
           let iv = CustomImageView()
           iv.translatesAutoresizingMaskIntoConstraints = false
           iv.image = UIImage(named: "img_placeholder")
           iv.contentMode = .scaleAspectFill
           iv.layer.masksToBounds = true
           iv.layer.cornerRadius = 25
           return iv
       }()
       
       lazy var titleLabel: UILabel = {
           let label = UILabel()
           label.translatesAutoresizingMaskIntoConstraints = false
           label.text = "DRINK MENU"
           label.font = UIFont(name: "Montserrat-Medium", size: 15)
           label.textColor = .black
           return label
       }()
    
    lazy var mainStackView:UIStackView = {
        var subStackView = UIStackView(arrangedSubviews: [sizeIcon,priceLabel])
           subStackView.axis = .horizontal
           subStackView.alignment = .fill
           subStackView.distribution = .equalSpacing
           subStackView.spacing = 5
           subStackView.translatesAutoresizingMaskIntoConstraints = false
           return subStackView
       }()
    
    lazy var priceLabel: UILabel = {
              let label = UILabel()
              label.translatesAutoresizingMaskIntoConstraints = false
              label.text = "1.2"
              label.font = UIFont(name: "Montserrat-Medium", size: 15)
              label.textColor = .black
              return label
          }()
    
    lazy var sizeIcon: UIButton = {
        let icon = UIButton()
        icon.layer.cornerRadius = 11
        icon.layer.masksToBounds = true
        icon.backgroundColor = #colorLiteral(red: 0.8559798598, green: 0.6504896879, blue: 0.2624273896, alpha: 1)
        let font = UIFont(name: "Montserrat-Medium", size: 16)
        icon.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: font!]), for: .normal)
        icon.isUserInteractionEnabled = false
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
}
