//
//  MenuCategoriesCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MenuCategoriesCell: UICollectionViewCell {
     let menuCellId = "menuCellId"
        
        var menuHeader: MenuHeaderCell?
        var selectedIndex: IndexPath?
        var slideShows: [SlideShowModel]? {
              didSet{
                if slideShows?.count == 0 {
                   collectionView.reloadData()
                }
                  
              }
          }
        var menuCategory: [CategoryModel]? {
            didSet{
                collectionView.reloadData()
            }
        }
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setupViews()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setupViews()
        {
            addSubview(titleLabel)
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
            addSubview(collectionView)
            collectionView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
            collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
            collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
            
            addSubview(lineView)
            lineView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
            lineView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
            lineView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.90).isActive = true
            lineView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        }
    
    lazy var lineView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .gray
        return v
    }()
    
        lazy var titleLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = "DRINK MENU"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.textColor = .black
            return label
        }()
        
        lazy var collectionView: UICollectionView = {
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            let vc = UICollectionView(frame: .zero, collectionViewLayout: layout)
            vc.translatesAutoresizingMaskIntoConstraints = false
            vc.backgroundColor = .white
            vc.showsHorizontalScrollIndicator = false
            vc.contentInset = UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20)
            vc.delegate = self
            vc.dataSource = self
            vc.register(MenuCell.self, forCellWithReuseIdentifier: menuCellId)
            return vc
        }()
    }

    extension MenuCategoriesCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return menuCategory?.count ?? 0
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuCellId, for: indexPath) as! MenuCell
            
            if indexPath == selectedIndex{
                cell.backgroundColor = #colorLiteral(red: 0.912833035, green: 0.9365020394, blue: 0.9863258004, alpha: 1)
            }else{
                cell.backgroundColor = .clear
            }
            let category = menuCategory?[indexPath.row]
            cell.titleLabel.text = category?.name
            if let imageUrl = category?.photo {
              let fullImageUrl = "\(KoiService.share.categoryUrl)\(imageUrl)"
                cell.imageView.loadImageUsingUrlString(urlString: fullImageUrl)
            }
            return cell
            
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            menuHeader?.onClickCategory(index: indexPath.row)
            self.selectedIndex = indexPath
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.frame.width / 3 , height: 200)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
    }
