//
//  PopupView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PopupView: UIView {

  lazy var bgPopup: UIView = {
    let subView = UIView()
    subView.backgroundColor = .red
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
    
    lazy var shadowView: UIView = {
        
        let containerView = UIView()
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        containerView.layer.shadowRadius = 3.0
        containerView.layer.shadowOpacity = 0.2
        containerView.translatesAutoresizingMaskIntoConstraints = false
        return containerView
        
    }()
    
  lazy var coverImageView: CustomImageView = {
    let imgView = CustomImageView()
    imgView.contentMode = .scaleAspectFit
    imgView.layer.masksToBounds = false
    imgView.image = UIImage(named: "img_placeholder")
    imgView.translatesAutoresizingMaskIntoConstraints = false
    return imgView
  }()
  
  
  lazy var titleLabel: UILabel = {
       let label = UILabel()
       label.numberOfLines = 0
       label.text = "Title"
       label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.textAlignment = .center
       label.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
  }()
  
  lazy var subtitleLabel: UILabel = {
       let label = UILabel()
       label.numberOfLines = 0
       label.text = "Subtitle"
       label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
       label.textAlignment = .center
       label.font = UIFont(name: "Montserrat-Regular", size: 14)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
  }()
  
  lazy var descriptionLabel: UILabel = {
       let label = UILabel()
       label.numberOfLines = 0
       label.text = "sjlfjlfjlfjlsflsfjlsdfl"
       label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
       label.textAlignment = .center
       label.font = UIFont(name: "Montserrat-Regular", size: 14)
       label.translatesAutoresizingMaskIntoConstraints = false
    label.isHidden = true
       return label
  }()
    lazy var closeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "ic_close"), for: .normal)
        
        return button
    }()
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupUI()
  }
  
  fileprivate func setupUI(){
    backgroundColor = .white
    addSubview(shadowView)
    NSLayoutConstraint.activate([
      shadowView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
      shadowView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
      shadowView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
      shadowView.heightAnchor.constraint(equalToConstant: 300)
    ])
    
    shadowView.addSubview(coverImageView)
    NSLayoutConstraint.activate([
        coverImageView.topAnchor.constraint(equalTo: shadowView.topAnchor),
        coverImageView.leadingAnchor.constraint(equalTo: shadowView.leadingAnchor),
        coverImageView.trailingAnchor.constraint(equalTo: shadowView.trailingAnchor),
      coverImageView.heightAnchor.constraint(equalToConstant: 300)
    ])
    
    addSubview(titleLabel)
    
    NSLayoutConstraint.activate([
    titleLabel.topAnchor.constraint(equalTo: coverImageView.bottomAnchor,constant: 10),
    titleLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
    titleLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
    ])
    
    addSubview(subtitleLabel)
    
    NSLayoutConstraint.activate([
       subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant: 10),
       subtitleLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
       subtitleLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
       ])
    
    addSubview(descriptionLabel)
    
    NSLayoutConstraint.activate([
      descriptionLabel.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor,constant: 10),
      descriptionLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
      descriptionLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
      descriptionLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor,constant: -10)
    ])
    
    shadowView.addSubview(closeButton)
    closeButton.topAnchor.constraint(equalTo: shadowView.topAnchor, constant: 0).isActive = true
    closeButton.rightAnchor.constraint(equalTo: shadowView.rightAnchor, constant: 0).isActive = true
    closeButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    closeButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
