//
//  MenuHeaderCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

import UIKit

class MenuHeaderCell: UICollectionViewCell {
     let slideCellId = "slideCellId"
        let menuCellId = "menuCellId"
        
        var homeMenuViewController: MenuVC?
        
        var slideShows: [Data]? {
            didSet{
                collectionView.reloadData()
            }
        }
        var menuCategory: [CategoryModel]? {
             didSet{
                 collectionView.reloadData()
             }
         }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setupViews()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        func onClickCategory(index: Int)
        {
            homeMenuViewController?.showCategory(index: index)
        }
        
        func setupViews()
        {
            addSubview(collectionView)
            collectionView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
            collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
            
        }
        lazy var collectionView: UICollectionView = {
            let layout = UICollectionViewFlowLayout()
            let vc = UICollectionView(frame: .zero, collectionViewLayout: layout)
            vc.translatesAutoresizingMaskIntoConstraints = false
            vc.delegate = self
            vc.dataSource = self
            vc.isScrollEnabled = false
            vc.backgroundColor = .white
            vc.register(MenuSlideShowCell.self, forCellWithReuseIdentifier: slideCellId)
            vc.register(MenuCategoriesCell.self, forCellWithReuseIdentifier: menuCellId)
        
            return vc
        }()
    }
    extension MenuHeaderCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 2
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let slideCell = collectionView.dequeueReusableCell(withReuseIdentifier: slideCellId, for: indexPath) as! MenuSlideShowCell
           
            slideCell.slideShows = slideShows
            
            
            if indexPath.row == 1 {
                let menuCell = collectionView.dequeueReusableCell(withReuseIdentifier: menuCellId, for: indexPath) as! MenuCategoriesCell
                menuCell.menuCategory = menuCategory
                menuCell.menuHeader = self
                return menuCell
            }
            return slideCell
        }
        
       
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            if indexPath.row == 1 {
                return CGSize(width: collectionView.frame.width, height: 230)
            }
            return CGSize(width: collectionView.frame.width, height: 200)
        }
    }
