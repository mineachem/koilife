//
//  MenuVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/9/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import EzPopup

class MenuVC: BaseCollectionviewController,LoadMoreDataProtocol {
  
 
    func loadMoreData(page: Int) {
        print("load page \(page)")

        loadCategoryById(page: page, caregoryId: categoryId)

          
    }
    
    

    
  private let cellId = "cellId"
  private let headerCellId = "headerCellId"
  
  var menuCategory = [CategoryModel]()
  var slideShows = [Data]()
    var slides = [String]()
  var menus = [MenuModel]()

    var categoryId = 0
     
       override func viewDidLoad() {
           super.viewDidLoad()
        self.deletegate = self
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        view.backgroundColor = .white
           setupViews()
           loadItem()
         if self.menus.count == 0 {
              self.collectionView.mj_footer?.isHidden = true
          }
       }
       
  
  
  

     func showCategory(index: Int)
     {
        self.page = 0
 
        self.menus.removeAll()
        categoryId = menuCategory[index].id
    
        loadCategoryById(page: 0, caregoryId: menuCategory[index].id)


        
        
     }
  
//    func loadAllData(page: Int)
//    {
//        let url = "\(KoiService.share.getProductAllCategory)\(page)"
//
//        self.collectionView.mj_footer?.isHidden = false
//
//       IGNetworkRequest.shareInstance.requestGET(url, success: { (response: MenuResponse) in
//            if response.response.code == 200 {
//                if response.length == self.allCategory.count {
//                 self.collectionView.mj_footer?.isHidden = true
//             }
//              for allMenu in response.results {
//                    self.allCategory.append(allMenu)
//                }
//                 DispatchQueue.main.async {
//                   self.collectionView.reloadData()
//                    if page > 0 {
//                        self.collectionView.mj_footer!.endRefreshing()
//                    }
//               }
//            }
//        }) { (err) in
//            print(err)
//        }
//    }
  
    func loadCategoryById(page: Int,caregoryId: Int)
    {

        showLoading()
    let url = "\(KoiService.share.getProductByCategory)catId=\(caregoryId)&size=10&page=\(page)"
    IGNetworkRequest.shareInstance.requestGET(url, success: { (response: MenuResponse) in
             if response.response.code == 200 {
                if response.length == self.menus.count {
                    self.collectionView.mj_footer?.isHidden = true
                }else {
                    self.collectionView.mj_footer?.isHidden = false
                }
              for allMenu in response.results {
                     self.menus.append(allMenu)
                 }
                  DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.collectionView.mj_footer!.endRefreshing()
                    self.hideLoading()
                }
             }
         }) { (err) in
             print(err)
         }
        
    }
  func loadItem()
     {
           
        let slides: [Data] = UserDefaults.standard.array(forKey: KoiService.share.SLIDESHOW) as? [Data] ?? [Data]()
    
        print("slides: \(slides)")
        if let myData: [Data] = slides as! [Data]  {
            self.slideShows = myData
             DispatchQueue.main.async {
                 self.collectionView.reloadData()
             }
        }
          //  as! [Data]
           
 IGNetworkRequest.shareInstance.requestGET(KoiService.share.categoriesUrl, success: { (response: CategoryResponse) in
        print("category",response.results)
                     if response.response.code == 200 {
                      
                         for menu in response.results {
                           
                             self.menuCategory.append(menu)
                           
                         }
                         self.myView.menuCategory = self.menuCategory
                       
                         DispatchQueue.main.async {
                             self.collectionView.reloadData()
                         }
                     }
                 }) { (err) in
                     print("Error \(err)")
                 }
                 
                 

     }
  
  override func scrollViewDidScroll(_ scrollView: UIScrollView) {
         if scrollView.contentOffset.y > 350.0 {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.myView.alpha  = 1
            }, completion: nil)
        
         }else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                       self.myView.alpha  = 0
                   }, completion: nil)
          
         }
     }
  
  func setupViews()
  {
      collectionView.showsVerticalScrollIndicator = false
      collectionView.backgroundColor = .white
      collectionView.register(ItemCardCell.self, forCellWithReuseIdentifier: cellId)
      collectionView.register(MenuHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId)
      view.addSubview(myView)
      myView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
      myView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
      myView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
      myView.heightAnchor.constraint(equalToConstant: 180).isActive = true
      
   myView.menuVC = self
  }
  
  lazy var myView: MenuBarView = {
      let v = MenuBarView()
      v.translatesAutoresizingMaskIntoConstraints = false
    v.backgroundColor = .white
      v.alpha  = 0
      return v
  }()
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return menus.count

    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ItemCardCell
  
            let menu = menus[indexPath.row]
           cell.titleLabel.text = menu.name
           if let imageUrl = menu.image {
             let fullUrl =  "\(KoiService.share.BASE_MENU_IMAGE)\(imageUrl)"
               cell.imageView.loadImageUsingUrlString(urlString: fullUrl)
           }
            
            if menu.name?.first != "S" && menu.name?.first != "M" {
                cell.sizeIcon.isHidden = true
            }else{
                cell.sizeIcon.isHidden = false
                if menu.name?.first == "S" {
                    cell.sizeIcon.setTitle("S", for: .normal)
                    cell.priceLabel.text = "\(String(describing: menu.price!))"
                }else if menu.name?.first == "M" {
                    cell.sizeIcon.setTitle("M", for: .normal)
                    cell.priceLabel.text = "\(String(describing: menu.priceMedium!))"
                }
            
        }
       
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        let popupMenuDetail = PopupMenuDetailVC()
   
          let menu = self.menus[indexPath.row]
            popupMenuDetail.menu = menu

          

        let popupVC = PopupViewController(contentController: popupMenuDetail, position: .center(CGPoint(x: 0, y: 30)), popupWidth: self.view.bounds.width-30, popupHeight: 400)
                          popupVC.backgroundAlpha = 0.3
                          popupVC.backgroundColor = .black
                          popupVC.canTapOutsideToDismiss = true
                          popupVC.cornerRadius = 10
                          popupVC.shadowEnabled = true
               
               self.present(popupVC, animated: true)
    
           
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath) as! MenuHeaderCell
        header.menuCategory = menuCategory
        header.slideShows = slideShows
        header.homeMenuViewController = self
        
        return header
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 2), height: collectionView.frame.width / 2 + 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 450)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 60, right: 0)
    }
  
}

