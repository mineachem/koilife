//
//  PopupMenuDetailVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PopupMenuDetailVC: UIViewController {

  var menu: MenuModel?
  lazy var popupView = PopupView()
  
  
  override func loadView() {
    super.loadView()
    self.view = popupView
  }
  
  
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        popupView.closeButton.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
    }
    @objc func handleClose()
    {
        self.dismiss(animated: true , completion: nil)
    }
    
  fileprivate func setupUI(){
    if let cover = menu?.image {
    //  let profileUrl = URL(string: KoiService.share.displayImgUrl)?.appendingPathComponent(cover)
        print("\(KoiService.share.displayImgUrl)\(cover)")
        self.popupView.coverImageView.loadImageUsingUrlString(urlString: "\(KoiService.share.displayImgUrl)\(cover)")
    }
    
    if let titlePopup = menu?.name {
      popupView.titleLabel.text = titlePopup
    }
    
    if let subTitlesPopup = menu?.price {
      popupView.subtitleLabel.text = subTitlesPopup.description
    }
    
    if let description = menu?.description {
      popupView.descriptionLabel.text = description.htmlToString
    }
  }
   

}
