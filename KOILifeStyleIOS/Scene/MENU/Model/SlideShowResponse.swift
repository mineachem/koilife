//
//  SlideShowResponse.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
struct SlideShowResponse: Codable {
    var response: ResponseModel
    var results: [SlideShowModel]
}
struct SlideShowModel: Codable {
    var id: Int
    var image: String
    var backgroundTypeApp: BackgroundTypeApp
    var status: Bool
    var version: Int
}
struct BackgroundTypeApp: Codable {
    var id: Int
    var name: String
    var status: Bool
    var version: Int
     
}
