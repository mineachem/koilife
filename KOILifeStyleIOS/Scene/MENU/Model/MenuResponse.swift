//
//  MenuResponse.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
struct MenuResponse: Codable {
    var response: ResponseModel
    var length: Int
    var results: [MenuModel]
}
struct MenuModel: Codable {
    var id: Int
    var pid: String?
    var name: String?
    var description: String?
    var price: Float?
    var priceMedium: Float?
    var image: String?
    var category: CategoryModel?

}

