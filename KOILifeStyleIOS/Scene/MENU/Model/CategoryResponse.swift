//
//  CategoryResponse.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
struct CategoryResponse: Codable {
    var response: ResponseModel
    var length: Int
    var results: [CategoryModel]
}
struct CategoryModel: Codable  {
      var id: Int
//      var catNo: String
      var name: String
//      var description: String?
      var photo: String?
//      var status: Bool
     
}
struct ResponseModel: Codable {
    var code: Int
    var message: String
}

