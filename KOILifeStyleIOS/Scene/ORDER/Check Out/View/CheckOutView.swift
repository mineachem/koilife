//
//  CheckOutView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/3/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class CheckOutView: UIView {

  lazy var totalAmoutCheckOutLabel: UILabel = {
    let label = UILabel()
    let attributedString = NSMutableAttributedString(string: "\nTotal amount Pick up")
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 4
    // *** Apply attribute to string ***
    attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
    label.attributedText = attributedString
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    label.numberOfLines = 0
    label.font = UIFont.systemFont(ofSize: 11, weight: .medium)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var priceCheckOutLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.text = "$103.99"
    label.numberOfLines = 1
    label.font = UIFont.systemFont(ofSize: 23, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var addressCheckOutLabel: UILabel = {
    let label = UILabel()
    let attributedString = NSMutableAttributedString(string: "KOI VATTANAC CAPITAL 66 Preah Monvivong Blvd,B1B! Sangkat Wat Phnom Khan Daun Penh, Phnom Penh,Cambodia")
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    label.numberOfLines = 0
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 4
    // *** Apply attribute to string ***
    attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
    label.attributedText = attributedString
    label.font = UIFont.systemFont(ofSize: 12, weight: .light)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var confirmCheckoutBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("CONFIRM", for: .normal)
    button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .medium)
    button.layer.cornerRadius = 15
    button.layer.masksToBounds = true
    button.contentMode = .scaleToFill
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var cancelCheckoutBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("CANCEL", for: .normal)
    button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .medium)
    button.layer.cornerRadius = 15
    button.layer.masksToBounds = true
    button.contentMode = .scaleToFill
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  fileprivate func setupUI(){
    backgroundColor = .white
    let contentDescriptionVStack = UIStackView(arrangedSubviews: [priceCheckOutLabel,addressCheckOutLabel])
    contentDescriptionVStack.axis = .vertical
    contentDescriptionVStack.alignment = .fill
    contentDescriptionVStack.distribution = .fill
    contentDescriptionVStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentCheckOutHStack = UIStackView(arrangedSubviews: [totalAmoutCheckOutLabel,contentDescriptionVStack])
    contentCheckOutHStack.axis = .horizontal
    contentCheckOutHStack.alignment = .top
    contentCheckOutHStack.distribution = .fill
    //contentCheckOutHStack.spacing = 10
    contentCheckOutHStack.translatesAutoresizingMaskIntoConstraints = false
    addSubview(contentCheckOutHStack)
    
    NSLayoutConstraint.activate([
      contentCheckOutHStack.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
      contentCheckOutHStack.centerXAnchor.constraint(equalTo: contentCheckOutHStack.centerXAnchor),
      contentCheckOutHStack.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 80),
      contentCheckOutHStack.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -30),
      totalAmoutCheckOutLabel.widthAnchor.constraint(equalToConstant: 90)
    ])
    
    let contentButtonCheckOutVStack = UIStackView(arrangedSubviews: [confirmCheckoutBtn,cancelCheckoutBtn])
    contentButtonCheckOutVStack.axis = .vertical
    contentButtonCheckOutVStack.alignment = .fill
    contentButtonCheckOutVStack.distribution = .fill
    contentButtonCheckOutVStack.spacing = 10
    contentButtonCheckOutVStack.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentButtonCheckOutVStack)
    
    NSLayoutConstraint.activate([
      contentButtonCheckOutVStack.topAnchor.constraint(equalTo: contentCheckOutHStack.bottomAnchor,constant: 30),
      contentButtonCheckOutVStack.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
      contentButtonCheckOutVStack.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20),
      confirmCheckoutBtn.heightAnchor.constraint(equalToConstant: 40),
      cancelCheckoutBtn.heightAnchor.constraint(equalTo: confirmCheckoutBtn.heightAnchor, multiplier: 1)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
