//
//  AddBasketView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/3/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AddBasketView: UIView {

  lazy var cardAddBasketView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .white
    subView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
    subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    subView.layer.shadowOpacity = 1.0
    subView.layer.shadowRadius = 0.0
    subView.layer.masksToBounds = false
    subView.layer.cornerRadius = 10.0
    subView.translatesAutoresizingMaskIntoConstraints  = false
    return subView
  }()
  
  lazy var titleAddBasketLabel: UILabel = {
    let label = UILabel()
    label.text = "Add To Basket"
    label.numberOfLines = 1
    label.font = UIFont.systemFont(ofSize: 11, weight: .medium)
    label.textAlignment = .center
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var titleProductKoiLabel: UILabel = {
    let label = UILabel()
    label.text = "Black Tea Machiato"
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var cardbgBtn: UIView = {
    let subView = UIView()
    subView.backgroundColor = .white
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var addQtyProductBasketBtn: UIButton = {
    let button = UIButton()
    button.setTitle("+", for: .normal)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var valueQtyProductBasketLabel: UILabel = {
    let label = UILabel()
    label.text = "1"
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var removeQtyProductBasketBtn: UIButton = {
    let button = UIButton()
    button.setTitle("-", for: .normal)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupUI()
  }
  
  fileprivate func setupUI(){
    backgroundColor = .white
    addSubview(cardAddBasketView)
    
    NSLayoutConstraint.activate([
      cardAddBasketView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
      cardAddBasketView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      cardAddBasketView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
      cardAddBasketView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20)
    ])
    
    cardAddBasketView.addSubview(titleAddBasketLabel)
    
    NSLayoutConstraint.activate([
      titleAddBasketLabel.centerXAnchor.constraint(equalTo: cardAddBasketView.centerXAnchor)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
