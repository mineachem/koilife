//
//  OutletLocation.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/3/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import GoogleMaps

class OutletLocationView: UIView {

    var locationNearByIdentifier = "locationNearByCell"
  
   lazy var mapOutletLocationView:GMSMapView = {
     let map = GMSMapView()
     map.translatesAutoresizingMaskIntoConstraints = false
     return map
   }()
  
  lazy var bgOutletLocationView: UIView = {
    let view = UIView()
    view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var cardbgOutletLocationView: UIView = {
     let subView = UIView()
     subView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    subView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
    subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    subView.layer.shadowOpacity = 1.0
    subView.layer.shadowRadius = 0.0
    subView.layer.masksToBounds = false
    subView.layer.cornerRadius = 10.0
    subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
  
  lazy var outletNearLocationBtn: UIButton = {
    let button = UIButton(type: .system)
    //button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    button.setImage(#imageLiteral(resourceName: "circle_icon"), for: .normal)
    button.contentMode = .scaleToFill
    button.layer.masksToBounds = true
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var outletNearTextField: UITextField = {
    let textField = UITextField()
    textField.text = "Outlet Near"
    textField.textAlignment = .left
    textField.font = UIFont.systemFont(ofSize: 12, weight: .medium)
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  lazy var lineOutletView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var outletOtherLocationBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "circle_icon"), for: .normal)
    button.contentMode = .scaleToFill
    button.layer.masksToBounds = true
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var outletOtherTextField: UITextField = {
     let textField = UITextField()
     textField.text = "Other Outlet"
     textField.textAlignment = .left
     textField.font = UIFont.systemFont(ofSize: 12, weight: .medium)
     textField.translatesAutoresizingMaskIntoConstraints = false
     return textField
   }()
  
  lazy var locationNearCollectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    collectionView.backgroundColor = .none
    collectionView.register(LocationNearByCell.self, forCellWithReuseIdentifier: locationNearByIdentifier)
    collectionView.alwaysBounceHorizontal = true
    collectionView.showsHorizontalScrollIndicator = false
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    return collectionView
  }()
  
  lazy var outletListLocationBtn: UIButton = {
         
         let button = UIButton(type: .system)
        button.setTitle("OUTLET LIST", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 10.0
        button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
  
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupUI()
  }
  
  fileprivate func setupUI(){
    backgroundColor = .white
    addSubview(mapOutletLocationView)

    NSLayoutConstraint.activate([
    mapOutletLocationView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 10),
    mapOutletLocationView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
    mapOutletLocationView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
    mapOutletLocationView.bottomAnchor.constraint(equalTo:safeAreaLayoutGuide.bottomAnchor,constant: -5)
    ])
    
    addSubview(bgOutletLocationView)
    
    NSLayoutConstraint.activate([
      bgOutletLocationView.heightAnchor.constraint(equalToConstant: 250),
      bgOutletLocationView.leadingAnchor.constraint(equalTo: leadingAnchor),
      bgOutletLocationView.trailingAnchor.constraint(equalTo: trailingAnchor),
      bgOutletLocationView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
    
    bgOutletLocationView.addSubview(cardbgOutletLocationView)
    
    NSLayoutConstraint.activate([
      cardbgOutletLocationView.topAnchor.constraint(equalTo: bgOutletLocationView.topAnchor,constant: 10),
      cardbgOutletLocationView.leadingAnchor.constraint(equalTo: bgOutletLocationView.leadingAnchor,constant: 10),
      cardbgOutletLocationView.trailingAnchor.constraint(equalTo: bgOutletLocationView.trailingAnchor,constant: -10),
      cardbgOutletLocationView.heightAnchor.constraint(equalToConstant: 130)
    ])
    
    let contentOutletNearMapHStack = UIStackView(arrangedSubviews: [outletNearLocationBtn,outletOtherLocationBtn])
    contentOutletNearMapHStack.axis = .vertical
    contentOutletNearMapHStack.alignment = .fill
    contentOutletNearMapHStack.distribution = .fill
    contentOutletNearMapHStack.spacing = 25
    contentOutletNearMapHStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentOutletOtherMapHStack = UIStackView(arrangedSubviews: [outletNearTextField,lineOutletView,outletOtherTextField])
       contentOutletOtherMapHStack.axis = .vertical
       contentOutletOtherMapHStack.alignment = .fill
       contentOutletOtherMapHStack.distribution = .fill
       contentOutletOtherMapHStack.spacing = 10
       contentOutletOtherMapHStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentOutletMapVStack = UIStackView(arrangedSubviews: [contentOutletNearMapHStack,contentOutletOtherMapHStack])
    contentOutletMapVStack.axis = .horizontal
    contentOutletMapVStack.alignment = .center
    contentOutletMapVStack.distribution = .fill
    contentOutletMapVStack.spacing = 10
    contentOutletMapVStack.translatesAutoresizingMaskIntoConstraints = false
    
    cardbgOutletLocationView.addSubview(contentOutletMapVStack)
    
    NSLayoutConstraint.activate([
      contentOutletMapVStack.topAnchor.constraint(equalTo: cardbgOutletLocationView.topAnchor,constant: 10),
      contentOutletMapVStack.leadingAnchor.constraint(equalTo: cardbgOutletLocationView.leadingAnchor,constant: 10),
      contentOutletMapVStack.trailingAnchor.constraint(equalTo: cardbgOutletLocationView.trailingAnchor,constant: -50),
      outletOtherLocationBtn.widthAnchor.constraint(equalToConstant: 10),
      outletOtherLocationBtn.heightAnchor.constraint(equalToConstant: 10),
      lineOutletView.heightAnchor.constraint(equalToConstant: 1),
      outletNearLocationBtn.widthAnchor.constraint(equalToConstant: 10),
      outletNearLocationBtn.heightAnchor.constraint(equalToConstant: 10)
    ])
    
    cardbgOutletLocationView.addSubview(locationNearCollectionView)
    
    NSLayoutConstraint.activate([
      locationNearCollectionView.topAnchor.constraint(equalTo: contentOutletMapVStack.bottomAnchor,constant: 10),
      locationNearCollectionView.leadingAnchor.constraint(equalTo: cardbgOutletLocationView.leadingAnchor,constant: 10),
      locationNearCollectionView.trailingAnchor.constraint(equalTo: cardbgOutletLocationView.trailingAnchor,constant: -10),
      locationNearCollectionView.bottomAnchor.constraint(equalTo: cardbgOutletLocationView.bottomAnchor,constant: -30)
    ])
    
    bgOutletLocationView.addSubview(outletListLocationBtn)
    
    NSLayoutConstraint.activate([
      outletListLocationBtn.topAnchor.constraint(equalTo: cardbgOutletLocationView.bottomAnchor,constant: 10),
      outletListLocationBtn.leadingAnchor.constraint(equalTo: bgOutletLocationView.leadingAnchor,constant: 10),
      outletListLocationBtn.trailingAnchor.constraint(equalTo: bgOutletLocationView.trailingAnchor,constant: -10),
      outletListLocationBtn.heightAnchor.constraint(equalToConstant: 40)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
