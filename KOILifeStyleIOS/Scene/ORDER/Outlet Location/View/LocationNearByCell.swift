//
//  LocationNearByCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/3/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class LocationNearByCell: UICollectionViewCell {
    
  lazy var cardBgOutletView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    subView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
          subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
          subView.layer.shadowOpacity = 1.0
          subView.layer.shadowRadius = 0.0
          subView.layer.masksToBounds = false
          subView.layer.cornerRadius = 10.0
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  lazy var titleLocationOutletLabel: UILabel = {
    let label = UILabel()
    label.text = "KOI IFL"
    label.numberOfLines = 0
    label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 11, weight: .medium)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  fileprivate func setupUI(){
    addSubview(cardBgOutletView)
    cardBgOutletView.addSubview(titleLocationOutletLabel)
    
    NSLayoutConstraint.activate([
      cardBgOutletView.topAnchor.constraint(equalTo: topAnchor,constant: 5),
      cardBgOutletView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 5),
      cardBgOutletView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -5),
      cardBgOutletView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -5),
      titleLocationOutletLabel.topAnchor.constraint(equalTo: cardBgOutletView.topAnchor,constant: 5),
      titleLocationOutletLabel.leadingAnchor.constraint(equalTo: cardBgOutletView.leadingAnchor,constant: 5),
      titleLocationOutletLabel.trailingAnchor.constraint(equalTo: cardBgOutletView.trailingAnchor,constant: -5),
      titleLocationOutletLabel.bottomAnchor.constraint(equalTo: cardBgOutletView.bottomAnchor,constant: -5)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
