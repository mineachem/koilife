//
//  OutletLocationVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/3/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import GoogleMaps
class OutletLocationVC: UIViewController{

   var currentLocationMarker: GMSMarker?
  var outletLocationView = OutletLocationView()
 
  override func loadView() {
    super.loadView()
    self.view = outletLocationView
  }
  
  override func viewDidLoad() {
        super.viewDidLoad()

    setupGoogleMapView()
    setupCollectionView()
    }
    
  fileprivate func setupGoogleMapView() {
    let camera = GMSCameraPosition.camera(withLatitude: 11.560803, longitude: 104.891336, zoom: 18)
    outletLocationView.mapOutletLocationView.animate(to: camera)
    //mapView.delegate = self
    
    // Creates a marker in the center of the map.
    currentLocationMarker = GMSMarker()
    currentLocationMarker!.position = CLLocationCoordinate2D(latitude:11.560803, longitude: 104.891336)
    currentLocationMarker!.title = "Minea"
    currentLocationMarker!.snippet = "first Location"
    currentLocationMarker!.map = outletLocationView.mapOutletLocationView
    outletLocationView.mapOutletLocationView.settings.compassButton = true
    outletLocationView.mapOutletLocationView.isMyLocationEnabled = true
  }
    
  fileprivate func setupCollectionView(){
    outletLocationView.locationNearCollectionView.delegate = self
    outletLocationView.locationNearCollectionView.dataSource = self
  }

}

extension OutletLocationVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 10
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let locationNearByCell = collectionView.dequeueReusableCell(withReuseIdentifier: outletLocationView.locationNearByIdentifier, for: indexPath) as! LocationNearByCell
    
    return locationNearByCell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 5
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }
}
