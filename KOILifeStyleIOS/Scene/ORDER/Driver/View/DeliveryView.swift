//
//  DeliveryView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/3/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class DeliveryView: UIView {

  lazy var pickupDeliveryImgView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "driver_icon")
    imageView.contentMode = .scaleAspectFit
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  lazy var pickupDeliveryLabel: UILabel = {
    let label = UILabel()
    label.text = "PICK UP"
    label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var linePickupDeliveryView: UIView = {
    let view = UIView()
    view.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var deliveryImgView: UIImageView = {
     let imageView = UIImageView()
     imageView.image = #imageLiteral(resourceName: "driver_icon")
     imageView.contentMode = .scaleAspectFit
     imageView.layer.masksToBounds = true
     imageView.translatesAutoresizingMaskIntoConstraints = false
     return imageView
   }()
   
   lazy var deliveryLabel: UILabel = {
     let label = UILabel()
     label.text = "DELIVERY"
     label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
     label.textAlignment = .center
     label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .white
    setupUI()
  }
  
  fileprivate func  setupUI(){
    let contentDeliveryStackView = UIStackView(arrangedSubviews: [pickupDeliveryImgView,pickupDeliveryLabel,linePickupDeliveryView,deliveryImgView,deliveryLabel])
    contentDeliveryStackView.axis = .vertical
    contentDeliveryStackView.alignment = .fill
    contentDeliveryStackView.distribution = .fill
    contentDeliveryStackView.spacing = 50
    contentDeliveryStackView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(contentDeliveryStackView)
    
    NSLayoutConstraint.activate([
      contentDeliveryStackView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
      contentDeliveryStackView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      contentDeliveryStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 40),
      contentDeliveryStackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -40),
      linePickupDeliveryView.heightAnchor.constraint(equalToConstant: 1),
    ])
    
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
