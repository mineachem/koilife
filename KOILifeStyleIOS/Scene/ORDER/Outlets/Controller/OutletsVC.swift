//
//  OutletsVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/3/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class OutletsVC: UIViewController {

  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    let label = UILabel()
    label.text = "OUTLETS"
   label.font = UIFont.boldSystemFont(ofSize: 14)
   navItem.titleView = label
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
    var koiNearByData = [ResultMapNearBy]()
  //var items = [OutletModel]()
  var outletTableView:UITableView!
  private let outletIdentifier = "outletCell"
    override func viewDidLoad() {
        super.viewDidLoad()
      view.backgroundColor = #colorLiteral(red: 0.9608082175, green: 0.9570518136, blue: 0.9485346675, alpha: 1)
      setupNavigation()
      setupTableView()
        
     
    }
      
  
  fileprivate func setupNavigation(){
    
  view.addSubview(titleNavigationBar)
  NSLayoutConstraint.activate([
    titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
    titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
    titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
  ])
  }
  
  @objc private func handleBackTapped(){
     self.dismiss(animated: true)
   }
  
  fileprivate func setupTableView(){
    outletTableView = UITableView()
    outletTableView.delegate = self
    outletTableView.dataSource = self
    outletTableView.separatorStyle = .none
    outletTableView.alwaysBounceHorizontal = false
    outletTableView.showsVerticalScrollIndicator = false
    outletTableView.showsHorizontalScrollIndicator = false
    outletTableView.backgroundColor = #colorLiteral(red: 0.9608082175, green: 0.9570518136, blue: 0.9485346675, alpha: 1)
    outletTableView.register(OutletsCell.self, forCellReuseIdentifier: outletIdentifier)
    outletTableView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(outletTableView)
    
    NSLayoutConstraint.activate([
      outletTableView.topAnchor.constraint(equalTo: titleNavigationBar.bottomAnchor,constant: 5),
      outletTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      outletTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      outletTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
    ])
  }
}

extension OutletsVC:UITableViewDelegate,UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return koiNearByData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let outletCell = tableView.dequeueReusableCell(withIdentifier: outletIdentifier, for: indexPath) as! OutletsCell
    
    outletCell.dataNearBy = koiNearByData[indexPath.row]
    
    return outletCell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
   let dataKoi = koiNearByData[indexPath.row]
    var phone = ""

    let detailOutletVC = OutletDetailVC()
    detailOutletVC.lng = dataKoi.lng
    detailOutletVC.lat = dataKoi.lat
    
    if dataKoi.phone1 != nil {
        phone = dataKoi.phone1 ?? ""
    }else {
        phone = dataKoi.phone2 ?? ""
    }
    detailOutletVC.call = phone
    detailOutletVC.outlets = dataKoi

    detailOutletVC.modalPresentationStyle = .overCurrentContext
    detailOutletVC.modalTransitionStyle = .crossDissolve
    self.present(detailOutletVC, animated: true)
  }
}
