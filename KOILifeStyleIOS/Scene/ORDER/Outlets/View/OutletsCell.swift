//
//  OutletsCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/3/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import CoreLocation
class OutletsCell: UITableViewCell {

  var dataNearBy: ResultMapNearBy?{
    didSet{
      if let name = dataNearBy?.name {
        titleOutletLabel.text = name
      }
        
      //My location
        
        if dataNearBy?.lat != nil {
//            let myLocation = CLLocation(latitude: CLLocationDegrees(exactly: Double( dataNearBy!.lat))!, longitude: Double( dataNearBy!.lat)!))
            let late: Double = (dataNearBy?.lat?.toDouble()) ?? 0
            let lng : Double = (dataNearBy?.lng?.toDouble()) ?? 0
  
  
            let myLocation = CLLocation(latitude: KoiService.mLate, longitude: KoiService.mLng)
              let myBuddysLocation = CLLocation(latitude: late, longitude: lng)
              let distance = myLocation.distance(from: myBuddysLocation) / 1000
               mileOutletLabel.text = String(format: "%.01f ( km )", distance)
         
        }
       
      
      if let address = dataNearBy?.address {
        subtitleOutletLabel.text = address
      }

    }
  }
  
  lazy var bgOutletView: UIView = {
    let view = UIView()
    view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
           view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
           view.layer.shadowOpacity = 1.0
           view.layer.shadowRadius = 0.0
           view.layer.cornerRadius = 10.0
           view.layer.masksToBounds = false
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var titleOutletLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.textAlignment = .left
    label.numberOfLines = 0
    label.font = UIFont(name: "Montserrat-SemiBold", size: 20)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var mileOutletLabel: UILabel = {
     let label = UILabel()
     label.textAlignment = .right
    label.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    label.numberOfLines = 1
     label.font = UIFont(name: "Montserrat-SemiBold", size: 13)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var subtitleOutletLabel: UILabel = {
    let label = UILabel()
    label.textColor = #colorLiteral(red: 0.3372502327, green: 0.3374921083, blue: 0.3249518275, alpha: 1)
    label.textAlignment = .right
    label.numberOfLines = 0
    label.font = UIFont(name: "Montserrat-Regular", size: 13)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    setupUI()
  }
  
  fileprivate func setupUI(){
    backgroundColor = .none
    addSubview(bgOutletView)
    NSLayoutConstraint.activate([
      bgOutletView.topAnchor.constraint(equalTo: topAnchor,constant: 5),
      bgOutletView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
      bgOutletView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
      bgOutletView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
    ])
    
    let contentSubTitleOutletVStack = UIStackView(arrangedSubviews: [mileOutletLabel,subtitleOutletLabel])
    contentSubTitleOutletVStack.axis = .vertical
    contentSubTitleOutletVStack.alignment = .trailing
    contentSubTitleOutletVStack.distribution = .fill
    contentSubTitleOutletVStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentOutletHStack = UIStackView(arrangedSubviews: [titleOutletLabel,contentSubTitleOutletVStack])
    contentOutletHStack.axis = .horizontal
    contentOutletHStack.alignment = .top
    contentOutletHStack.distribution = .equalSpacing
    contentOutletHStack.translatesAutoresizingMaskIntoConstraints = false
    bgOutletView.addSubview(contentOutletHStack)
    
    NSLayoutConstraint.activate([
      contentOutletHStack.topAnchor.constraint(equalTo: bgOutletView.topAnchor,constant: 5),
      contentOutletHStack.leadingAnchor.constraint(equalTo: bgOutletView.leadingAnchor,constant: 10),
      contentOutletHStack.trailingAnchor.constraint(equalTo: bgOutletView.trailingAnchor,constant: -10),
      contentOutletHStack.bottomAnchor.constraint(equalTo: bgOutletView.bottomAnchor,constant: -5)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
