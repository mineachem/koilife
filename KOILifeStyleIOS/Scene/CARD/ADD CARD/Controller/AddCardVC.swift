//
//  AddCardVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/7/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import Alamofire

class AddCardVC: UIViewController {
  
  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    let titleNav = UILabel()
    titleNav.text = "ADD CARD"
    titleNav.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    navItem.titleView = titleNav
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "left-arrow").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
  
  lazy var addCardView = AddCardView()
  var updateUIProtocol: UpdateUIProtocol?
  let accNumber = UserDefaults.standard.value(forKey: "accNumber") as? String
  
  override func loadView() {
    super.loadView()
    self.view = addCardView
  }
    override func viewDidLoad() {
        super.viewDidLoad()

      setupNavigationBar()
        addCardView.doneAddCardBtn.addTarget(self, action: #selector(addCard), for: .touchUpInside)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleHideKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func handleHideKeyboard(){
             view.endEditing(true)
      }
    
  fileprivate func setupNavigationBar(){
      view.addSubview(titleNavigationBar)
      NSLayoutConstraint.activate([
        titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
        titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
      ])
    }
  
   @objc private func handleBackTapped(){
     self.dismiss(animated: true)
   }

   @objc func addCard()
    {
      
        let cardNumber = addCardView.koicardNumTextField.text!
        let serialNumber = addCardView.digitCodeTextField.text!
                    
      
        let param:[String: Any]  = ["cardNumber": cardNumber,"serialNumber":serialNumber,"accNumber":accNumber!]
        IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.customerAddCard, params: param, success: { (response: CustomerAddCardResponse) in
          
            if response.response.code == 200 {
              
              self.updateUIProtocol?.onUpdateUI(customCardsModel: response.results)
                self.dismiss(animated: true, completion: nil)
            }

        }) { (err) in
            print(err)
        }
        
    }
 

}
