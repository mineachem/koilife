//
//  CustomCardResponse.swift
//  TestProject
//
//  Created by Apple on 12/14/19.
//  Copyright © 2019 IG. All rights reserved.
//

import Foundation
struct CustomerCardParam: Codable {
    var customerWalletID: Int
}
struct CustomCardResponse: Codable {
    var response: ResponseModel
    var results: [CustomCardsModel]
}
struct CustomCardsModel: Codable {
    var id: Int
    var cardLabel: String
    var cardNumber: String
    var uuid: String?
    var expiredDate: Int?
    var cardStatus: Bool?
    var serialNumber: String?
    var customerWallet: CustomerWalletModel?
    var memberType: MemberTypes?
    var image:String?
}

struct CustomerAddCardResponse: Codable {
     var response: ResponseModel
       var results: CustomCardsModel
}

struct CustomerUpdateCardResponse: Codable {
     var response: ResponseModel
    //   var results: CustomCardsModel
}

struct CustomerWalletModel: Codable {
    var id: Int
    var accNumber: String?
    var accName: String?
    var membershipDate: Int?
    var balance: Float?
    var score: Float?
    var rewardScore: Float?
    var qty: Int?
    var customer: CustomerModel?
    var endYear: Int?
    var status: Bool?
}
struct CustomerModel: Codable {
     var id: Int
    var first_name: String?
    var last_name: String?
    var userType: String?
    var phoneNo: String?
    var email: String?
    var address: String?
    var dob: Int?
    var gender: String?
    var imageFile: String?
    var confirmCode: String?
     var photo: String?
    var status: Bool?
    var passcodeLockOn: Bool?
    var confirmCodeExpired: Bool?
}

struct MemberTypes: Codable {
    var id: Int
    var type: String?
    var minQty: Int?
    var maxQty: Int?
    var status: Bool?
}
