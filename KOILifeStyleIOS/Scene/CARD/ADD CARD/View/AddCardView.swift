//
//  AddCardView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/7/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AddCardView: UIView {
  
  lazy var bgAddCardView: UIView = {
     let subView = UIView()
     subView.backgroundColor = .none
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
  
  lazy var cardmodelAddCardImgView: UIImageView = {
     let imageView = UIImageView()
     imageView.image = #imageLiteral(resourceName: "img_bg_add_card")
     imageView.contentMode = .scaleToFill
     imageView.layer.masksToBounds = true
     imageView.translatesAutoresizingMaskIntoConstraints = false
     return imageView
   }()
   
   
   lazy var doneAddCardBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Done", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12,weight: .medium)
        button.layer.cornerRadius = 15
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 10.0
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
      }()
  
  
  lazy var koicardNumTextField: UITextField = {
    let textField = UITextField()
    let placeholder = NSAttributedString(string: "KOI CARD NUMBER", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
      textField.attributedPlaceholder = placeholder
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.keyboardType = .default
    textField.autocapitalizationType = .none
    textField.textAlignment = .center
    textField.textColor = .white
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  lazy var digitCodeTextField: UITextField = {
    let textField = UITextField()
    let placeholder = NSAttributedString(string: "Serial number", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
    textField.attributedPlaceholder = placeholder
    textField.autocapitalizationType = .none
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.keyboardType = .default
    textField.textAlignment = .center
    textField.textColor = .white
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupUI()
    
  }
  
  fileprivate func setupUI(){
    backgroundColor = .white
    addSubview(bgAddCardView)
    bgAddCardView.addSubview(cardmodelAddCardImgView)
    
    NSLayoutConstraint.activate([
      bgAddCardView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      bgAddCardView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
      bgAddCardView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
      bgAddCardView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
      bgAddCardView.heightAnchor.constraint(equalToConstant: 200),
      cardmodelAddCardImgView.topAnchor.constraint(equalTo: bgAddCardView.topAnchor),
      cardmodelAddCardImgView.leadingAnchor.constraint(equalTo: bgAddCardView.leadingAnchor),
      cardmodelAddCardImgView.trailingAnchor.constraint(equalTo: bgAddCardView.trailingAnchor),
      cardmodelAddCardImgView.bottomAnchor.constraint(equalTo: bgAddCardView.bottomAnchor)
    ])
    
    let contentTextFieldAddCardVStack = UIStackView(arrangedSubviews: [koicardNumTextField,digitCodeTextField])
    contentTextFieldAddCardVStack.axis = .vertical
    contentTextFieldAddCardVStack.alignment = .fill
    contentTextFieldAddCardVStack.distribution = .fillEqually
    contentTextFieldAddCardVStack.spacing = 10
    contentTextFieldAddCardVStack.translatesAutoresizingMaskIntoConstraints = false
    
    bgAddCardView.addSubview(contentTextFieldAddCardVStack)
    
    NSLayoutConstraint.activate([
      contentTextFieldAddCardVStack.centerXAnchor.constraint(equalTo: bgAddCardView.centerXAnchor),
      contentTextFieldAddCardVStack.centerYAnchor.constraint(equalTo: bgAddCardView.centerYAnchor,constant: -5),
      contentTextFieldAddCardVStack.widthAnchor.constraint(equalToConstant: 150)
    ])
    
    addSubview(doneAddCardBtn)
    
    NSLayoutConstraint.activate([
      doneAddCardBtn.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
      doneAddCardBtn.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20),
      doneAddCardBtn.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor,constant: -10)
      
    ])
    doneAddCardBtn.heightAnchor.constraint(equalToConstant: 35).isActive = true
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
