//
//  CommingCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
class CommingCell: UICollectionViewCell {
    
    
    override func layoutSubviews() {
          super.layoutSubviews()
          
      
          let shadowPath = UIBezierPath(rect: contentView.bounds)
          
          contentView.layer.masksToBounds = false
          contentView.layer.shadowColor = UIColor.black.cgColor
          contentView.layer.shadowOffset = CGSize(width: 0, height: 0.5)
          contentView.layer.shadowOpacity = 0.5
          contentView.layer.shadowPath = shadowPath.cgPath
          
       
          
          
      }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(thumnail)
        thumnail.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        thumnail.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        thumnail.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        thumnail.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var thumnail: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.image = UIImage(named: "slide")
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 10
        iv.contentMode = .scaleToFill
        return iv
    }()
}

