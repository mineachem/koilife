//
//  CardStatus.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
// MARK: - CardStatus
struct CardStatus: Codable {
    let response: ResponseStatus?
    let results: ResultsStatus?
}

// MARK: - Response
struct ResponseStatus: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Results
struct ResultsStatus: Codable {
    let id: Int?
    let cardLabel, cardNumber, uuid: String?
    let expiredDate: String?
    let serialNumber: String?
    let cardType: String?
    let cardStatus: Bool?
    let customerWallet: CustomerWalletCard?
    let memberType: MemberType?
    let status: Bool?
    let version: Int?
    let createById: Int?
    let updatedById: Int?
}

//// MARK: - CustomerWallet
struct CustomerWalletCard: Codable {
    let id: Int?
    let accNumber, accName: String?
    let membershipDate: Int?
    let balance: Double?
    let score, rewardScore, qty: Int?
    let customer: Customer?
    let memberType: MemberType?
    let endYear: Int?
    let status: Bool?
    let version: Int?
    let createById, updatedById: String?
}

// MARK: - Customer
struct Customer: Codable {
    let id: Int?
    let firstName, lastName, userType, phoneNo: String?
    let facebookId, email, address: String?
    let dob: Int?
    let gender: String?
    let imageFile: String?
    let confirmCode, photo: String?
    let passcodeLock: String?
    let status: Bool?
    let version: Int?
    let createById, updatedById: String?
    let confirmCodeExpired, blocked, passcodeLockOn: Bool?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case userType, phoneNo, facebookId, email, address, dob, gender, imageFile, confirmCode, photo, passcodeLock, status, version, createById, updatedById, confirmCodeExpired, blocked, passcodeLockOn
    }
}

//// MARK: - MemberType
//struct MemberType: Codable {
//    let id: Int?
//    let type: String?
//    let minQty, maxQty: Int?
//    let status: Bool?
//    let version: Int?
//    let createById, updatedById: JSONNull?
//}
