//
//  CardcollectionVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/7/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import Alamofire


class DeactivateCardVC: UIViewController {

  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    let titleNav = UILabel()
    titleNav.text = "MEMEMBERSHIP CARD"
    titleNav.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    navItem.titleView = titleNav
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "left-arrow").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
  
  lazy var cardCollectionView = DeactivateCardView()
  var customerCardId:Int?
  var cardNumber:String?
  override func loadView() {
    super.loadView()
    
    self.view = cardCollectionView
    
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
      setupNavigationBar()
      setupEvent()
    }
  
  
  fileprivate func setupNavigationBar(){
       view.addSubview(titleNavigationBar)
       NSLayoutConstraint.activate([
         titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
         titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
         titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
       ])
     }
   
    @objc private func handleBackTapped(){
      self.dismiss(animated: true)
    }
  
  
  fileprivate func setupEvent(){
   // let cardCollectionTapped = UITapGestureRecognizer(target: self, action: #selector(handleCardCollectionTapped))
    //cardCollectionView.cardmodelImgView.addGestureRecognizer(cardCollectionTapped)
   // cardCollectionView.cardmodelImgView.isUserInteractionEnabled = true
    
    cardCollectionView.deactivateBtn.addTarget(self, action: #selector(handleAddCardTapped(_:)), for: .touchUpInside)
  }
    
//  @objc private func handleCardCollectionTapped(){
//    let membershipCardVC = MembershipCardVC()
//    membershipCardVC.modalPresentationStyle = .overCurrentContext
//    membershipCardVC.modalTransitionStyle = .crossDissolve
//    self.present(membershipCardVC, animated: true)
//  }

  @objc private func handleAddCardTapped(_ sender:UIButton){
    
    if sender.currentTitle == "DEACTIVATE"{
      sender.setTitle("ACTIVATE", for: .normal)
      
      getStatusCard(sender:0)
      
    }else {
      getStatusCard(sender:1)
      
      sender.setTitle("DEACTIVATE", for: .normal)
    }
  }
  
  func getStatusCard(sender:Int){
    let param:[String:Any] = [
        "id": customerCardId!,
        "cardStatus": sender
    ]
    
    let alertCardStatus = UIAlertController(title: "Deactivate Card", message: "Are you sure want to deactivate this card [\(cardNumber!)]", preferredStyle: .alert)
    let noAction = UIAlertAction(title: "NO", style: .cancel, handler: nil)
    let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
      IGNetworkRequest.shareInstance.requestPUTURL(KoiService.share.deactivateUrl, params: param, success: { (cardStatus:CardStatus) in

        if cardStatus.response?.code == 200 {
          let alertCardStatus = UIAlertController(title: "Card Membership", message: "Your card \(cardStatus.response!.message)", preferredStyle: .alert)
          let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
          
          alertCardStatus.addAction(okAction)
          self.present(alertCardStatus, animated: true)
        }else {
          print(cardStatus.response?.message ?? "")
          
        }
      }) { (failure) in
        print(failure)
      }
    }
    alertCardStatus.addAction(noAction)
    alertCardStatus.addAction(yesAction)
    
    self.present(alertCardStatus, animated: true)
    
    
  }
}
