//
//  CardcollectionView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/7/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class DeactivateCardView: UIView {

  lazy var titleCardCollectLabel: UILabel = {
    let label = UILabel()
    label.text = "YOUR CARD COLLECTION"
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  
  lazy var cardmodelImgView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "img_card_front_mn_fa_05")
    imageView.contentMode = .scaleToFill
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  
  lazy var deactivateBtn: UIButton = {
       let button = UIButton(type: .system)
       button.setTitle("DEACTIVATE", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 12,weight: .medium)
       button.layer.cornerRadius = 15
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
       button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
       button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
       button.layer.shadowOpacity = 1.0
       button.layer.shadowRadius = 0.0
       button.layer.masksToBounds = false
       button.layer.cornerRadius = 10.0
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupUI()
  }
  
  fileprivate func setupUI(){
    backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
//    addSubview(titleCardCollectLabel)
//    NSLayoutConstraint.activate([
//      //titleCardCollectLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 44),
//      titleCardCollectLabel.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
//      titleCardCollectLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
//      titleCardCollectLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor)
//    ])
    
    addSubview(cardmodelImgView)
    
    NSLayoutConstraint.activate([
      //cardmodelImgView.topAnchor.constraint(equalTo: titleCardCollectLabel.bottomAnchor,constant: 10),
      cardmodelImgView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      cardmodelImgView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
      cardmodelImgView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
      cardmodelImgView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20),
      cardmodelImgView.heightAnchor.constraint(equalToConstant: 250)
    ])
    
    addSubview(deactivateBtn)
    
    NSLayoutConstraint.activate([
      deactivateBtn.heightAnchor.constraint(equalToConstant: 35),
      deactivateBtn.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      deactivateBtn.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
      deactivateBtn.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20),
      deactivateBtn.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor,constant: -5)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
