//
//  MembershipCardView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/7/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MembershipCardView: UIView {


  lazy var titleCardNumValueBackLabel: UILabel = {
    let label = UILabel()
    label.text = "8888880059610"
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var titleSerialNumBackLabel: UILabel = {
      let label = UILabel()
      label.text = "Serial Number"
      label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
      label.textAlignment = .center
      label.font = UIFont.systemFont(ofSize: 9, weight: .medium)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
  
  lazy var titleSerialNumValueBackLabel: UILabel = {
     let label = UILabel()
     label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     label.textAlignment = .center
     label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var memberCardNumfrontView: UIView = {
       let subView = UIView()
       subView.backgroundColor = .none
     subView.isHidden = true
       subView.translatesAutoresizingMaskIntoConstraints = false
       return subView
     }()
  
  lazy var qrcodeMemberBackImgView: UIImageView = {
      let imageView = UIImageView()
      imageView.contentMode = .scaleToFill
      imageView.layer.masksToBounds = true
      imageView.translatesAutoresizingMaskIntoConstraints = false
      return imageView
    }()
 
  lazy var cardmodelMembershipFrontImgView: UIImageView = {
     let imageView = UIImageView()
     imageView.image = #imageLiteral(resourceName: "img_card_front_mn_fa_05")
     imageView.contentMode = .scaleToFill
     imageView.layer.masksToBounds = true
     imageView.translatesAutoresizingMaskIntoConstraints = false
     return imageView
   }()
  
  lazy var memberCardNumBackView: UIView = {
      let subView = UIView()
      subView.backgroundColor = .none
      subView.translatesAutoresizingMaskIntoConstraints = false
      return subView
    }()
  
  lazy var cardmodelMembershipBackImgView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "img_card_back")
    imageView.contentMode = .scaleToFill
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
    lazy var activeView: UIView = {
      let v = UIView()
      v.isHidden = true
      v.translatesAutoresizingMaskIntoConstraints = false
      v.backgroundColor = UIColor(white: 1, alpha: 0.5)
      return v
  }()
    
  lazy var deactivateBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("DEACTIVATE CARD", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12,weight: .medium)
        button.layer.cornerRadius = 15
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 10.0
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
      }()
  
  lazy var payMembershipBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("PAY", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12,weight: .medium)
        button.layer.cornerRadius = 15
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 10.0
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
      }()
  
  lazy var doneMembershipBtn: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("DONE", for: .normal)
    button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 12,weight: .medium)
    button.layer.cornerRadius = 15
    button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
    button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    button.layer.shadowOpacity = 1.0
    button.layer.shadowRadius = 0.0
    button.layer.masksToBounds = false
    button.layer.cornerRadius = 10.0
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
    
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  fileprivate func setupUI(){
    backgroundColor = .white
    addSubview(memberCardNumfrontView)
    memberCardNumfrontView.addSubview(cardmodelMembershipFrontImgView)
    
    NSLayoutConstraint.activate([
      memberCardNumfrontView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      memberCardNumfrontView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
      memberCardNumfrontView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
      memberCardNumfrontView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20),
      memberCardNumfrontView.heightAnchor.constraint(equalToConstant: 220),
      cardmodelMembershipFrontImgView.topAnchor.constraint(equalTo: memberCardNumfrontView.topAnchor),
      cardmodelMembershipFrontImgView.leadingAnchor.constraint(equalTo: memberCardNumfrontView.leadingAnchor),
      cardmodelMembershipFrontImgView.trailingAnchor.constraint(equalTo: memberCardNumfrontView.trailingAnchor),
      cardmodelMembershipFrontImgView.bottomAnchor.constraint(equalTo: memberCardNumfrontView.bottomAnchor)
    ])
    
    addSubview(memberCardNumBackView)
    memberCardNumBackView.addSubview(cardmodelMembershipBackImgView)
  
    
    NSLayoutConstraint.activate([
        memberCardNumBackView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
        memberCardNumBackView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
        memberCardNumBackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
        memberCardNumBackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20),
        memberCardNumBackView.heightAnchor.constraint(equalToConstant: 220),
        cardmodelMembershipBackImgView.topAnchor.constraint(equalTo: memberCardNumBackView.topAnchor),
        cardmodelMembershipBackImgView.leadingAnchor.constraint(equalTo: memberCardNumBackView.leadingAnchor),
        cardmodelMembershipBackImgView.trailingAnchor.constraint(equalTo: memberCardNumBackView.trailingAnchor),
        cardmodelMembershipBackImgView.bottomAnchor.constraint(equalTo: memberCardNumBackView.bottomAnchor)
    ])
    memberCardNumBackView.addSubview(titleCardNumValueBackLabel)
    
    NSLayoutConstraint.activate([
      titleCardNumValueBackLabel.topAnchor.constraint(equalTo: memberCardNumBackView.topAnchor,constant: 45),
      titleCardNumValueBackLabel.leadingAnchor.constraint(equalTo: memberCardNumBackView.leadingAnchor,constant: 20)
    ])
    
    
    let contentTitleSerialVStack = UIStackView(arrangedSubviews: [titleSerialNumBackLabel,titleSerialNumValueBackLabel])
    contentTitleSerialVStack.axis = .vertical
    contentTitleSerialVStack.alignment = .fill
    contentTitleSerialVStack.distribution = .fill
    contentTitleSerialVStack.spacing = 10
    contentTitleSerialVStack.translatesAutoresizingMaskIntoConstraints = false
    
    memberCardNumBackView.addSubview(contentTitleSerialVStack)
    
    NSLayoutConstraint.activate([
      titleSerialNumBackLabel.rightAnchor.constraint(equalTo: memberCardNumBackView.rightAnchor,constant: -50),
      titleSerialNumBackLabel.topAnchor.constraint(equalTo: memberCardNumBackView.topAnchor,constant: 25)
    ])
    
   
    
    memberCardNumBackView.addSubview(qrcodeMemberBackImgView)
    
    NSLayoutConstraint.activate([
      qrcodeMemberBackImgView.leadingAnchor.constraint(equalTo: memberCardNumBackView.leadingAnchor,constant:  20),
      qrcodeMemberBackImgView.bottomAnchor.constraint(equalTo: memberCardNumBackView.bottomAnchor,constant: -5),
      qrcodeMemberBackImgView.widthAnchor.constraint(equalToConstant: 50),
      qrcodeMemberBackImgView.heightAnchor.constraint(equalToConstant: 50)
    ])
    
    
    let contentMembershipBtnVStack = UIStackView(arrangedSubviews: [deactivateBtn,doneMembershipBtn])
    contentMembershipBtnVStack.axis = .vertical
    contentMembershipBtnVStack.alignment = .fill
    contentMembershipBtnVStack.distribution = .fillEqually
    contentMembershipBtnVStack.spacing = 10
    contentMembershipBtnVStack.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentMembershipBtnVStack)
    
    NSLayoutConstraint.activate([
      contentMembershipBtnVStack.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
      contentMembershipBtnVStack.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20),
      contentMembershipBtnVStack.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor,constant: -5)
    ])
    
    memberCardNumBackView.addSubview(activeView)
    activeView.fillSuperview()
    
    doneMembershipBtn.heightAnchor.constraint(equalToConstant: 35).isActive = true
    deactivateBtn.heightAnchor.constraint(equalToConstant: 35).isActive = true
  }
  
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
