//
//  PopupQrCodeView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/21/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PopupQrCodeView: UIView {

  lazy var bgQrcodeView:UIView = {
    let subView = UIView()
    subView.backgroundColor = .white
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var qrcodeImage: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "img_qrcode")
    imageView.contentMode = .scaleAspectFit
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  lazy var closeImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "ic_close")
    imageView.contentMode = .scaleToFill
    imageView.layer.borderWidth = 1
    imageView.layer.cornerRadius = 13
    imageView.layer.borderColor = UIColor.white.cgColor
    imageView.layer.masksToBounds = true
    return imageView
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupUI()
  }
  
  fileprivate func setupUI(){
    addSubview(bgQrcodeView)
    NSLayoutConstraint.activate([
      bgQrcodeView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      bgQrcodeView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor)
    ])
    
    bgQrcodeView.addSubview(qrcodeImage)
    NSLayoutConstraint.activate([
      //qrcodeImage.topAnchor.constraint(equalTo: bgQrcodeView.topAnchor,constant: 10),
      qrcodeImage.leadingAnchor.constraint(equalTo: bgQrcodeView.leadingAnchor,constant: 10),
      qrcodeImage.trailingAnchor.constraint(equalTo: bgQrcodeView.trailingAnchor,constant: -10),
      qrcodeImage.centerXAnchor.constraint(equalTo: bgQrcodeView.centerXAnchor),
      qrcodeImage.centerYAnchor.constraint(equalTo: bgQrcodeView.centerYAnchor),
      qrcodeImage.widthAnchor.constraint(equalToConstant: 250),
      qrcodeImage.heightAnchor.constraint(equalToConstant: 250)
    ])
  }
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
