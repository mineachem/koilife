//
//  PopupQrCodeVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/21/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PopupQrCodeVC: UIViewController {

  var qrCode:UIImage? = nil
  lazy var popupQrCodeView = PopupQrCodeView()
  
  override func loadView() {
    super.loadView()
    self.view = popupQrCodeView
  }
  
  
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupEvent()
    }
    
  fileprivate func setupUI(){
    popupQrCodeView.qrcodeImage.image = qrCode
  }
  
  fileprivate func setupEvent(){
    
    let closeTapped = UITapGestureRecognizer(target: self, action: #selector(handleCloseTapped))
    popupQrCodeView.closeImageView.addGestureRecognizer(closeTapped)
    popupQrCodeView.closeImageView.isUserInteractionEnabled = true
  }
  
  @objc private func handleCloseTapped(){
    self.dismiss(animated: true)
  }
}
