//
//  MembershipCardVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/7/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import EzPopup
import Alamofire

class MembershipCardVC: UIViewController {
  
  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    let titleNav = UILabel()
    titleNav.text = "MEMEMBERSHIP CARD"
    titleNav.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    navItem.titleView = titleNav
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "left-arrow").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
  
  lazy var membershipCardView = MembershipCardView()
  var imageQrcode: UIImage? = nil
  
  //properties
  var isFrontVisibale:Bool = true
  var customCardModel: CustomCardsModel?
  
    var updateStatusUI: UpdateStatusUI?
  override func loadView() {
    super.loadView()
    
    self.view = membershipCardView
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()

      setupNavigationBar()
      setupUI()
      setupEvent()
    }
    
  
  fileprivate func setupNavigationBar(){
      view.addSubview(titleNavigationBar)
      NSLayoutConstraint.activate([
        titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
        titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
      ])
    }
  
  fileprivate func setupUI(){
    
    let uuid = customCardModel!.uuid!
    // current date and time
    let someDate = Date()
    // time interval since 1970
    let myTimeStamp = someDate.millisecondsSince1970
    let qr = "qr"
    
    let generateQrcode = "\(uuid)_\(myTimeStamp)_\(qr)"
    
    let imageCardQrcode = generateQRCode(from: generateQrcode)
    
    membershipCardView.qrcodeMemberBackImgView.image = imageCardQrcode
    imageQrcode = imageCardQrcode
    
    if !customCardModel!.cardStatus! {
        self.membershipCardView.activeView.isHidden = false
        self.membershipCardView.deactivateBtn.setTitle("ACTIVATE CARD", for: .normal)
    }else {
        self.membershipCardView.deactivateBtn.setTitle("DEACTIVATE CARD", for: .normal)
    }
  
    if let cardNum = customCardModel?.cardNumber{
      membershipCardView.titleCardNumValueBackLabel.text = cardNum
    }
    if let serialNum = customCardModel?.serialNumber {
      membershipCardView.titleSerialNumValueBackLabel.text = serialNum
    }
    
  }
  
  func generateQRCode(from string: String) -> UIImage? {
      let data = string.data(using: String.Encoding.ascii)

      if let filter = CIFilter(name: "CIQRCodeGenerator") {
          filter.setValue(data, forKey: "inputMessage")
          let transform = CGAffineTransform(scaleX: 3, y: 3)

          if let output = filter.outputImage?.transformed(by: transform) {
              return UIImage(ciImage: output)
          }
      }
      return nil
  }
  
  fileprivate func setupEvent(){
    let membercardFrontTapped = UITapGestureRecognizer(target: self, action: #selector(handlememberTapped))
    membershipCardView.memberCardNumfrontView.addGestureRecognizer(membercardFrontTapped)
    
    let membercardBackTapped = UITapGestureRecognizer(target: self, action: #selector(handlememberTapped))
    membershipCardView.memberCardNumBackView.addGestureRecognizer(membercardBackTapped)
    
    membershipCardView.memberCardNumfrontView.isUserInteractionEnabled = true
    membershipCardView.memberCardNumBackView.isUserInteractionEnabled = true
    

    membershipCardView.deactivateBtn.addTarget(self, action: #selector(handleAddCardTapped(_:)), for: .touchUpInside)
    
    let qrcodeZoomTapped = UITapGestureRecognizer(target: self, action: #selector(QrcodeZoomImageTapped))
    membershipCardView.qrcodeMemberBackImgView.addGestureRecognizer(qrcodeZoomTapped)
    membershipCardView.qrcodeMemberBackImgView.isUserInteractionEnabled = true
    
    membershipCardView.doneMembershipBtn.addTarget(self, action: #selector(handleDoneTapped), for: .touchUpInside)
  }
  
  @objc private func handleDoneTapped(){
    self.dismiss(animated: true)
    
  }
    @objc private func handleAddCardTapped(_ sender:UIButton){

        var alertTitle = ""
        var alertMessage = ""
        var yesAction: UIAlertAction?

        if sender.currentTitle == "DEACTIVATE CARD" {
            alertTitle = "DEACTIVATE CARD"
            alertMessage = "Are you sure want to deactivate this card [\(self.customCardModel!.cardNumber)]?"
          yesAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            self.getStatusCard(sender: 0, senderBtn: sender)
          })
          
            
        }else{
          
         // sender.setTitle("DEACTIVATE CARD", for: .normal)
            alertTitle = "ACTIVATE CARD"
            alertMessage = "Are you sure want to active your card [\(self.customCardModel!.cardNumber)]?"
            yesAction = UIAlertAction(title: "Yes", style: .default,handler: { (action) in
              self.getStatusCard(sender: 1, senderBtn: sender)
            })
            
        }
        let noAction = UIAlertAction(title: "No", style: .destructive, handler: nil)
        let alertCardStatus = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        alertCardStatus.addAction(noAction)
        alertCardStatus.addAction(yesAction!)

        self.present(alertCardStatus, animated: true)
    }
    
  func getStatusCard(sender:Int,senderBtn:UIButton){
      
      let param:[String:Any] = [
          "id": customCardModel!.id,
          "cardStatus": sender
      ]

      IGNetworkRequest.shareInstance.requestPUTURL(KoiService.share.deactivateUrl, params: param, success: { (cardStatus:CardStatus) in

        if cardStatus.response?.code == 200 {

            var title = ""
            self.updateStatusUI?.onCardChange(status: false)
            if sender == 0 {
//                self.membershipCardView.memberCardNumBackView.isUserInteractionEnabled = false
//                self.membershipCardView.memberCardNumBackView.alpha = 0.5
                 self.membershipCardView.activeView.isHidden = false
                title = "DEACTIVATE CARD"
              senderBtn.setTitle("ACTIVATE CARD", for: .normal)
             
            }else{
//                self.membershipCardView.memberCardNumBackView.isUserInteractionEnabled = true
//                self.membershipCardView.memberCardNumBackView.alpha = 1
                self.membershipCardView.activeView.isHidden = true
                title = "ACTIVATE CARD"
              senderBtn.setTitle("DEACTIVATE CARD", for: .normal)
            }

            

          let alertCardStatus = UIAlertController(title: "Card Membership", message: "\(title) your card \(String(describing: cardStatus.response!.message!))", preferredStyle: .alert)
          let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)

          alertCardStatus.addAction(okAction)
          self.present(alertCardStatus, animated: true)
        }else {
          print(cardStatus.response?.message ?? "")

        }
      }) { (failure) in
        print(failure)
      }
     
      
      
      
    }
  
  @objc private func QrcodeZoomImageTapped(){
    
    let popupQrcodeVC = PopupQrCodeVC()
      popupQrcodeVC.qrCode = imageQrcode
    let popupVC = PopupViewController(contentController: popupQrcodeVC, position: .center(CGPoint(x: 0, y: 0)), popupWidth: 250, popupHeight: 250)
         popupVC.backgroundAlpha = 0.3
         popupVC.backgroundColor = .black
         popupVC.canTapOutsideToDismiss = true
         popupVC.cornerRadius = 10
         popupVC.shadowEnabled = true

    self.present(popupVC, animated: true)
  }
  
   @objc private func handleBackTapped(){
    self.dismiss(animated: true, completion: nil)
   }
  
  
  @objc private func handleDeactivateTapped(){
    let deactivateVC = DeactivateCardVC()
    if let customercardId = customCardModel?.id,let cardNumber = customCardModel?.cardNumber{
      deactivateVC.customerCardId = customercardId
      deactivateVC.cardNumber = cardNumber
      
    }
  
    deactivateVC.modalTransitionStyle = .crossDissolve
    deactivateVC.modalPresentationStyle = .overCurrentContext
    self.present(deactivateVC, animated: true)
  }
  @objc private func handlememberTapped(){

  }

   
}
