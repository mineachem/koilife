//
//  AddCardTableViewController.swift
//  TestProject
//
//  Created by Apple on 12/11/19.
//  Copyright © 2019 IG. All rights reserved.
//

import UIKit
import EzPopup
import NVActivityIndicatorView

class AddCardTableViewController: UITableViewController,UpdateUIProtocol,UpdateStatusUI {
    
    func onCardChange(status: Bool) {
        print("onCardChange")
        getAllCard()
    }
    
  let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
  
    func onUpdateUI(customCardsModel: CustomCardsModel) {
        
        self.cards.append(customCardsModel)
      DispatchQueue.main.async {
          self.tableView.reloadData()
      }
        let jeremyGif = UIImage.gifImageWithName("KOI_Ribbon")
                 let imageView = UIImageView(image: jeremyGif)
                 imageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                 view.addSubview(imageView)
              DispatchQueue.main.asyncAfter(deadline: .now() + 9.0) {
                  UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
                        imageView.removeFromSuperview()
                  
                  }, completion: nil)
                
              }
    }
    
  
    
    
    let cellID = "cellID"
    let commingSoonID = "commingSoonID"
    var cards = [CustomCardsModel]()
    
    func onUpdateUI() {
      
    }
    
    
 let passcode = UserDefaults.standard.value(forKey: "passcode") as? String
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupIndicator()
        getAllCard()
    }
    
    fileprivate func setupIndicator(){
      
         loading.translatesAutoresizingMaskIntoConstraints = false
          
         view.addSubview(loading)
         NSLayoutConstraint.activate([
           loading.widthAnchor.constraint(equalToConstant: 40),
           loading.heightAnchor.constraint(equalToConstant: 40),
           loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
           loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
         ])
      
    }
    func showLoading()
    {
      loading.startAnimating()
    }

    func hideLoading()
    {
      
      loading.stopAnimating()
    }
  
    func getAllCard()
    {
        let customerWalletID = UserDefaultManager.sharedInstance.getInt(key: "customerWalletId")
        let model = CustomerCardParam(customerWalletID: customerWalletID)
                       let jsonEncoder = JSONEncoder()
                       let jsonData = try! jsonEncoder.encode(model)
                       
                       let jsonString = String(data: jsonData, encoding: .utf8)
                       
                  let headers: [String: String] = [
                              
                                "Content-Type": "application/json"
                            ]
      self.showLoading()
        IGNetworkRequest.shareInstance.requestPOSTBODYURL(KoiService.share.customerCardListUrl, params: jsonString!, header: headers, success: { (response: CustomCardResponse) in
           // print("resposne: \(response)")
                  if response.response.code == 200 {
                  self.cards = response.results
                      DispatchQueue.main.async {
                         self.tableView.reloadData()
                  }
                         
                      }
                  }) { (err) in
                      print(err)
                  }
        self.hideLoading()
               
    }
    
    
    func showDetail(pos: Int)
    {
   
      if (passcode != nil) {
      
          let passcodeVerifyVC = PasscodeVerifyVC()
          passcodeVerifyVC.getPasscode = self.passcode
          passcodeVerifyVC.customCardModel = self.cards[pos]
          let popupVC = PopupViewController(contentController: passcodeVerifyVC, position: .center(CGPoint(x: 0, y: 0)), popupWidth: self.view.bounds.width, popupHeight: self.view.bounds.height)
          popupVC.backgroundAlpha = 0.5
          popupVC.backgroundColor = .black
          popupVC.canTapOutsideToDismiss = true
          popupVC.cornerRadius = 10
          popupVC.shadowEnabled = true
          self.present(popupVC, animated: true)
        
      
      }else {
       
          let vc = MembershipCardVC()
          vc.customCardModel = self.cards[pos]
        vc.updateStatusUI = self
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true)
      }

    }
    
    func setupViews()
    {
       tableView.register(SlideCell.self, forCellReuseIdentifier: cellID)
       tableView.register(ItemCard.self, forCellReuseIdentifier: commingSoonID)
       tableView.contentInset = UIEdgeInsets(top: 40, left: 0, bottom: 50, right: 0)
       tableView.separatorStyle = .none
        
        view.addSubview(backgroundButton)
        backgroundButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        backgroundButton.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        backgroundButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        backgroundButton.addSubview(AddButton)
        AddButton.bottomAnchor.constraint(equalTo: backgroundButton.bottomAnchor, constant: -10).isActive = true
        AddButton.centerXAnchor.constraint(equalTo: backgroundButton.centerXAnchor).isActive = true
        AddButton.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.80).isActive = true

        AddButton.heightAnchor.constraint(equalToConstant: 35).isActive = true

        view.addSubview(imageGift)
        
    }
    
    
    lazy var imageGift: UIImageView = {
        let image = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage.gifImageWithName("KOI_Ribbon")
        image.isHidden = true
        return image
    }()
    
    @objc func handleAdd()
    {

      let addCardVC = AddCardVC()
      addCardVC.modalPresentationStyle = .overCurrentContext
      addCardVC.modalTransitionStyle = .crossDissolve
        addCardVC.updateUIProtocol = self
      self.present(addCardVC, animated: true)
      
    }
    
    lazy var backgroundButton: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .white
        return v
    }()
    
    lazy var AddButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("ADD CARD", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 15
        button.setTitleColor(.black, for: .normal)
        
        button.addTarget(self, action: #selector(handleAdd), for: .touchUpInside)
        return button
    }()
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! SlideCell
        cell.cards = cards
        cell.addCardTableViewController = self
        cell.selectionStyle = .none
        if indexPath.row == 1 {
            let commingCell = tableView.dequeueReusableCell(withIdentifier: commingSoonID, for: indexPath)
          commingCell.selectionStyle = .none
            return commingCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UIScreen.main.bounds.height * 0.32
        }
        return UITableView.automaticDimension
    }
}
