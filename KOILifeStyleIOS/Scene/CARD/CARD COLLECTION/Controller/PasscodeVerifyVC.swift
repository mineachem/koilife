//
//  PasscodeVerifyVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/17/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import Alamofire


class PasscodeVerifyVC: UIViewController {
  
  
  lazy var passcodeVerifyView = PasscodeVerifyView()

  var getPasscode:String? = ""
  var collectionOTPBtn:[UIButton]{
    return [passcodeVerifyView.numOneBtn,passcodeVerifyView.numTwoBtn,passcodeVerifyView.numThreeBtn,passcodeVerifyView.numFourBtn,passcodeVerifyView.numFiveBtn,passcodeVerifyView.numSixBtn,passcodeVerifyView.numSevenBtn,passcodeVerifyView.numEightBtn,passcodeVerifyView.numNineBtn,passcodeVerifyView.numZeroBtn,passcodeVerifyView.signCrossBtn]
  }
  
   var customCardModel: CustomCardsModel?
  
  override func loadView() {
    super.loadView()
    self.view = passcodeVerifyView
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
      setupView()
      setupEvent()
        
    }
    
  
  fileprivate func setupView(){
    
//    passcodeVerifyView.otpPinOneTextField.delegate = self
//    passcodeVerifyView.otpPinTwoTextField.delegate = self
//    passcodeVerifyView.otpPinThreeTextField.delegate = self
//    passcodeVerifyView.otpPinFourTextField.delegate = self
    
  }
  
  fileprivate func setupEvent(){
    passcodeVerifyView.closeBtn.addTarget(self, action: #selector(handleCloseTapped), for: .touchUpInside)
    
    collectionOTPBtn.forEach{
      $0.addTarget(self, action: #selector(handleOtppinTapped), for: .touchUpInside)
    }
    
    passcodeVerifyView.forgetBtn.addTarget(self, action: #selector(handleForgotTapped), for: .touchUpInside)
  }
   
  
  @objc private func handleForgotTapped(){
    let alertController = UIAlertController(title: "", message: "You can sign out to turn off your passcode. \n Do you want to sign out now?", preferredStyle: .alert)

            let alertNo = UIAlertAction(title: "No", style: .cancel) { (action) in
                  self.dismiss(animated: true)
                }
                    
    alertController.addAction(alertNo)
    
    let alertYes = UIAlertAction(title: "Yes", style: .default) { (action) in

            UserDefaults.standard.removeObject(forKey: "login")
             UserDefaults.standard.removeObject(forKey: "passcode")
            let authenticationVC = UINavigationController(rootViewController: AuthenticationVC())
            authenticationVC.navigationBar.isHidden = true
            UIApplication.shared.windows.first?.rootViewController = authenticationVC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
 
            UserDefaults.standard.removeObject(forKey: "id")
            UserDefaults.standard.removeObject(forKey: "firstname")
            UserDefaults.standard.removeObject(forKey: "lastname")
            UserDefaults.standard.removeObject(forKey: "customerWalletId")
            UserDefaults.standard.removeObject(forKey: "balance")
            UserDefaults.standard.removeObject(forKey: "accName")
            UserDefaults.standard.removeObject(forKey: "score")
            UserDefaults.standard.removeObject(forKey: "membershipDate")
            UserDefaults.standard.removeObject(forKey: "rewardscore")
            UserDefaults.standard.removeObject(forKey: "endyear")
            UserDefaults.standard.removeObject(forKey: "accNumber")
            UserDefaults.standard.removeObject(forKey: "qty")

            UserDefaults.standard.removeObject(forKey: "memberTypeId")
            UserDefaults.standard.removeObject(forKey: "memberTypeMaxqty")
            UserDefaults.standard.removeObject(forKey: "memberTypeMinqty")
            UserDefaults.standard.removeObject(forKey: "memberType")
            UserDefaults.standard.removeObject(forKey: "memberTypeStatus")
      
    }
    
    alertController.addAction(alertYes)
  self.present(alertController, animated: true)
  }
  
  @objc private func handleOtppinTapped(sender:UIButton){
    sender.isSelected = !sender.isSelected
    
    switch sender.tag {
    case 0:
      getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 1:
       getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 2:
      getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 3:
       getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 4:
        getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 5:
        getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 6:
       getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 7:
        getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 8:
       getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 9:
      getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    case 10:
     getValueFromButton(sender: sender, textOne:passcodeVerifyView.otpPinOneTextField, textTwo: passcodeVerifyView.otpPinTwoTextField, textThree: passcodeVerifyView.otpPinThreeTextField, textFour: passcodeVerifyView.otpPinFourTextField)
    default:
      print("nothing")
    }
    
  }
  
  
  private func clearTextField(textOne:UITextField,textTwo:UITextField,textThree:UITextField,textFour:UITextField){
    if ((textFour.text?.count)! >= 1){
      textFour.text = ""
      self.passcodeVerifyView.otpPinFourTextField.becomeFirstResponder()
    }else if ((textThree.text?.count)! >= 1){
      textThree.text = ""
         self.passcodeVerifyView.otpPinThreeTextField.becomeFirstResponder()
    }else if ((textTwo.text?.count)! >= 1){
        textTwo.text = ""
      self.passcodeVerifyView.otpPinTwoTextField.becomeFirstResponder()
    }else if ((textOne.text?.count)! >= 1){
      textOne.text = ""
      self.passcodeVerifyView.otpPinOneTextField.resignFirstResponder()
    }
    
  }
  
  private func getValueFromButton(sender:UIButton,textOne:UITextField,textTwo:UITextField,textThree:UITextField,textFour:UITextField){
    
    if ((textOne.text?.count)! < 1) {
      textOne.text = sender.currentTitle
      self.passcodeVerifyView.otpPinTwoTextField.becomeFirstResponder()
    }else if ((textTwo.text?.count)! < 1){
      textTwo.text = sender.currentTitle
      self.passcodeVerifyView.otpPinThreeTextField.becomeFirstResponder()
    }else if ((textThree.text?.count)! < 1){
      textThree.text = sender.currentTitle
      self.passcodeVerifyView.otpPinFourTextField.becomeFirstResponder()
    }else if ((textFour.text?.count)! < 1){
      textFour.text = sender.currentTitle
       self.passcodeVerifyView.otpPinFourTextField.resignFirstResponder()
    }
    
   let getValue = "\(textOne.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
   
  // let passcode = UserDefaults.standard.value(forKey: "passcode") as? String
    
      if getValue.count == 4 {
        if getValue == getPasscode! {
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            let vc = MembershipCardVC()
                //vc.customCardModel = cards[pos]
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true)
          }
        }else {
             let alertController = UIAlertController(title: "", message: "Incorrect passcode Please try again later", preferredStyle: .alert)

                 let alertOk = UIAlertAction(title: "OK", style: .cancel) { (action) in
                  self.dismiss(animated: true)
                 }
                 alertController.addAction(alertOk)
                 self.present(alertController, animated: true)
        }
      }
    
  }
  
  @objc private func handleCloseTapped(){
    
    self.view.window?.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
    //self.dismiss(animated: true)
    
  }
  
}

