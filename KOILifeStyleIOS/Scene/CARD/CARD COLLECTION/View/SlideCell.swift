//
//  SlideCell.swift
//  TestProject
//
//  Created by Apple on 12/11/19.
//  Copyright © 2019 IG. All rights reserved.
//

import UIKit
import FSPagerView
import SDWebImage

class SlideCell: UITableViewCell {
  
  lazy var cardCollectionLabel: UILabel = {
    let label = UILabel()
    label.text = "YOUR CARD COLLECTION"
    label.textAlignment = .left
    label.font = UIFont.boldSystemFont(ofSize: 15)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
    lazy var emptyCardView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
  
  lazy var emptyImgView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "error")
    imageView.contentMode = .scaleAspectFit
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()

    var addCardTableViewController: AddCardTableViewController?
    var cards: [CustomCardsModel]? {
        didSet{
            print("xxxx get it")
            collectionViewObj.reloadData()
            indecatorView.numberOfPages = cards?.count ?? 0
        }
    }


    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
          let x  = targetContentOffset.pointee.x
          indecatorView.currentPage = Int(x / collectionViewObj.frame.width)
      }
      
    
    let cellId = "cellId"
 
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews()
    {
      
      addSubview(cardCollectionLabel)
      NSLayoutConstraint.activate([
        cardCollectionLabel.topAnchor.constraint(equalTo: topAnchor,constant: 30),
        cardCollectionLabel.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 20),
        cardCollectionLabel.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -20)
      ])
        
        let height = UIScreen.main.bounds.height * 0.30
      
        contentView.addSubview(collectionViewObj)
        collectionViewObj.anchor(top: cardCollectionLabel.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor, padding: UIEdgeInsets.init(top: 20, left: 0, bottom: 0, right: 0))
        collectionViewObj.heightAnchor.constraint(equalToConstant: height).isActive = true
        
        
        
        contentView.addSubview(indecatorView)
        indecatorView.topAnchor.constraint(equalTo: collectionViewObj.bottomAnchor, constant: 0).isActive = true
        indecatorView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        
        
        contentView.addSubview(emptyCardView)
        emptyCardView.heightAnchor.constraint(equalToConstant: height).isActive = true
        emptyCardView.topAnchor.constraint(equalTo: cardCollectionLabel.bottomAnchor,constant: 20).isActive = true
        emptyImgView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.85).isActive = true
       
        

      emptyCardView.addSubview(emptyImgView)
      emptyImgView.centerXAnchor.constraint(equalTo: emptyCardView.centerXAnchor).isActive = true
      emptyImgView.centerYAnchor.constraint(equalTo: emptyCardView.centerYAnchor).isActive = true
      

    }
    
  lazy var indecatorView: UIPageControl = {
        let pager = UIPageControl()
        pager.translatesAutoresizingMaskIntoConstraints = false
        pager.currentPage = 0
        pager.currentPageIndicatorTintColor = #colorLiteral(red: 0.8559798598, green: 0.6504896879, blue: 0.2624273896, alpha: 1)
        pager.pageIndicatorTintColor = .gray
        return pager
    }()
    

    
    lazy var collectionViewObj: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .white
        cv.isPagingEnabled = true
        cv.dataSource = self
        cv.register(MyCardCell.self, forCellWithReuseIdentifier: cellId)
        return cv
    }()
    
    
    
}

extension SlideCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cards?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MyCardCell
        let card = cards?[indexPath.row]
        
                if let imageFile = card?.image {
                    cell.imageView.sd_setImage(with: URL(string:  "\(KoiService.share.BASE_CARD_IMAGE)\(imageFile)"))
                    emptyCardView.isHidden = true
                }else{
                    emptyCardView.isHidden = false
                }
        
        
                if !(card?.cardStatus)! {
                    cell.activeView.isHidden = false
                }else {
                    cell.activeView.isHidden = true
        }
        
              
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewObj.frame.width, height: collectionViewObj.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        addCardTableViewController?.showDetail(pos: indexPath.row)
    }
}
