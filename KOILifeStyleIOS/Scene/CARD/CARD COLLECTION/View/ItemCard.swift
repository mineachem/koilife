//
//  ItemCard.swift
//  TestProject
//
//  Created by Apple on 12/12/19.
//  Copyright © 2019 IG. All rights reserved.
//

import UIKit
class ItemCard: UITableViewCell {
    
    private let cellID = "cellID"
    
    var images = ["card_1","card_2","card_3","card_4"]
     var colViewObj: UICollectionView!
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
     
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews()
    {
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 100).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
        
          let layout = UICollectionViewFlowLayout()
             colViewObj = UICollectionView(frame: self.frame, collectionViewLayout: layout)
             colViewObj.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
             colViewObj.register(CommingCell.self, forCellWithReuseIdentifier: cellID)
             colViewObj.isScrollEnabled = false
             colViewObj.alwaysBounceVertical = false
             
             colViewObj.backgroundColor = .white
             colViewObj.delegate = self
             colViewObj.dataSource = self
             addSubview(colViewObj)
             colViewObj.translatesAutoresizingMaskIntoConstraints = false
             colViewObj.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
             colViewObj.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
             colViewObj.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
             colViewObj.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 130).isActive = true
             
    }
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
          
          // If the cell's size has to be exactly the content
          // Size of the collection View, just return the
          // collectionViewLayout's collectionViewContentSize.
          
          self.colViewObj.frame = CGRect(x: 0, y: 0,
                                         width: targetSize.width, height: UIScreen.main.bounds.width)
          self.colViewObj.layoutIfNeeded()
          
          // It Tells what size is required for the CollectionView
          return self.colViewObj.collectionViewLayout.collectionViewContentSize
          
      }
  
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "COMING SOON"
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = .black
        return label
    }()
    
}
extension ItemCard: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! CommingCell
        cell.thumnail.image = UIImage(named: images[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: colViewObj.frame.width / 2-10, height: 100)
    }
    
    
    
}
