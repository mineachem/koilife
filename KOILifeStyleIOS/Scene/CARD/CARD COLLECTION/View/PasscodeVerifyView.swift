//
//  PasscodeVerifyView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/17/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PasscodeVerifyView: UIView {

    lazy var bgPasscodeVerifyView: UIView = {
        let subView = UIView()
        subView.backgroundColor = .none
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
      }()
     
  
    lazy var bgNumKeyboardView: UIView = {
      let subView = UIView()
    subView.backgroundColor = .white
      subView.translatesAutoresizingMaskIntoConstraints = false
      return subView
    }()
  
     lazy var titlePasscodeVerifyLabel: UILabel = {
       let label = UILabel()
       label.text = "Passcode Verify"
       label.textAlignment = .center
       label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
     }()
     
    
     lazy var bgOTPPinPasscodeVerifyView: UIView = {
         let subView = UIView()
         subView.backgroundColor = .none
         subView.translatesAutoresizingMaskIntoConstraints = false
         return subView
       }()
     
     lazy var otpPinOneTextField: UITextField = {
       let textField = UITextField()
       textField.font = UIFont.systemFont(ofSize: 14)
       textField.addLine(position: .LINE_POSITION_BOTTOM, color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), width: 0.5)
       textField.textAlignment = .center
       textField.isEnabled = false
       textField.tag = 0
       textField.keyboardType = .numberPad
       textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       textField.translatesAutoresizingMaskIntoConstraints = false
       return textField
     }()
     
     lazy var otpPinTwoTextField: UITextField = {
       let textField = UITextField()
       textField.font = UIFont.systemFont(ofSize: 14)
       textField.addLine(position: .LINE_POSITION_BOTTOM, color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), width: 0.5)
       textField.textAlignment = .center
       textField.tag = 1
       textField.isEnabled = false
       textField.keyboardType = .numberPad
       textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       textField.translatesAutoresizingMaskIntoConstraints = false
       return textField
     }()
     
     lazy var otpPinThreeTextField: UITextField = {
       let textField = UITextField()
       textField.font = UIFont.systemFont(ofSize: 14)
       textField.addLine(position: .LINE_POSITION_BOTTOM, color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), width: 0.5)
       textField.keyboardType = .numberPad
       textField.tag = 2
       textField.isEnabled = false
       textField.textAlignment = .center
       textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       textField.translatesAutoresizingMaskIntoConstraints = false
       return textField
     }()
     
     lazy var otpPinFourTextField: UITextField = {
       let textField = UITextField()
       textField.font = UIFont.systemFont(ofSize: 14)
       textField.addLine(position: .LINE_POSITION_BOTTOM, color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), width: 0.5)
       textField.keyboardType = .numberPad
       textField.tag = 3
       textField.isEnabled = false
       textField.textAlignment = .center
       textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       textField.translatesAutoresizingMaskIntoConstraints = false
       return textField
     }()
     
     lazy var titleDescriptionPinCodeLabel: UILabel = {
        let label = UILabel()
        label.text = "Please enter passcode to verify before continue."
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
      }()
     
      lazy var cardPasscodeLockView: UIView = {
         let subView = UIView()
         subView.backgroundColor = .white
         subView.layer.cornerRadius = 10
       subView.layer.masksToBounds = true
         subView.translatesAutoresizingMaskIntoConstraints = false
         return subView
       }()
       
        lazy var keylockPasscodeImgView: UIImageView = {
           let imageView = UIImageView()
           imageView.image = #imageLiteral(resourceName: "ic_lock")
           imageView.contentMode = .scaleAspectFit
           imageView.alpha = 0.3
           //imageView.layer.masksToBounds = true
           imageView.translatesAutoresizingMaskIntoConstraints = false
           return imageView
         }()
       

       lazy var turnPasscodeBtn: UIButton = {
           let button = UIButton(type: .system)
           button.setTitle("TURN PASSCODE ON", for: .normal)
           button.setTitleColor(#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), for: .normal)
           button.titleLabel?.font = UIFont.systemFont(ofSize: 14,weight: .medium)
           button.layer.cornerRadius = 15
           button.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
           button.translatesAutoresizingMaskIntoConstraints = false
           return button
         }()
       
     
     lazy var closeBtn: UIButton = {
       let button = UIButton()
       button.setTitle("X", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 18,weight: .bold)
       button.layer.cornerRadius = 15
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
       
     lazy var cardPasscodeLockOnView: UIView = {
       let subView = UIView()
       subView.backgroundColor = .white
       subView.layer.cornerRadius = 10
     subView.layer.masksToBounds = true
       subView.translatesAutoresizingMaskIntoConstraints = false
       return subView
     }()
     
     lazy var numOneBtn: UIButton = {
       let button = UIButton()
       button.setTitle("1", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
       button.tag = 0
       button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     lazy var numTwoBtn: UIButton = {
       let button = UIButton()
       button.setTitle("2", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.tag = 1
       button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     lazy var numThreeBtn: UIButton = {
       let button = UIButton()
       button.setTitle("3", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.tag = 2
       button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     lazy var numFourBtn: UIButton = {
       let button = UIButton()
       button.setTitle("4", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.tag = 3
       button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     lazy var numFiveBtn: UIButton = {
       let button = UIButton()
       button.setTitle("5", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.tag = 4
       button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     lazy var numSixBtn: UIButton = {
        let button = UIButton()
        button.setTitle("6", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.tag = 5
        button.layer.cornerRadius = 13
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
      }()
     
     lazy var numSevenBtn: UIButton = {
       let button = UIButton()
       button.setTitle("7", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.tag = 6
       button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     lazy var numEightBtn: UIButton = {
       let button = UIButton()
       button.setTitle("8", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.tag = 7
       button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     lazy var numNineBtn: UIButton = {
       let button = UIButton()
       button.setTitle("9", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.tag = 8
       button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     lazy var numZeroBtn: UIButton = {
       let button = UIButton()
       button.setTitle("0", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.tag = 9
       button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     lazy var signCrossBtn: UIButton = {
       let button = UIButton()
       button.setImage(#imageLiteral(resourceName: "ic_key_delete"), for: .normal)
       button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
       button.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        button.tag = 10
      button.layer.cornerRadius = 13
       button.translatesAutoresizingMaskIntoConstraints = false
       return button
     }()
     
     
  
  lazy var forgetBtn: UIButton = {
    let button = UIButton()
    button.setTitle("Forgot passcode?", for: .normal)
    button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
       override init(frame: CGRect) {
         super.init(frame: frame)
         setupUI()
       }
       
       
       fileprivate func setupUI(){
         addSubview(bgPasscodeVerifyView)
       
         NSLayoutConstraint.activate([
           bgPasscodeVerifyView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
           bgPasscodeVerifyView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
           bgPasscodeVerifyView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
           bgPasscodeVerifyView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
         ])

          bgPasscodeVerifyView.addSubview(cardPasscodeLockView)
         
         NSLayoutConstraint.activate([
           cardPasscodeLockView.topAnchor.constraint(equalTo: bgPasscodeVerifyView.topAnchor,constant: 30),
           cardPasscodeLockView.centerXAnchor.constraint(equalTo: bgPasscodeVerifyView.centerXAnchor),
           cardPasscodeLockView.leadingAnchor.constraint(equalTo: bgPasscodeVerifyView.leadingAnchor,constant: 40),
           cardPasscodeLockView.trailingAnchor.constraint(equalTo: bgPasscodeVerifyView.trailingAnchor,constant: -40),
         ])
         
         cardPasscodeLockView.addSubview(keylockPasscodeImgView)
         
         NSLayoutConstraint.activate([
           keylockPasscodeImgView.topAnchor.constraint(equalTo: cardPasscodeLockView.topAnchor,constant: 30),
           keylockPasscodeImgView.centerXAnchor.constraint(equalTo: cardPasscodeLockView.centerXAnchor),
           keylockPasscodeImgView.widthAnchor.constraint(equalToConstant: 150),
           keylockPasscodeImgView.heightAnchor.constraint(equalToConstant: 150)
         ])
         
         cardPasscodeLockView.addSubview(closeBtn)
         NSLayoutConstraint.activate([
           closeBtn.topAnchor.constraint(equalTo: cardPasscodeLockView.topAnchor,constant: 10),
           closeBtn.rightAnchor.constraint(equalTo: cardPasscodeLockView.rightAnchor,constant: -10)
         ])
         
         cardPasscodeLockView.addSubview(titlePasscodeVerifyLabel)
         
         NSLayoutConstraint.activate([
           titlePasscodeVerifyLabel.topAnchor.constraint(equalTo: cardPasscodeLockView.topAnchor,constant: 50),
           titlePasscodeVerifyLabel.centerXAnchor.constraint(equalTo: cardPasscodeLockView.centerXAnchor)
         ])
         
         cardPasscodeLockView.addSubview(bgOTPPinPasscodeVerifyView)
         
         NSLayoutConstraint.activate([
           bgOTPPinPasscodeVerifyView.topAnchor.constraint(equalTo: titlePasscodeVerifyLabel.topAnchor,constant: 40),
           bgOTPPinPasscodeVerifyView.leadingAnchor.constraint(equalTo: cardPasscodeLockView.leadingAnchor,constant: 85),
           bgOTPPinPasscodeVerifyView.trailingAnchor.constraint(equalTo: cardPasscodeLockView.trailingAnchor,constant: -85),
           bgOTPPinPasscodeVerifyView.heightAnchor.constraint(equalToConstant: 40)
         ])
         
         let contentOtpPinCodeHStack = UIStackView(arrangedSubviews: [otpPinOneTextField,otpPinTwoTextField,otpPinThreeTextField,otpPinFourTextField])
         contentOtpPinCodeHStack.axis = .horizontal
         contentOtpPinCodeHStack.alignment = .fill
         contentOtpPinCodeHStack.distribution = .fillEqually
         contentOtpPinCodeHStack.spacing = 7
         contentOtpPinCodeHStack.translatesAutoresizingMaskIntoConstraints = false
         bgOTPPinPasscodeVerifyView.addSubview(contentOtpPinCodeHStack)
         
         NSLayoutConstraint.activate([
           contentOtpPinCodeHStack.topAnchor.constraint(equalTo: bgOTPPinPasscodeVerifyView.topAnchor),
           contentOtpPinCodeHStack.leadingAnchor.constraint(equalTo: bgOTPPinPasscodeVerifyView.leadingAnchor),
           contentOtpPinCodeHStack.trailingAnchor.constraint(equalTo: bgOTPPinPasscodeVerifyView.trailingAnchor),
           contentOtpPinCodeHStack.bottomAnchor.constraint(equalTo: bgOTPPinPasscodeVerifyView.bottomAnchor)
         ])
         
         bgOTPPinPasscodeVerifyView.addSubview(titleDescriptionPinCodeLabel)
         
         NSLayoutConstraint.activate([
           titleDescriptionPinCodeLabel.topAnchor.constraint(equalTo: contentOtpPinCodeHStack.bottomAnchor,constant: 20),
           titleDescriptionPinCodeLabel.centerXAnchor.constraint(equalTo: cardPasscodeLockView.centerXAnchor),
           titleDescriptionPinCodeLabel.leadingAnchor.constraint(equalTo: cardPasscodeLockView.leadingAnchor,constant: 20),
           titleDescriptionPinCodeLabel.trailingAnchor.constraint(equalTo: cardPasscodeLockView.trailingAnchor,constant: -20),
         ])
        
        cardPasscodeLockView.addSubview(forgetBtn)
        
        NSLayoutConstraint.activate([
          forgetBtn.bottomAnchor.constraint(equalTo: cardPasscodeLockView.bottomAnchor,constant: -10),
          forgetBtn.centerXAnchor.constraint(equalTo: cardPasscodeLockView.centerXAnchor)
        ])
         //////

         let contentRowOneTurnPasscodeHStack = UIStackView(arrangedSubviews: [numOneBtn,numTwoBtn,numThreeBtn])
         contentRowOneTurnPasscodeHStack.axis = .horizontal
         contentRowOneTurnPasscodeHStack.alignment = .fill
         contentRowOneTurnPasscodeHStack.distribution = .equalSpacing
         contentRowOneTurnPasscodeHStack.spacing = 10
         contentRowOneTurnPasscodeHStack.translatesAutoresizingMaskIntoConstraints = false

         let contentRowTwoTurnPasscodeHStack = UIStackView(arrangedSubviews: [numFourBtn,numFiveBtn,numSixBtn])
         contentRowTwoTurnPasscodeHStack.axis = .horizontal
         contentRowTwoTurnPasscodeHStack.alignment = .fill
         contentRowTwoTurnPasscodeHStack.distribution = .equalSpacing
         contentRowTwoTurnPasscodeHStack.spacing = 10
         contentRowTwoTurnPasscodeHStack.translatesAutoresizingMaskIntoConstraints = false

         let contentRowThreeTurnPasscodeHStack = UIStackView(arrangedSubviews: [numSevenBtn,numEightBtn,numNineBtn])
         contentRowThreeTurnPasscodeHStack.axis = .horizontal
         contentRowThreeTurnPasscodeHStack.alignment = .fill
         contentRowThreeTurnPasscodeHStack.distribution = .equalSpacing
         contentRowThreeTurnPasscodeHStack.spacing = 10
         contentRowThreeTurnPasscodeHStack.translatesAutoresizingMaskIntoConstraints = false

     let contentRowFourTurnPasscodeHStack = UIStackView(arrangedSubviews: [numZeroBtn,signCrossBtn])
         contentRowFourTurnPasscodeHStack.axis = .horizontal
         contentRowFourTurnPasscodeHStack.alignment = .fill
         contentRowFourTurnPasscodeHStack.distribution = .equalSpacing
         contentRowFourTurnPasscodeHStack.spacing = 10
         contentRowFourTurnPasscodeHStack.translatesAutoresizingMaskIntoConstraints = false

       let contentBtnTurnPassCodeVStack = UIStackView(arrangedSubviews: [contentRowOneTurnPasscodeHStack,contentRowTwoTurnPasscodeHStack,contentRowThreeTurnPasscodeHStack,contentRowFourTurnPasscodeHStack])
         contentBtnTurnPassCodeVStack.axis = .vertical
         contentBtnTurnPassCodeVStack.alignment = .fill
         contentBtnTurnPassCodeVStack.distribution = .fillEqually
         contentBtnTurnPassCodeVStack.spacing = 10
         contentBtnTurnPassCodeVStack.translatesAutoresizingMaskIntoConstraints = false

        bgPasscodeVerifyView.addSubview(bgNumKeyboardView)
             NSLayoutConstraint.activate([
               bgNumKeyboardView.topAnchor.constraint(equalTo: cardPasscodeLockView.bottomAnchor,constant: 80),
               bgNumKeyboardView.leadingAnchor.constraint(equalTo: bgPasscodeVerifyView.leadingAnchor),
               bgNumKeyboardView.trailingAnchor.constraint(equalTo: bgPasscodeVerifyView.trailingAnchor),
               bgNumKeyboardView.heightAnchor.constraint(equalToConstant: 170),
               bgNumKeyboardView.bottomAnchor.constraint(equalTo: bgPasscodeVerifyView.bottomAnchor),
             ])
         bgNumKeyboardView.addSubview(contentBtnTurnPassCodeVStack)

         NSLayoutConstraint.activate([

           contentBtnTurnPassCodeVStack.topAnchor.constraint(equalTo: bgNumKeyboardView.topAnchor,constant: 10),
           contentBtnTurnPassCodeVStack.leadingAnchor.constraint(equalTo: bgNumKeyboardView.leadingAnchor,constant: 20),
           contentBtnTurnPassCodeVStack.trailingAnchor.constraint(equalTo: bgNumKeyboardView.trailingAnchor,constant: -20),
           contentBtnTurnPassCodeVStack.bottomAnchor.constraint(equalTo: bgNumKeyboardView.bottomAnchor,constant: -5),
           
           numOneBtn.widthAnchor.constraint(equalToConstant: 100),
           numTwoBtn.widthAnchor.constraint(equalTo: numOneBtn.widthAnchor, multiplier: 1),
           numThreeBtn.widthAnchor.constraint(equalTo: numOneBtn.widthAnchor, multiplier: 1),
           numFourBtn.widthAnchor.constraint(equalTo: numOneBtn.widthAnchor, multiplier: 1),
           numFiveBtn.widthAnchor.constraint(equalTo: numOneBtn.widthAnchor, multiplier: 1),
           numSixBtn.widthAnchor.constraint(equalTo: numOneBtn.widthAnchor, multiplier: 1),
           numSevenBtn.widthAnchor.constraint(equalTo: numOneBtn.widthAnchor, multiplier: 1),
           numEightBtn.widthAnchor.constraint(equalTo: numOneBtn.widthAnchor, multiplier: 1),
           numNineBtn.widthAnchor.constraint(equalTo: numOneBtn.widthAnchor, multiplier: 1),
           numZeroBtn.widthAnchor.constraint(equalTo: contentBtnTurnPassCodeVStack.widthAnchor, multiplier: 0.65),
           signCrossBtn.widthAnchor.constraint(equalTo: numOneBtn.widthAnchor, multiplier: 1)

         ])
        }
       
       required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
       }

}
