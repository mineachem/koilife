//
//  MyCardCell.swift
//  KOILifeStyleIOS
//
//  Created by Apple on 12/28/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
class MyCardCell: UICollectionViewCell {
    
    
 
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: contentView.frame.height).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.85).isActive = true
        
        imageView.addSubview(activeView)
        activeView.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.image = UIImage(named: "img_card_front_1cup")
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 20
         iv.layer.shadowColor = UIColor.black.cgColor
         iv.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
         iv.layer.shadowRadius = 3.0
         iv.layer.shadowOpacity = 0.2
        iv.layer.borderWidth = 0.5
        iv.layer.borderColor = hexStringToUIColor(hex: "#eaeaea").cgColor
        return iv
    }()
    
    lazy var activeView: UIView = {
        let v = UIView()
        v.isHidden = true
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor(white: 1, alpha: 0.5)
        return v
    }()
}
