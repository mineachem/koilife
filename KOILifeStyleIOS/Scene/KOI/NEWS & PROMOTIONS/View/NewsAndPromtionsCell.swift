//
//  NewsAndPromtionsCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class NewsAndPromtionsCell: UITableViewCell {

 
  lazy var bgNewAndPromotionView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .white
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  lazy var coverNewsAndPromtionsImgView: CustomImageView = {
    let imageView = CustomImageView()
    imageView.contentMode = .scaleAspectFit
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  
  lazy var titleOfcoverNewsAndPromotionLabel: UILabel = {
    let label = UILabel()
    label.text = "ភេសជ្ជៈប្រចាំខែ"
    //label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 17, weight: .medium)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var subtitleOfcoverNewsAndPromotionLabel: UILabel = {
     let label = UILabel()
    let attributedString = NSMutableAttributedString(string: "ចាប់បើកហើយ!សាខាថ្មីស្រឡាង ខយតេ អូឡាំពិក!!! ទីតាំងថ្មីបរិយាកាសដ៏ទំនើប និង តែស្រស់ថ្មីៗ រសជាតិដ៏សែនឆ្ងាញ់ កំពុងរង")
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 4
    // *** Apply attribute to string ***
    attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))

     label.attributedText = attributedString
  //   label.textAlignment = .center
     label.numberOfLines = 2
     label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    setupUI()
  }

  fileprivate func setupUI(){
    
    addSubview(bgNewAndPromotionView)
    
    NSLayoutConstraint.activate([
      bgNewAndPromotionView.topAnchor.constraint(equalTo: topAnchor),
      bgNewAndPromotionView.leadingAnchor.constraint(equalTo: leadingAnchor),
      bgNewAndPromotionView.trailingAnchor.constraint(equalTo: trailingAnchor),
      bgNewAndPromotionView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
    ])
    bgNewAndPromotionView.addSubview(coverNewsAndPromtionsImgView)
    
    NSLayoutConstraint.activate([
      coverNewsAndPromtionsImgView.topAnchor.constraint(equalTo: bgNewAndPromotionView.topAnchor),
      coverNewsAndPromtionsImgView.leadingAnchor.constraint(equalTo: bgNewAndPromotionView.leadingAnchor),
      coverNewsAndPromtionsImgView.trailingAnchor.constraint(equalTo: bgNewAndPromotionView.trailingAnchor),
      coverNewsAndPromtionsImgView.heightAnchor.constraint(equalToConstant: 300)
    ])
    
    let contentNewsAndPromtionsVStack = UIStackView(arrangedSubviews: [titleOfcoverNewsAndPromotionLabel,subtitleOfcoverNewsAndPromotionLabel])
    contentNewsAndPromtionsVStack.axis = .vertical
    contentNewsAndPromtionsVStack.alignment = .fill
    contentNewsAndPromtionsVStack.distribution = .fill
    contentNewsAndPromtionsVStack.spacing = 10
    contentNewsAndPromtionsVStack.translatesAutoresizingMaskIntoConstraints = false
    
    bgNewAndPromotionView.addSubview(contentNewsAndPromtionsVStack)
    
    NSLayoutConstraint.activate([
      contentNewsAndPromtionsVStack.topAnchor.constraint(equalTo: coverNewsAndPromtionsImgView.bottomAnchor,constant: 10),
      contentNewsAndPromtionsVStack.leadingAnchor.constraint(equalTo: bgNewAndPromotionView.leadingAnchor,constant: 10),
      contentNewsAndPromtionsVStack.trailingAnchor.constraint(equalTo: bgNewAndPromotionView.trailingAnchor,constant: -10),
      contentNewsAndPromtionsVStack.bottomAnchor.constraint(equalTo: bgNewAndPromotionView.bottomAnchor,constant: -10)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
