//
//  NewsAndPromotionsView.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/29/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class NewsAndPromotionsDetailView: UIView {
  
 lazy var newsAndPromotionDetailImgView: CustomImageView = {
   let imageView = CustomImageView()
  imageView.contentMode = .scaleAspectFill
   imageView.layer.masksToBounds = true
  imageView.layer.cornerRadius = 10
   imageView.translatesAutoresizingMaskIntoConstraints = false
   return imageView
 }()
 
 lazy var titleNewsAndPromotionLabel: UILabel = {
   let label = UILabel()
   label.text = "FIRE TUMBLER"
   label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
   label.font = UIFont.systemFont(ofSize: 25, weight: .bold)
   label.translatesAutoresizingMaskIntoConstraints = false
   return label
 }()
 

 lazy var descriptionNewsAndPromotionLabel: UILabel = {
    let label = UILabel()
    label.text = "Beautiful Beautiful Beautiful Beautiful Beautiful Beautiful Beautiful"
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
   label.numberOfLines = 0
    label.textAlignment = .left
    label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
 
 override init(frame: CGRect) {
   super.init(frame: frame)
   setupUI()
 }
 fileprivate func setupUI(){
   backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
   addSubview(newsAndPromotionDetailImgView)
   
   NSLayoutConstraint.activate([
     newsAndPromotionDetailImgView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 50),
     newsAndPromotionDetailImgView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
     newsAndPromotionDetailImgView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
     newsAndPromotionDetailImgView.heightAnchor.constraint(equalTo: safeAreaLayoutGuide.heightAnchor,multiplier: 0.7)
   ])
   
   addSubview(titleNewsAndPromotionLabel)
   NSLayoutConstraint.activate([
     titleNewsAndPromotionLabel.topAnchor.constraint(equalTo: newsAndPromotionDetailImgView.bottomAnchor,constant: 10),
     titleNewsAndPromotionLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
     titleNewsAndPromotionLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10)
   ])
   
   
   addSubview(descriptionNewsAndPromotionLabel)
   
   NSLayoutConstraint.activate([
     descriptionNewsAndPromotionLabel.topAnchor.constraint(equalTo: titleNewsAndPromotionLabel.bottomAnchor,constant: 10),
     descriptionNewsAndPromotionLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
     descriptionNewsAndPromotionLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
     //descriptionRewardPointLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor,constant: -10)
   ])
 }
 
 required init?(coder: NSCoder) {
   fatalError("init(coder:) has not been implemented")
 }
  
}
