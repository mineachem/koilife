//
//  Promotion.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/13/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation

// MARK: - Promotion
struct Promotion: Codable {
    let response: ResponsePromotion?
    let length: Int?
    let results: [ResultPromotion]?
}

// MARK: - Response
struct ResponsePromotion: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Result
struct ResultPromotion: Codable {
    let id: Int?
    let promotion: PromotionData?
    let status, read: Bool?
}

// MARK: - PromotionClass
struct PromotionData: Codable {
    var id: Int?
    var message: String?
    var title: String?
    var promotionDescription: String?
    var timeAlert: Int?
    var expireDate:Date?
    var imageFile: String?
    var image: String?
    var thumbnail: String?
    var view: Int?
    var createById:Int?
    var updatedById:Int?
    var status: Bool?
    var active: Bool?
    var pushed: Bool?
    var postFacebook: Bool?

    enum CodingKeys: String, CodingKey {
        case id, message, title
        case promotionDescription = "description"
        case timeAlert, expireDate, imageFile, image, thumbnail, view,createById,updatedById,status, active, pushed, postFacebook
    }
}
