//
//
//  NewsAndPromotionDetailVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class NewsAndPromotionDetailVC: UIViewController {

  
  var promotionResultData: PromotionData?
  
  lazy var titleNavigationBar: UINavigationBar = {
   let navigationBar = UINavigationBar()
   let navItem = UINavigationItem()
     let label = UILabel()
     label.text = "Reward Points"
     label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     navItem.titleView = label
   navigationBar.setBackgroundImage(UIImage(), for: .default)
   navigationBar.shadowImage = UIImage()
   let backBtn = UIButton(type: .system)
   backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
   backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
   backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
   let leftBarButton = UIBarButtonItem()
   leftBarButton.customView = backBtn
   navItem.leftBarButtonItem = leftBarButton
   navigationBar.setItems([navItem], animated: false)
   navigationBar.translatesAutoresizingMaskIntoConstraints = false
   return navigationBar
   }()
     
   
   lazy var newsandPromotionView = NewsAndPromotionsDetailView()
   
   override func loadView() {
     super.loadView()
     self.view = newsandPromotionView
     
   }
     override func viewDidLoad() {
         super.viewDidLoad()

      setupNavigation()
      setupView()
     }
     

    fileprivate func setupNavigation(){
      
    view.addSubview(titleNavigationBar)
    NSLayoutConstraint.activate([
      titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
    ])
    }
   
   
   @objc private func handleBackTapped(){
     self.dismiss(animated: true)
   }

  fileprivate func setupView(){
    if let coverImageUrl = promotionResultData?.image {
     newsandPromotionView.newsAndPromotionDetailImgView.loadImageUsingUrlString(urlString: "\(KoiService.share.displayImgUrl)\(coverImageUrl)")
    }

    if let titles = promotionResultData?.title {
        self.newsandPromotionView.titleNewsAndPromotionLabel.text = titles.htmlToString
    }
    
    if let descriptions = promotionResultData?.message {
      self.newsandPromotionView.descriptionNewsAndPromotionLabel.text = descriptions.htmlToString
    }
   
  }
  


}
