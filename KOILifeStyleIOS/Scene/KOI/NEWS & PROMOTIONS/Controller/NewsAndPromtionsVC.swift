//
//  NewsAndPromtionsVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/29/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import EzPopup
import Alamofire
import NVActivityIndicatorView

class NewsAndPromtionsVC: UIViewController {
    
    var emptyView = EmptyInboxView()
    var refreshCtr = UIRefreshControl()
    lazy var loading: NVActivityIndicatorView = {
        let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
        loading.translatesAutoresizingMaskIntoConstraints = false
        return loading
    }()

  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    let titleNav = UILabel()
     titleNav.text = "NEWS & PROMOTIONS"
    titleNav.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    navItem.titleView = titleNav
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "left-arrow").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
  
  var newsAndPromtionsTableView:UITableView!
  private let newsAndPromtionIdentifier = "newsAndPromotionsCell"
  var promotionResults = [ResultPromotion]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIndicator()
        emptyView.emptyDescription.text = "News & Promotions has no items."
        view.backgroundColor = .white
        getDataFromService()
        setupNavigationBar()
        setupTableView()
        newsAndPromtionsTableView.backgroundView = loading
    }
    
  fileprivate func setupIndicator(){
       view.addSubview(loading)
       NSLayoutConstraint.activate([
         loading.widthAnchor.constraint(equalToConstant: 40),
         loading.heightAnchor.constraint(equalToConstant: 40),
         loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
       ])

  }
  
  fileprivate func getDataFromService(){
    
    let customerId = UserDefaults.standard.value(forKey: "id") as? Int
    self.showLoading()
    IGNetworkRequest.shareInstance.requestGET(KoiService.share.promotionAllUrl + "?customerId=\(customerId!)&size=10&page=0", success: { (promotionResponse:Promotion) in
       //  print("promotionResponse: \(promotionResponse)")
          if promotionResponse.response?.code == 200 {
            
            self.promotionResults = promotionResponse.results!
            
            if self.promotionResults.count > 0{
                DispatchQueue.main.async {
                  self.newsAndPromtionsTableView.reloadData()
                    self.newsAndPromtionsTableView.backgroundView = .none
                }
            }else{
                self.newsAndPromtionsTableView.backgroundView = self.emptyView
            }
                  
        }else {
           // print(promotionResponse.response?.message ?? "")
            self.newsAndPromtionsTableView.backgroundView = self.emptyView
                }
            self.hideLoading()
            self.refreshCtr.endRefreshing()
        }) { (failure) in
          print(failure)
            self.hideLoading()
            self.refreshCtr.endRefreshing()
            self.newsAndPromtionsTableView.backgroundView = self.emptyView
        }
    
  }
  
  fileprivate func setupNavigationBar(){
      view.addSubview(titleNavigationBar)
      NSLayoutConstraint.activate([
        titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
        titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
      ])
    }
  
  fileprivate func  setupTableView(){
    newsAndPromtionsTableView = UITableView()
    newsAndPromtionsTableView.delegate = self
    newsAndPromtionsTableView.dataSource = self
    refreshCtr.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
    newsAndPromtionsTableView.addSubview(refreshCtr)
    newsAndPromtionsTableView.showsHorizontalScrollIndicator = false
    newsAndPromtionsTableView.showsVerticalScrollIndicator = false
    newsAndPromtionsTableView.separatorStyle = .none
    newsAndPromtionsTableView.register(NewsAndPromtionsCell.self, forCellReuseIdentifier: newsAndPromtionIdentifier)
    newsAndPromtionsTableView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(newsAndPromtionsTableView)
    NSLayoutConstraint.activate([
      newsAndPromtionsTableView.topAnchor.constraint(equalTo: titleNavigationBar.bottomAnchor),
      newsAndPromtionsTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      newsAndPromtionsTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      newsAndPromtionsTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
    ])
  }
   
    func showLoading()
    {
        self.loading.startAnimating()
    }

    func hideLoading()
    {
         loading.stopAnimating()
    }
    
    @objc func refresh() {
        getDataFromService()
    }
  
  @objc private func handleBackTapped(){
    self.dismiss(animated: true)
  }
}

extension NewsAndPromtionsVC:UITableViewDelegate,UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return promotionResults.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let newsAndPromtionCell = tableView.dequeueReusableCell(withIdentifier: newsAndPromtionIdentifier, for: indexPath) as! NewsAndPromtionsCell
    let item = promotionResults[indexPath.row]

        DispatchQueue.main.async {
                if let imageUrl = item.promotion?.thumbnail {
                    newsAndPromtionCell.coverNewsAndPromtionsImgView.sd_setImage(with: URL(string: "\(KoiService.share.displayImgUrl)\(imageUrl)"), placeholderImage: UIImage(named: "img_placeholder"), options: .waitStoreCache, progress: .none, completed: nil)
                    //loadImageUsingUrlString(urlString: "\(KoiService.share.displayImgUrl)\(imageUrl)")
                  //  self.sd_setImage(with: url, placeholderImage: UIImage(named: "img_placeholder"))
            }
        }
      
    
    newsAndPromtionCell.titleOfcoverNewsAndPromotionLabel.text = item.promotion?.title?.htmlToString ?? ""
    newsAndPromtionCell.subtitleOfcoverNewsAndPromotionLabel.text = item.promotion?.message?.htmlToString ?? ""
   
    
    return newsAndPromtionCell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let newsAndPromotionDetailVC = NewsAndPromotionDetailVC()
    newsAndPromotionDetailVC.promotionResultData = promotionResults[indexPath.row].promotion
    newsAndPromotionDetailVC.modalPresentationStyle = .overCurrentContext
    newsAndPromotionDetailVC.modalTransitionStyle = .crossDissolve
    self.present(newsAndPromotionDetailVC, animated: true)
    
  }
  
}
