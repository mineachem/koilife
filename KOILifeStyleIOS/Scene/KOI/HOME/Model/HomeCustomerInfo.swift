//
//  HomeCustomerInfo.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/24/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation

// MARK: - HomeCustomerInfo
struct HomeCustomerInfo: Codable {
    let id: Int?
    let accNumber, accName: String?
    let balance:Double?
    let membershipDate, score, rewardScore: Int?
    let qty: Int?
    let customer: CustomerInfoData?
    let memberType: MemberTypeData?
    let endYear: Int?
    let status: Bool?
    let version: Int?
    let createById, updatedById: String?
}

// MARK: - Customer
struct CustomerInfoData: Codable {
    let id: Int?
    let firstName, lastName: String?
    let fullName: String?
    let userType, phoneNo: String?
    let facebookId: String?
    let email, address: String?
    let dob: Int?
    let gender: String?
    let imageFile: String?
    let confirmCode, photo: String?
    let passcodeLock: String?
    let status: Bool?
    let version: Int?
    let createById, updatedById: String?
    let blocked, passcodeLockOn, confirmCodeExpired: Bool?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case fullName, userType, phoneNo, facebookId, email, address, dob, gender, imageFile, confirmCode, photo, passcodeLock, status, version, createById, updatedById, blocked, passcodeLockOn, confirmCodeExpired
    }
}

// MARK: - MemberType
struct MemberTypeData: Codable {
    let id: Int?
    let type: String?
    let minQty: Int?
    let maxQty: Int?
    let status: Bool?
    let version: Int?
    let createById, updatedById: String?
}

