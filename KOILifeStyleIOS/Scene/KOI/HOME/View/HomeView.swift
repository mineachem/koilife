//
//  HomeView.swift
//  KOI
//
//  Created by MOLIDA LOEUNG on 11/28/19.
//  Copyright © 2019 MOLIDA LOEUNG. All rights reserved.
//

import UIKit

class HomeView: UIView {
      
  lazy var bgHomeImgView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "bgsign-in")
    imageView.contentMode = .scaleAspectFill
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  

  lazy var progressBar: ProgressBar = {
         let v = ProgressBar()
         //v.progress = 90
         v.translatesAutoresizingMaskIntoConstraints = false
         return v
     }()
  
  lazy var usernameLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
   
    label.font = UIFont(name: "Montserrat-SemiBold", size: 23)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  lazy var accountAnSettingLabel: UILabel = {
    let label = UILabel()
    label.text = "Account & Settings"
    label.numberOfLines = 0
    label.textAlignment = .center
    label.font = UIFont(name: "Montserrat-Medium", size: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var cardrewardPointView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    subView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
    subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    subView.layer.shadowOpacity = 1.0
    subView.layer.shadowRadius = 0.0
    subView.layer.masksToBounds = false
    subView.layer.cornerRadius = 10.0
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  

    lazy var balanceNumberLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .red
        label.font = UIFont(name: "Montserrat-SemiBold", size: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label.text = "BALANCE"
        label.textAlignment = .center
        label.font = UIFont(name: "Montserrat-Medium", size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
      
  
    lazy var rewardPointNumberLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        label.numberOfLines = 1
        label.font = UIFont(name: "Montserrat-SemiBold", size: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var rewardPointLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
       // label.text = "REWARD POINTS A gift claim unlimited"
      
      let attributedString = NSMutableAttributedString(string: "REWARD\nPOINT")
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.lineSpacing = 2
      // *** Apply attribute to string ***
      attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
      label.attributedText = attributedString
      label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
      label.numberOfLines = 0
      label.textAlignment = .left
      label.font = UIFont.boldSystemFont(ofSize: 14)
      label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
  

  lazy var subRewardPointLabel: UILabel = {
      let label = UILabel()
      label.textAlignment = .center
     // label.text = "REWARD POINTS A gift claim unlimited"
    
    let attributedString = NSMutableAttributedString(string: "Clain awesome\ngifts")
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 2
    // *** Apply attribute to string ***
    attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
    label.attributedText = attributedString
      label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
      label.numberOfLines = 0
      label.textAlignment = .left
      label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
      
  }()
    
    lazy var blackMembershipNumberLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0.8052856326, green: 0.5708308816, blue: 0.3027378321, alpha: 1)
        label.font = UIFont(name: "Montserrat-Medium", size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    lazy var blackMembershipLabel: UILabel = {
        let label = UILabel()
        label.text = "BALANCE"
         label.textAlignment = .left
        label.font = UIFont(name: "Montserrat-Medium", size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
  
  lazy var subBlackMembershipLabel: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
         return label
     }()
    
    
    lazy var lineMiddleStackLine: UIView = {
        let line = UIView()
        line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
        
    }()
    
    lazy var inboxButton: UIButton = {
        
        let button = UIButton(type: .system)
       button.setTitle("INBOX", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
       button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
       button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
       button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
       button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
       button.layer.shadowOpacity = 1.0
       button.layer.shadowRadius = 0.0
       button.layer.masksToBounds = false
       button.layer.cornerRadius = 10.0
       button.translatesAutoresizingMaskIntoConstraints = false
      return button
    }()
    
    lazy var accountButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("ACCOUNT", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
      // Shadow and Radius
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.cornerRadius = 10.0
        button.layer.masksToBounds = false
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
        
    }()
    
    lazy var newsPromotionsButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("NEWS & PROMOTIONS", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 10.0
       
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
        
    }()
    
  
  lazy var contentAccountSettingVStack:UIStackView = {
    let stackView = UIStackView(arrangedSubviews: [usernameLabel,accountAnSettingLabel])
       stackView.axis = .vertical
       stackView.alignment = .center
       stackView.distribution = .equalSpacing
       stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }()
  
  
  lazy var contentRewardStackView:UIStackView = {
    let stackView = UIStackView(arrangedSubviews: [rewardPointLabel,subRewardPointLabel])
       stackView.axis = .vertical
       stackView.alignment = .leading
       stackView.distribution = .equalSpacing
       stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }()
  
  lazy var contentMemberStackView: UIStackView = {
    let stackView = UIStackView(arrangedSubviews: [blackMembershipLabel,subBlackMembershipLabel])
         stackView.axis = .vertical
         stackView.alignment = .leading
         stackView.distribution = .equalSpacing
         stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }()
  
    override init(frame: CGRect) {
        super.init(frame: frame)
      
        
        setupView()
    }
    
  
    func setupView(){
      
      addSubview(bgHomeImgView)
      
      NSLayoutConstraint.activate([
        bgHomeImgView.topAnchor.constraint(equalTo: topAnchor),
        bgHomeImgView.leadingAnchor.constraint(equalTo: leadingAnchor),
        bgHomeImgView.trailingAnchor.constraint(equalTo: trailingAnchor),
        bgHomeImgView.bottomAnchor.constraint(equalTo: bottomAnchor)
      ])
      
      addSubview(progressBar)
      
      NSLayoutConstraint.activate([
        progressBar.topAnchor.constraint(equalTo:safeAreaLayoutGuide.topAnchor,constant: 50),
        progressBar.centerXAnchor.constraint(equalTo: centerXAnchor),
        progressBar.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height * 0.30),
        progressBar.widthAnchor.constraint(equalToConstant: 200),
      ])
      
      addSubview(contentAccountSettingVStack)
      NSLayoutConstraint.activate([
        contentAccountSettingVStack.topAnchor.constraint(equalTo: progressBar.bottomAnchor,constant: 10),
        contentAccountSettingVStack.centerXAnchor.constraint(equalTo: centerXAnchor),
        contentAccountSettingVStack.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 20),
        contentAccountSettingVStack.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -20),
      ])
      addSubview(cardrewardPointView)
      cardrewardPointView.addSubview(contentRewardStackView)
      cardrewardPointView.addSubview(lineMiddleStackLine)
      cardrewardPointView.addSubview(contentMemberStackView)
      cardrewardPointView.addSubview(rewardPointNumberLabel)
      cardrewardPointView.addSubview(balanceNumberLabel)
      NSLayoutConstraint.activate([
        cardrewardPointView.topAnchor.constraint(equalTo: contentAccountSettingVStack.bottomAnchor,constant: 20),
        cardrewardPointView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
       // cardrewardPointView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
        cardrewardPointView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
        cardrewardPointView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20),
        
        cardrewardPointView.heightAnchor.constraint(equalToConstant: 100),
        contentRewardStackView.topAnchor.constraint(equalTo: cardrewardPointView.topAnchor,constant: 10),
        contentRewardStackView.leadingAnchor.constraint(equalTo: cardrewardPointView.leadingAnchor,constant: 10),
        lineMiddleStackLine.centerXAnchor.constraint(equalTo: cardrewardPointView.centerXAnchor),
        lineMiddleStackLine.topAnchor.constraint(equalTo: cardrewardPointView.topAnchor,constant: 5),
        lineMiddleStackLine.bottomAnchor.constraint(equalTo: cardrewardPointView.bottomAnchor,constant: -5),
        lineMiddleStackLine.widthAnchor.constraint(equalToConstant: 1),
        
        contentMemberStackView.leadingAnchor.constraint(equalTo: lineMiddleStackLine.leadingAnchor,constant: 10),
        contentMemberStackView.topAnchor.constraint(equalTo: cardrewardPointView.topAnchor,constant: 10),
        
        rewardPointNumberLabel.rightAnchor.constraint(equalTo: lineMiddleStackLine.rightAnchor,constant: -10),
        rewardPointNumberLabel.bottomAnchor.constraint(equalTo: cardrewardPointView.bottomAnchor),
        
        balanceNumberLabel.rightAnchor.constraint(equalTo: cardrewardPointView.rightAnchor,constant: -10),
        balanceNumberLabel.bottomAnchor.constraint(equalTo: cardrewardPointView.bottomAnchor),
      ])


      let contentHomeButton = UIStackView(arrangedSubviews: [inboxButton,accountButton,newsPromotionsButton])
      contentHomeButton.axis = .vertical
      contentHomeButton.alignment = .fill
      contentHomeButton.distribution = .fillEqually
      contentHomeButton.spacing = 10
      contentHomeButton.translatesAutoresizingMaskIntoConstraints = false
      
      addSubview(contentHomeButton)

      NSLayoutConstraint.activate([
        contentHomeButton.topAnchor.constraint(equalTo: cardrewardPointView.bottomAnchor,constant: 20),
        contentHomeButton.centerXAnchor.constraint(equalTo: centerXAnchor),
        contentHomeButton.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 20),
        contentHomeButton.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -20),
        inboxButton.heightAnchor.constraint(equalToConstant: 40),
        accountButton.heightAnchor.constraint(equalTo: inboxButton.heightAnchor, multiplier: 1),
        newsPromotionsButton.heightAnchor.constraint(equalTo: inboxButton.heightAnchor, multiplier: 1)
      ])
      
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
