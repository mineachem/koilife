//
//  HomeVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/27/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class HomeVC: UIViewController {

  lazy var homeView = HomeView()
   var countFired: CGFloat = 0
  override func loadView() {
    super.loadView()
    self.view = homeView
  }
    
  let scoreSection = UserDefaults.standard.value(forKey: "score") as? Int
  let typeMemberSection =  UserDefaults.standard.value(forKey: "memberType") as? String
  let expireYearSection = UserDefaults.standard.value(forKey: "endyear") as? Int
  let balanceSection = UserDefaults.standard.value(forKey: "balance") as? Double
  let rewardpointSection = UserDefaults.standard.value(forKey: "rewardscore") as? Int
  let accName = UserDefaults.standard.value(forKey: "accName") as? String
  let firstname = UserDefaults.standard.value(forKey: "firstname") as? String
  let lastname = UserDefaults.standard.value(forKey: "lastname") as? String
  
  let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
  
    var getCustomerInfo:HomeCustomerInfo?
//    {
//        didSet{
//            setupView()
//        }
//    }
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadAllBackground()
    }
  //MARK:Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIndicator()
        getDataFromService()
        setupView()
        view.backgroundColor = #colorLiteral(red: 0.9608082175, green: 0.9570518136, blue: 0.9485346675, alpha: 1)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUserHasUpdatedProfile), name: NSNotification.Name("userHasUpdatedProfile"), object: nil)
        setupEvent()
        
       
    }
    
  fileprivate func setupIndicator(){
    
       loading.translatesAutoresizingMaskIntoConstraints = false
        
       view.addSubview(loading)
       NSLayoutConstraint.activate([
         loading.widthAnchor.constraint(equalToConstant: 40),
         loading.heightAnchor.constraint(equalToConstant: 40),
         loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
       ])
    
  }
  func showLoading()
  {
    loading.startAnimating()
    loading.isHidden = false
  }

  func hideLoading()
  {
    loading.isHidden = true
    loading.stopAnimating()
  }
  
    @objc func handleUserHasUpdatedProfile(){
        self.getDataFromService()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
  

  var localImages = [Data]()
  func loadAllBackground()
  {
    
        IGNetworkRequest.shareInstance.requestGET(KoiService.share.getAllBackgroundURL, success: { (response: SlideShowResponse) in
               
                if response.response.code == 200 {
                  for slide in response.results {
                    print("response \(response)")
                      if slide.backgroundTypeApp.name.contains("Slide Show") {
                          let images = slide.image.components(separatedBy: [","])
                          for image in images {
                              let url = URL(string: "\(KoiService.share.BASE_IMAGE)\(image)")
                                   if let data = try? Data(contentsOf: url!) {
                                      self.localImages.append(data)
          
                                   }
                          }
                    
                  let version = UserDefaultManager.sharedInstance.getData(key: "SlideVersion")
          
                  if version.isEmpty {
                      UserDefaultManager.sharedInstance.saveData(key: "SlideVersion", value: "\(slide.backgroundTypeApp.version)")
                       UserDefaultManager.sharedInstance.saveData(key: KoiService.share.SLIDESHOW, value: self.localImages)
                  }

                  if version != String(slide.backgroundTypeApp.version) {
                    UserDefaultManager.sharedInstance.saveData(key: KoiService.share.SLIDESHOW, value: self.localImages)
                  }
       
                      } else if slide.backgroundTypeApp.name.contains("Background Home Page") {
                       
                        let versionHome = UserDefaultManager.sharedInstance.getData(key: "HomePagerVersion")
                                 
                   
                        let homeUrl = URL(string: "\(KoiService.share.BASE_IMAGE)\(slide.image)")
                         if let homeData = try? Data(contentsOf: homeUrl!) {
                           self.homeView.bgHomeImgView.image = UIImage(data: homeData)
                            if versionHome.isEmpty {
                                   UserDefaultManager.sharedInstance.saveData(key: "HomePagerVersion", value: "\(slide.backgroundTypeApp.version)")
                                    UserDefaultManager.sharedInstance.saveData(key: KoiService.share.HOMEPAGE, value: homeData)
                               }

                               if versionHome != String(slide.backgroundTypeApp.version) {
                                 UserDefaultManager.sharedInstance.saveData(key: KoiService.share.HOMEPAGE, value: homeData)
                               }else {
                                  }
                         }
                        
                        
                        
                       
                        
                    }
              }
                
                    
                }
            }) { (err) in
                print(err)
            }
                     
  }
    


    
     func getDataFromService(){
    let walledtId = UserDefaults.standard.value(forKey: "customerWalletId") as? Int
        
        IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.getCustomerInfoURL + "?walletId=\(walledtId!)", params:nil, success: { (customerInfo:HomeCustomerInfo) in
            self.getCustomerInfo = customerInfo
            
            if let balance = self.getCustomerInfo?.balance {
                print("balance \(balance)")
            self.homeView.balanceNumberLabel.text = "$" + String(format: "%.2f", balance)
           }
            self.homeView.usernameLabel.text = "\(customerInfo.customer?.lastName ?? "")".uppercased() + " " + "\(customerInfo.customer?.firstName ?? "")".uppercased()
          
          if let rewardpoint = self.getCustomerInfo?.rewardScore {
            self.homeView.rewardPointNumberLabel.text = rewardpoint.description
           }
    
           // self.homeView.rewardPointNumberLabel.text = rewardpoint.description
        }) { (failure) in
            print(failure)
        }

    }
  //MARK: VIEW
func setupView(){
    
    Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (timer) in
              self.countFired += 1

              DispatchQueue.main.async {
                
                if self.getCustomerInfo != nil {
          
                                if let typeMember = self.getCustomerInfo?.memberType?.type{
                                   
                                    self.homeView.progressBar.typeLabel.text = typeMember
                                    print("typeMember \(typeMember)")
                                    switch typeMember{
                                    case "BLACK MEMBERSHIP":
                                      
                                      if let score = self.getCustomerInfo?.score{
                                               self.homeView.progressBar.progress = min(CGFloat(score)/150 , 1)
                                              self.homeView.progressBar.scoreValue.text = score.description
                                          }
                                         self.homeView.progressBar.gradientLayer!.colors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor, #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor]
                                       self.homeView.progressBar.typeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                      self.homeView.balanceLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                      self.homeView.balanceNumberLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                      self.homeView.blackMembershipLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                    case "SILVER MEMBERSHIP":
                                      if let score = self.getCustomerInfo?.score{
                                           self.homeView.progressBar.progress = min(CGFloat(score)/300 , 1)
                                          self.homeView.progressBar.scoreValue.text = score.description
                                      }
                                       self.homeView.progressBar.gradientLayer!.colors = [#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor]
                                          if let exspireYears = self.getCustomerInfo?.endYear {
                                              let getdateString = exspireYears.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
                                                let dateFormat = DateFormatter()
                                                dateFormat.dateFormat = "MMM dd,yyyy"
                                                let dateString = dateFormat.string(from: getdateString)
                                             self.homeView.balanceLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                                             self.homeView.progressBar.typeLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                                             self.homeView.balanceNumberLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                                             self.homeView.blackMembershipLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                                             self.homeView.progressBar.exprieDate.text = dateString
                                            
                                           
                                            }
                                      case "GOLD MEMBERSHIP":
                                        
                                        if let score = self.getCustomerInfo?.score{
                                             self.homeView.progressBar.progress = min(CGFloat(score)/301 , 1)
                                            self.homeView.progressBar.scoreValue.text = score.description
                                        }
                                        self.homeView.progressBar.gradientLayer!.colors = [#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1).cgColor, #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1).cgColor]
                                          if let exspireYears = self.getCustomerInfo?.endYear {
                                            let getdateString = exspireYears.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
                                                              let dateFormat = DateFormatter()
                                                              dateFormat.dateFormat = "MMM dd,yyyy"
                                                              let dateString = dateFormat.string(from: getdateString)
                                              self.homeView.balanceLabel.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                                             self.homeView.progressBar.typeLabel.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                                              self.homeView.balanceNumberLabel.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                                              self.homeView.blackMembershipLabel.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                                           self.homeView.progressBar.exprieDate.text = dateString
                            
                                          }
                                    default: return
                                    }
                                    
                                  

                }
                    
                     if let balance = self.getCustomerInfo?.balance {
                             self.homeView.balanceNumberLabel.text = "$" + String(format: "%.2f", balance)
                        }
                    
                    if let rewardpoint = self.getCustomerInfo?.rewardScore {
                        self.homeView.rewardPointNumberLabel.text = rewardpoint.description
                        }
                }else {
                  
                  if let typeMember = self.typeMemberSection{
                                self.homeView.progressBar.typeLabel.text = typeMember
                                      switch typeMember{
                                      case "BLACK MEMBERSHIP":
                                        
                                        self.homeView.progressBar.progress = min(CGFloat(self.scoreSection!)/150 , 1)
                                        self.homeView.progressBar.scoreValue.text = self.scoreSection!.description
                                        
                                          self.homeView.progressBar.gradientLayer!.colors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor, #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor]
                                         
                                          self.homeView.balanceLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                          self.homeView.balanceNumberLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                          self.homeView.blackMembershipLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                      case "SILVER MEMBERSHIP":
                                        self.homeView.progressBar.progress = min(CGFloat(self.scoreSection!)/300 , 1)
                                        self.homeView.progressBar.scoreValue.text = self.scoreSection!.description
                                          self.homeView.progressBar.gradientLayer!.colors = [#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor]
                                        
                                        if self.expireYearSection != nil {
                                           let getdateString = self.expireYearSection!.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
                                                let dateFormat = DateFormatter()
                                                dateFormat.dateFormat = "MMM dd,yyyy"
                                                let dateString = dateFormat.string(from: getdateString)
                                          self.homeView.progressBar.title.text = "Expires on"
                                          self.homeView.progressBar.exprieDate.text = dateString
                                          self.homeView.progressBar.typeLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                                           self.homeView.progressBar.exprieDate.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                          self.homeView.balanceLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                                          self.homeView.balanceNumberLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                                          self.homeView.blackMembershipLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                                          }
                                      case "GOLD MEMBERSHIP":
                                        
                                        self.homeView.progressBar.progress = min(CGFloat(self.scoreSection!)/301 , 1)
                                        self.homeView.progressBar.scoreValue.text = self.scoreSection!.description
                                        self.homeView.progressBar.gradientLayer!.colors = [#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1).cgColor, #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1).cgColor]
                                        
                                        if self.expireYearSection != nil {
                                            let getdateString = self.expireYearSection!.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
                                                 let dateFormat = DateFormatter()
                                                 dateFormat.dateFormat = "MMM dd,yyyy"
                                                 let dateString = dateFormat.string(from: getdateString)
                                          self.homeView.progressBar.title.text = "Expires on"
                                            self.homeView.progressBar.exprieDate.text = dateString
                                          self.homeView.progressBar.typeLabel.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                                          self.homeView.progressBar.exprieDate.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                          self.homeView.balanceLabel.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                                          self.homeView.balanceNumberLabel.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                                          self.homeView.blackMembershipLabel.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                                           }
                                      default: return
                                      
                    }
                    
                  //  if  self.balanceSection != nil {
                    if let myBalance = self.getCustomerInfo?.balance {
                        self.homeView.balanceNumberLabel.text = "$" + String(format: "%.2f", myBalance)
                    }
                    
                   //     }
                       
                       if self.rewardpointSection != nil {
                         self.homeView.rewardPointNumberLabel.text = self.rewardpointSection!.description
                        }
                       
                  //  self.homeView.usernameLabel.text = "\(self.lastname!)\(self.firstname!)".uppercased()
                    self.homeView.usernameLabel.text = "\(self.lastname ?? "")".uppercased() + " " + "\(self.firstname ?? "")".uppercased()
                  }
                  
          }

                if self.homeView.progressBar.progress == 1 {
                  timer.invalidate()
                }
              }
          }
  }
  
  //MARK:EVENT
  fileprivate func setupEvent(){
    homeView.inboxButton.addTarget(self, action: #selector(handleInboxTapped), for: .touchUpInside)
    homeView.accountButton.addTarget(self, action: #selector(handleAccountTapped), for: .touchUpInside)
    homeView.newsPromotionsButton.addTarget(self, action: #selector(handleNewsPromotionsTapped), for: .touchUpInside)
    
    let accountTapped = UITapGestureRecognizer(target: self, action: #selector(handleAccountSettingTapped))
    homeView.contentAccountSettingVStack.addGestureRecognizer(accountTapped)
    homeView.contentAccountSettingVStack.isUserInteractionEnabled = true
    
    let rewardTapped = UITapGestureRecognizer(target: self, action: #selector(handleRewardTapped))
    homeView.contentRewardStackView.addGestureRecognizer(rewardTapped)
    homeView.contentRewardStackView.isUserInteractionEnabled = true
    
    
    let memberTapped = UITapGestureRecognizer(target: self, action: #selector(handleMemberTapped))
//       homeView.contentMemberStackView.addGestureRecognizer(memberTapped)
//       homeView.contentMemberStackView.isUserInteractionEnabled = true
    
    homeView.progressBar.addGestureRecognizer(memberTapped)
    homeView.progressBar.isUserInteractionEnabled = true
    
  }
  
  @objc private func handleRewardTapped(){
  
      let rewardPointVC = RewardPointsVC()
         rewardPointVC.modalPresentationStyle = .overCurrentContext
         rewardPointVC.modalTransitionStyle = .crossDissolve
         self.present(rewardPointVC, animated: true)
    
  }
  
  @objc private func handleMemberTapped(){
    let membershipVC = MembershipVC()
    membershipVC.modalPresentationStyle = .overCurrentContext
    membershipVC.modalTransitionStyle = .crossDissolve
    self.present(membershipVC, animated: true)
    
  }
  
  @objc private func handleAccountSettingTapped(){
    
      let accountAndSettingVC = AccountAndSettingsVC()
    ///accountAndSettingVC.getCustomerInfo = getCustomerInfo
      accountAndSettingVC.modalPresentationStyle = .overCurrentContext
      accountAndSettingVC.modalTransitionStyle = .crossDissolve
      self.present(accountAndSettingVC, animated: true)
    
   // self.navigationController?.pushViewController(accountAndSettingVC, animated: true)
  }
  
  @objc private func handleInboxTapped(){
    
    let inboxVC = InboxVC()
    inboxVC.modalPresentationStyle = .overCurrentContext
    inboxVC.modalTransitionStyle = .crossDissolve
    self.present(inboxVC, animated: true)
    
  }
  
  @objc private func handleprofileTapped(){
   
    let acccountSettingVC = AccountAndSettingsVC()
    acccountSettingVC.modalPresentationStyle = .overCurrentContext
    acccountSettingVC.modalTransitionStyle = .crossDissolve
    self.present(acccountSettingVC, animated: true)
    
  }

  @objc private func handleAccountTapped(){
    
    let accountVC = AccountTransitionVC()
    accountVC.modalPresentationStyle = .overCurrentContext
    accountVC.modalTransitionStyle = .crossDissolve
    self.present(accountVC, animated: true)
    
  }
  
  @objc private func handleNewsPromotionsTapped(){
   
    let newsPromotionVC = NewsAndPromtionsVC()
    newsPromotionVC.modalPresentationStyle = .overCurrentContext
    newsPromotionVC.modalTransitionStyle = .crossDissolve
    self.present(newsPromotionVC, animated: true)
    
  }
  
}
