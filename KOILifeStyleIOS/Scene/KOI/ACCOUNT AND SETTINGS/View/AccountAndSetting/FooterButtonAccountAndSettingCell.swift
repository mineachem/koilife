//
//  FooterButtonAccountAndSettingCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/6/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class FooterButtonAccountAndSettingCell: UITableViewCell {

  lazy var signOutAccountAndSettingBtn: UIButton = {
  let button = UIButton(type: .system)
  button.setTitle("SIGN OUT", for: .normal)
  button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
  button.titleLabel?.font = UIFont.systemFont(ofSize: 14,weight: .medium)
  button.layer.cornerRadius = 15
  button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
  button.translatesAutoresizingMaskIntoConstraints = false
  return button
  }()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    backgroundColor = .none
    selectionStyle = .none
    setupUI()
  }
  
  fileprivate func setupUI(){
    addSubview(signOutAccountAndSettingBtn)
    
    NSLayoutConstraint.activate([
      signOutAccountAndSettingBtn.topAnchor.constraint(equalTo: topAnchor,constant: 10),
      signOutAccountAndSettingBtn.heightAnchor.constraint(equalToConstant: 40),
      signOutAccountAndSettingBtn.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 60),
      signOutAccountAndSettingBtn.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -60),
      signOutAccountAndSettingBtn.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
