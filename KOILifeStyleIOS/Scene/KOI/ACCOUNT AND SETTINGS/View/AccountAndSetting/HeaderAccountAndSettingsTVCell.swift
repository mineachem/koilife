//
//  HeaderAccountAndSettingsTVCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/29/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class HeaderAccountAndSettingsTVCell: UITableViewCell {

  
  var getCustomerInfo:ResultCustomerInfo?{
    didSet{
      switch getCustomerInfo?.userType {
      case "Facebook":
        if getCustomerInfo?.photo != nil {
          
                  let profilePhoto = URL(string:getCustomerInfo!.photo!)
                      DispatchQueue.global().async {
                        guard let imageData = try? Data(contentsOf: profilePhoto!) else { return }
                                       let image = UIImage(data: imageData)
                           DispatchQueue.main.async {
                            if image != nil {
                              self.profileImgView.image = image
                            }else {
                              self.profileImgView.image = #imageLiteral(resourceName: "img_default_user_male")
                            }
                           }
                       }
            }
      case "Phone":
        if getCustomerInfo?.photo != nil {
          
                  let profilePhoto = URL(string:KoiService.share.displayImgUrl)?.appendingPathComponent(getCustomerInfo!.photo!)
          
                      DispatchQueue.global().async {
                        guard let imageData = try? Data(contentsOf: profilePhoto!) else { return }
                                       let image = UIImage(data: imageData)
                           DispatchQueue.main.async {
                            if image != nil {
                              self.profileImgView.image = image
                            }else {
                              self.profileImgView.image = #imageLiteral(resourceName: "img_default_user_male")
                            }
                           }
                    }
            }
      default:
        print("nothing")
      }
    
      
          if let firstname = getCustomerInfo?.firstName ,let lastname = getCustomerInfo?.lastName{
            usernameLabel.text = "\(lastname) \(firstname)"
            
          }else {
            usernameLabel.text = ""
          }
    }
  }
  
  lazy var profileImgView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "img_default_user_male")
    imageView.contentMode = .scaleToFill
    imageView.layer.masksToBounds = true
    imageView.layer.cornerRadius = 40
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  lazy var usernameLabel: UILabel = {
    let label = UILabel()
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.numberOfLines = 0
    label.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
    label.textAlignment = .center
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
    return label
  }()

  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    selectionStyle = .none
    backgroundColor = .none
    
     addSubview(profileImgView)
    NSLayoutConstraint.activate([
      profileImgView.topAnchor.constraint(equalTo: topAnchor,constant: 30),
      profileImgView.widthAnchor.constraint(equalToConstant: 80),
      profileImgView.heightAnchor.constraint(equalToConstant: 80),
      profileImgView.centerXAnchor.constraint(equalTo: centerXAnchor)
    ])
    
    addSubview(usernameLabel)
    NSLayoutConstraint.activate([
      usernameLabel.topAnchor.constraint(equalTo: profileImgView.bottomAnchor,constant: 5),
      usernameLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
      usernameLabel.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -30)
    ])

  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
