//
//  EditProfileInfoTVCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/29/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AccountAndSettingsTVCell: UITableViewCell {

  lazy var backgroundTitle: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    subView.layer.cornerRadius = 10
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.text = "Edit Profile Info"
    label.font = UIFont(name: "Montserrat-Medium", size: 14)!
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var arrowAccountImgView: UIImageView = {
    let image = UIImageView()
    image.image = #imageLiteral(resourceName: "ic_list_arrow_orange")
    image.contentMode = .scaleAspectFit
    image.layer.masksToBounds = true
    image.translatesAutoresizingMaskIntoConstraints = false
    return image
  }()
  
  lazy var circleView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.8559798598, green: 0.6504896879, blue: 0.2624273896, alpha: 1)
    subView.layer.cornerRadius = 5
    subView.layer.masksToBounds = true
    subView.contentMode = .scaleToFill
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    backgroundColor = .none
    addSubview(backgroundTitle)
    NSLayoutConstraint.activate([
      backgroundTitle.heightAnchor.constraint(equalToConstant: 40),
      backgroundTitle.topAnchor.constraint(equalTo: topAnchor),
      backgroundTitle.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 30),
      backgroundTitle.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -30),
      backgroundTitle.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
    ])
    
    backgroundTitle.addSubview(titleLabel)
    backgroundTitle.addSubview(circleView)
    
    NSLayoutConstraint.activate([
      titleLabel.centerXAnchor.constraint(equalTo: backgroundTitle.centerXAnchor),
      titleLabel.leadingAnchor.constraint(equalTo: backgroundTitle.leadingAnchor,constant: 10),
      titleLabel.topAnchor.constraint(equalTo: backgroundTitle.topAnchor,constant: 10),
      titleLabel.trailingAnchor.constraint(equalTo: backgroundTitle.trailingAnchor,constant: -10),
      titleLabel.bottomAnchor.constraint(equalTo: backgroundTitle.bottomAnchor,constant: -10),
      
      circleView.rightAnchor.constraint(equalTo: backgroundTitle.rightAnchor,constant: -10),
      circleView.topAnchor.constraint(equalTo: backgroundTitle.topAnchor,constant: 15),
      circleView.widthAnchor.constraint(equalToConstant: 10),
      circleView.heightAnchor.constraint(equalToConstant: 10)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
