//
//  EditProfileHeaderCell.swift
//  KOI
//
//  Created by MOLIDA LOEUNG on 11/29/19.
//  Copyright © 2019 MOLIDA LOEUNG. All rights reserved.
//

import UIKit

protocol UploadProfileDelegate:class {
  func uploadProfile()
}
class EditProfileHeaderCell: UITableViewCell {
  
  weak var uploadProfileDelegate:UploadProfileDelegate?
  
  var getCustomerInfo:ResultCustomerInfo?{
    didSet{
      switch getCustomerInfo?.userType {
      case "Facebook":
        
       if let photo = getCustomerInfo?.photo{
          let profileUrl = URL(string:photo)
               DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: profileUrl!) else { return }
              let image = UIImage(data: imageData)
              DispatchQueue.main.async {
                  if image != nil {
                      self.profileImgView.image = image
                 
                  }else {
                      self.profileImgView.image = #imageLiteral(resourceName: "img_default_user_male")
                       }
                     }
               }
            }else {
            profileImgView.image = #imageLiteral(resourceName: "img_default_user_male")
          }
      case "Phone":
        
         if let photo = getCustomerInfo?.photo{
                      let profileUrl = URL(string: KoiService.share.displayImgUrl)?.appendingPathComponent(photo)
                   DispatchQueue.global().async {
                guard let imageData = try? Data(contentsOf: profileUrl!) else { return }
                  let image = UIImage(data: imageData)

                  DispatchQueue.main.async {
                      if image != nil {
                          self.profileImgView.image = image
                     
                      }else {
                          self.profileImgView.image = #imageLiteral(resourceName: "img_default_user_male")
                           }
                         }
                   }
                  }else {
                        profileImgView.image = #imageLiteral(resourceName: "img_default_user_male")
                    }
      default:
        print("hello")
      }
              
        if let firstname = getCustomerInfo?.firstName,let lastname = getCustomerInfo?.lastName {
              firstNameLabel.text = "\(firstname)\(lastname)"
          }
    }
  }
    lazy var profileImgView: UIImageView = {
        let imageView = UIImageView()
        //imageView.image = #imageLiteral(resourceName: "img_default_user_female")
        imageView.contentMode = .scaleToFill
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 40
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
      }()
      
      lazy var firstNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.numberOfLines = 0
        label.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
      }()
      
      
      override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = .none
        
        setupUI()
        setupEvent()
      }
  
    fileprivate func setupUI(){
        addSubview(profileImgView)
           NSLayoutConstraint.activate([
             profileImgView.topAnchor.constraint(equalTo: topAnchor,constant: 10),
             profileImgView.widthAnchor.constraint(equalToConstant: 80),
             profileImgView.heightAnchor.constraint(equalToConstant: 80),
             profileImgView.centerXAnchor.constraint(equalTo: centerXAnchor)
           ])
           
           addSubview(firstNameLabel)
           NSLayoutConstraint.activate([
             firstNameLabel.topAnchor.constraint(equalTo: profileImgView.bottomAnchor),
             firstNameLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
             firstNameLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
           ])
    }
  
  fileprivate func setupEvent(){
    let profileTapped = UITapGestureRecognizer(target: self, action: #selector(handleUploadProfileTapped))
    profileImgView.addGestureRecognizer(profileTapped)
    profileImgView.isUserInteractionEnabled = true
  }
      
  @objc private func handleUploadProfileTapped(){
    uploadProfileDelegate?.uploadProfile()
  }
      required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
      }
      
      
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }

    
}
