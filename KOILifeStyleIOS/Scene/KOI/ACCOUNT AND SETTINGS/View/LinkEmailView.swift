//
//  LinkEmailView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/5/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class LinkEmailView: UIView {

  lazy var bgLinkEmailView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .none
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var cardLinkEmailView: UIView = {
     let subView = UIView()
     subView.backgroundColor = .white
     subView.layer.cornerRadius = 10
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
   
  
  lazy var crosImagView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "ic_close")
    imageView.contentMode = .scaleAspectFill
    imageView.layer.borderWidth = 1
    imageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    imageView.layer.masksToBounds = true
    imageView.layer.cornerRadius = 13
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  lazy var infoimagView: UIImageView = {
    let imgView = UIImageView()
    imgView.image = #imageLiteral(resourceName: "info")
    imgView.contentMode = .scaleAspectFit
    imgView.layer.masksToBounds = true
    imgView.translatesAutoresizingMaskIntoConstraints = false
    return imgView
  }()
  
   lazy var descriptionLinkEmailLabel: UILabel = {
      let label = UILabel()
      label.text = "You can login with email by link your email address to the current KOI Account"
      label.textAlignment = .center
     label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     label.numberOfLines = 0
     label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
     label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
   
   lazy var  emailLinkEmailLabel: UILabel = {
      let label = UILabel()
      label.text = "Email"
      label.textAlignment = .left
      label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
      label.font = UIFont.systemFont(ofSize: 12, weight: .thin)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
   
   lazy var emailLinkTextField: UITextField = {
      let textField = UITextField()
      textField.placeholder = "Email"
    textField.tag = 0
    textField.autocapitalizationType = .none
      textField.font = UIFont.systemFont(ofSize: 14)
     textField.keyboardType = .emailAddress
      textField.translatesAutoresizingMaskIntoConstraints = false
      return textField
    }()
   
   lazy var bottomLineEmailView: UIView = {
     let subView = UIView()
     subView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
   
  
  lazy var confirmEmailLinkLabel: UILabel = {
    let label = UILabel()
    label.text = "Confirm Email"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 12, weight: .thin)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var  requirementLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
    label.textColor = .red
    label.isHidden = true
    label.font = UIFont.systemFont(ofSize: 12, weight: .thin)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var confirmEmailLinkTextField: UITextField = {
     let textField = UITextField()
     textField.placeholder = "Confirm Email"
     textField.font = UIFont.systemFont(ofSize: 14)
    textField.autocapitalizationType = .none
    textField.tag = 1
    textField.keyboardType = .emailAddress
     textField.translatesAutoresizingMaskIntoConstraints = false
     return textField
   }()
  
  lazy var bottomconfirmEmailLinkTextFieldView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
   lazy var requireMessageLabel: UILabel = {
     let label = UILabel()
     label.isHidden = true
     label.textColor = .red
     label.textAlignment = .center
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
   
   
    lazy var submitBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("SUBMIT", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.layer.cornerRadius = 15
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
      }()
   
   lazy var loginBtn: UIButton = {
     let button = UIButton(type: .system)
     button.setTitle("Login", for: .normal)
     button.setTitleColor(.black, for: .normal)
     button.titleLabel?.font = UIFont.systemFont(ofSize: 14,weight: .bold)
     button.translatesAutoresizingMaskIntoConstraints = false
     return button
   }()
   
   lazy var alreadyLabel: UILabel = {
     let label = UILabel()
     label.text = "Already have an account?"
     label.textAlignment = .center
     label.font = UIFont.systemFont(ofSize: 14)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
   
   lazy var bgSignUpImagView: UIImageView = {
      let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "img_bg_account_setting")
        imageView.contentMode = .scaleToFill
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
     return imageView
   }()
   
   override init(frame: CGRect) {
     super.init(frame: frame)
     setupUI()
   }
   
   fileprivate func setupUI(){
     
//      addSubview(bgLinkEmailView)
//
//    NSLayoutConstraint.activate([
//      bgLinkEmailView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
//      bgLinkEmailView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
//      bgLinkEmailView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
//      bgLinkEmailView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
//    ])
    
   addSubview(cardLinkEmailView)
   
    cardLinkEmailView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 20).isActive = true
    cardLinkEmailView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -20).isActive = true
    cardLinkEmailView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    cardLinkEmailView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
     
    
    addSubview(crosImagView)
    NSLayoutConstraint.activate([
      crosImagView.topAnchor.constraint(equalTo: cardLinkEmailView.topAnchor,constant: -10),
      crosImagView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
      crosImagView.widthAnchor.constraint(equalToConstant: 25),
      crosImagView.heightAnchor.constraint(equalToConstant: 25)
    ])
    
    cardLinkEmailView.addSubview(infoimagView)
    
    NSLayoutConstraint.activate([
      infoimagView.topAnchor.constraint(equalTo: cardLinkEmailView.topAnchor,constant: 50),
      infoimagView.centerXAnchor.constraint(equalTo: cardLinkEmailView.centerXAnchor),
      infoimagView.widthAnchor.constraint(equalToConstant: 25),
      infoimagView.heightAnchor.constraint(equalToConstant: 25)
    ])
    
    cardLinkEmailView.addSubview(descriptionLinkEmailLabel)
    NSLayoutConstraint.activate([
      descriptionLinkEmailLabel.topAnchor.constraint(equalTo: infoimagView.bottomAnchor,constant: 10),
      descriptionLinkEmailLabel.centerXAnchor.constraint(equalTo: cardLinkEmailView.centerXAnchor),
      descriptionLinkEmailLabel.leadingAnchor.constraint(equalTo: cardLinkEmailView.leadingAnchor,constant: 50),
      descriptionLinkEmailLabel.trailingAnchor.constraint(equalTo: cardLinkEmailView.trailingAnchor,constant: -50),
    ])
     
       let contentEmailHStack = UIStackView(arrangedSubviews: [emailLinkEmailLabel,emailLinkTextField])
       contentEmailHStack.axis = .horizontal
       contentEmailHStack.alignment = .fill
       contentEmailHStack.distribution = .equalCentering
       contentEmailHStack.translatesAutoresizingMaskIntoConstraints = false
    
       let contentConfirmEmailHStack = UIStackView(arrangedSubviews: [confirmEmailLinkLabel,confirmEmailLinkTextField])
        contentConfirmEmailHStack.axis = .horizontal
        contentConfirmEmailHStack.alignment = .fill
        contentConfirmEmailHStack.distribution = .equalCentering
    
     contentConfirmEmailHStack.translatesAutoresizingMaskIntoConstraints = false

    let contentEmailVStack = UIStackView(arrangedSubviews: [contentEmailHStack,bottomLineEmailView])
    contentEmailVStack.axis = .vertical
    contentEmailVStack.alignment = .fill
    contentEmailVStack.distribution = .equalSpacing
    contentEmailVStack.spacing = 5
    contentEmailVStack.translatesAutoresizingMaskIntoConstraints = false

    let contentConfirmEmailVStack = UIStackView(arrangedSubviews: [contentConfirmEmailHStack,bottomconfirmEmailLinkTextFieldView])
    contentConfirmEmailVStack.axis = .vertical
    contentConfirmEmailVStack.alignment = .fill
    contentConfirmEmailVStack.distribution = .equalSpacing
    contentConfirmEmailVStack.spacing = 5
    contentConfirmEmailVStack.translatesAutoresizingMaskIntoConstraints = false

   let contentLinkEmailVStackView = UIStackView(arrangedSubviews: [contentEmailVStack,contentConfirmEmailVStack,requirementLabel])
   contentLinkEmailVStackView.axis = .vertical
   contentLinkEmailVStackView.alignment = .fill
   contentLinkEmailVStackView.distribution = .fill
    contentLinkEmailVStackView.spacing = 20
   contentLinkEmailVStackView.translatesAutoresizingMaskIntoConstraints = false

   cardLinkEmailView.addSubview(contentLinkEmailVStackView)

     NSLayoutConstraint.activate([
      contentLinkEmailVStackView.topAnchor.constraint(equalTo: descriptionLinkEmailLabel.bottomAnchor,constant: 30),
      contentLinkEmailVStackView.leftAnchor.constraint(equalTo: cardLinkEmailView.leftAnchor,constant: 50),
      contentLinkEmailVStackView.rightAnchor.constraint(equalTo: cardLinkEmailView.rightAnchor,constant: -50),
      //contentLinkEmailVStackView.bottomAnchor.constraint(equalTo: cardLinkEmailView.bottomAnchor,constant: -50),
       bottomLineEmailView.heightAnchor.constraint(equalToConstant: 0.5),
       emailLinkTextField.leadingAnchor.constraint(equalTo: emailLinkEmailLabel.leadingAnchor,constant: 70),
       confirmEmailLinkTextField.leadingAnchor.constraint(equalTo: confirmEmailLinkLabel.leadingAnchor,constant: 90),
       bottomconfirmEmailLinkTextFieldView.heightAnchor.constraint(equalToConstant: 0.5)
       
     ])
    
    cardLinkEmailView.addSubview(submitBtn)
    
    NSLayoutConstraint.activate([
      submitBtn.topAnchor.constraint(equalTo: contentLinkEmailVStackView.bottomAnchor,constant: 40),
      submitBtn.heightAnchor.constraint(equalToConstant: 35),
      submitBtn.leadingAnchor.constraint(equalTo: cardLinkEmailView.leadingAnchor,constant: 40),
      submitBtn.trailingAnchor.constraint(equalTo: cardLinkEmailView.trailingAnchor,constant: -40),
      submitBtn.bottomAnchor.constraint(equalTo: cardLinkEmailView.bottomAnchor,constant: -40)
    ])
   }
   required init?(coder: NSCoder) {
     fatalError("init(coder:) has not been implemented")
   }

}
