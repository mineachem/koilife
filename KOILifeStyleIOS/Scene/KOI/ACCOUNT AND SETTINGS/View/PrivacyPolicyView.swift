//
//  PrivacyPolicyView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/12/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PrivacyPolicyView: UIView {
    
    lazy var emptyView: EmptyInboxView = {
        let view = EmptyInboxView()
        view.emptyIcon.image = UIImage(named: "error")
        view.emptyDescription.text = "An unexpected error occured."
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var bodyHelpView: UITextView = {
      let textView = UITextView()
      
      textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
      textView.showsVerticalScrollIndicator = false
      textView.showsHorizontalScrollIndicator = false
      textView.textAlignment = .justified
      textView.font = UIFont.systemFont(ofSize: 14, weight: .regular)
      textView.isEditable = false
      textView.translatesAutoresizingMaskIntoConstraints = false
      return textView
    }()
      
    
    override init(frame: CGRect) {
      super.init(frame: frame)
      
      setupUI()
    }
    
    fileprivate func setupUI(){
      backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
      addSubview(bodyHelpView)
      addSubview(emptyView)
      
      NSLayoutConstraint.activate([
        bodyHelpView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 40),
        bodyHelpView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 10),
        bodyHelpView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -10),
        bodyHelpView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        
        emptyView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 40),
        emptyView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
        emptyView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
        emptyView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
      ])
    }
    
    required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }

}
