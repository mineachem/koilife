//
//  AddressView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/14/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

protocol GetDataAddressDelegate{
  func getDataAddress(value:String)
}
class AddressView: NSObject,UIPickerViewDelegate,UIPickerViewDataSource {
 
  let cellHight:CGFloat = 8
  var getDataAddressDelegate:GetDataAddressDelegate?
  var titleStringArr = ["Phnom Penh","Banteay Meanchey","Battambang","Kampong Cham","Kampong Chhnang","Kampong Speu","Kampong Thom","Kampot","Kandal","Koh Kong","Kep","Kratie","Kratié","Mondulkiri","Pailin","Preah Sihanouk","Preash Vihear","Pursat","Prey Veng","Ratanakiri","Siem Reap","Stung Treng","Svay Rieng","Takéo","Tboung Khmum"]
  let blackView = UIView()
  
  lazy var tableview:UITableView = {
    let tableView = UITableView()
    tableView.backgroundColor = .white
    let subView = UIView()
    subView.backgroundColor = .white
    tableView.tableFooterView = subView
    return tableView
  }()
  
  lazy var pickerView: UIPickerView = {
    let pickerView = UIPickerView()
    pickerView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    return pickerView
  }()
  var editProfileVC:EditProfileVC?
  
  func showAddress(){
    //show menu
    if let window = UIApplication.shared.keyWindow {
      self.blackView.backgroundColor = UIColor(white: 0, alpha: 0)
      self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
      window.addSubview(blackView)
      //window.addSubview(tableview)
      window.addSubview(pickerView)
      
      let height:CGFloat = CGFloat(titleStringArr.count) * cellHight
      let y = window.frame.height - height
      pickerView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
      self.blackView.frame = window.frame
      self.blackView.alpha = 0
      
      UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        self.blackView.alpha = 1
      
        self.pickerView.frame = CGRect(x: 0, y: y, width: self.pickerView.frame.width, height: self.pickerView.frame.height)
      }, completion: nil)
    }
  }
  
  @objc func handleDismiss(){
    UIView.animate(withDuration: 0.5) {
      self.blackView.alpha = 0
      if let window = UIApplication.shared.keyWindow {
        self.pickerView.frame = CGRect(x: 0, y: window.frame.height, width: self.pickerView.frame.width, height: self.pickerView.frame.height)

      }
    }
  }
  
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
     return 1
   }
   
   func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return titleStringArr.count
   }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return titleStringArr[row]
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
     let data = titleStringArr[row]
    
    getDataAddressDelegate?.getDataAddress(value: data)

   
  }
  

  
  override init() {
    super.init()
    pickerView.delegate = self
    pickerView.dataSource = self
    
  }
}
