//
//  PasscodeLockView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/6/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PasscodeLockView: UIView {

  lazy var bgPasscodeLockView: UIView = {
    let subView = UIView()
    subView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var cardPasscodeLockView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .white
    subView.layer.cornerRadius = 10
  subView.layer.masksToBounds = true
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
   lazy var keylockPasscodeImgView: UIImageView = {
      let imageView = UIImageView()
      imageView.image = #imageLiteral(resourceName: "ic_lock")
      imageView.contentMode = .scaleAspectFit
      //imageView.layer.masksToBounds = true
      imageView.translatesAutoresizingMaskIntoConstraints = false
      return imageView
    }()
  

  lazy var turnPasscodeBtn: UIButton = {
      let button = UIButton(type: .system)
      button.setTitle("TURN PASSCODE ON", for: .normal)
      button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
      button.titleLabel?.font = UIFont.systemFont(ofSize: 14,weight: .medium)
      button.layer.cornerRadius = 15
      button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
      button.translatesAutoresizingMaskIntoConstraints = false
      return button
    }()
  
  lazy var crossPasscodeImagView: UIImageView = {
      let imageView = UIImageView()
      imageView.image = #imageLiteral(resourceName: "ic_close")
      imageView.contentMode = .scaleToFill
      imageView.layer.masksToBounds = true
      imageView.layer.cornerRadius = 13
      imageView.layer.borderWidth = 2
      imageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
      imageView.translatesAutoresizingMaskIntoConstraints = false
      return imageView
    }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  
  fileprivate func setupUI(){
     
    addSubview(bgPasscodeLockView)
   

    NSLayoutConstraint.activate([
      bgPasscodeLockView.topAnchor.constraint(equalTo: topAnchor),
      bgPasscodeLockView.leadingAnchor.constraint(equalTo: leadingAnchor),
      bgPasscodeLockView.trailingAnchor.constraint(equalTo: trailingAnchor),
      bgPasscodeLockView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
    
     bgPasscodeLockView.addSubview(cardPasscodeLockView)
    
    NSLayoutConstraint.activate([
      cardPasscodeLockView.centerXAnchor.constraint(equalTo: bgPasscodeLockView.centerXAnchor),
      cardPasscodeLockView.leadingAnchor.constraint(equalTo: bgPasscodeLockView.leadingAnchor,constant: 20),
      cardPasscodeLockView.trailingAnchor.constraint(equalTo: bgPasscodeLockView.trailingAnchor,constant: -20),
      cardPasscodeLockView.centerYAnchor.constraint(equalTo: bgPasscodeLockView.centerYAnchor)
    ])
    
      bgPasscodeLockView.addSubview(crossPasscodeImagView)
    
    NSLayoutConstraint.activate([
      crossPasscodeImagView.widthAnchor.constraint(equalToConstant: 25),
      crossPasscodeImagView.heightAnchor.constraint(equalToConstant: 25),
      crossPasscodeImagView.topAnchor.constraint(equalTo: cardPasscodeLockView.topAnchor,constant: -10),
      crossPasscodeImagView.rightAnchor.constraint(equalTo: cardPasscodeLockView.rightAnchor,constant: 10)
    ])
    
    

    
    cardPasscodeLockView.addSubview(keylockPasscodeImgView)
    cardPasscodeLockView.addSubview(turnPasscodeBtn)
    
    NSLayoutConstraint.activate([
      keylockPasscodeImgView.widthAnchor.constraint(equalToConstant: 150),
      keylockPasscodeImgView.heightAnchor.constraint(equalToConstant: 150),
      keylockPasscodeImgView.centerXAnchor.constraint(equalTo: cardPasscodeLockView.centerXAnchor),
      keylockPasscodeImgView.topAnchor.constraint(equalTo: cardPasscodeLockView.topAnchor,constant: 30),
      turnPasscodeBtn.topAnchor.constraint(equalTo: keylockPasscodeImgView.bottomAnchor,constant: 20),
      turnPasscodeBtn.leadingAnchor.constraint(equalTo: cardPasscodeLockView.leadingAnchor,constant: 40),
      turnPasscodeBtn.trailingAnchor.constraint(equalTo: cardPasscodeLockView.trailingAnchor,constant: -40),
      turnPasscodeBtn.bottomAnchor.constraint(equalTo: cardPasscodeLockView.bottomAnchor,constant: -30),
      turnPasscodeBtn.heightAnchor.constraint(equalToConstant: 35)
    ])
    
   }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
 
}
