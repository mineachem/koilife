//
//  EditProfileFormCell.swift
//  KOI
//
//  Created by MOLIDA LOEUNG on 11/29/19.
//  Copyright © 2019 MOLIDA LOEUNG. All rights reserved.
//

import UIKit

protocol GetValueEditProfileDelegate:class {
 
    func getGender(value:String)
  func getAddress()
}

class EditProfileFormCell: UITableViewCell{
  
    let genderString = ["Male", "Female"]
  weak var dateOfBirthDelegate:GetValueEditProfileDelegate?
  lazy var genderPicker = UIPickerView()
  
  var getCustomerInfo:ResultCustomerInfo?{
    didSet{
      if let firstnames = getCustomerInfo?.firstName {
        firstnametextField.text = firstnames
      }
      
      if let lastname = getCustomerInfo?.lastName {
        lastnametextField.text = lastname
      }
      
      if let email = getCustomerInfo?.email {
        emailtextField.text = email
          if email == "" {
              emailtextField.isUserInteractionEnabled = true
          }else{
              emailtextField.isUserInteractionEnabled = false
          }
      }
      
      if let phoneNum = getCustomerInfo?.phoneNo {
        phoneNumtextField.text = phoneNum
      }
      
      if let gender = getCustomerInfo?.gender {
        gendertextField.text = gender
      }
      
      if let dob = getCustomerInfo?.dob {
        let getdateString = dob.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "yyyy/MM/dd"
            let dateString = dateFormat.string(from: getdateString)
            dobtextField.text = dateString
      }
      
      if let address =  getCustomerInfo?.address {
        citytextField.text = address
      }
    }
  }
  

  
 
    lazy var titleFirstNameLabel: UILabel = {
        let label = UILabel()
     
      label.text = "First Name<sup>*</sup>".htmlToString
     
        label.textAlignment = .left
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label

    }()

    lazy var firstnametextField: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "First Name"
        textfield.tag = 0
        textfield.autocapitalizationType = .none
        textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
  
  lazy var bottomLineFirstnameView: UIView = {
        let line = UIView()
        line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
  
  lazy var titleLastNameLabel: UILabel = {
        let label = UILabel()
    label.text = "Last Name<sup>*</sup>".htmlToString
        label.textAlignment = .left
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label

    }()

    lazy var lastnametextField: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Last Name"
        textfield.autocapitalizationType = .none
        textfield.tag = 1
        textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
  
  lazy var bottomLineLastnameView: UIView = {
        let line = UIView()
        line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
  
  lazy var titleEmailLabel: UILabel = {
         let label = UILabel()
         label.text = "Email"
         label.textAlignment = .left
         label.font = UIFont(name: "Montserrat-Regular", size: 12)
         label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
         label.translatesAutoresizingMaskIntoConstraints = false
         return label

     }()

    lazy var emailtextField: UITextField = {
         let textfield = UITextField()
         textfield.placeholder = "Email"
         textfield.tag = 2
         textfield.autocapitalizationType = .none
         textfield.font = UIFont.systemFont(ofSize: 14, weight: .medium)
         textfield.translatesAutoresizingMaskIntoConstraints = false
         return textfield
     }()
  
  lazy var bottomLineEmailView: UIView = {
        let line = UIView()
        line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
  
  lazy var titleCityLabel: UILabel = {
        let label = UILabel()
        label.text = "City/Province"
        label.textAlignment = .left
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label

    }()

   lazy var citytextField: UITextField = {
        let textfield = UITextField()
       textfield.placeholder = "City/Province"
        textfield.autocapitalizationType = .none
        textfield.tag = 3
        textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
   
   
   lazy var bottomLineAddressView: UIView = {
         let line = UIView()
         line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
         line.translatesAutoresizingMaskIntoConstraints = false
         return line
     }()
  
  lazy var titledobLabel: UILabel = {
        let label = UILabel()
        label.text = "Date Of birth"
        label.textAlignment = .left
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label

    }()

   lazy var dobtextField: UITextField = {
        let textfield = UITextField()
      textfield.placeholder = "Date Of birth"
      textfield.autocapitalizationType = .none
      textfield.isEnabled = false
      textfield.tintColor = .clear
      textfield.tag = 4
      textfield.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
      textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
      textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
   
   lazy var bottomLinedobView: UIView = {
         let line = UIView()
         line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
         line.translatesAutoresizingMaskIntoConstraints = false
         return line
     }()
  
  lazy var titleGenderLabel: UILabel = {
        let label = UILabel()
        label.text = "Gender"
        label.textAlignment = .left
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label

    }()

  
   lazy var gendertextField: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Gender"
        textfield.tintColor = .clear
        textfield.tag = 5
        textfield.autocapitalizationType = .none
        textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
  
  lazy var bottomLinegenderView: UIView = {
        let line = UIView()
        line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
  
  lazy var titlePhoneNumLabel: UILabel = {
       let label = UILabel()
       label.text = "Phone Number"
       label.textAlignment = .left
       label.font = UIFont(name: "Montserrat-Regular", size: 12)
       label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label

   }()

  lazy var phoneNumtextField: UITextField = {
       let textfield = UITextField()
       textfield.placeholder = "Phone Number"
       textfield.isEnabled = false
       textfield.tag = 6
       textfield.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
       textfield.autocapitalizationType = .none
       textfield.font = UIFont(name: "Montserrat-Medium", size: 14)
       textfield.translatesAutoresizingMaskIntoConstraints = false
       return textfield
   }()
  
  lazy var bottomLinephoneNumView: UIView = {
        let line = UIView()
        line.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
  

  
 
  
 
  

  
  lazy var requirementLabel: UILabel = {
    let label = UILabel()
    label.text = "fklllkslkslsllsdf"
    label.textAlignment = .center
    label.textColor = .red
    label.isHidden = true
    label.font = UIFont(name: "Montserrat-Regular", size: 14)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  var editProfileVC:EditProfileVC?
  
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
      selectionStyle = .none
        setupView()
        setupEvent()
        setupPicker()
    }
    
   fileprivate func setupView(){
      
       let contentFirstNameHStack = UIStackView(arrangedSubviews: [titleFirstNameLabel, firstnametextField])
           contentFirstNameHStack.axis = .horizontal
           contentFirstNameHStack.alignment = .fill
           contentFirstNameHStack.distribution = .equalSpacing
           contentFirstNameHStack.translatesAutoresizingMaskIntoConstraints = false

      
      let contentLastNameHStack = UIStackView(arrangedSubviews: [titleLastNameLabel, lastnametextField])
                contentLastNameHStack.axis = .horizontal
                contentLastNameHStack.alignment = .fill
                contentLastNameHStack.distribution = .equalSpacing
                contentLastNameHStack.translatesAutoresizingMaskIntoConstraints = false

      let contentEmailHStack = UIStackView(arrangedSubviews: [titleEmailLabel, emailtextField])
                contentEmailHStack.axis = .horizontal
                contentEmailHStack.alignment = .fill
                contentEmailHStack.distribution = .equalSpacing
                contentEmailHStack.translatesAutoresizingMaskIntoConstraints = false

      
      let contentPhoneNumHStack = UIStackView(arrangedSubviews: [titlePhoneNumLabel, phoneNumtextField])
                contentPhoneNumHStack.axis = .horizontal
                contentPhoneNumHStack.alignment = .fill
                contentPhoneNumHStack.distribution = .equalSpacing
                contentPhoneNumHStack.translatesAutoresizingMaskIntoConstraints = false


      let contentGenderHStack = UIStackView(arrangedSubviews: [titleGenderLabel, gendertextField])
                contentGenderHStack.axis = .horizontal
                contentGenderHStack.alignment = .fill
                contentGenderHStack.distribution = .equalSpacing
                contentFirstNameHStack.translatesAutoresizingMaskIntoConstraints = false



      let contentdobHStack = UIStackView(arrangedSubviews: [titledobLabel, dobtextField])
                contentdobHStack.axis = .horizontal
                contentdobHStack.alignment = .fill
                contentdobHStack.distribution = .equalSpacing
                contentdobHStack.translatesAutoresizingMaskIntoConstraints = false



      let contentCityHStack = UIStackView(arrangedSubviews: [titleCityLabel, citytextField])
                contentCityHStack.axis = .horizontal
                contentCityHStack.alignment = .fill
                contentCityHStack.distribution = .equalSpacing
                contentCityHStack.translatesAutoresizingMaskIntoConstraints = false


      let contentFirstNameVStack = UIStackView(arrangedSubviews: [contentFirstNameHStack,bottomLineFirstnameView])
      contentFirstNameVStack.axis = .vertical
      contentFirstNameVStack.alignment = .fill
      contentFirstNameVStack.distribution = .fill
      
      contentFirstNameVStack.translatesAutoresizingMaskIntoConstraints = false
      
  let contentLastNameVStack = UIStackView(arrangedSubviews: [contentLastNameHStack,bottomLineLastnameView])
       contentLastNameVStack.axis = .vertical
       contentLastNameVStack.alignment = .fill
       contentLastNameVStack.distribution = .fill
      
       contentLastNameVStack.translatesAutoresizingMaskIntoConstraints = false
      
  let contentEmailVStack = UIStackView(arrangedSubviews: [contentEmailHStack,bottomLineEmailView])
        contentEmailVStack.axis = .vertical
        contentEmailVStack.alignment = .fill
        contentEmailVStack.distribution = .fill
        
        contentEmailVStack.translatesAutoresizingMaskIntoConstraints = false
      
  let contentPhoneNumVStack = UIStackView(arrangedSubviews: [contentPhoneNumHStack,bottomLinephoneNumView])
  contentPhoneNumVStack.axis = .vertical
  contentPhoneNumVStack.alignment = .fill
  contentPhoneNumVStack.distribution = .fill
  
  contentPhoneNumVStack.translatesAutoresizingMaskIntoConstraints = false
      
 let contentGenderVStack = UIStackView(arrangedSubviews: [contentGenderHStack,bottomLinegenderView])
 contentGenderVStack.axis = .vertical
 contentGenderVStack.alignment = .fill
 contentGenderVStack.distribution = .fill
 
 contentGenderVStack.translatesAutoresizingMaskIntoConstraints = false
      
let contentdobVStack = UIStackView(arrangedSubviews: [contentdobHStack,bottomLinedobView])
contentdobVStack.axis = .vertical
contentdobVStack.alignment = .fill
contentdobVStack.distribution = .fill
contentdobVStack.translatesAutoresizingMaskIntoConstraints = false
      
  let contentCityVStack = UIStackView(arrangedSubviews: [contentCityHStack,bottomLineAddressView])
  contentCityVStack.axis = .vertical
  contentCityVStack.alignment = .fill
  contentCityVStack.distribution = .fill
  contentCityVStack.translatesAutoresizingMaskIntoConstraints = false
      
    
  let contentEditProfileFormVStack = UIStackView(arrangedSubviews: [contentFirstNameVStack,contentLastNameVStack,contentEmailVStack,contentCityVStack,contentdobVStack,contentGenderVStack,contentPhoneNumVStack,requirementLabel])
      contentEditProfileFormVStack.axis = .vertical
      contentEditProfileFormVStack.alignment = .fill
      contentEditProfileFormVStack.distribution = .fillEqually
      contentEditProfileFormVStack.translatesAutoresizingMaskIntoConstraints = false
      
      addSubview(contentEditProfileFormVStack)
      
      NSLayoutConstraint.activate([
        contentEditProfileFormVStack.topAnchor.constraint(equalTo: topAnchor,constant: 20),
        contentEditProfileFormVStack.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
        contentEditProfileFormVStack.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
        contentEditProfileFormVStack.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10),
        firstnametextField.leadingAnchor.constraint(equalTo: titleFirstNameLabel.leadingAnchor,constant: 90),
        lastnametextField.leadingAnchor.constraint(equalTo: titleLastNameLabel.leadingAnchor,constant: 90),
        phoneNumtextField.leadingAnchor.constraint(equalTo: titlePhoneNumLabel.leadingAnchor,constant: 110),
        emailtextField.leadingAnchor.constraint(equalTo: titleEmailLabel.leadingAnchor,constant: 50),
        gendertextField.leadingAnchor.constraint(equalTo: titleGenderLabel.leadingAnchor,constant: 80),
        dobtextField.leadingAnchor.constraint(equalTo: titledobLabel.leadingAnchor,constant: 90),
        citytextField.leadingAnchor.constraint(equalTo: titleCityLabel.leadingAnchor,constant: 90),
        bottomLineFirstnameView.heightAnchor.constraint(equalToConstant: 0.5),
        bottomLinephoneNumView.heightAnchor.constraint(equalToConstant: 0.5),
        bottomLineLastnameView.heightAnchor.constraint(equalToConstant: 0.5),
        bottomLineEmailView.heightAnchor.constraint(equalToConstant: 0.5),
        bottomLinegenderView.heightAnchor.constraint(equalToConstant: 0.5),
        bottomLinedobView.heightAnchor.constraint(equalToConstant: 0.5),
        bottomLineAddressView.heightAnchor.constraint(equalToConstant: 0.5)
      ])
      
    }
    
  fileprivate func setupEvent(){
//    gendertextField.addTarget(self, action: #selector(handlegenderTapped), for: .touchDown)
    citytextField.addTarget(self, action: #selector(handleAddressTapped), for: .touchDown)
    
  }
  
  
  
  
  @objc private func handleAddressTapped(){
    
  //  dateOfBirthDelegate?.getAddress()
    
  }
  
  @objc private func handlegenderTapped(){
    
   
    
  }
  

  
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension EditProfileFormCell: UIPickerViewDataSource, UIPickerViewDelegate{
    
     func setupPicker(){
         genderPicker.backgroundColor = .white
         genderPicker.delegate = self
         genderPicker.dataSource = self
         gendertextField.inputView = genderPicker
     }

     func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
     }

     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return genderString.count
     }

     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return genderString[row]
     }

     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         gendertextField.text = genderString[row]
         dateOfBirthDelegate?.getGender(value:gendertextField.text!)
     }
     
 }

