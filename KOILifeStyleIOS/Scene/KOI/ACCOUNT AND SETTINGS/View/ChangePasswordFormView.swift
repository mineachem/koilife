//
//  ChangePasswordFormView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/5/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class ChangePasswordFormView: UIView {


  lazy var bgChangePsWView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .none
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  lazy var cardChangePsWView: UIView = {
      let subView = UIView()
      subView.backgroundColor = .white
      subView.layer.cornerRadius = 10
    subView.layer.masksToBounds = true
      subView.translatesAutoresizingMaskIntoConstraints = false
      return subView
    }()
  
  lazy var crosImagView: UIImageView = {
     let imageView = UIImageView()
     imageView.image = #imageLiteral(resourceName: "ic_close")
     imageView.contentMode = .scaleAspectFill
     imageView.layer.borderWidth = 1
     imageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
     imageView.layer.masksToBounds = true
     imageView.layer.cornerRadius = 13
     imageView.translatesAutoresizingMaskIntoConstraints = false
     return imageView
   }()
  
  lazy var iconInfoPasWImagView: UIImageView = {
     let imageView = UIImageView()
     imageView.image = #imageLiteral(resourceName: "info")
     imageView.contentMode = .scaleToFill
     imageView.layer.masksToBounds = true
     imageView.translatesAutoresizingMaskIntoConstraints = false
     return imageView
   }()
  
  lazy var titleChangePswLabel: UILabel = {
    let label = UILabel()
    label.text = "Change Password"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var titleCurrentPswLabel: UILabel = {
     let label = UILabel()
     label.text = "Current Password"
     label.textAlignment = .left
     label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var currentPasswordTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Current Password"
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.keyboardType = .default
    textField.tag = 0
    textField.isSecureTextEntry = true
    textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  
  lazy var bottomCurrentPasswordView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var titleNewPswLabel: UILabel = {
    let label = UILabel()
    label.text = "New Password"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var newPasswordTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "New Password"
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.keyboardType = .default
    textField.tag = 1
    textField.isSecureTextEntry = true
    textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  
  lazy var bottomNewPaswView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var titleConfirmPswLabel: UILabel = {
    let label = UILabel()
    label.text = "Confirm Password"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var confirmPasswordTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Confirm Password"
    textField.font = UIFont.systemFont(ofSize: 14)
    textField.keyboardType = .default
    textField.isSecureTextEntry = true
    textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    textField.tag = 2
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  
  lazy var bottomConfirmPaswView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var requirementLabel: UILabel = {
     let label = UILabel()
     label.textAlignment = .center
     label.textColor = .red
     label.isHidden = true
    label.numberOfLines = 0
     label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var submitBtn: UIButton = {
      let button = UIButton(type: .system)
      button.setTitle("SUBMIT", for: .normal)
      button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 14,weight: .medium)
      button.layer.cornerRadius = 15
      button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
      button.translatesAutoresizingMaskIntoConstraints = false
      return button
    }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupUI()
  }
  
  fileprivate func setupUI(){
    
    addSubview(bgChangePsWView)
    bgChangePsWView.addSubview(cardChangePsWView)
    
    NSLayoutConstraint.activate([
      bgChangePsWView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
      bgChangePsWView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
      bgChangePsWView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
      bgChangePsWView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
    ])
    

    NSLayoutConstraint.activate([
      cardChangePsWView.centerXAnchor.constraint(equalTo: bgChangePsWView.centerXAnchor),
      cardChangePsWView.leadingAnchor.constraint(equalTo: bgChangePsWView.leadingAnchor,constant: 20),
      cardChangePsWView.trailingAnchor.constraint(equalTo: bgChangePsWView.trailingAnchor,constant: -20),
      cardChangePsWView.centerYAnchor.constraint(equalTo: bgChangePsWView.centerYAnchor)
    ])
    
    bgChangePsWView.addSubview(crosImagView)
       NSLayoutConstraint.activate([
        crosImagView.topAnchor.constraint(equalTo: cardChangePsWView.topAnchor,constant: -10),
        crosImagView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
        crosImagView.widthAnchor.constraint(equalToConstant: 25),
        crosImagView.heightAnchor.constraint(equalToConstant: 25)
       ])
       
    
    cardChangePsWView.addSubview(iconInfoPasWImagView)

       NSLayoutConstraint.activate([
        iconInfoPasWImagView.heightAnchor.constraint(equalToConstant: 30),
        iconInfoPasWImagView.widthAnchor.constraint(equalToConstant: 30),
        iconInfoPasWImagView.topAnchor.constraint(equalTo: cardChangePsWView.topAnchor,constant: 20),
        iconInfoPasWImagView.centerXAnchor.constraint(equalTo: cardChangePsWView.centerXAnchor)
       ])
    
    cardChangePsWView.addSubview(titleChangePswLabel)

    NSLayoutConstraint.activate([
      titleChangePswLabel.topAnchor.constraint(equalTo: iconInfoPasWImagView.bottomAnchor,constant: 15),
      titleChangePswLabel.centerXAnchor.constraint(equalTo: cardChangePsWView.centerXAnchor),
    ])

    let contentCurrentPswVStack = UIStackView(arrangedSubviews: [titleCurrentPswLabel,currentPasswordTextField,bottomCurrentPasswordView])
    contentCurrentPswVStack.axis = .vertical
    contentCurrentPswVStack.alignment = .fill
    contentCurrentPswVStack.distribution = .fill
    contentCurrentPswVStack.spacing = 5
    contentCurrentPswVStack.translatesAutoresizingMaskIntoConstraints = false

    let contentNewPswVStack = UIStackView(arrangedSubviews: [titleNewPswLabel,newPasswordTextField,bottomNewPaswView])
       contentNewPswVStack.axis = .vertical
       contentNewPswVStack.alignment = .fill
       contentNewPswVStack.distribution = .fill
    contentNewPswVStack.spacing = 5
       contentNewPswVStack.translatesAutoresizingMaskIntoConstraints = false

    let contentConfirmPswVStack = UIStackView(arrangedSubviews: [titleConfirmPswLabel,confirmPasswordTextField,bottomConfirmPaswView])
          contentConfirmPswVStack.axis = .vertical
          contentConfirmPswVStack.alignment = .fill
          contentConfirmPswVStack.distribution = .fill
    contentConfirmPswVStack.spacing = 5
          contentConfirmPswVStack.translatesAutoresizingMaskIntoConstraints = false

    let contentChangePswVStack = UIStackView(arrangedSubviews: [contentCurrentPswVStack,contentNewPswVStack,contentConfirmPswVStack,requirementLabel])
    contentChangePswVStack.axis = .vertical
    contentChangePswVStack.alignment = .fill
    contentChangePswVStack.distribution = .fill
    contentChangePswVStack.spacing = 10
    contentChangePswVStack.translatesAutoresizingMaskIntoConstraints = false
    cardChangePsWView.addSubview(contentChangePswVStack)

    NSLayoutConstraint.activate([
      contentChangePswVStack.topAnchor.constraint(equalTo: titleChangePswLabel.bottomAnchor,constant: 40),
      contentChangePswVStack.leadingAnchor.constraint(equalTo: cardChangePsWView.leadingAnchor,constant: 40),
      contentChangePswVStack.trailingAnchor.constraint(equalTo: cardChangePsWView.trailingAnchor,constant: -40),
      //contentChangePswVStack.bottomAnchor.constraint(equalTo: cardChangePsWView.bottomAnchor,constant: -20),
      bottomCurrentPasswordView.heightAnchor.constraint(equalToConstant: 1),
      bottomNewPaswView.heightAnchor.constraint(equalToConstant: 1),
      bottomConfirmPaswView.heightAnchor.constraint(equalToConstant: 1),
    
    ])

    cardChangePsWView.addSubview(submitBtn)
    NSLayoutConstraint.activate([
      submitBtn.topAnchor.constraint(equalTo: contentChangePswVStack.bottomAnchor,constant: 40),
      submitBtn.heightAnchor.constraint(equalToConstant: 35),
      submitBtn.leadingAnchor.constraint(equalTo: cardChangePsWView.leadingAnchor,constant: 40),
      submitBtn.trailingAnchor.constraint(equalTo: cardChangePsWView.trailingAnchor,constant: -40),
      submitBtn.bottomAnchor.constraint(equalTo: cardChangePsWView.bottomAnchor,constant: -40)
    ])
  }
  

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
