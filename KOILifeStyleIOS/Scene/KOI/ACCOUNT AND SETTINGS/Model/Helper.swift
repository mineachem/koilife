//
//  Helper.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
// MARK: - Helper
struct Helper: Codable {
    let response: ResponseHelper?
    let results: [ResultHelper]?
}

// MARK: - Response
struct ResponseHelper: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Result
struct ResultHelper: Codable {
    let id: Int?
    let resultDescription: String?
    let status: Bool?
    let version: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case resultDescription = "description"
        case status, version
    }
}
