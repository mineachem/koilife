//
//  EditProfile.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/18/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation

// MARK: - EditProfile
struct EditProfile: Codable {
    let response: ResponseEditProfile?
    let results: ResultsEditProfile?
}

// MARK: - Response
struct ResponseEditProfile: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Results
struct ResultsEditProfile: Codable {
    let id: Int?
    let firstName, lastName, userType, phoneNo: String?
    let email, address: String?
    let dob: Int?
    let gender: String?
    let imageFile: String?
    let confirmCode, photo: String?
    let passcodeLock: String?
    let status: Bool?
    let version: Int?
    let confirmCodeExpired, blocked, passcodeLockOn: Bool?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case userType, phoneNo, email, address, dob, gender, imageFile, confirmCode, photo, passcodeLock, status, version, confirmCodeExpired, blocked, passcodeLockOn
    }
}
