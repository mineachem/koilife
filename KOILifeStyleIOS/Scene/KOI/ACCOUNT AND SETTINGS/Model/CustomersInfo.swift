//
//  CustomerInfo.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/11/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation

// MARK: - CustomerInfo
struct CustomersInfo: Codable {
    let response: ResponseCustomerInfo?
    let results: [ResultCustomerInfo]?
}

//MARK:- CustomerInfo
struct CustomerInfo: Codable {
    let response: ResponseCustomerInfo?
    let results: ResultCustomerInfo?
}

// MARK: - Response
struct ResponseCustomerInfo: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Result
struct ResultCustomerInfo: Codable {
    let id: Int?
    let firstName, lastName: String?
    let userType: String?
    let phoneNo, email: String?
    let address: String?
    let dob: Int?
    let gender: String?
    let imageFile: String?
    let confirmCode, photo: String?
    let passcodeLock: String?
    let status: Bool?
    let passcodeLockOn, confirmCodeExpired: Bool?

 
    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case userType, phoneNo, email, address, dob, gender, imageFile, confirmCode, photo, passcodeLock, status, passcodeLockOn, confirmCodeExpired
    }
}
