//
//  LinkToEmail.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/12/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation

// MARK: - CustomerInfo
struct LinkToEmail: Codable {
    let data: DataEmail?
    let response: ResponseEmail?
}

// MARK: - DataClass
struct DataEmail: Codable {
    let id: Int?
    let firstName, lastName, userType, phoneNo: String?
    let email, address: String?
    let dob: Int?
    let gender: String?
    let imageFile: String?
    let confirmCode, photo: String?
    let passcodeLock: String?
    let status, passcodeLockOn, confirmCodeExpired: Bool?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case userType, phoneNo, email, address, dob, gender, imageFile, confirmCode, photo, passcodeLock, status, passcodeLockOn, confirmCodeExpired
    }
}

// MARK: - Response
struct ResponseEmail: Codable {
    let code: Int?
    let message: String?
}
