//
//  Logout.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/14/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
// MARK: - Response
struct ResponseLogout:Codable{
  let response:Logout?
}
struct Logout: Codable {
    let code: Int?
    let message: String?
}
