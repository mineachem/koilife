//
//  TermConditon.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
// MARK: - TermCondition
struct TermCondition: Codable {
    let response: ResponseTerm?
    let length: Int?
    let results: [ResultTerm]?
}

// MARK: - Response
struct ResponseTerm: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Result
struct ResultTerm: Codable {
    let id: Int?
    let resultDescription: String?
    let status: Bool?
    let version: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case resultDescription = "description"
        case status, version
    }
}
