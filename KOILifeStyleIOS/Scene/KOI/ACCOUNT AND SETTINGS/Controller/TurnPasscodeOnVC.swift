//
//  TurnPasscodeOnVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/6/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class TurnPasscodeOnVC: UIViewController {

  lazy var turnPasscodeOnView = TurnPasscodeOnView()
  var getValueOne:String? = ""
  var getValueTwo:String? = ""
  var collectionOTPBtn:[UIButton]{
    return [turnPasscodeOnView.numOneBtn,turnPasscodeOnView.numTwoBtn,turnPasscodeOnView.numThreeBtn,turnPasscodeOnView.numFourBtn,turnPasscodeOnView.numFiveBtn,turnPasscodeOnView.numSixBtn,turnPasscodeOnView.numSevenBtn,turnPasscodeOnView.numEightBtn,turnPasscodeOnView.numNineBtn,turnPasscodeOnView.numZeroBtn,turnPasscodeOnView.signCrossBtn]
  }
  
  var checkStatus:Bool = false
  override func loadView() {
    super.loadView()
    
    self.view = turnPasscodeOnView
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()

      //setupView()
      setupEvent()
    }
    
  fileprivate func setupView(){
    
//    turnPasscodeOnView.otpPinOneTextField.delegate = self
//    turnPasscodeOnView.otpPinTwoTextField.delegate = self
//    turnPasscodeOnView.otpPinThreeTextField.delegate = self
//    turnPasscodeOnView.otpPinFourTextField.delegate = self
    
  }
  
  fileprivate func setupEvent(){
    turnPasscodeOnView.closeBtn.addTarget(self, action: #selector(handleCloseTapped), for: .touchUpInside)
    
    collectionOTPBtn.forEach{
      $0.addTarget(self, action: #selector(handleOtppinTapped), for: .touchUpInside)
    }
  }
   
  
  @objc private func handleOtppinTapped(sender:UIButton){
    sender.isSelected = !sender.isSelected
    
    switch sender.tag {
    case 0:
      getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 1:
       getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 2:
       getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 3:
       getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 4:
        getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 5:
        getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 6:
       getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 7:
        getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 8:
       getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 9:
      getValueFromButton(sender: sender, textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    case 10:
      clearTextField(textOne:turnPasscodeOnView.otpPinOneTextField, textTwo: turnPasscodeOnView.otpPinTwoTextField, textThree: turnPasscodeOnView.otpPinThreeTextField, textFour: turnPasscodeOnView.otpPinFourTextField)
    default:
      print("nothing")
    }
    
  }
  
  
  private func clearTextField(textOne:UITextField,textTwo:UITextField,textThree:UITextField,textFour:UITextField){
    if ((textFour.text?.count)! >= 1){
      textFour.text = ""
      self.turnPasscodeOnView.otpPinFourTextField.becomeFirstResponder()
    }else if ((textThree.text?.count)! >= 1){
      textThree.text = ""
         self.turnPasscodeOnView.otpPinThreeTextField.becomeFirstResponder()
    }else if ((textTwo.text?.count)! >= 1){
        textTwo.text = ""
      self.turnPasscodeOnView.otpPinTwoTextField.becomeFirstResponder()
    }else if ((textOne.text?.count)! >= 1){
      textOne.text = ""
      self.turnPasscodeOnView.otpPinOneTextField.resignFirstResponder()
    }
    
  }
  
  private func getValueFromButton(sender:UIButton,textOne:UITextField,textTwo:UITextField,textThree:UITextField,textFour:UITextField){
    
    if ((textOne.text?.count)! < 1) {
      textOne.text = sender.currentTitle
      self.turnPasscodeOnView.otpPinTwoTextField.becomeFirstResponder()
    }else if ((textTwo.text?.count)! < 1){
      textTwo.text = sender.currentTitle
      self.turnPasscodeOnView.otpPinThreeTextField.becomeFirstResponder()
    }else if ((textThree.text?.count)! < 1){
      textThree.text = sender.currentTitle
      self.turnPasscodeOnView.otpPinFourTextField.becomeFirstResponder()
    }else if ((textFour.text?.count)! < 1){
      textFour.text = sender.currentTitle
       self.turnPasscodeOnView.otpPinFourTextField.resignFirstResponder()
    }
    
    
    if checkStatus == false {
      getValueOne = "\(textOne.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
      if getValueOne!.count == 4 {
                print("text:",getValueOne)
               DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                 self.turnPasscodeOnView.otpPinOneTextField.text = ""
                 self.turnPasscodeOnView.otpPinTwoTextField.text = ""
                 self.turnPasscodeOnView.otpPinThreeTextField.text = ""
                 self.turnPasscodeOnView.otpPinFourTextField.text = ""
                 
                 self.turnPasscodeOnView.titleTurnPasscodeOnLabel.text = "Confirm Password"
                 self.turnPasscodeOnView.titleDescriptionPinCodeLabel.text = "Please re-enter passcode to set your passcode."
                
                self.checkStatus = true
               }
        }
      
    }else {
       getValueTwo = "\(textOne.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
      if getValueTwo!.count == 4{
        if getValueOne == getValueTwo {
           UserDefaults.standard.set(self.getValueTwo!, forKey: "passcode")
          let alertController = UIAlertController(title: "", message: "Passcode has been turned on successfully", preferredStyle: .alert)
          let alertOk = UIAlertAction(title: "OK", style: .default) { (action) in
            //self.view.window?.rootViewController?.presentedViewController?.dismiss(animated: false, completion: nil)
            self.presentingViewController?
              .presentingViewController?
              .dismiss(animated: false, completion: nil)
               }
               alertController.addAction(alertOk)
               self.present(alertController, animated: true)
        }else {
          print("not match")
        }
     
      }
    }
    
    
     
   
  }
  
  @objc private func handleCloseTapped(){
  self.presentingViewController?
  .presentingViewController?
  .dismiss(animated: false, completion: nil)
    //self.view.window?.rootViewController?.presentedViewController?.dismiss(animated: false)
  
  }
  


}

//extension TurnPasscodeOnVC:UITextFieldDelegate {
//
//  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//    if ((textField.text?.count)! < 1) && (string.count > 0){
//      if textField == turnPasscodeOnView.otpPinOneTextField {
//        turnPasscodeOnView.otpPinTwoTextField.becomeFirstResponder()
//      }
//
//      if textField == turnPasscodeOnView.otpPinTwoTextField {
//        turnPasscodeOnView.otpPinThreeTextField.becomeFirstResponder()
//      }
//
//      if textField == turnPasscodeOnView.otpPinThreeTextField {
//             turnPasscodeOnView.otpPinFourTextField.becomeFirstResponder()
//        }
//
//      if textField == turnPasscodeOnView.otpPinFourTextField {
//            turnPasscodeOnView.otpPinFourTextField.resignFirstResponder()
//        }
//
//      textField.text = string
//      return false
//    }else if ((textField.text?.count)! >= 1) && (string.count == 0){
//      if textField == turnPasscodeOnView.otpPinTwoTextField{
//        self.turnPasscodeOnView.otpPinOneTextField.becomeFirstResponder()
//      }
//
//      if textField == turnPasscodeOnView.otpPinThreeTextField{
//        self.turnPasscodeOnView.otpPinTwoTextField.becomeFirstResponder()
//      }
//
//      if textField == turnPasscodeOnView.otpPinFourTextField{
//        self.turnPasscodeOnView.otpPinThreeTextField.becomeFirstResponder()
//      }
//
//      if textField == turnPasscodeOnView.otpPinOneTextField{
//        self.turnPasscodeOnView.otpPinOneTextField.resignFirstResponder()
//      }
//
//      textField.text = ""
//      return false
//    }else if (textField.text?.count)! >= 1 {
//      textField.text = string
//      return false
//    }
//
//    return true
//  }
//}
