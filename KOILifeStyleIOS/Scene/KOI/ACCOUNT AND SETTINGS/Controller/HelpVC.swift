//
//  HelpVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/12/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class HelpVC: UIViewController {

    lazy var helpView = HelpView()

    lazy var loading: NVActivityIndicatorView = {
        let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
        loading.translatesAutoresizingMaskIntoConstraints = false
        return loading
    }()
  
  lazy var titleNavigationBar: UINavigationBar = {
   let navigationBar = UINavigationBar()
   let navItem = UINavigationItem()
     let label = UILabel()
     label.text = "Helps"
     label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     navItem.titleView = label
   navigationBar.setBackgroundImage(UIImage(), for: .default)
   navigationBar.shadowImage = UIImage()
   let backBtn = UIButton(type: .system)
   backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
   backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
   backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
   let leftBarButton = UIBarButtonItem()
   leftBarButton.customView = backBtn
   navItem.leftBarButtonItem = leftBarButton
   navigationBar.setItems([navItem], animated: false)
   navigationBar.translatesAutoresizingMaskIntoConstraints = false
   return navigationBar
   }()
  
  
  override func loadView() {
    super.loadView()
    self.view = helpView
    helpView.emptyView.isHidden = true
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupIndicator()
        getdataFromService()
    }
    
    fileprivate func setupIndicator(){
         view.addSubview(loading)
         NSLayoutConstraint.activate([
           loading.widthAnchor.constraint(equalToConstant: 40),
           loading.heightAnchor.constraint(equalToConstant: 40),
           loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
           loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
         ])

    }
    
  
    func getdataFromService(){
        self.loading.startAnimating()
        
             IGNetworkRequest.shareInstance.requestGET(KoiService.share.helperUrl, success: { (helper:Helper) in
              if helper.response?.code == 200 {
               
                helper.results?.forEach({ (helper) in
                 if let description = helper.resultDescription?.htmlToString{
                    self.helpView.bodyHelpView.text = description
                    self.loading.stopAnimating()
                 }
               })
               
              }else {
                print(helper.response?.message ?? "")
                self.loading.stopAnimating()
                self.helpView.emptyView.isHidden = false
              }
            }) { (failure) in
                print(failure)
                self.loading.stopAnimating()
                self.helpView.emptyView.isHidden = false
                
            }
    }
  
  fileprivate func setupNavigation(){
    
  view.addSubview(titleNavigationBar)
  NSLayoutConstraint.activate([
    titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
    titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
    titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
  ])
  }
  
  @objc private func handleBackTapped(){
    self.dismiss(animated: true)
  }
}
