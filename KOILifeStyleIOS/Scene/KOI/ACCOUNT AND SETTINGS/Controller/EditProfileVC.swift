//
//  EditProfileVC.swift
//  KOI
//
//  Created by MOLIDA LOEUNG on 11/28/19.
//  Copyright © 2019 MOLIDA LOEUNG. All rights reserved.
//

import UIKit
import Alamofire




class EditProfileVC: BaseViewController {

  //MARK:PROPERTIES
   lazy var editProfileView = EditProfileView()
  var getCustomerInfo:ResultCustomerInfo?
  //MARK:Properties
  var indexPath:IndexPath?
    var profileImage:UIImage?
     var firstnameValue:String?
     var lastnameValue:String?
     var emailValue:String?
     var genderValue:String?
     var dobValue:Date?
     var phonenumberValue:String?
     var addressValue:String?
    
//    let genderString = ["Male", "Female"]
//    lazy var genderPicker = UIPickerView()
//    lazy var genderTextField: UITextField = {
//
//        let text = UITextField()
//        text.placeholder = "Select Gender"
//        text.textAlignment = .right
//        text.tintColor = .clear
////        text.delegate = self
//        text.font = UIFont(name: "Montserrat-Regular", size: 16)
//        text.translatesAutoresizingMaskIntoConstraints = false
//        return text
//
//    }()
    
//  lazy var genderView: GenderView = {
//    let genderView = GenderView()
//    genderView.editProfileVC = self
//    genderView.getDataGenderDelegate = self
//    return genderView
//  }()
  
//  lazy var addressView: AddressView = {
//    let addressView = AddressView()
//    addressView.editProfileVC = self
//    addressView.getDataAddressDelegate = self
//    return addressView
//  }()
  
  var dob:Int? = 0
  var phonenumber:String? = ""
  var email:String? = ""
  var gender:String? = ""
  var photo:URL? = nil
  var firstname:String? = ""
  var lastname:String? = ""
  var addresss:String? = ""
  var customId:Int? = 0
  var userType:String? = ""

  
  //MARK:Life Cycle
  
  override func setupLoadView() {
    super.setupLoadView()
     self.view = editProfileView
  }
  
  
  override func setupNavigationBar() {
    super.setupNavigationBar()
      let label = UILabel()
      label.text = "EDIT PROFILE INFO"
      label.textColor = .black
      label.font = UIFont(name: "Montserrat-Medium", size: 14)
    titleNavigationBar.topItem?.titleView = label
  }
  
  //MARK:SETUP VIEW
  override func setupUI() {
    super.setupUI()
    //setupPicker()
    editProfileView.tableView.delegate = self
    editProfileView.tableView.dataSource = self
    

  
  }
  
  
  
  //MARK:SETUP EVENT
  override func setupEvent(){
    super.setupEvent()
    
    editProfileView.doneBtn.addTarget(self, action: #selector(handleDoneTapped), for: .touchUpInside)
//    let closeTapped = UITapGestureRecognizer(target: self, action: #selector(handleCloseTapped))
//    editProfileView.crosImagView.addGestureRecognizer(closeTapped)
//    editProfileView.crosImagView.isUserInteractionEnabled = true

    
  }
  
  @objc fileprivate func handleCloseTapped(){
    self.dismiss(animated: true)
  }
  
  @objc fileprivate func handleDoneTapped(){
    
      let getdateString = dob!.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
        let dateUpdateFormat = DateFormatter()
        dateUpdateFormat.dateFormat = "YYYY-MM-ddTHH:mm:ss.sssZ"
       let dobString = dateUpdateFormat.string(from: getdateString)

        let param:[String:Any] = [
                  "confirmCodeExpired": true,
                  "id": getCustomerInfo!.id!,
                  "passcodeLockOn": true,
                  "status": true,
                  "first_name": firstnameValue ?? "",
                  "last_name": lastnameValue ?? "",
                  "userType": userType ?? "",
                  "phoneNo": phonenumber ?? "",
                  "email": email!,
                  "address": addressValue ?? "",
                  "dob": dobString ,
                  "gender": genderValue ?? "",
                  "isPasscodeLockOn": true
        ]
    self.showLoading()
    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
        // your code here
        
        IGNetworkRequest.shareInstance.requestPUTURL(KoiService.share.editprofileUrl + "\(self.getCustomerInfo!.id!)", params: param, success: { (editProfile:EditProfile) in
          
          if editProfile.response?.code == 200 {
            NotificationCenter.default.post(name: NSNotification.Name("userHasUpdatedProfile"), object: nil)
           
              let param:[String:Any] = ["userId":self.getCustomerInfo!.id!]
              
            if self.profileImage != nil {
                self.upload(param: param,imageData:self.profileImage!.pngData())
            }else {
        
               self.upload(param: param,imageData:Data())
            }
            
              self.dismiss(animated: true)
            
          }else {
            self.hideLoading()
          }
        }) { (failure) in
          print(failure)
           self.hideLoading()
          let cell = self.editProfileView.tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! EditProfileFormCell
          cell.requirementLabel.isHidden = false
          cell.requirementLabel.text = "No Internet connection"
          
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            cell.requirementLabel.isHidden = true
          }
          
        }
    }

        
    }
    
}

//MARK: SETUP TABLEVIEW
extension EditProfileVC:UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return section == 0 ? 1:1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.section {
      case 0:
        let cellEditProfileHeader = tableView.dequeueReusableCell(withIdentifier: "editProfileHeader", for: indexPath) as! EditProfileHeaderCell
            cellEditProfileHeader.uploadProfileDelegate = self
            cellEditProfileHeader.getCustomerInfo = getCustomerInfo
          return cellEditProfileHeader
      case 1:
        let cell = tableView.dequeueReusableCell(withIdentifier: "editProfileCell", for: indexPath) as! EditProfileFormCell
        
         cell.getCustomerInfo = getCustomerInfo
         
         cell.firstnametextField.delegate = self
         cell.lastnametextField.delegate = self
         cell.emailtextField.delegate = self
         cell.phoneNumtextField.delegate = self
         cell.gendertextField.delegate = self
         cell.dobtextField.delegate = self
         cell.dateOfBirthDelegate = self
         cell.citytextField.delegate = self
                
                 return cell
      default:
        return UITableViewCell()
      }
  
    }
    
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 130 : 290
    }
    
}


//MARK: GET VALUE EDIT PROFILE DELEGATE
extension EditProfileVC:GetValueEditProfileDelegate{
    func getGender(value: String) {
        genderValue = value
    }
 
  func getAddress() {
    //self.addressValue = value
    //addressView.showAddress()
  }
  
  func getDobValue(value: Date) {
    
    dobValue = value
  }

}

//MARK: UPLOAD PROFILE DELEGATE
extension EditProfileVC:UploadProfileDelegate{
  func uploadProfile() {
    
    let alert = UIAlertController(title: "Choose File or Image", message: nil, preferredStyle: .actionSheet)
    alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
        self.openCamera()
    }))
    
    alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
        self.openGallery()
    }))
    
    alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
    
    self.present(alert, animated: true, completion: nil)
  }
  
  
  func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
        
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
          
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}


//MARK: UPLOAD PHOTO BY UIIMAGEPICKERCONTROLLERDELEGATE
extension EditProfileVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
  
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
//    if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
//      profileImageData = image.pngData()
//
//    } else
    if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
      profileImage = image
          
    }
    
    let cell = editProfileView.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! EditProfileHeaderCell
    cell.profileImgView.image = profileImage
    dismiss(animated: true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true)
  }


  
  fileprivate func upload(param:[String:Any],imageData:Data?){
    
    let headers: HTTPHeaders = [

        "Content-type": "multipart/form-data"
    ]
        Alamofire.upload(multipartFormData: { (multiPath) in
          let timestamp = NSDate().timeIntervalSince1970
          for (key, value) in param {
            multiPath.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
          }
          if let data = imageData {
            multiPath.append(data, withName: "imageFile",fileName: "\(timestamp).png",mimeType: "png")
          }
        }, usingThreshold: UInt64.init(), to: KoiService.share.uploadUrl, method: .post,headers: headers) { (result) in
          
          switch result {
            
          case .success(let upload, _, _):
            
            upload.responseJSON { (response) in
              switch response.result {
                
              case .success:
                
                let jsonData = response.data!
                
                let jsonString = String(data: jsonData, encoding: .utf8)
                
                print(jsonString!)
                self.hideLoading()
              case .failure(let failure ):
                print("failure response image:\(failure)")
                self.hideLoading()
              }
            }
          case .failure(let failure):
            print(failure.localizedDescription)
            self.hideLoading()
          }
    }
  }
}

//MARK: GET DATA GENDER AND DATA ADDRESS DELEGATE
extension EditProfileVC:GetDataGenderDelegate,GetDataAddressDelegate{
  func getDataAddress(value: String) {
     let cell = editProfileView.tableView.cellForRow(at: IndexPath(item: 0, section: 1)) as! EditProfileFormCell
    cell.citytextField.text = value
    self.addressValue = value
  }
  
  func getDataGender(value: String) {
    
    let cell = editProfileView.tableView.cellForRow(at: IndexPath(item: 0, section: 1)) as! EditProfileFormCell
    cell.gendertextField.text = value
    self.genderValue = value

  }
  
  
}

//MAKR: TEXTFIELD DELEGATE
extension EditProfileVC:UITextFieldDelegate {
    
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//    if textField.tag == 5 {
//      textField.resignFirstResponder()
//      return false
//    }
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //let indexOf = controls.index(of: )
  
      switch textField.tag {
      case 0:
        guard let currentText = textField.text else { return true }
        let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        self.firstnameValue = finalText
      case 1:
        
        guard let currentText = textField.text else { return true }
        let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
        self.lastnameValue = finalText
        
      case 2:
        guard let currentText = textField.text else { return true }
        let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
        self.email = finalText
    
      case 3:
      guard let currentText = textField.text else { return true }
      let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
     self.addressValue = finalText
      case 4:
      guard let currentText = textField.text else { return true }
      let _ = (currentText as NSString).replacingCharacters(in: range, with: string)
      
      textField.resignFirstResponder()
     // self.genderValue = finalText
      case 5:
      guard let currentText = textField.text else { return true }
      let _ = (currentText as NSString).replacingCharacters(in: range, with: string)
      //self.dobValue = finalText
      case 6:
      guard let currentText = textField.text else { return true }
      let _ = (currentText as NSString).replacingCharacters(in: range, with: string)
      
      default:
        print("nothing")
      }
      return true
    }
  }

