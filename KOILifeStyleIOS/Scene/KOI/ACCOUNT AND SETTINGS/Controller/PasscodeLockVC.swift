//
//  PasscodeLockVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/6/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PasscodeLockVC: BaseViewController {

  lazy var passcodeLockView = PasscodeLockView()
  
  override func setupLoadView() {
    super.setupLoadView()
    self.view = passcodeLockView
  }
  
   override func setupEvent(){
    let crossPasscodeTapped = UITapGestureRecognizer(target: self, action: #selector(handleCrosspassCodeTapped))
    passcodeLockView.crossPasscodeImagView.addGestureRecognizer(crossPasscodeTapped)
    passcodeLockView.crossPasscodeImagView.isUserInteractionEnabled = true
    
    passcodeLockView.turnPasscodeBtn.addTarget(self, action: #selector(handleTurnPasscodeTapped), for: .touchUpInside)
  }
   
  
  @objc func handleCrosspassCodeTapped(){
    self.dismiss(animated: true)
  }
  
  @objc func handleTurnPasscodeTapped(){
    let turnPasscodeOnVC = TurnPasscodeOnVC()
    turnPasscodeOnVC.modalPresentationStyle = .overCurrentContext
    turnPasscodeOnVC.modalTransitionStyle = .crossDissolve
    passcodeLockView.isHidden = true
    self.present(turnPasscodeOnVC, animated: true)
  }
}
