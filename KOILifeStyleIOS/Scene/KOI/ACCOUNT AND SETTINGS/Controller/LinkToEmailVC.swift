//
//  LinkToEmailVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/5/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import Alamofire

class LinkToEmailVC: BaseViewController {

  
  lazy var linkToEmailView = LinkEmailView()
  
  //properties
  var emailValue:String? = ""
  var confirmEmailValue:String? = ""
  var customerId:Int? = 0
  
  override func setupLoadView() {
    super.setupLoadView()
    self.view = linkToEmailView
  }
  

  override func setupUI(){
    
    linkToEmailView.emailLinkTextField.delegate = self
    linkToEmailView.confirmEmailLinkTextField.delegate = self
    titleNavigationBar.isHidden = true
  }
  override func setupEvent(){
    super.setupEvent()
    
    linkToEmailView.submitBtn.addTarget(self, action: #selector(handleSubmitTapped), for: .touchUpInside)
    
    let closeTapped = UITapGestureRecognizer(target: self, action: #selector(handleCloseTapped))
    linkToEmailView.crosImagView.addGestureRecognizer(closeTapped)
    linkToEmailView.crosImagView.isUserInteractionEnabled = true
  }
  
  @objc private func handleCloseTapped(){
    self.dismiss(animated: true)
  }
  
  @objc private func handleSubmitTapped(){
    
    if (emailValue!.isEmpty) && (confirmEmailValue!.isEmpty){
      
      linkToEmailView.requirementLabel.isHidden = false
      linkToEmailView.requirementLabel.text = "please completed all fields"
      linkToEmailView.emailLinkTextField.becomeFirstResponder()
    }else if emailValue!.isEmpty {
      linkToEmailView.requirementLabel.isHidden = false
      linkToEmailView.requirementLabel.text = "your email is empty"
      linkToEmailView.emailLinkTextField.becomeFirstResponder()
    }else if (!emailValue!.isValidEmail()){
      linkToEmailView.requirementLabel.isHidden = false
      linkToEmailView.requirementLabel.text = "your email is invalid"
      linkToEmailView.emailLinkTextField.becomeFirstResponder()
    }else if confirmEmailValue!.isEmpty {
      linkToEmailView.requirementLabel.isHidden = false
      linkToEmailView.requirementLabel.text = "your confirm email is empty"
      linkToEmailView.confirmEmailLinkTextField.becomeFirstResponder()
    }else if (!confirmEmailValue!.isValidEmail()){
      linkToEmailView.requirementLabel.isHidden = false
      linkToEmailView.requirementLabel.text = "your confirm email is invalid"
      linkToEmailView.confirmEmailLinkTextField.becomeFirstResponder()
    }
    
    if (!emailValue!.isEmpty && !confirmEmailValue!.isEmpty) && (emailValue == confirmEmailValue) {
      linkToEmailView.requirementLabel.isHidden = true
      let param:[String:Any] = [
          "userId": customerId!,
          "email": emailValue!,
          "confirm": confirmEmailValue!
      ]
      self.showLoading()
      IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.linkToEmailUrl, params: param, success: { (linkToEmail:LinkToEmail) in
        if linkToEmail.response?.code == 200 {
          self.hideLoading()
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            print("response link to email:\(String(describing: linkToEmail.data))")
            self.dismiss(animated: true)
          }
           
        }else {
          self.hideLoading()
          self.linkToEmailView.requirementLabel.isHidden = false
          self.linkToEmailView.requirementLabel.text = "your email not incorrect"
          print(linkToEmail.response!.message!)
        }
      }) { (failure) in
        self.hideLoading()
        self.linkToEmailView.requirementLabel.isHidden = false
        self.linkToEmailView.requirementLabel.text = "Internet not connect"
        print(failure)
      }
    }else {
      print("please enter your email or confirm email")
    }
    
    
  }
  
}


extension LinkToEmailVC:UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

      switch textField.tag {
      case 0:
        guard let currentText = textField.text else { return true }
        let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        self.emailValue = finalText
      case 1:
        
        guard let currentText = textField.text else { return true }
        let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
        self.confirmEmailValue = finalText
        
      default:
        print("nothing")
      }
      return true
    }
}
