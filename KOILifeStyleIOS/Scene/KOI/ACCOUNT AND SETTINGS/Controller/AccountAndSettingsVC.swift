//
//  EditProfileInfoVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/29/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import EzPopup
import Alamofire


class AccountAndSettingsVC: BaseViewController {

    
  //Properties
  var editProfileTableView:UITableView!
  
  var titleArray = [["","Edit Profile Info","Change Password","Passcode Lock"],["Help","Terms & Conditions","Privacy Policy","version 1.0.0"]]
  
   let userId = UserDefaults.standard.value(forKey: "id") as? Int
    var getCustomerInfo:ResultCustomerInfo?
  
  lazy var signoutBtn: UIButton = {
         let button = UIButton(type: .system)
         button.setTitle("SIGN OUT", for: .normal)
         button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
         button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)!
         button.layer.cornerRadius = 15
         button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
         button.translatesAutoresizingMaskIntoConstraints = false
         return button
    }()
  
  
  override func setupNavigationBar() {
     super.setupNavigationBar()
       let label = UILabel()
       label.text = "ACCOUNT & SETTINGS"
       label.font = UIFont(name: "Montserrat-Medium", size: 14)
     titleNavigationBar.topItem?.titleView = label
   }

  override func setupUI() {
    view.backgroundColor = #colorLiteral(red: 0.9608082175, green: 0.9570518136, blue: 0.9485346675, alpha: 1)
    editProfileTableView = UITableView()
     editProfileTableView.delegate = self
     editProfileTableView.dataSource = self
     editProfileTableView.separatorStyle = .none
     editProfileTableView.isScrollEnabled = false
     editProfileTableView.backgroundColor = .none
     editProfileTableView.register(AccountAndSettingsTVCell.self, forCellReuseIdentifier: "EditProfileIdentifier")
     editProfileTableView.register(HeaderAccountAndSettingsTVCell.self, forCellReuseIdentifier: "headerCell")
    editProfileTableView.register(UITableViewCell.self, forCellReuseIdentifier: "version")
     editProfileTableView.translatesAutoresizingMaskIntoConstraints = false
     view.addSubview(editProfileTableView)
    // view.addSubview(signoutBtn)
     
     NSLayoutConstraint.activate([
       editProfileTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 30),
       editProfileTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
       editProfileTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
       editProfileTableView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.86)
      // editProfileTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),

     ])
     
     view.addSubview(signoutBtn)
        NSLayoutConstraint.activate([
          signoutBtn.topAnchor.constraint(equalTo: editProfileTableView.bottomAnchor),
          signoutBtn.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 30),
          signoutBtn.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -30),
          signoutBtn.heightAnchor.constraint(equalToConstant: 35)
       ])
  }
    
  //MARK:SETUP EVENT
  override func setupEvent(){
    super.setupEvent()
    
    signoutBtn.addTarget(self, action: #selector(handleSignoutTapped), for: .touchUpInside)
  }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(userHasUpdatedProfile), name: NSNotification.Name("userHasUpdatedProfile"), object: nil)
        print("viewWillAppear")
    }
    
    @objc func userHasUpdatedProfile(){
        self.getDataFromService()
    }
  
  
  @objc private func handleSignoutTapped(){
    let param:[String:Any] = [ "userId":userId!,"firebaseToken": "string"]
    
    IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.signoutUrl, params: param, success: { (responseLogout:ResponseLogout) in
      
      if responseLogout.response?.code == 200 {
        
        let alertController = UIAlertController(title: "Warning", message: "Are you sure to log out?", preferredStyle: .alert)
        
        let alertCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(alertCancel)
        
        let alertOk = UIAlertAction(title: "OK", style: .default) { (action) in
          UserDefaults.standard.removeObject(forKey: "login")
          UserDefaults.standard.removeObject(forKey: "passcode")
          UserDefaults.standard.removeObject(forKey: "id")

          UserDefaults.standard.removeObject(forKey: "customerWalletId")
          UserDefaults.standard.removeObject(forKey: "balance")
          UserDefaults.standard.removeObject(forKey: "accName")
          UserDefaults.standard.removeObject(forKey: "score")
          UserDefaults.standard.removeObject(forKey: "membershipDate")
          UserDefaults.standard.removeObject(forKey: "rewardscore")
          UserDefaults.standard.removeObject(forKey: "endyear")
          UserDefaults.standard.removeObject(forKey: "accNumber")
          UserDefaults.standard.removeObject(forKey: "qty")

          UserDefaults.standard.removeObject(forKey: "memberTypeId")
          UserDefaults.standard.removeObject(forKey: "memberTypeMaxqty")
          UserDefaults.standard.removeObject(forKey: "memberTypeMinqty")
          UserDefaults.standard.removeObject(forKey: "memberType")
          UserDefaults.standard.removeObject(forKey: "memberTypeStatus")

          let authenticationVC = UINavigationController(rootViewController: AuthenticationVC())
            authenticationVC.navigationBar.isHidden = true
            UIApplication.shared.windows.first?.rootViewController = authenticationVC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
        alertController.addAction(alertOk)
        
        self.present(alertController, animated: true)
      }else {
        print(responseLogout.response?.message ?? "")
      }
    }) { (failure) in
      print(failure)
    }
  }
    
  override func getDataFromService(){
    
    
    let customId =  UserDefaults.standard.value(forKey: "id") as? Int
    IGNetworkRequest.shareInstance.requestGET(KoiService.share.customerInfoUrl, success: { (customerInfo:CustomersInfo) in

      if customerInfo.response?.code == 200 {
        customerInfo.results?.forEach({ (results) in
          if customId == results.id! {
            
            self.getCustomerInfo = results
            DispatchQueue.main.async {
                self.editProfileTableView.reloadData()
            }
            NotificationCenter.default.post(name: NSNotification.Name("handleUserHasUpdatedProfile"), object: nil)
          }
        })
      }else {
        print(customerInfo.response!.message!)
      }
    }) { (failure) in
      print(failure)
    }
  }
  
}


//MARK:EXTENSION TABLEVIEW

extension AccountAndSettingsVC:UITableViewDelegate,UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return titleArray.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return titleArray[section].count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    //MARK: Header Content
    if indexPath.section == 0  && indexPath.row == 0{
       let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell",for: indexPath) as! HeaderAccountAndSettingsTVCell
      headerCell.getCustomerInfo = getCustomerInfo
        return headerCell
    }
    
    if indexPath.section == 1 && indexPath.row == 3 {
      let cellVersion = tableView.dequeueReusableCell(withIdentifier: "version", for: indexPath)
      cellVersion.selectionStyle = .none
      cellVersion.textLabel?.textAlignment = .center
      cellVersion.backgroundColor = .none
      cellVersion.textLabel?.text = "version 1.0.0"
      return cellVersion
    }
    
    //MARK:Content Account And Setting
    let editProfileCell = tableView.dequeueReusableCell(withIdentifier: "EditProfileIdentifier", for: indexPath) as! AccountAndSettingsTVCell
    
      editProfileCell.titleLabel.text = titleArray[indexPath.section][indexPath.row]
    return editProfileCell
  }
  

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch indexPath.section{
    case 0:
      switch indexPath.row {
      case 1:
        let editProfileVC = EditProfileVC()
        editProfileVC.getCustomerInfo = getCustomerInfo
          
        editProfileVC.modalPresentationStyle = .overCurrentContext
        editProfileVC.modalTransitionStyle = .crossDissolve
    
        self.present(editProfileVC, animated: true)
      case 2:
        let changePsWVC = ChangePasswordVC()
        if let customerId = getCustomerInfo?.id{
        changePsWVC.customerId = customerId
        }
        let popupVC = PopupViewController(contentController: changePsWVC, position: .center(CGPoint(x: 0, y: 0)), popupWidth: view.bounds.width, popupHeight: view.bounds.height)
        popupVC.backgroundAlpha = 0.3
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        self.present(popupVC, animated: true)
      case 3:
        let passcodeLockVC = PasscodeLockVC()
          passcodeLockVC.modalPresentationStyle = .overCurrentContext
          passcodeLockVC.modalTransitionStyle = .crossDissolve
          self.present(passcodeLockVC, animated: false)
      default:
        print("nothing")
      }
    case 1:
      switch indexPath.row {
      case 0:
        let helpVC = HelpVC()
             helpVC.modalPresentationStyle = .overCurrentContext
             helpVC.modalTransitionStyle = .crossDissolve
             self.present(helpVC, animated: true)
      case 1:
        let termAndCondtionVC = TermsAndConditionsVC()
        termAndCondtionVC.modalPresentationStyle = .overCurrentContext
        termAndCondtionVC.modalTransitionStyle = .crossDissolve
        self.present(termAndCondtionVC, animated: true)
      case 2:
        let privacyPolicyVC = PrivacyPolicyVC()
             privacyPolicyVC.modalPresentationStyle = .overCurrentContext
             privacyPolicyVC.modalTransitionStyle = .crossDissolve
             self.present(privacyPolicyVC, animated: true)
      case 3:
       print("button")
      default:
        print("nothing")
        
      }
    default:
      print("nothing")
    }
  }
  
  
  func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
   
    if indexPath.section == 0 && indexPath.row == 0 {
      print("nothing")
    }else if indexPath.section == 1 && indexPath.row == 3 {
      print("nothing")
    }else {
      let cell  = tableView.cellForRow(at: indexPath) as! AccountAndSettingsTVCell
         cell.titleLabel.textColor = .gray
    }
     
  }

  func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
    
    if indexPath.section == 0 && indexPath.row == 0 {
      print("nothing")
    }else if indexPath.section == 1 && indexPath.row == 3 {
      print("nothing")
    }else {
      let cell  = tableView.cellForRow(at: indexPath) as! AccountAndSettingsTVCell
        // Add timer to be able see the effect
      
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (_) in
          cell.titleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
      
  }
}
