//
//  ChangePasswordVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/5/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import Alamofire

class ChangePasswordVC: BaseViewController {

  lazy var changePswView = ChangePasswordFormView()
 
  override func setupLoadView() {
    super.setupLoadView()
     self.view = changePswView
  }
  
  var currentPaswValue:String? = ""
  var newPaswValue:String? = ""
  var confirmPaswValue:String? = ""
  var customerId:Int? = 0
   
    
   override func setupUI() {
    changePswView.newPasswordTextField.delegate = self
    changePswView.currentPasswordTextField.delegate = self
    changePswView.confirmPasswordTextField.delegate = self
    titleNavigationBar.isHidden = true
  }
  
  override func setupEvent(){
    super.setupEvent()
    
    changePswView.submitBtn.addTarget(self, action: #selector(handlesubmitTapped), for: .touchUpInside)
    let closeTapped = UITapGestureRecognizer(target: self, action: #selector(handleCloseTapped))
    changePswView.crosImagView.addGestureRecognizer(closeTapped)
    changePswView.crosImagView.isUserInteractionEnabled = true
  }
  
  @objc private func handleCloseTapped(){
    self.dismiss(animated: true)
  }
  
  @objc private func handlesubmitTapped(){
    let currentPasswordValue = UserDefaults.standard.value(forKey: "password") as? String
    
    if (currentPaswValue!.isEmpty) && (newPaswValue!.isEmpty) && (confirmPaswValue!.isEmpty){
      changePswView.requirementLabel.isHidden = false
      changePswView.requirementLabel.text = "please completed all fields"
      changePswView.currentPasswordTextField.becomeFirstResponder()
    }else if (currentPaswValue!.isEmpty){
      changePswView.requirementLabel.isHidden = false
      changePswView.requirementLabel.text = "your password is empty"
      changePswView.currentPasswordTextField.becomeFirstResponder()
    }else if (currentPaswValue != currentPasswordValue){
      changePswView.requirementLabel.isHidden = false
      changePswView.requirementLabel.text = "your current password is wrong"
      changePswView.newPasswordTextField.becomeFirstResponder()
    }else if (newPaswValue!.isEmpty) {
      changePswView.requirementLabel.isHidden = false
      changePswView.requirementLabel.text = "your new password is empty"
      changePswView.newPasswordTextField.becomeFirstResponder()
    }else if (confirmPaswValue!.isEmpty){
      changePswView.requirementLabel.isHidden = false
      changePswView.requirementLabel.text = "your new confirm password is empty"
      changePswView.confirmPasswordTextField.becomeFirstResponder()
    }else if (newPaswValue != confirmPaswValue){
      changePswView.requirementLabel.isHidden = false
      changePswView.requirementLabel.text = "your new password and confirm password not match"
      changePswView.confirmPasswordTextField.becomeFirstResponder()
    }
    
    let KEY = "KoiTheCambodia0123456789@Pass0rd"
    let IV = "UjXn2r5u8x/A?D(G"
    
    let encryptinCurrentPass = AESUtils.instance.encryptionAES(value: currentPaswValue!, key256: KEY, iv: IV)
    let encryptinNewPass = AESUtils.instance.encryptionAES(value: newPaswValue!, key256: KEY, iv: IV)
    
    let encryptinConfirmPass = AESUtils.instance.encryptionAES(value: confirmPaswValue!, key256: KEY, iv: IV)
    
    if (!currentPaswValue!.isEmpty && currentPasswordValue == currentPaswValue) && (!newPaswValue!.isEmpty) && (!confirmPaswValue!.isEmpty) && (newPaswValue == confirmPaswValue) {
      
      changePswView.requirementLabel.isHidden = true
     let param:[String:Any] = [
        "currentInputPassword": encryptinCurrentPass,
        "newInputPassword": encryptinNewPass,
        "confirmPassword": encryptinConfirmPass
      ]
      
      self.showLoading()
      IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.changePasswordUrl + "\(customerId!)", params: param, success: { (responseChangePassword:CustomerInfo) in
        
        if responseChangePassword.response?.code == 200 {
          self.hideLoading()
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            self.dismiss(animated: true)
          }
          
        }else {
          self.hideLoading()
          self.changePswView.requirementLabel.isHidden = false
          self.changePswView.requirementLabel.text = "no data"
          print(responseChangePassword.response?.message ?? "")
        }
      }) { (failure) in
        print(failure)
        self.hideLoading()
        self.changePswView.requirementLabel.isHidden = false
        self.changePswView.requirementLabel.text = "Internet not connection"
      }
    }
  }
}


extension ChangePasswordVC:UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

    switch textField.tag {
    case 0:
      guard let currentText = textField.text else { return true }
      let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
      
      self.currentPaswValue = finalText
    case 1:
      
      guard let currentText = textField.text else { return true }
      let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
      self.newPaswValue = finalText
      
      case 2:
           guard let currentText = textField.text else { return true }
           let finalText = (currentText as NSString).replacingCharacters(in: range, with: string)
           self.confirmPaswValue = finalText
    default:
      print("nothing")
    }
    return true
  }
  
}
