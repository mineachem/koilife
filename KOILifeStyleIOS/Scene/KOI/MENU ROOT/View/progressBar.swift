//
//  progressBar.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class ProgressBar: UIView {
  
  @IBInspectable public var backGroundCircleColor: UIColor = UIColor.white
  @IBInspectable public var startGradientColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
  @IBInspectable public var endGradientColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
  @IBInspectable public var textColor: UIColor = UIColor.white
  
  private var backgroundLayer: CAShapeLayer!
  private var foregroundLayer: CAShapeLayer!
  private var textLayer: CATextLayer!
  var gradientLayer: CAGradientLayer?
  
  public var progress: CGFloat = 0 {
    didSet {
      didProgressUpdated()
    }
  }
  
  // Only override draw() if you perform custom drawing.
  // An empty implementation adversely affects performance during animation.
  override func draw(_ rect: CGRect) {
    // Drawing code
    
    guard layer.sublayers == nil else {
      return
    }
    
    let width = rect.width
    let height = rect.height
    
    let lineWidth = 0.1 * min(width, height)
    
    backgroundLayer = createCircularLayer(rect: rect, strokeColor: backGroundCircleColor.cgColor, fillColor: UIColor.clear.cgColor, lineWidth: lineWidth)
    
    foregroundLayer = createCircularLayer(rect: rect, strokeColor: UIColor.init(displayP3Red: 243/255, green: 175/255, blue: 34/255, alpha: 1.0).cgColor, fillColor: UIColor.clear.cgColor, lineWidth: lineWidth)
    
    gradientLayer = CAGradientLayer()
    gradientLayer!.startPoint = CGPoint(x: 0.5, y: 0.0)
    gradientLayer!.endPoint = CGPoint(x: 0.5, y: 1.0)
    
    gradientLayer!.colors = [startGradientColor.cgColor, endGradientColor.cgColor]
    gradientLayer!.frame = rect
    gradientLayer!.mask = foregroundLayer
    
    textLayer = createTextLayer(rect: rect, textColor: textColor.cgColor)
    
    layer.addSublayer(backgroundLayer)
    layer.addSublayer(gradientLayer!)
    layer.addSublayer(textLayer)
    

    addSubview(mainStackView)
    
    mainStackView.centerYAnchor.constraint(equalTo: centerYAnchor.self).isActive = true
    mainStackView.centerXAnchor.constraint(equalTo: centerXAnchor.self).isActive = true
    
  }
    
    lazy var mainStackView:UIStackView = {
                    var subStackView = UIStackView(arrangedSubviews: [scoreValue,typeLabel,title,exprieDate])
                    subStackView.axis = .vertical
                subStackView.alignment = .fill
              subStackView.distribution = .equalSpacing
            
                    subStackView.translatesAutoresizingMaskIntoConstraints = false
                    return subStackView
                }()
    
    
    lazy var typeLabel: UILabel = {
           let label = UILabel()
           label.translatesAutoresizingMaskIntoConstraints = false
      label.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        label.textAlignment = .center
       
           return label
       }()
    lazy var scoreValue: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont(name: "Montserrat-SemiBold", size: 35)
        return label
    }()
    lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Montserrat-Regular", size: 14)!
        label.textAlignment = .center
        return label
    }()
  lazy var exprieDate: UILabel = {
         let label = UILabel()
        label.font = UIFont(name: "Montserrat-Regular", size: 14)!
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
         return label
     }()
  private func createCircularLayer(rect: CGRect, strokeColor: CGColor,
                                   fillColor: CGColor, lineWidth: CGFloat) -> CAShapeLayer {
    
    let width = rect.width
    let height = rect.height
    
    let center = CGPoint(x: width / 2, y: height / 2)
    let radius = (min(width, height) - lineWidth) / 2
    
    let startAngle = -CGFloat.pi / 2
    let endAngle = startAngle + 2 * CGFloat.pi
    
    let circularPath = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
    
    let shapeLayer = CAShapeLayer()
    
    shapeLayer.path = circularPath.cgPath
    
    shapeLayer.strokeColor = strokeColor
    shapeLayer.fillColor = fillColor
    shapeLayer.lineWidth = lineWidth
    shapeLayer.lineCap = .round
    
    return shapeLayer
  }
  
  private func createTextLayer(rect: CGRect, textColor: CGColor) -> CATextLayer {
    
    let width = rect.width
    let height = rect.height
    
    let fontSize = min(width, height) / 4
    let offset = min(width, height) * 0.1

    let layer = CATextLayer()
    //layer.string = "\(Int(progress * 100))"
    layer.backgroundColor = UIColor.clear.cgColor
    layer.foregroundColor = textColor
    layer.fontSize = fontSize
    layer.frame = CGRect(x: 0, y: (height - fontSize - offset) / 2, width: width, height: fontSize + offset)
    layer.alignmentMode = .center
    
    return layer
  }
  
  private func didProgressUpdated() {
   // textLayer?.string = "\(Int(progress * 100))"
    foregroundLayer?.strokeEnd = progress
  }
  
}

