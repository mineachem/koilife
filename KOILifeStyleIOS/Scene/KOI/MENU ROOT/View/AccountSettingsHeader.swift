//
//  AccountSettingsHeader.swift
//  KOI
//
//  Created by MOLIDA LOEUNG on 11/28/19.
//  Copyright © 2019 MOLIDA LOEUNG. All rights reserved.
//

import UIKit

class AccountSettingsHeader: UITableViewCell{
    
    lazy var userProfile: UIImageView = {
        
        let image = UIImageView(image: UIImage(named: "imageProfile"))
        image.layer.cornerRadius = 50
        image.layer.masksToBounds = true
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
        
    }()
    
    lazy var username: UILabel = {
        
        let label = UILabel()
        label.text = "Molida Loeung"
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    lazy var membershipLabel: UILabel = {
        
        let label = UILabel()
        label.text = "BLACK MEMBERSHIP"
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    lazy var line: UIView = {
        
        let line = UIView()
        line.backgroundColor = #colorLiteral(red: 0.9017696977, green: 0.7642835975, blue: 0.4540341496, alpha: 1)
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
        
    }()
    
    lazy var stack: UIStackView = {
        
        let stack = UIStackView(arrangedSubviews: [username, line, membershipLabel])
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.spacing = 3
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
        
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(userProfile)
        addSubview(stack)
        
        setupView()
    }
    
    func setupView(){
        
        NSLayoutConstraint.activate([
            userProfile.widthAnchor.constraint(equalToConstant: 100),
            userProfile.heightAnchor.constraint(equalToConstant: 100),
            userProfile.centerYAnchor.constraint(equalTo: centerYAnchor),
            userProfile.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 50),
            
            line.heightAnchor.constraint(equalToConstant: 1),
            stack.leadingAnchor.constraint(equalTo: userProfile.trailingAnchor, constant: 20),
            stack.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
