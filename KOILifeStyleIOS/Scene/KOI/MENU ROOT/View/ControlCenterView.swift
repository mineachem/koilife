//
//  ControlCenterView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/8/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

protocol ControlCenterViewDelegate:class {
  func stateChanged(scrollDirection:UICollectionView.ScrollDirection)
  func stateChanged(scrollToEdgeEnabled:Bool)
}
class ControlCenterView: UIView {

 let segmentedControl = UISegmentedControl()
  let scrollToLabel = UILabel()
  
  weak var delegate:ControlCenterViewDelegate?
    
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    backgroundColor = .clear
    segmentedControl.tintColor = .white
    scrollToLabel.textColor = .white
    
    //set property state
    segmentedControl.insertSegment(withTitle: "KOI", at: 0, animated: false)
    segmentedControl.insertSegment(withTitle: "CARD", at: 1, animated: false)
    segmentedControl.insertSegment(withTitle: "OUTLET", at: 2, animated: false)
    segmentedControl.insertSegment(withTitle: "ORDER", at: 3, animated: false)
    segmentedControl.selectedSegmentIndex = 0
    segmentedControl.addTarget(self, action: #selector(controlStateChange(sender:)), for: .valueChanged)
  
  
    //layout subviews
    preservesSuperviewLayoutMargins = false
    
    let scrollToStackView = UIStackView()
    scrollToStackView.alignment = .fill
    scrollToStackView.addArrangedSubview(scrollToLabel)
    
    addSubview(segmentedControl)
    addSubview(scrollToStackView)
    
    segmentedControl.translatesAutoresizingMaskIntoConstraints = false
    segmentedControl.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      segmentedControl.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor,constant: 15),
      segmentedControl.centerXAnchor.constraint(equalTo: layoutMarginsGuide.centerXAnchor),
      segmentedControl.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor,constant: 15),
      
      scrollToStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor,constant: 15),
      scrollToStackView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor,constant: 15),
      scrollToStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor,constant: 15),
      scrollToStackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor,constant: -15)
    ])
    
  }
  
  @objc func controlStateChange(sender:UISegmentedControl){
    guard let scrollDirection = UICollectionView.ScrollDirection(rawValue: sender.selectedSegmentIndex) else { return }
    
    delegate?.stateChanged(scrollDirection: scrollDirection)
  }
  
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
