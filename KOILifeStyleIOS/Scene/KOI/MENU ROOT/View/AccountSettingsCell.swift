//
//  AccountSettingsCell.swift
//  KOI
//
//  Created by MOLIDA LOEUNG on 11/28/19.
//  Copyright © 2019 MOLIDA LOEUNG. All rights reserved.
//

import UIKit

class AccountSettingsCell: UITableViewCell{
    
    lazy var container: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
    
    lazy var settingTitle: UILabel = {
        
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.9017696977, green: 0.7642835975, blue: 0.4540341496, alpha: 1)
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    lazy var indicator: UIImageView = {
        
        let view = UIImageView()
        view.image = UIImage(named: "ic_list_arrow_orange")
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(container)
        container.addSubview(settingTitle)
        container.addSubview(indicator)
      
        setupView()
        
    }
    
    func setupView(){
        
        NSLayoutConstraint.activate([
            
            container.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            container.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            container.leadingAnchor.constraint(equalTo: leadingAnchor),
            container.trailingAnchor.constraint(equalTo: trailingAnchor),
        
            settingTitle.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 40),
            settingTitle.centerYAnchor.constraint(equalTo: container.centerYAnchor),
            
            indicator.widthAnchor.constraint(equalToConstant: 20),
            indicator.heightAnchor.constraint(equalToConstant: 20),
            indicator.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -40),
            indicator.centerYAnchor.constraint(equalTo: container.centerYAnchor)
        
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
