//
//  MenuRootVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/16/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import BetterSegmentedControl

class MenuRootVC: UIViewController {

  
  private lazy var koiViewController:HomeVC = {
     let homeVC = HomeVC()
     self.add(asChildViewController: homeVC)
     return homeVC
   }()
   

   private lazy var cardViewController:AddCardTableViewController = {
     let cardVC = AddCardTableViewController()
     self.add(asChildViewController: cardVC)
     return cardVC
   }()
   
  
   private lazy var outletViewController:OutletMapviewVC = {
      let outletVC = OutletMapviewVC()
      self.add(asChildViewController: outletVC)
      return outletVC
    }()
   
   
   private lazy var menuViewController:MenuVC = {
     let layout = UICollectionViewFlowLayout()
     let menuVC = MenuVC(collectionViewLayout:layout)
       self.add(asChildViewController: menuVC)
       return menuVC
     }()
   
   lazy var containerView: UIView = {
     let subView = UIView()
     subView.backgroundColor = .none
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
   
  
  lazy var bettersegmentControl: BetterSegmentedControl = {
    let segmentControl = BetterSegmentedControl()
    segmentControl.cornerRadius = 15

    segmentControl.backgroundColor = .none
    segmentControl.translatesAutoresizingMaskIntoConstraints = false
    return segmentControl
  }()
  
  lazy var containerMenuView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .none
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var containerSubMenuView: UIView = {
     let subView = UIView()
     subView.backgroundColor = .none
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
  
 
  lazy var bgImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "menu_bg")
    imageView.isHidden = true
    imageView.contentMode = .scaleToFill
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  override func viewDidLoad() {
        super.viewDidLoad()

      view.backgroundColor = .white
    setupSegmentControl()
    setupDefaultView()
      setupEvent()
  
    }
    
 
  fileprivate func setupDefaultView(){
   
      remove(asChildViewController: cardViewController)
       add(asChildViewController: koiViewController)
   
   }
  
 
  fileprivate func setupSegmentControl() {
    
    view.addSubview(containerMenuView)
  
      NSLayoutConstraint.activate([
        containerMenuView.topAnchor.constraint(equalTo: view.topAnchor),
        containerMenuView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        containerMenuView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        containerMenuView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ])
    
    view.addSubview(containerSubMenuView)
    
    NSLayoutConstraint.activate([
     containerSubMenuView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
     containerSubMenuView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
     containerSubMenuView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
     containerSubMenuView.heightAnchor.constraint(equalToConstant: 30)
    ])
    
    containerSubMenuView.addSubview(bgImageView)
    NSLayoutConstraint.activate([
      bgImageView.topAnchor.constraint(equalTo: containerSubMenuView.topAnchor),
      bgImageView.leadingAnchor.constraint(equalTo: containerSubMenuView.leadingAnchor),
      bgImageView.trailingAnchor.constraint(equalTo: containerSubMenuView.trailingAnchor),
      bgImageView.bottomAnchor.constraint(equalTo: containerSubMenuView.bottomAnchor)
    ])
    
    containerSubMenuView.addSubview(bettersegmentControl)
    
    NSLayoutConstraint.activate([
      bettersegmentControl.topAnchor.constraint(equalTo: containerSubMenuView.topAnchor),
      bettersegmentControl.leadingAnchor.constraint(equalTo: containerSubMenuView.leadingAnchor),
      bettersegmentControl.trailingAnchor.constraint(equalTo: containerSubMenuView.trailingAnchor),
      bettersegmentControl.bottomAnchor.constraint(equalTo: containerSubMenuView.bottomAnchor)
    ])
    
    
    bettersegmentControl.segments = LabelSegment.segments(withTitles: ["KOI", "CARD", "OUTLET", "MENU"],
                                                          normalFont: UIFont(name: "Montserrat-SemiBold", size: 14)!,
                                                          normalTextColor:.black,
                                                          selectedBackgroundColor: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1),
                                                          selectedFont: UIFont(name: "Montserrat-SemiBold", size: 14)!,
                                                          selectedTextColor: .black)
  }
  
  
  fileprivate func setupEvent(){
    bettersegmentControl.addTarget(self, action: #selector(handleSegmentTapped(_:)), for: .valueChanged)
  }

 
  @objc private func handleSegmentTapped(_ sender:BetterSegmentedControl){
    switch sender.index {
     case 0:
        bettersegmentControl.backgroundColor = .clear
         remove(asChildViewController: cardViewController)
         add(asChildViewController: koiViewController)
        koiViewController.getDataFromService()
         UIApplication.shared.statusBarUIView?.backgroundColor = .clear
       case 1:
         remove(asChildViewController: koiViewController)
         add(asChildViewController: cardViewController)
         bettersegmentControl.backgroundColor = .white
       case 2:
         remove(asChildViewController: cardViewController)
         remove(asChildViewController: menuViewController)
         add(asChildViewController: outletViewController)
        bettersegmentControl.backgroundColor = .white
        UIApplication.shared.statusBarUIView?.backgroundColor = .white
       case 3:
         remove(asChildViewController: outletViewController)
         remove(asChildViewController: cardViewController)
         add(asChildViewController: menuViewController)
        bettersegmentControl.backgroundColor = .white
        UIApplication.shared.statusBarUIView?.backgroundColor = .white
    default:
      print("nothing")
  }
}

  
  private func add(asChildViewController viewController:UIViewController){
     //Add Child View Controller
     addChild(viewController)
     //Add Child View as Subview
     containerMenuView.addSubview(viewController.view)
     
     //Configure Child View
     viewController.view.frame = containerMenuView.bounds
     viewController.view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
     
     //Notify Child View Controller
     viewController.didMove(toParent: self)
   }
   
   private func remove(asChildViewController viewController:UIViewController){
     //Notify Child View Controller
     viewController.willMove(toParent: nil)
     
     //Remove Child View From Superview
     viewController.view.removeFromSuperview()
     
     //Notify Child View Controller
     viewController.removeFromParent()
   }
}
