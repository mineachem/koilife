//
//  Inbox.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/5/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
//// MARK: - Inbox
//struct Inbox: Codable {
//    let response: Response?
//    let length: Int?
//    let results: [ResultInbox]?
//}
//
//// MARK: - Response
//struct Response: Codable {
//    let code: Int?
//    let message: String?
//}
//
//// MARK: - Result
//struct ResultInbox: Codable {
//    let id: Int?
//    let inbox: InboxData?
//    let status: Bool?
//    let createById, updatedById: String?
//    let read: Bool?
//}
//
//// MARK: - InboxClass
//struct InboxData: Codable {
//    let id: Int?
//    let title, message: String?
//    let topic: Description?
//    let inboxDescription: Description?
//    let timeAlert: Int?
//    let imageFile: String?
//    let image: Description?
//    let status: Bool?
//    let createById, updatedById: String?
//    let pushed: Bool?
//
//    enum CodingKeys: String, CodingKey {
//        case id, title, message, topic
//        case inboxDescription = "description"
//        case timeAlert, imageFile, image, status, createById, updatedById, pushed
//    }
//}
//
//enum Description: String, Codable {
//    case string = "string"
//}

// MARK: - Inbox
struct Inbox: Codable {
    let response: ResponseInbox?
    let length: Int?
    let results: [ResultInbox]?
}

// MARK: - Response
struct ResponseInbox: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Result
struct ResultInbox: Codable {
    let id: Int?
    let inbox: InboxData?
    let status, read: Bool?
}

// MARK: - InboxClass
struct InboxData: Codable {
    var id: Int?
    var title, message: String?
    var topic: String?
    var inboxDescription: String?
    var timeAlert: Int?
    var imageFile: String?
    var image: String?
    var status, pushed: Bool?

  
    enum CodingKeys: String, CodingKey {
        case id, title, message, topic
        case inboxDescription = "description"
        case timeAlert, imageFile, image, status, pushed
    }
}
