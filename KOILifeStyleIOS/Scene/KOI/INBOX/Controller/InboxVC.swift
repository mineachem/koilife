//
//  InboxVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/28/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import EzPopup
import Alamofire
import NVActivityIndicatorView

class InboxVC: UIViewController {
    
    var emptyView = EmptyInboxView()
    var refreshCtr = UIRefreshControl()
    lazy var loading: NVActivityIndicatorView = {
        let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
        loading.translatesAutoresizingMaskIntoConstraints = false
        return loading
    }()

  lazy var titleNavigationBar: UINavigationBar = {
     let navigationBar = UINavigationBar()
     let navItem = UINavigationItem()
     let titleNav = UILabel()
     titleNav.text = "INBOX"
    titleNav.font = UIFont.boldSystemFont(ofSize: 14)
     titleNav.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     navItem.titleView = titleNav
     navigationBar.setBackgroundImage(UIImage(), for: .default)
     navigationBar.shadowImage = UIImage()
     let backBtn = UIButton(type: .system)
     backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
     backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
     backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
     let leftBarButton = UIBarButtonItem()
     leftBarButton.customView = backBtn
     navItem.leftBarButtonItem = leftBarButton
     navigationBar.setItems([navItem], animated: false)
     navigationBar.translatesAutoresizingMaskIntoConstraints = false
     return navigationBar
   }()
  
  //Properites
  var inboxTableView:UITableView!
  let inboxIdentifier = "InboxCell"
  var inboxResult = [ResultInbox]()
  
//    override func loadView() {
//        super.loadView()
//        self.view = emptyView
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIndicator()
        emptyView.emptyDescription.text = "Inbox has no items."
        view.backgroundColor = .white
        getDataFromService()
        setupNavigationBar()
        setupTableView()
        inboxTableView.backgroundView = loading
    }
    
  fileprivate func setupNavigationBar(){
     view.addSubview(titleNavigationBar)
     NSLayoutConstraint.activate([
       titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
       titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
       titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
     ])
   }
    
    fileprivate func setupIndicator(){
         view.addSubview(loading)
         NSLayoutConstraint.activate([
           loading.widthAnchor.constraint(equalToConstant: 40),
           loading.heightAnchor.constraint(equalToConstant: 40),
           loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
           loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
         ])

    }
  
  
    fileprivate func getDataFromService(){

            let customerId = UserDefaults.standard.value(forKey: "id") as? Int
        self.showLoading()
         IGNetworkRequest.shareInstance.requestGET(KoiService.share.inboxUrl+"?customerId=\(customerId!)&size=10&page=0", success: { (inboxResponse:Inbox) in

            if inboxResponse.response?.code == 200 {
                self.inboxResult = inboxResponse.results!
                if self.inboxResult.count > 0{
                    self.inboxTableView.reloadData()
                    self.inboxTableView.backgroundView = .none
                }else{
                    self.inboxTableView.backgroundView = self.emptyView
                }
            }else {
                print(inboxResponse.response?.message ?? "")
                self.inboxTableView.backgroundView = self.emptyView
            }
            self.hideLoading()
            self.refreshCtr.endRefreshing()
        }) { (failure) in
            print(failure)
            self.hideLoading()
            self.refreshCtr.endRefreshing()
            self.inboxTableView.backgroundView = self.emptyView
        }
        
    }
  
  
  fileprivate func setupTableView(){
    inboxTableView = UITableView()
    inboxTableView.delegate = self
    inboxTableView.dataSource = self
    refreshCtr.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
    // TableView
    inboxTableView.addSubview(refreshCtr)
//    inboxTableView.separatorStyle = .none
    inboxTableView.separatorInset = UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 15)
    inboxTableView.showsVerticalScrollIndicator = false
    inboxTableView.showsHorizontalScrollIndicator = false
    inboxTableView.tableFooterView = UIView()
    inboxTableView.translatesAutoresizingMaskIntoConstraints = false
    inboxTableView.register(InboxTVCell.self, forCellReuseIdentifier: inboxIdentifier)
    view.addSubview(inboxTableView)
    
    NSLayoutConstraint.activate([
      inboxTableView.topAnchor.constraint(equalTo: titleNavigationBar.bottomAnchor),
      inboxTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      inboxTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      inboxTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
    ])
    
    
  }
    
    func showLoading()
    {
         DispatchQueue.main.async {
             self.loading.isHidden = false
             self.loading.startAnimating()
         }
    }

    func hideLoading()
    {
         loading.isHidden = true
         loading.stopAnimating()
    }
    
    @objc func refresh() {
        getDataFromService()
    }
  
  
  @objc private func handleBackTapped(){
    self.dismiss(animated: true)
  }
 

}

extension InboxVC:UITableViewDelegate,UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return inboxResult.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let inboxCell = tableView.dequeueReusableCell(withIdentifier: inboxIdentifier, for: indexPath) as! InboxTVCell
    inboxCell.resultInbox = inboxResult[indexPath.row]
    return inboxCell
  }
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let inboxDetailData = inboxResult[indexPath.row].inbox
    
    let inboxDetailVC = InboxDetailVC()
    inboxDetailVC.inboxDetailData = inboxDetailData!
    
    inboxDetailVC.modalPresentationStyle = .overCurrentContext
    inboxDetailVC.modalTransitionStyle = .crossDissolve
    self.present(inboxDetailVC, animated: true)
    
  }
  
}
