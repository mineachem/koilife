//
//  InboxDetailVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/28/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit


class InboxDetailVC: UIViewController {

  var inboxDetailData:InboxData?
  
  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    let label = UILabel()
    label.text = "INBOX"
   label.font = UIFont.boldSystemFont(ofSize: 14)
   navItem.titleView = label
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
  
  lazy var inboxDetailView = InboxDetailView()
  
  override func loadView() {
    super.loadView()
    self.view = inboxDetailView
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
      setupNavigation()
      setupView()
      setupEvent()
    }
    
  fileprivate func setupNavigation(){
     
   view.addSubview(titleNavigationBar)
   NSLayoutConstraint.activate([
     titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
     titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
     titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
   ])
   }
  
  fileprivate func setupView(){
    if let titleInbox = inboxDetailData?.title?.htmlToString {
      
      inboxDetailView.titleInboxDetailLabel.text = titleInbox
    }
    
    if let descriptionInbox = inboxDetailData?.message?.htmlToString {
      
      inboxDetailView.desctiptonInboxDetailLabel.text = descriptionInbox
    }
    
    if let dataImage = inboxDetailData?.image {
      let imageUrl = URL(string: KoiService.share.displayImgUrl)?.appendingPathComponent((dataImage))
      
             guard let imageData = try? Data(contentsOf:imageUrl!) else { return }
                     let image = UIImage(data: imageData)
                     DispatchQueue.main.async {
                       if image != nil {
                         self.inboxDetailView.coverInboxDetailImgView.image = image
                       }else {
                         self.inboxDetailView.coverInboxDetailImgView.image = #imageLiteral(resourceName: "koi-adv")
                       }
                }
           }
  }
  
  @objc private func handleBackTapped(){
     self.dismiss(animated: true)
   }
  
  fileprivate func setupEvent(){
    let closeTapped = UITapGestureRecognizer(target: self, action: #selector(handleCloseTapped))
    inboxDetailView.closeImgView.addGestureRecognizer(closeTapped)
    inboxDetailView.closeImgView.isUserInteractionEnabled = true
  }
  
  @objc private func handleCloseTapped(){
    self.dismiss(animated: true)
  }
}
