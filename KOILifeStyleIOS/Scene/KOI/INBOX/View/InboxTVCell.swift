//
//  InboxTVCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/28/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class InboxTVCell: UITableViewCell {

  
  var resultInbox:ResultInbox? {
    didSet{
      if let titleInbox = resultInbox?.inbox?.title?.htmlToString{
        titleLabel.text = titleInbox
      }
      
      if let dateString = resultInbox?.inbox?.timeAlert {
        let getdateString = dateString.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy/MM/dd"
        let dateInbox = dateFormat.string(from: getdateString)
        dateLabel.text = dateInbox
      }
      
      if let description = resultInbox?.inbox?.message?.htmlToString {
        descriptionLabel.text = description
      }
    }
  }
  
 
  lazy var bgInboxView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.6862642765, blue: 0.1313454807, alpha: 0.5)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var bgBlackInboxView: UIView = {
     let subView = UIView()
     subView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
  
  lazy var titleLabel: UILabel = {
      let label = UILabel()
     label.text = "អបអរសាទរព្រះរាជពិធីបុណ្យអ៊ុំទូក បណ្តែតប្រទីប សំពះព្រះខែ អកអំបក"
     label.font = UIFont(name: "Montserrat-SemiBold", size: 18)
     label.numberOfLines = 1
     label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
  
  lazy var dateLabel: UILabel = {
     let label = UILabel()
    label.text = "2019/11/10"
    label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    label.numberOfLines = 1
    label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var descriptionLabel: UILabel = {
    let label = UILabel()
   label.text = "អបអរសាទរព្រះរាជពិធីបុណ្យអ៊ុំទូក បណ្តែតប្រទីប អកអំបុក និង សំពះព្រះខែ! ខយ សូមជូនពរឲ្យអ្នកជួបតែភាពរីករាយ និង មានសុវត្ថិភាពទាំងអស់គ្នា"
    label.font = UIFont.systemFont(ofSize: 15, weight: .thin)
   label.numberOfLines = 2
   label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  override func awakeFromNib() {
       super.awakeFromNib()
     
    
       // Initialization code
   }
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupUI()
//    backgroundColor = #colorLiteral(red: 0.9042258859, green: 0.9562941194, blue: 1, alpha: 1)
    backgroundColor = .white
    selectionStyle = .none
  }
  
  fileprivate func setupUI(){
    
//    addSubview(bgBlackInboxView)
//
//    NSLayoutConstraint.activate([
//         bgBlackInboxView.topAnchor.constraint(equalTo: topAnchor),
//         bgBlackInboxView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 20),
//         bgBlackInboxView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -20),
//         bgBlackInboxView.bottomAnchor.constraint(equalTo: bottomAnchor)
//    ])
//
//    bgBlackInboxView.addSubview(bgInboxView)
//
//    NSLayoutConstraint.activate([
//      bgInboxView.topAnchor.constraint(equalTo: bgBlackInboxView.topAnchor,constant: 0.3),
//      bgInboxView.leadingAnchor.constraint(equalTo: bgBlackInboxView.leadingAnchor),
//      bgInboxView.trailingAnchor.constraint(equalTo: bgBlackInboxView.trailingAnchor),
//      bgInboxView.bottomAnchor.constraint(equalTo: bgBlackInboxView.bottomAnchor,constant: -0.22)
//    ])
    
    let contentTitleStackView = UIStackView(arrangedSubviews: [titleLabel ,dateLabel])
    contentTitleStackView.axis = .vertical
    contentTitleStackView.alignment = .leading
    contentTitleStackView.distribution = .equalSpacing
    contentTitleStackView.translatesAutoresizingMaskIntoConstraints = false
    
    let contentInboxStackView = UIStackView(arrangedSubviews: [contentTitleStackView,descriptionLabel])
     contentInboxStackView.axis = .vertical
     contentInboxStackView.alignment = .fill
    contentInboxStackView.distribution = .equalSpacing
    contentInboxStackView.spacing = 10
     contentInboxStackView.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentInboxStackView)
    
    NSLayoutConstraint.activate([
        
      contentInboxStackView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 30),
      contentInboxStackView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -30),
      contentInboxStackView.topAnchor.constraint(equalTo: topAnchor,constant: 15),
      contentInboxStackView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -15),

    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
    
}
