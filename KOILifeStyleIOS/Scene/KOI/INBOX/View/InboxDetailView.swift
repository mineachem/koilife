//
//  InboxDetailView.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/28/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class InboxDetailView: UIView {

  lazy var cardInboxDetailView: UIView = {
     let subView = UIView()
     subView.backgroundColor = .white
     subView.layer.cornerRadius = 10
     subView.translatesAutoresizingMaskIntoConstraints = false
     return subView
   }()
  
  lazy var coverInboxDetailImgView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "koi-branch")
    imageView.contentMode = .scaleToFill
    imageView.layer.cornerRadius = 10
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  lazy var closeImgView: UIImageView = {
     let imageView = UIImageView()
     imageView.image = #imageLiteral(resourceName: "ic_close")
    imageView.contentMode = .scaleAspectFit
     imageView.layer.masksToBounds = true
     imageView.translatesAutoresizingMaskIntoConstraints = false
     return imageView
   }()
  
  lazy var titleInboxDetailLabel: UILabel = {
      let label = UILabel()
      label.text = "អបអរសាទរព្រះរាជពិធីបុណ្យអុំទូក បណ្តែតប្រទីប អកអំបុក និង សំពះព្រះខែ!"
      label.textAlignment = .left
      label.numberOfLines = 2
      label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
  
  lazy var desctiptonInboxDetailLabel: UILabel = {
       let label = UILabel()
       label.text = "Happy Water Festival! KOI wishes you to have only happiness. and to be safe on your trips during this delightful occasion!"
      label.textAlignment = .left
      label.numberOfLines = 0
      label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
      label.translatesAutoresizingMaskIntoConstraints = false
       return label
     }()
  
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupUI()
    
  }
  
  fileprivate func setupUI(){
    backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    addSubview(coverInboxDetailImgView)
    
    NSLayoutConstraint.activate([
      coverInboxDetailImgView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 44),
      coverInboxDetailImgView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
      coverInboxDetailImgView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
      coverInboxDetailImgView.heightAnchor.constraint(equalTo: safeAreaLayoutGuide.heightAnchor,multiplier: 0.7)
    ])
    
    addSubview(titleInboxDetailLabel)
    
    NSLayoutConstraint.activate([
      titleInboxDetailLabel.topAnchor.constraint(equalTo: coverInboxDetailImgView.bottomAnchor,constant: 10),
      titleInboxDetailLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
      titleInboxDetailLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
    ])
    
    addSubview(desctiptonInboxDetailLabel)
    
    NSLayoutConstraint.activate([
      desctiptonInboxDetailLabel.topAnchor.constraint(equalTo: titleInboxDetailLabel.bottomAnchor,constant: 10),
      desctiptonInboxDetailLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
      desctiptonInboxDetailLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10)
    ])
  
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
