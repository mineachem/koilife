//
//  CustomerHistory.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/19/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
// MARK: - CustomerHistory
struct CustomerHistory: Codable {
    let response: ResponseHistory?
    let length: Int?
    let results: [ResultHistory]?
}

// MARK: - Response
struct ResponseHistory: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Result
struct ResultHistory: Codable {
    let id: Int?
    let cmd: String?
    let fromAccount, toAccount: String?
    let value, transferType, refId, rewardPoint: Int?
    let membershipPoint, rewardType, customerId: Int?
    let branch: Branch?
    let branchCode: String?
    let amt: Double?
    let cancelType:String?
    let transactionDate: Int?
    let cardNumber: String?
    let status: Bool?
    let version: Int?
}

// MARK: - Branch
struct Branch: Codable {
    let id: Int?
    let code, name, phone1: String?
    let phone2: String?
    let address, companyCode: String?
    let email, branchDescription: String?
    let lat, lng, startHour, stopHour: String?
    let terminalId, images: String?
    let status: Bool?
    let version: Int?
    let hq: Bool?

    enum CodingKeys: String, CodingKey {
        case id, code, name, phone1, phone2, address, companyCode, email
        case branchDescription = "description"
        case lat, lng, startHour, stopHour, terminalId, images, status, version, hq
    }
}
