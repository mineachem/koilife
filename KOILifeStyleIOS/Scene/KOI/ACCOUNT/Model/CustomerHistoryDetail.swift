//
//  CustomerHistoryDetail.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/19/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
// MARK: - CustomerHistoryDetail
struct CustomerHistoryDetail: Codable {
    let response: ResponseHistoryDetail?
    let results: ResultsHistoryDetail?
}

// MARK: - Response
struct ResponseHistoryDetail: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Results
struct ResultsHistoryDetail: Codable {
    let paymentRef: String?
    let transactionDate: Int?
    let branchName: String?
    let amt:Double?
    let rewardPoint, memberPoint: Int?
    let products: [Product]?
}

// MARK: - Product
struct Product: Codable {
    let id: Int?
    let pid, pname: String?
    let pr: Double?
    let qty: Int?
    let status: Bool?
    let version: Int?
}
