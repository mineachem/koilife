//
//  AccountTransitonDetailHeaderCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AccountTransitonDetailHeaderCell: UITableViewCell {

  var getCustomerDetail: ResultHistory?{
    didSet{
  
             if let dateTransaction = getCustomerDetail?.transactionDate {
               let getdateString = dateTransaction.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "dd-MM-yy HH:mm a"
                let dateTransaction = dateFormat.string(from: getdateString)
                dateTranDetailLabel.text =  "Date:" + dateTransaction
             }
      
         if let branch = getCustomerDetail?.branch?.name {
          branchTranDetailLabel.text = "Branch: \(branch)"
      }
    }
  }

  lazy var titleTransitionDetailLabel: UILabel = {
      let label = UILabel()
      label.text = "TRANSACTION DETAIL"
      label.textAlignment = .center
      label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
      label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
    
   lazy var dateTranDetailLabel: UILabel = {
       let label = UILabel()
       label.text = "Date: 12-11-2019 3:04 PM"
       label.textAlignment = .left
       label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
       label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
     }()
   
   lazy var branchTranDetailLabel: UILabel = {
     let label = UILabel()
     label.text = "KOI The Rule"
     label.textAlignment = .left
     label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
     label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
   
    lazy var lineAboveTranDetailView: UIView = {
        let subView = UIView()
        subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
      }()
    
    lazy var descriptionTranDetailLabel: UILabel = {
       let label = UILabel()
       label.text = "Description"
       label.textAlignment = .left
       label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
     }()
    

    
    lazy var lineBellowTranDetailView: UIView = {
        let subView = UIView()
        subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
      }()
  
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    setupUI()
  }
  
  fileprivate func setupUI(){
    addSubview(titleTransitionDetailLabel)
     NSLayoutConstraint.activate([
      titleTransitionDetailLabel.topAnchor.constraint(equalTo: topAnchor,constant: 10),
      titleTransitionDetailLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
     ])
     
     let contentAddressTranVStack = UIStackView(arrangedSubviews: [dateTranDetailLabel,branchTranDetailLabel])
     contentAddressTranVStack.axis = .vertical
     contentAddressTranVStack.alignment = .fill
     contentAddressTranVStack.distribution = .fill
     contentAddressTranVStack.translatesAutoresizingMaskIntoConstraints = false
     
     addSubview(contentAddressTranVStack)
    
     NSLayoutConstraint.activate([
       contentAddressTranVStack.topAnchor.constraint(equalTo: titleTransitionDetailLabel.bottomAnchor,constant: 10),
       contentAddressTranVStack.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
       contentAddressTranVStack.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
     ])
    
    let contentDescriptionHStack = UIStackView(arrangedSubviews: [descriptionTranDetailLabel])
    contentDescriptionHStack.axis = .horizontal
    contentDescriptionHStack.alignment = .fill
    contentDescriptionHStack.distribution = .equalSpacing
    contentDescriptionHStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentDescriptionVStack = UIStackView(arrangedSubviews: [lineAboveTranDetailView,contentDescriptionHStack,lineBellowTranDetailView])
    contentDescriptionVStack.axis = .vertical
    contentDescriptionVStack.alignment = .fill
    contentDescriptionVStack.distribution = .fill
    contentDescriptionVStack.spacing = 10
    contentDescriptionVStack.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentDescriptionVStack)
    
    NSLayoutConstraint.activate([
      contentDescriptionVStack.topAnchor.constraint(equalTo: contentAddressTranVStack.bottomAnchor,constant: 5),
      contentDescriptionVStack.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
      contentDescriptionVStack.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
      contentDescriptionVStack.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10),
      lineBellowTranDetailView.heightAnchor.constraint(equalToConstant: 1),
      lineAboveTranDetailView.heightAnchor.constraint(equalToConstant: 1)
    ])
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
