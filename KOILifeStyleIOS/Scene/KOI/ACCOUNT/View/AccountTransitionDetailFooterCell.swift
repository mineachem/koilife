//
//  AccountTransitionDetailFooterCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AccountTransitionDetailFooterCell: UITableViewCell {
  
  var resultProduct: ResultsHistoryDetail?{
    didSet{
      if let amount = resultProduct?.amt {
        orderTranDetailLabel.text = "Order Amount: $" + String(format: "%.2f", amount)
      }
      
      if let rewardpoint = resultProduct?.rewardPoint {
        rewardPointTranDetailLabel.text = "RewardPoint: \(rewardpoint) point(s)"
      }
      
      if let membership = resultProduct?.memberPoint {
        memberRewardTranDetailLabel.text = "Membership Reward: \(membership) point(s)"
      }
    }
  }
  
  lazy var lineAbove: UIView = {
    let subView = UIView()
   // subView.backgroundColor = .black
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var orderTranDetailLabel: UILabel = {
    let label = UILabel()
    label.text = "Order Amount: $4.20"
    label.textAlignment = .center
    label.textColor = .black
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var rewardPointTranDetailLabel: UILabel = {
    let label = UILabel()
    label.text = "RewardPoint: 4 point(s)"
    label.textAlignment = .center
    label.textColor = .black
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var memberRewardTranDetailLabel: UILabel = {
    let label = UILabel()
    label.text = "Membership Reward: 2 point(s)"
    label.textAlignment = .center
    label.textColor = .black
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupUI()
  }
  
  fileprivate func setupUI(){
       addSubview(lineAbove)
    NSLayoutConstraint.activate([
      lineAbove.topAnchor.constraint(equalTo: topAnchor),
      lineAbove.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
      lineAbove.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
      lineAbove.heightAnchor.constraint(equalToConstant: 1)
    ])
    let contentBottomTransitionDetailVStack = UIStackView(arrangedSubviews: [orderTranDetailLabel,rewardPointTranDetailLabel,memberRewardTranDetailLabel])
    contentBottomTransitionDetailVStack.axis = .vertical
    contentBottomTransitionDetailVStack.alignment = .fill
    contentBottomTransitionDetailVStack.distribution = .equalSpacing
    contentBottomTransitionDetailVStack.spacing = 10
    contentBottomTransitionDetailVStack.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentBottomTransitionDetailVStack)
    
    NSLayoutConstraint.activate([
      contentBottomTransitionDetailVStack.topAnchor.constraint(equalTo: lineAbove.bottomAnchor,constant: 10),
      contentBottomTransitionDetailVStack.centerXAnchor.constraint(equalTo: centerXAnchor),
      //contentBottomTransitionDetailVStack.leadingAnchor.constraint(equalTo: leadingAnchor),
      //contentBottomTransitionDetailVStack.trailingAnchor.constraint(equalTo: trailingAnchor),
      contentBottomTransitionDetailVStack.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
