//
//  AccountTransitionDetailCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AccountTransitionDetailCell: UITableViewCell {

  var getproductItem: Product?{
    didSet{
      if let nameproduct = getproductItem?.pname,let qty = getproductItem?.qty,let priceProduct = getproductItem?.pr {

        itemValue.text = nameproduct
        qtyValue.text =  "\(qty)"
        priceValue.text = "$ " + priceProduct.description
  

//        let rightParagraph = NSMutableParagraphStyle()
//        rightParagraph.alignment = .right
//
//        let titleAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.black]
//        let titleString = NSMutableAttributedString(string: title, attributes: titleAttributes)
//
//        let subtitleAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 11), NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.paragraphStyle: rightParagraph]
//        let subtitleString = NSAttributedString(string: subtitle, attributes: subtitleAttributes)
//
//        let paragraph = NSMutableParagraphStyle()
//        paragraph.setParagraphStyle(.default)
//        //paragraph.lineSpacing = 2
//
//        let range = NSMakeRange(0, titleString.string.count)
//        titleString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraph, range: range)
//
//        titleString.append(subtitleString)
//
//        textLabel?.attributedText = titleString
//
//        textLabel?.font = UIFont.systemFont(ofSize: 11)
      }

    }
  }
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    let color = #colorLiteral(red: 0.8559798598, green: 0.6504896879, blue: 0.2624273896, alpha: 1).cgColor

       let shapeLayer:CAShapeLayer = CAShapeLayer()
       let frameSize = self.frame.size
       let shapeRect = CGRect(x: 0, y: -18, width: frameSize.width, height: 0)

       shapeLayer.bounds = shapeRect
       shapeLayer.position = CGPoint(x: frameSize.width/3, y: frameSize.height)
       shapeLayer.fillColor = UIColor.clear.cgColor
       shapeLayer.strokeColor = color
       shapeLayer.lineWidth = 0.5
       shapeLayer.lineJoin = CAShapeLayerLineJoin.round
       shapeLayer.lineDashPattern = [9,6]
       shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: shapeRect.height, width: shapeRect.width, height: 0), cornerRadius: 0).cgPath

       layer.addSublayer(shapeLayer)
    
     setupViews()
  }
 
    func setupViews()
    {
        contentView.addSubview(itemLabel)
        itemLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        itemLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5).isActive = true
        itemLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        contentView.addSubview(itemValue)
        itemValue.centerYAnchor.constraint(equalTo: itemLabel.centerYAnchor).isActive = true
        itemValue.leadingAnchor.constraint(equalTo: itemLabel.trailingAnchor, constant: 10).isActive = true
        itemValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        
        contentView.addSubview(qtyLabel)
          qtyLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
          qtyLabel.topAnchor.constraint(equalTo: itemLabel.bottomAnchor, constant: 0).isActive = true
           qtyLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
          contentView.addSubview(qtyValue)
          qtyValue.centerYAnchor.constraint(equalTo: qtyLabel.centerYAnchor).isActive = true
          qtyValue.leadingAnchor.constraint(equalTo: qtyLabel.trailingAnchor, constant: 10).isActive = true
          qtyValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
          
        
           contentView.addSubview(priceLabel)
            priceLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
            priceLabel.topAnchor.constraint(equalTo: qtyLabel.bottomAnchor, constant: 0).isActive = true
            priceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
            priceLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true

            contentView.addSubview(priceValue)
            priceValue.centerYAnchor.constraint(equalTo: priceLabel.centerYAnchor).isActive = true
            priceValue.leadingAnchor.constraint(equalTo: priceLabel.trailingAnchor, constant: 10).isActive = true
            priceValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true

        
    }
    lazy var itemLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Item :"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 15)
        return label
    }()
    lazy var qtyLabel: UILabel = {
           let label = UILabel()
           label.translatesAutoresizingMaskIntoConstraints = false
           label.text = "QTY :"
        label.textColor = .black
           label.font = UIFont.boldSystemFont(ofSize: 15)
           return label
       }()
    lazy var priceLabel: UILabel = {
             let label = UILabel()
             label.translatesAutoresizingMaskIntoConstraints = false
             label.text = "Price :"
        label.textColor = .black
             label.font = UIFont.boldSystemFont(ofSize: 15)
             return label
         }()
    lazy var itemValue: UILabel = {
          let label = UILabel()
          label.translatesAutoresizingMaskIntoConstraints = false
          label.text = "1dsfdaf"
           label.textColor = .lightGray
          label.font = UIFont.boldSystemFont(ofSize: 15)
          return label
       }()
    lazy var qtyValue: UILabel = {
       let label = UILabel()
       label.translatesAutoresizingMaskIntoConstraints = false
       label.text = "1"
        label.textColor = .lightGray
       label.font = UIFont.boldSystemFont(ofSize: 15)
       return label
    }()
    
    lazy var priceValue: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "$ 2.0"
       label.textColor = #colorLiteral(red: 0.8559798598, green: 0.6504896879, blue: 0.2624273896, alpha: 1)
      label.font = UIFont.boldSystemFont(ofSize: 15)
      return label
  }()
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
