//
//  AccountTransitonDetailView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AccountTransitonDetailView: UIView {
  
  let transitionDetailIdentifier = "transitionDetailCell"
  
  lazy var cardTransactionDetailView: UIView = {
       let subView = UIView()
       subView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
       subView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
       subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
       subView.layer.shadowOpacity = 1.0
       subView.layer.shadowRadius = 0.0
       subView.layer.masksToBounds = false
       subView.layer.cornerRadius = 10.0
       subView.translatesAutoresizingMaskIntoConstraints = false
       return subView
     }()
   
   lazy var titleTransitionDetailLabel: UILabel = {
     let label = UILabel()
     label.text = "TRANSACTION DETAIL"
     label.textAlignment = .center
     label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
   
   lazy var paymentRefTranDetailLabel: UILabel = {
     let label = UILabel()
     label.text = "Payment Ref: #15764545544556778756656"
     label.textAlignment = .left
     label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
     label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
   
  lazy var dateTranDetailLabel: UILabel = {
      let label = UILabel()
      label.text = "Date: 12-11-2019 3:04 PM"
      label.textAlignment = .left
      label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
      label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
  
  lazy var branchTranDetailLabel: UILabel = {
    let label = UILabel()
    label.text = "KOI The Rule"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
   lazy var lineAboveTranDetailView: UIView = {
       let subView = UIView()
       subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       subView.translatesAutoresizingMaskIntoConstraints = false
       return subView
     }()
   
   lazy var descriptionTranDetailLabel: UILabel = {
      let label = UILabel()
      label.text = "Description"
      label.textAlignment = .left
      label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
      label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
   
   lazy var titleAmountTranDetailLabel: UILabel = {
     let label = UILabel()
     label.text = "Amount"
     label.textAlignment = .right
     label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
   
   lazy var lineBellowTranDetailView: UIView = {
       let subView = UIView()
       subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       subView.translatesAutoresizingMaskIntoConstraints = false
       return subView
     }()
  
  
  lazy var transitonDetailTableView:UITableView = {
   let tableView = UITableView()
        tableView.register(AccountTransitionDetailCell.self, forCellReuseIdentifier: transitionDetailIdentifier)
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
    return tableView
  }()
  
  
  lazy var orderTranDetailLabel: UILabel = {
    let label = UILabel()
    label.text = "Order Amount: $4.20"
    label.textAlignment = .right
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var rewardPointTranDetailLabel: UILabel = {
    let label = UILabel()
    label.text = "RewardPoint: 4 point(s)"
    label.textAlignment = .right
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var memberRewardTranDetailLabel: UILabel = {
    let label = UILabel()
    label.text = "Membership Reward: 2 point(s)"
    label.textAlignment = .right
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    setupUI()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  fileprivate func setupUI(){
     addSubview(titleTransitionDetailLabel)
     NSLayoutConstraint.activate([
      titleTransitionDetailLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 10),
      titleTransitionDetailLabel.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor)
     ])
     
     let contentAddressTranVStack = UIStackView(arrangedSubviews: [paymentRefTranDetailLabel,dateTranDetailLabel,branchTranDetailLabel])
     contentAddressTranVStack.axis = .vertical
     contentAddressTranVStack.alignment = .fill
     contentAddressTranVStack.distribution = .fill
     contentAddressTranVStack.translatesAutoresizingMaskIntoConstraints = false
     
     addSubview(contentAddressTranVStack)
    
     NSLayoutConstraint.activate([
       contentAddressTranVStack.topAnchor.constraint(equalTo: titleTransitionDetailLabel.bottomAnchor,constant: 10),
       contentAddressTranVStack.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
       contentAddressTranVStack.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
     ])
    
    let contentDescriptionHStack = UIStackView(arrangedSubviews: [descriptionTranDetailLabel,titleAmountTranDetailLabel])
    contentDescriptionHStack.axis = .horizontal
    contentDescriptionHStack.alignment = .fill
    contentDescriptionHStack.distribution = .equalSpacing
    contentDescriptionHStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentDescriptionVStack = UIStackView(arrangedSubviews: [lineAboveTranDetailView,contentDescriptionHStack,lineBellowTranDetailView])
    contentDescriptionVStack.axis = .vertical
    contentDescriptionVStack.alignment = .fill
    contentDescriptionVStack.distribution = .fill
    contentDescriptionVStack.spacing = 10
    contentDescriptionVStack.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentDescriptionVStack)
    
    NSLayoutConstraint.activate([
      contentDescriptionVStack.topAnchor.constraint(equalTo: contentAddressTranVStack.bottomAnchor,constant: 5),
      contentDescriptionVStack.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 10),
      contentDescriptionVStack.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
      lineBellowTranDetailView.heightAnchor.constraint(equalToConstant: 1),
      lineAboveTranDetailView.heightAnchor.constraint(equalToConstant: 1)
    ])
    
    addSubview(transitonDetailTableView)
         NSLayoutConstraint.activate([
           transitonDetailTableView.topAnchor.constraint(equalTo: contentDescriptionVStack.bottomAnchor,constant: 5),
           transitonDetailTableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
           transitonDetailTableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
           //transitonDetailTableView.heightAnchor.constraint(equalToConstant: <#T##CGFloat#>)
           transitonDetailTableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
         ])
    
    let contentBottomTransitionDetailVStack = UIStackView(arrangedSubviews: [orderTranDetailLabel,rewardPointTranDetailLabel,memberRewardTranDetailLabel])
    contentBottomTransitionDetailVStack.axis = .vertical
    contentBottomTransitionDetailVStack.alignment = .fill
    contentBottomTransitionDetailVStack.distribution = .fill
    contentBottomTransitionDetailVStack.spacing = 10
    contentBottomTransitionDetailVStack.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentBottomTransitionDetailVStack)
    
    NSLayoutConstraint.activate([
      contentBottomTransitionDetailVStack.topAnchor.constraint(equalTo: transitonDetailTableView.bottomAnchor,constant: 10),
      contentBottomTransitionDetailVStack.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
    ])
   }
  
  
  
}
