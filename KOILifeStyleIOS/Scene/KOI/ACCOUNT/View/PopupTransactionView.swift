//
//  PopupTransactionView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/21/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PopupTransactionView: UIView {
  
  lazy var bgSubView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .white
    subView.layer.cornerRadius = 10
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var crossImagView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "ic_close")
    imageView.contentMode = .scaleAspectFit
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  lazy var titleTransitionDetailLabel: UILabel = {
      let label = UILabel()
      label.text = "TRANSACTION DETAIL"
      label.textAlignment = .center
      label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
      label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
    
    lazy var paymentRefTranDetailLabel: UILabel = {
      let label = UILabel()
      label.textAlignment = .left
      label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
      label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
    
   lazy var dateTranDetailLabel: UILabel = {
       let label = UILabel()
       label.text = "Date: 12-11-2019 3:04 PM"
       label.textAlignment = .left
       label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
       label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
     }()
   
   lazy var branchTranDetailLabel: UILabel = {
     let label = UILabel()
     label.text = "KOI The Rule"
     label.textAlignment = .left
     label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
     label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
   
    lazy var lineAboveTranDetailView: UIView = {
        let subView = UIView()
        subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
      }()
    
    lazy var descriptionTranDetailLabel: UILabel = {
       let label = UILabel()
       label.text = "Description"
       label.textAlignment = .left
       label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
     }()
    
    lazy var titleAmountTranDetailLabel: UILabel = {
      let label = UILabel()
      label.text = "Amount"
      label.textAlignment = .right
      label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
      label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
    }()
    
    lazy var lineBellowTranDetailView: UIView = {
        let subView = UIView()
        subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
      }()
  
  lazy var descriptionValueLabel: UILabel = {
    let label = UILabel()
    label.text = "Top-Up to card 5803580050350053"
    label.textAlignment = .right
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  
  lazy var amountValueLabel: UILabel = {
    let label = UILabel()
    label.text = "+$2.00"
    label.textAlignment = .right
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var lineBellowView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  
  fileprivate func setupUI(){
    //backgroundColor = .white
    
    addSubview(bgSubView)
    NSLayoutConstraint.activate([
      bgSubView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 20),
      bgSubView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -20),
      bgSubView.centerXAnchor.constraint(equalTo: centerXAnchor),
      bgSubView.centerYAnchor.constraint(equalTo: centerYAnchor)
    ])
    bgSubView.addSubview(crossImagView)
    NSLayoutConstraint.activate([
      crossImagView.trailingAnchor.constraint(equalTo: bgSubView.trailingAnchor,constant: 10),
      crossImagView.topAnchor.constraint(equalTo: bgSubView.topAnchor,constant: -10),
      crossImagView.widthAnchor.constraint(equalToConstant: 40),
      crossImagView.heightAnchor.constraint(equalToConstant: 40)
    ])
    
    bgSubView.addSubview(titleTransitionDetailLabel)
     NSLayoutConstraint.activate([
      titleTransitionDetailLabel.topAnchor.constraint(equalTo: bgSubView.topAnchor),
      titleTransitionDetailLabel.leadingAnchor.constraint(equalTo: bgSubView.leadingAnchor,constant: 20),
      titleTransitionDetailLabel.trailingAnchor.constraint(equalTo: bgSubView.trailingAnchor,constant: -20),
      
     ])
     
     let contentAddressTranVStack = UIStackView(arrangedSubviews: [dateTranDetailLabel,branchTranDetailLabel])
     contentAddressTranVStack.axis = .vertical
     contentAddressTranVStack.alignment = .fill
     contentAddressTranVStack.distribution = .fill
     contentAddressTranVStack.translatesAutoresizingMaskIntoConstraints = false
     
    bgSubView.addSubview(contentAddressTranVStack)
    
     NSLayoutConstraint.activate([
       contentAddressTranVStack.topAnchor.constraint(equalTo: titleTransitionDetailLabel.bottomAnchor,constant: 10),
       contentAddressTranVStack.leadingAnchor.constraint(equalTo: bgSubView.leadingAnchor,constant: 10),
       contentAddressTranVStack.trailingAnchor.constraint(equalTo: bgSubView.trailingAnchor,constant: -10),
     ])
    
    let contentDescriptionHStack = UIStackView(arrangedSubviews: [descriptionTranDetailLabel,titleAmountTranDetailLabel])
    contentDescriptionHStack.axis = .horizontal
    contentDescriptionHStack.alignment = .fill
    contentDescriptionHStack.distribution = .equalSpacing
    contentDescriptionHStack.translatesAutoresizingMaskIntoConstraints = false
    
    
    let contentValueDescriptionHStack = UIStackView(arrangedSubviews: [descriptionValueLabel,amountValueLabel])
    contentValueDescriptionHStack.axis = .horizontal
    contentValueDescriptionHStack.alignment = .fill
    contentValueDescriptionHStack.distribution = .equalSpacing
    contentValueDescriptionHStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentDescriptionVStack = UIStackView(arrangedSubviews: [lineAboveTranDetailView,contentDescriptionHStack,lineBellowTranDetailView,contentValueDescriptionHStack,lineBellowView])
    contentDescriptionVStack.axis = .vertical
    contentDescriptionVStack.alignment = .fill
    contentDescriptionVStack.distribution = .fill
    contentDescriptionVStack.spacing = 10
    contentDescriptionVStack.translatesAutoresizingMaskIntoConstraints = false
    
    bgSubView.addSubview(contentDescriptionVStack)
    
    NSLayoutConstraint.activate([
      contentDescriptionVStack.topAnchor.constraint(equalTo: contentAddressTranVStack.bottomAnchor,constant: 5),
      contentDescriptionVStack.leadingAnchor.constraint(equalTo: bgSubView.leadingAnchor,constant: 20),
      contentDescriptionVStack.trailingAnchor.constraint(equalTo: bgSubView.trailingAnchor,constant: -20),
      contentAddressTranVStack.centerXAnchor.constraint(equalTo: bgSubView.centerXAnchor),
      contentAddressTranVStack.centerYAnchor.constraint(equalTo: bgSubView.centerYAnchor),
      contentDescriptionVStack.bottomAnchor.constraint(equalTo: bgSubView.bottomAnchor,constant: -10),
      lineBellowTranDetailView.heightAnchor.constraint(equalToConstant: 1),
      lineAboveTranDetailView.heightAnchor.constraint(equalToConstant: 1),
      lineBellowView.heightAnchor.constraint(equalToConstant: 1)
    ])
    
  }
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
