//
//  AccountHeaderCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AccountHeaderCell: UITableViewCell {

  lazy var bgAccountHeaderView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var titleHeaderAccountLabel: UILabel = {
    let label = UILabel()
    let attributedString = NSMutableAttributedString(string: "BALANCE\n$2.10")
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 2
    // *** Apply attribute to string ***
    attributedString.addAttribute(.font, value:UIFont.systemFont(ofSize: 23, weight: .bold), range:NSRange(location: 0, length: attributedString.length))
    attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
    label.attributedText = attributedString
    label.numberOfLines = 0
    label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textAlignment = .center
    return label
  }()
  
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
   
    setupUI()
  }
  
  fileprivate func setupUI(){
     addSubview(bgAccountHeaderView)
    
    NSLayoutConstraint.activate([
      bgAccountHeaderView.topAnchor.constraint(equalTo: topAnchor),
      bgAccountHeaderView.leadingAnchor.constraint(equalTo: leadingAnchor),
      bgAccountHeaderView.trailingAnchor.constraint(equalTo: trailingAnchor),
      bgAccountHeaderView.bottomAnchor.constraint(equalTo: bottomAnchor),
    ])
    
    bgAccountHeaderView.addSubview(titleHeaderAccountLabel)
    
    NSLayoutConstraint.activate([
      titleHeaderAccountLabel.centerXAnchor.constraint(equalTo: bgAccountHeaderView.centerXAnchor),
      titleHeaderAccountLabel.centerYAnchor.constraint(equalTo: bgAccountHeaderView.centerYAnchor),
      titleHeaderAccountLabel.topAnchor.constraint(equalTo: bgAccountHeaderView.topAnchor,constant: 10),
      titleHeaderAccountLabel.bottomAnchor.constraint(equalTo: bgAccountHeaderView.bottomAnchor,constant: -10)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
