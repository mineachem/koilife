//
//  AccountTransitionCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AccountTransitionCell: UITableViewCell {

  
  var customerHistoryItemList: ResultHistory? {
    didSet{
      print("cmd \(customerHistoryItemList!.cmd) \(customerHistoryItemList?.amt) \(customerHistoryItemList?.rewardType)")
        

        if let transactionDate = customerHistoryItemList?.transactionDate {
                let getdateString = transactionDate.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "dd/MM/yyyy HH:mm a"
                let dateTransaction = dateFormat.string(from: getdateString)
                titledateTransitionLabel.text = dateTransaction
              }
        if let cardNumber = customerHistoryItemList?.cardNumber {
                   cardIssueTransitionLabel.text = "Card Issue:\(cardNumber)"
                 }
                 if let amount = customerHistoryItemList?.amt {
                   priceTransitionLabel.text = "$" + String(format: "%.2f", amount)
                 }
        
      switch customerHistoryItemList!.cmd {
        case "rwd":
        if let nameBranch = customerHistoryItemList?.branch?.name {
             switch customerHistoryItemList?.rewardType {
                   case 1:
                   statusTransitionLabel.text = "Birthday at branch \(nameBranch)"
                   case 2:
                   statusTransitionLabel.text = "Allowance at branch \(nameBranch)"
                   case 3:
                   statusTransitionLabel.text = "Discount at branch \(nameBranch)"
                     case 4:
                        statusTransitionLabel.text = "Dis Birthday at branch \(nameBranch)"
                   default:
                        statusTransitionLabel.text = "Product Claim at branch \(nameBranch)"
                   }
              }
        case "tpp":
          if let nameBranch = customerHistoryItemList?.branch?.name {
            statusTransitionLabel.text = "Top up at branch \(nameBranch)"
          }
        
          case "pay":
             if let nameBranch = customerHistoryItemList?.branch?.name {
                statusTransitionLabel.text = "Pay at branch \(nameBranch)"
              }
            break
        
        case "cnc":
                 switch customerHistoryItemList!.cancelType {
                 case "rwd":
                          if let nameBranch = customerHistoryItemList?.branch?.name {
                           if customerHistoryItemList?.rewardType == 5 {
                        statusTransitionLabel.text = "Cancel Product Claim at branch \(nameBranch)"
           
                       }
                     }
                 case "tpp":
                               
                   
                     if let nameBranch = customerHistoryItemList?.branch?.name {
                        statusTransitionLabel.text = "Cancel Top up at branch \(nameBranch)"
                   
                     }
                   
                 default:
                    if let nameBranch = customerHistoryItemList?.branch?.name {
                       statusTransitionLabel.text = "Cancel Pay at branch \(nameBranch)"
                  
                    }
                 }
        default:
          print("nothing")
        }
      
    }
  }
  lazy var titledateTransitionLabel: UILabel = {
    let label = UILabel()
    label.text = "12/11/2019 03:04 PM"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var cardIssueTransitionLabel: UILabel = {
    let label = UILabel()
    label.text = "Card Issue: 8888880059610"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var statusTransitionLabel: UILabel = {
    let label = UILabel()
    label.text = "Pay to branch KOI The Rule"
    label.textAlignment = .left
    label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  
  lazy var priceTransitionLabel: UILabel = {
     let label = UILabel()
     label.text = "-$4.20"
     label.textAlignment = .right
     label.textColor = #colorLiteral(red: 0.8559798598, green: 0.6504896879, blue: 0.2624273896, alpha: 1)
     label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    setupUI()
  }
  
  fileprivate func setupUI(){
    let contentTransitionVStack = UIStackView(arrangedSubviews: [titledateTransitionLabel,cardIssueTransitionLabel,statusTransitionLabel])
    contentTransitionVStack.axis = .vertical
    contentTransitionVStack.alignment = .fill
    contentTransitionVStack.distribution = .fillEqually
    contentTransitionVStack.spacing = 10
    contentTransitionVStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentTransitionHStack = UIStackView(arrangedSubviews: [contentTransitionVStack,priceTransitionLabel])
    contentTransitionHStack.axis = .horizontal
    contentTransitionHStack.alignment = .fill
    contentTransitionHStack.distribution = .equalSpacing
    contentTransitionHStack.translatesAutoresizingMaskIntoConstraints = false
    addSubview(contentTransitionHStack)
    
    NSLayoutConstraint.activate([
      contentTransitionHStack.centerXAnchor.constraint(equalTo: centerXAnchor),
      contentTransitionHStack.topAnchor.constraint(equalTo: topAnchor,constant: 10),
      contentTransitionHStack.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
      contentTransitionHStack.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
      contentTransitionHStack.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
    ])
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
