//
//  AccountVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import EzPopup
import NVActivityIndicatorView

class AccountTransitionVC: UIViewController {
    
    var emptyView = EmptyInboxView()
    var refreshCtr = UIRefreshControl()
    lazy var loading: NVActivityIndicatorView = {
        let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
        loading.translatesAutoresizingMaskIntoConstraints = false
        return loading
    }()
  
  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    let titleNav = UILabel()
    titleNav.text = "ACCOUNT"
    titleNav.font = UIFont.boldSystemFont(ofSize: 14)
    titleNav.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    navItem.titleView = titleNav
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "left-arrow").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
  
  var accountTableView:UITableView!
   private let accountTransitionIdentifier = "accountCell"
   private let accountHeaderIdentifier = "accontHeaderCell"
  
  var customerHistoryItemList = [ResultHistory]()
 // var customerItemProduct:ResultsHistoryDetail?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIndicator()
        emptyView.emptyDescription.text = "Account has no items."
      view.backgroundColor = .white
      getHistoryFromService()
      setupNavigationBar()
        setupTableView()
        accountTableView.backgroundView = loading
    }
    
  
  fileprivate func getHistoryFromService(){
    let customerWalletId = UserDefaults.standard.value(forKey: "id") as? Int
    self.showLoading()
    
    IGNetworkRequest.shareInstance.requestGET(KoiService.share.customerHistoryListUrl + "\(customerWalletId!)?&page=0&size=10", success: { (customerHistory:CustomerHistory) in

      if customerHistory.response?.code == 200 {

        self.customerHistoryItemList = customerHistory.results!
        if self.customerHistoryItemList.count > 0{
            DispatchQueue.main.async {
              self.accountTableView.reloadData()
                self.accountTableView.backgroundView = .none
            }
        }else{
            self.accountTableView.backgroundView = self.emptyView
        }
        
      }else {
        print(customerHistory.response?.message)
        self.accountTableView.backgroundView = self.emptyView
      }
        self.hideLoading()
        self.refreshCtr.endRefreshing()
    }) { (failure) in
        print(failure)
        self.hideLoading()
        self.refreshCtr.endRefreshing()
        self.accountTableView.backgroundView = self.emptyView
    }
  }
  
  fileprivate func setupNavigationBar(){
      view.addSubview(titleNavigationBar)
      NSLayoutConstraint.activate([
        titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
        titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
      ])
    }
    
    fileprivate func setupIndicator(){
         view.addSubview(loading)
         NSLayoutConstraint.activate([
           loading.widthAnchor.constraint(equalToConstant: 40),
           loading.heightAnchor.constraint(equalToConstant: 40),
           loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
           loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
         ])

    }
  
  fileprivate func setupTableView(){
      accountTableView = UITableView()
    accountTableView.delegate = self
    accountTableView.dataSource = self
    refreshCtr.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
    accountTableView.addSubview(refreshCtr)
    accountTableView.register(AccountTransitionCell.self, forCellReuseIdentifier: accountTransitionIdentifier)
    accountTableView.register(AccountHeaderCell.self, forCellReuseIdentifier: accountHeaderIdentifier)
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    accountTableView.tableFooterView = subView
    accountTableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    accountTableView.showsVerticalScrollIndicator = false
    accountTableView.showsHorizontalScrollIndicator = false
    accountTableView.translatesAutoresizingMaskIntoConstraints = false
    
    view.addSubview(accountTableView)
    
    NSLayoutConstraint.activate([
      accountTableView.topAnchor.constraint(equalTo: titleNavigationBar.bottomAnchor),
      accountTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      accountTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      accountTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
    ])
  }
    
    func showLoading()
    {
        self.loading.startAnimating()
    }

    func hideLoading()
    {
         loading.stopAnimating()
    }
    
    @objc func refresh() {
        getHistoryFromService()
    }
  
  @objc private func handleBackTapped(){
     self.dismiss(animated: true)
   }
  
  
}

extension AccountTransitionVC:UITableViewDelegate,UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return customerHistoryItemList.count
   
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
    let accountCell = tableView.dequeueReusableCell(withIdentifier: accountTransitionIdentifier, for: indexPath) as! AccountTransitionCell
    accountCell.customerHistoryItemList = customerHistoryItemList[indexPath.row]
    return accountCell
  }
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
   let getCustomerDetail = customerHistoryItemList[indexPath.row]
    
    switch getCustomerDetail.cmd {
    case "tpp","ver":
        let popupTransactionVC = PopupTransactionVC()
          popupTransactionVC.getCustomerDetail = getCustomerDetail
          let popupVC = PopupViewController(contentController: popupTransactionVC, position: .center(CGPoint(x: 0, y: 30)), popupWidth: view.bounds.width, popupHeight: view.bounds.height-60)
          popupVC.backgroundAlpha = 0.3
          popupVC.backgroundColor = .black
          popupVC.canTapOutsideToDismiss = true
          popupVC.cornerRadius = 10
          popupVC.shadowEnabled = true
          self.present(popupVC, animated: true)
    case "cnc":
      switch getCustomerDetail.cancelType {
      case "tpp","pay","rwd":
       let popupTransactionVC = PopupTransactionVC()
        popupTransactionVC.getCustomerDetail = getCustomerDetail
        let popupVC = PopupViewController(contentController: popupTransactionVC, position: .center(CGPoint(x: 0, y: 30)), popupWidth: view.bounds.width, popupHeight: view.bounds.height-60)
        popupVC.backgroundAlpha = 0.3
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
      
      default:
        print("nothing")
      }
      let popupTransactionVC = PopupTransactionVC()
      popupTransactionVC.getCustomerDetail = getCustomerDetail
      let popupVC = PopupViewController(contentController: popupTransactionVC, position: .center(CGPoint(x: 0, y: 30)), popupWidth: view.bounds.width, popupHeight: view.bounds.height-60)
      popupVC.backgroundAlpha = 0.3
      popupVC.backgroundColor = .black
      popupVC.canTapOutsideToDismiss = true
      popupVC.cornerRadius = 10
      popupVC.shadowEnabled = true
      self.present(popupVC, animated: true)
    default:
  
      self.getCustomerDetail(cmd:getCustomerDetail.cmd!,refId:getCustomerDetail.refId!)
    }

    
  }
  
  private func getCustomerDetail(cmd:String,refId:Int){
    
    IGNetworkRequest.shareInstance.requestGET(KoiService.share.customerHistoryDetailUrl + "cmd/\(cmd)/refId/\(refId)", success: { (customerHistoryDetail:CustomerHistoryDetail) in
      
      if customerHistoryDetail.response?.code == 200 {
        
        let customerItemProduct = customerHistoryDetail.results
         let accountTransitonDetailVC = AccountTransitionDetailVC()
        accountTransitonDetailVC.customerItemProduct = customerItemProduct
        let popupVC = PopupViewController(contentController: accountTransitonDetailVC, position: .center(CGPoint(x: 0, y: 10)), popupWidth: self.view.bounds.width-30, popupHeight: self.view.bounds.height-30)
                    popupVC.backgroundAlpha = 0.3
                    popupVC.backgroundColor = .black
                    popupVC.canTapOutsideToDismiss = true
                    popupVC.cornerRadius = 10
                    popupVC.shadowEnabled = true
                    self.present(popupVC, animated: true)
      }else {
        print(customerHistoryDetail.response?.message ?? "")
      }
    }) { (failure) in
      print(failure)
    }
  }
}
