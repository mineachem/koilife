//
//  AccountTransitionDetailVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/4/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class AccountTransitionDetailVC: UIViewController {

  
  
  var getCustomerDetail: ResultHistory?
  var customerItemProduct:ResultsHistoryDetail?
  
  var transitonDetailTableView:UITableView!
  private let transitionDetailIdentifier = "transitionDetailCell"
  private let transitionDetailHeaderIdentifier = "transitonDetailHeaderCell"
  private let transitionDetailFooterIdentifier = "transitonDetailFooterCell"
  
  let cellHight:CGFloat = 8
  
  lazy var crossImagView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "ic_close")
    imageView.contentMode = .scaleAspectFit
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  lazy var bgView: UIView = {
    let subView = UIView()
    
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
    override func viewDidLoad() {
        super.viewDidLoad()
      //view.backgroundColor = .white
        setupTableView()
        setupView()
        setupEvent()
    }
    
  fileprivate func setupView(){
    view.addSubview(crossImagView)
    NSLayoutConstraint.activate([
      crossImagView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 10),
      crossImagView.rightAnchor.constraint(equalTo: transitonDetailTableView.rightAnchor,constant: 10),
      crossImagView.widthAnchor.constraint(equalToConstant: 20),
      crossImagView.heightAnchor.constraint(equalToConstant: 20)
    ])
  }
  fileprivate func setupTableView(){
    transitonDetailTableView = UITableView()
    transitonDetailTableView.delegate = self
    transitonDetailTableView.dataSource = self
    transitonDetailTableView.separatorColor = .clear
    transitonDetailTableView.register(AccountTransitionDetailCell.self, forCellReuseIdentifier: transitionDetailIdentifier)
    transitonDetailTableView.register(AccountTransitonDetailHeaderCell.self, forCellReuseIdentifier: transitionDetailHeaderIdentifier)
    transitonDetailTableView.register(AccountTransitionDetailFooterCell.self, forCellReuseIdentifier: transitionDetailFooterIdentifier)
    transitonDetailTableView.showsVerticalScrollIndicator = false
    transitonDetailTableView.showsHorizontalScrollIndicator = false
   // transitonDetailTableView.isScrollEnabled = false
    transitonDetailTableView.separatorStyle = .none
    transitonDetailTableView.layer.cornerRadius = 10
    transitonDetailTableView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(transitonDetailTableView)
    
    NSLayoutConstraint.activate([
      transitonDetailTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 20),
      transitonDetailTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 20),
      transitonDetailTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -20),
      transitonDetailTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -10)
    ])
  }

  fileprivate func setupEvent(){
    let crossTapped = UITapGestureRecognizer(target: self, action: #selector(handlecrossTapped))
    crossImagView.addGestureRecognizer(crossTapped)
    crossImagView.isUserInteractionEnabled = true
  }
  
  @objc private func handlecrossTapped(){
    self.dismiss(animated: true)
  }
}

extension AccountTransitionDetailVC:UITableViewDelegate,UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return 1
    }else if section == 2{
      return 1
    }else {
      return customerItemProduct?.products?.count ?? 0
    }
    
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.section == 0 && indexPath.row == 0 {
      let headerCell = tableView.dequeueReusableCell(withIdentifier: transitionDetailHeaderIdentifier) as! AccountTransitonDetailHeaderCell
      headerCell.getCustomerDetail = getCustomerDetail
         headerCell.backgroundColor = .white
         return headerCell
    }
    
    if indexPath.section == 2 && indexPath.row == 0 {
          let footerCell = tableView.dequeueReusableCell(withIdentifier: transitionDetailFooterIdentifier) as! AccountTransitionDetailFooterCell
         footerCell.resultProduct = customerItemProduct
          footerCell.backgroundColor = .white
          return footerCell
    }
    
      let transitionDetailCell = tableView.dequeueReusableCell(withIdentifier: transitionDetailIdentifier, for: indexPath) as! AccountTransitionDetailCell
    
       transitionDetailCell.getproductItem = customerItemProduct!.products![indexPath.row]
    
      return transitionDetailCell
  
  }
    
    
  
}
