//
//  PopupTransactionVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/21/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class PopupTransactionVC: UIViewController {

   var getCustomerDetail: ResultHistory?
  lazy var popupTranView = PopupTransactionView()
  
  override func loadView() {
    super.loadView()
    self.view = popupTranView
  }
  
  
    override func viewDidLoad() {
        super.viewDidLoad()

      setupUI()
      setupEvent()
    }
  fileprivate func setupUI(){
           
          if let dateTransaction = getCustomerDetail?.transactionDate {
            let getdateString = dateTransaction.dateFromMilliseconds(format: "yyyy-MM-dd HH:mm:ss Z")
             let dateFormat = DateFormatter()
             dateFormat.dateFormat = "dd-MM-yy HH:mm a"
             let dateTransaction = dateFormat.string(from: getdateString)
            popupTranView.dateTranDetailLabel.text =  "Date:" + dateTransaction
          }
         
            if let branch = getCustomerDetail?.branch?.name {
              popupTranView.branchTranDetailLabel.text = "Branch:" + branch
         }
    
       if let cardNumber = getCustomerDetail?.cardNumber {
        popupTranView.descriptionValueLabel.text = "Card Issue:\(cardNumber)"
         }
         
        
         
         if let amount = getCustomerDetail?.amt {
          popupTranView.amountValueLabel.text = "$" + String(format: "%.2f", amount)
         }
  }
  
  fileprivate func setupEvent(){
    let closeTapped = UITapGestureRecognizer(target: self, action: #selector(handleCloseTapped))
    popupTranView.crossImagView.addGestureRecognizer(closeTapped)
    popupTranView.crossImagView.isUserInteractionEnabled = true
  }

  @objc private func handleCloseTapped(){
    self.dismiss(animated: true)
  }
}
