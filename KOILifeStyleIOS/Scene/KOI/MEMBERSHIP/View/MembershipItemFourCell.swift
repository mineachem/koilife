//
//  MembershipItemFourCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MembershipItemFourCell: UITableViewCell {
    
    
  lazy var bgGoldMemberView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    subView.layer.cornerRadius = 10
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
    lazy var titlekoiLabel: UILabel = {
      let label = UILabel()
      let attributedString = NSMutableAttributedString(string:
      """
          កាតសមាជិកមាស (300 ពិន្ទុ)
          អត្ថប្រយោជន៍ នៃកាតសមាជិកមាស

          1.កាដូពិសេសសម្រាប់ថ្ងៃខួបកំណើត
              .ទទួលបានភេសជ្ជៈ កែវកណ្តាលមួយកែវដោយឥតគិតថ្លៃ
          
          2.ទទួលបានភេសជ្ជៈពីរកែវ តូច ដោយឥតគិតថ្លៃ ជារៀងរាល់ខែ
          3.រីករាយជាមួយឱកាសចូលរួមកម្មវិធី ប្រូម៉ូសិនមុន គេបង្អស់ និង បន្តបានយូរជាងគេ (សម្រាប់តែ កាតសមាជិកប្រាក់ និង មាសតែប៉ុណ្ណោះ)

          4.រីករាយជាមួយកម្មវិធីពិសេស ដែលបង្កើត សម្រាប់តែសមាជិកទឹកប្រាក់ និង មាសតែ ប៉ុណ្ណោះ

          5.រីករាយជាមួយពិន្ទុគុណនឹងពីរ ទៅតាមកម្មវិធី ដែលបានកំណត់

          6.រីករាយនឹងការទទួលបានសំបុត្រអញ្ចើញចូលរួម កម្មវិធីពិសេសៗរបស់ ខយ (សម្រាប់តែ សមាជិកមាសតែប៉ុណ្ណោះ)
          
         វិិធីប្រើប្រាស់កាតសមាជិកប្រាក់

        1.លោកអ្នកត្រូវមានកាតសមាជិកខ្មៅ ដែលចុះឈ្មោះរួច និង មានដំណើរការ
        2.ការសន្សំពិន្ទុ KOI Point របស់ លោកអ្នកពី 150 ទៅ 300 ពិន្ទុ (1 កែវ = 1 ពិន្ទុ)
        3.ការសន្សំពិន្ទុ KOI Point របស់ លោកអ្នកពី 301 ពិន្ទុឡើងទៅ (1 កែវ = 1 ពិន្ទុ)
      """)
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.lineSpacing = 2
      // *** Apply attribute to string ***
      
      
      attributedString.addAttribute(.font, value:UIFont(name: "Montserrat-Medium", size: 14)!, range:NSRange(location: 0, length: attributedString.length))
      attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
      label.attributedText = attributedString
      label.numberOfLines = 0
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .white
      return label
    }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
     super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupView()
   }
   
  fileprivate func setupView(){
    backgroundColor = .clear
    addSubview(bgGoldMemberView)
    NSLayoutConstraint.activate([
      bgGoldMemberView.topAnchor.constraint(equalTo: topAnchor,constant: 10),
      bgGoldMemberView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 15),
      bgGoldMemberView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -15),
      bgGoldMemberView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
    ])
    
    bgGoldMemberView.addSubview(titlekoiLabel)
    NSLayoutConstraint.activate([
      titlekoiLabel.topAnchor.constraint(equalTo: bgGoldMemberView.topAnchor,constant: 10),
      titlekoiLabel.leadingAnchor.constraint(equalTo: bgGoldMemberView.leadingAnchor,constant: 5),
      titlekoiLabel.trailingAnchor.constraint(equalTo: bgGoldMemberView.trailingAnchor,constant: -10),
      titlekoiLabel.bottomAnchor.constraint(equalTo: bgGoldMemberView.bottomAnchor,constant: -10)
    ])
  }
  
   required init?(coder: NSCoder) {
     fatalError("init(coder:) has not been implemented")
   }
}
