//
//  MembershipItemSevenCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MembershipItemSevenCell: UITableViewCell {
     
     lazy var bgSliverMemberView: UIView = {
       let subView = UIView()
       subView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
       subView.layer.cornerRadius = 10
       subView.translatesAutoresizingMaskIntoConstraints = false
       return subView
     }()
     
       lazy var titlekoiLabel: UILabel = {
         let label = UILabel()
         let attributedString = NSMutableAttributedString(string:
         """
             Silver Membership (150 Point - 300 Points)
             Benefits of Silver Membership

             1.Special Birthday Presents

                 .Obtain One Small-Sized drink (our treat of course)
             
             2.Get ONE Free small-sized monthly

             3.Get an early and extended access to promotions and events! (Applicable for Silver and Gold Memberships ONLY)

             4.Get special privileges to join our EXCLUSIVE promotions tailored specificall for Silver and Gold Memberships ONLY

           Criteria of Silver Membership Card

           1.You must have a registered Black Membership Card

           2.Save your KOI Membership Point from 151 to 300 points (1 Cup = 1 Point)

         """)
         let paragraphStyle = NSMutableParagraphStyle()
         paragraphStyle.lineSpacing = 2
         // *** Apply attribute to string ***
         
         
         attributedString.addAttribute(.font, value:UIFont(name: "Montserrat-Medium", size: 14)!, range:NSRange(location: 0, length: attributedString.length))
         attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
         label.attributedText = attributedString
         label.numberOfLines = 0
         label.translatesAutoresizingMaskIntoConstraints = false
         label.textColor = .white
         return label
       }()

     override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       setupView()
      }
      
     fileprivate func setupView(){
        backgroundColor = .clear
       addSubview(bgSliverMemberView)
       NSLayoutConstraint.activate([
         bgSliverMemberView.topAnchor.constraint(equalTo: topAnchor,constant: 10),
         bgSliverMemberView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 15),
         bgSliverMemberView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -15),
         bgSliverMemberView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
       ])
       
       bgSliverMemberView.addSubview(titlekoiLabel)
       NSLayoutConstraint.activate([
         titlekoiLabel.topAnchor.constraint(equalTo: bgSliverMemberView.topAnchor,constant: 10),
         titlekoiLabel.leadingAnchor.constraint(equalTo: bgSliverMemberView.leadingAnchor,constant: 5),
         titlekoiLabel.trailingAnchor.constraint(equalTo: bgSliverMemberView.trailingAnchor,constant: -10),
         titlekoiLabel.bottomAnchor.constraint(equalTo: bgSliverMemberView.bottomAnchor,constant: -10)
       ])
     }
     
      required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
      }
}
