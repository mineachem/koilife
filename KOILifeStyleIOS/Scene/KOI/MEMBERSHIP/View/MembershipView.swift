//
//  MembershipView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/13/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MembershipView: UIView {


  lazy var membertableView: UITableView = {
    let tableView = UITableView()
    tableView.separatorStyle = .none
    tableView.backgroundColor = .clear
    tableView.alwaysBounceVertical = false
    tableView.showsVerticalScrollIndicator = false
    tableView.showsHorizontalScrollIndicator = false
    
    tableView.register(MembershipItemOneCell.self, forCellReuseIdentifier: "MembershipItemOneCell")
    tableView.register(MembershipItemTwoCell.self, forCellReuseIdentifier: "MembershipItemTwoCell")
    tableView.register(MembershipItemThreeCell.self, forCellReuseIdentifier: "MembershipItemThreeCell")
    tableView.register(MembershipItemFourCell.self, forCellReuseIdentifier: "MembershipItemFourCell")
    tableView.register(MembershipItemFiveCell.self, forCellReuseIdentifier: "MembershipItemFiveCell")
    tableView.register(MembershipItemSixCell.self, forCellReuseIdentifier: "MembershipItemSixCell")
    tableView.register(MembershipItemSevenCell.self, forCellReuseIdentifier: "MembershipItemSevenCell")
    tableView.register(MembershipItemEightCell.self, forCellReuseIdentifier: "MembershipItemEightCell")
    tableView.translatesAutoresizingMaskIntoConstraints = false
    return tableView
  }()
  
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.image = UIImage(named: "img_bg_article")
        iv.contentMode = .scaleToFill
        return iv
    }()
     override init(frame: CGRect) {
       super.init(frame: frame)
       
       setupUI()
     }
     
     fileprivate func setupUI(){
    
       addSubview(backgroundImage)
        backgroundImage.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor)
    
       addSubview(membertableView)
      NSLayoutConstraint.activate([
        membertableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 44),
        membertableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
        membertableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
        membertableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
      ])
     }
     
     required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
     }
}
