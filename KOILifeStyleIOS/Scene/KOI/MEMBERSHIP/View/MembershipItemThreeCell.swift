//
//  MembershipItemThreeCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MembershipItemThreeCell: UITableViewCell {
    
  
  lazy var bgSliverMemberView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    subView.layer.cornerRadius = 10
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
    lazy var titlekoiLabel: UILabel = {
      let label = UILabel()
      let attributedString = NSMutableAttributedString(string:
      """
          កាតសមាជិកប្រាក់ (150 ពិន្ទុ- 300 ពិន្ទុ)
          អត្ថប្រយោជន៍ នៃកាតសមាជិកប្រាក់

          1.កាដូពិសេសសម្រាប់ថ្ងៃខួបកំណើត

              .ទទួលបានភេសជ្ជៈ មួយកែវ តូចដោយឥតគិតថ្លៃ
          
          2.ទទួលបានភេសជ្ជៈមួយកែវ តូច ដោយឥតគិតថ្លៃ ជារៀងរាល់ខែ

          3.រីករាយជាមួយឱកាសចូលរួមកម្មវិធី ប្រូម៉ូសិនមុន គេបង្អស់ និង បន្តបានយូរជាងគេ (សម្រាប់តែ កាតសមាជិកប្រាក់ និង មាសតែប៉ុណ្ណោះ)

          4.រីករាយជាមួយកម្មវិធីពិសេស ដែលបង្កើត សម្រាប់តែសមាជិកទឹកប្រាក់ និង មាសតែ ប៉ុណ្ណោះ

         វិិធីប្រើប្រាស់កាតសមាជិកប្រាក់

        1.លោកអ្នកត្រូវមានកាតសមាជិកខ្មៅ ដែលចុះឈ្មោះរួច និង មានដំណើរការ

        2.ការសន្សំពិន្ទុ KOI Point របស់ លោកអ្នកពី 150 ទៅ 300 ពិន្ទុ (1 កែវ = 1 ពិន្ទុ)

      """)
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.lineSpacing = 2
      // *** Apply attribute to string ***
      
      
      attributedString.addAttribute(.font, value:UIFont(name: "Montserrat-Medium", size: 14)!, range:NSRange(location: 0, length: attributedString.length))
      attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
      label.attributedText = attributedString
      label.numberOfLines = 0
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .white
      return label
    }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
     super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupView()
   }
   
  fileprivate func setupView(){
    backgroundColor = .clear
    addSubview(bgSliverMemberView)
    NSLayoutConstraint.activate([
      bgSliverMemberView.topAnchor.constraint(equalTo: topAnchor,constant: 10),
      bgSliverMemberView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 15),
      bgSliverMemberView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -15),
      bgSliverMemberView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
    ])
    
    bgSliverMemberView.addSubview(titlekoiLabel)
    NSLayoutConstraint.activate([
      titlekoiLabel.topAnchor.constraint(equalTo: bgSliverMemberView.topAnchor,constant: 10),
      titlekoiLabel.leadingAnchor.constraint(equalTo: bgSliverMemberView.leadingAnchor,constant: 5),
      titlekoiLabel.trailingAnchor.constraint(equalTo: bgSliverMemberView.trailingAnchor,constant: -10),
      titlekoiLabel.bottomAnchor.constraint(equalTo: bgSliverMemberView.bottomAnchor,constant: -10)
    ])
  }
  
   required init?(coder: NSCoder) {
     fatalError("init(coder:) has not been implemented")
   }
}
