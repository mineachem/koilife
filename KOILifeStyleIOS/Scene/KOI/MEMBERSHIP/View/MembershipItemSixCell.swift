//
//  MembershipItemSixCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MembershipItemSixCell: UITableViewCell {
   
     lazy var bgBlackMemberView: UIView = {
       let subView = UIView()
       subView.backgroundColor = .black
       subView.layer.cornerRadius = 10
       subView.translatesAutoresizingMaskIntoConstraints = false
       return subView
     }()
     
       lazy var titlekoiLabel: UILabel = {
         let label = UILabel()
         let attributedString = NSMutableAttributedString(string:
         """
             Black Membership (0 point - 150 Point)
             Benefits of Balck Membership

             1.Special Birthday Presents

                 .Obtain One Small-Sized drink (our treat of course)

             Criteria of Black Membership Card

             1.Purchase a Black membership card at any KOI outlet

             2.Register your card with our KOI app

             3.Save your KOI Membership Point from 0 to 150
             
             4.That's it! Start earning points right away!

         """)
         let paragraphStyle = NSMutableParagraphStyle()
         paragraphStyle.lineSpacing = 2
         // *** Apply attribute to string ***
         
         
         attributedString.addAttribute(.font, value:UIFont(name: "Montserrat-Medium", size: 14)!, range:NSRange(location: 0, length: attributedString.length))
         attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
         label.attributedText = attributedString
         label.numberOfLines = 0
         label.translatesAutoresizingMaskIntoConstraints = false
         label.textColor = .white
         return label
       }()

     override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       setupView()
      }
      
     fileprivate func setupView(){
        backgroundColor = .clear
       addSubview(bgBlackMemberView)
       NSLayoutConstraint.activate([
         bgBlackMemberView.topAnchor.constraint(equalTo: topAnchor,constant: 10),
         bgBlackMemberView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 15),
         bgBlackMemberView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -15),
         bgBlackMemberView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
       ])
       
       bgBlackMemberView.addSubview(titlekoiLabel)
       NSLayoutConstraint.activate([
         titlekoiLabel.topAnchor.constraint(equalTo: bgBlackMemberView.topAnchor,constant: 10),
         titlekoiLabel.leadingAnchor.constraint(equalTo: bgBlackMemberView.leadingAnchor,constant: 5),
         titlekoiLabel.trailingAnchor.constraint(equalTo: bgBlackMemberView.trailingAnchor,constant: -10),
         titlekoiLabel.bottomAnchor.constraint(equalTo: bgBlackMemberView.bottomAnchor,constant: -10)
       ])
     }
      required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
      }
}
