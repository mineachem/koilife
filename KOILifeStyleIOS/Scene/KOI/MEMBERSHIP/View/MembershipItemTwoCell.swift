//
//  MembershipItemTwoCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MembershipItemTwoCell: UITableViewCell {
  
  lazy var bgBlackMemberView: UIView = {
    let subView = UIView()
    subView.backgroundColor = .black
    subView.layer.cornerRadius = 10
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
    lazy var titlekoiLabel: UILabel = {
      let label = UILabel()
      let attributedString = NSMutableAttributedString(string:
      """
          កាតសមាជិកខ្មៅ (0 ពិន្ទុ- 150 ពិន្ទុ)
          អត្ថប្រយោជន៍ នៃកាតសមាជិកខ្មៅ
          1.កាដូពិសេសសម្រាប់ថ្ងៃខួបកំណើត

              .ទទួលបានភេសជ្ជៈ មួយកែវ តូចដោយឥតគិតថ្លៃ

          វិធីប្រើប្រាស់កាតសមាជិកខ្មៅ

          1.ជាវកាតសមាជិក ខយ នៅគ្រប់សាខា ខយ ទាំងអស់

          2.ចុះឈ្មោះកាតរបស់លោកអ្នកជា មួយកម្មវិធីទូរស័ព្ទដៃ ខយ តេ

          3.ការសន្សំពិន្ទុសមាជិក ខយ (KOI Membership Point)
          របស់លោកអ្នកពី 0 ទៅ 150 ពិន្ទុ (1 កែវ = 1 ពិន្ទុ)

      """)
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.lineSpacing = 2
      // *** Apply attribute to string ***
      
      
      attributedString.addAttribute(.font, value:UIFont(name: "Montserrat-Medium", size: 14)!, range:NSRange(location: 0, length: attributedString.length))
      attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
      label.attributedText = attributedString
      label.numberOfLines = 0
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .white
      return label
    }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
     super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupView()
   }
   
  fileprivate func setupView(){
    backgroundColor = .clear
    addSubview(bgBlackMemberView)
    NSLayoutConstraint.activate([
      bgBlackMemberView.topAnchor.constraint(equalTo: topAnchor,constant: 10),
      bgBlackMemberView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 15),
      bgBlackMemberView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -15),
      bgBlackMemberView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
    ])
    
    bgBlackMemberView.addSubview(titlekoiLabel)
    NSLayoutConstraint.activate([
      titlekoiLabel.topAnchor.constraint(equalTo: bgBlackMemberView.topAnchor,constant: 10),
      titlekoiLabel.leadingAnchor.constraint(equalTo: bgBlackMemberView.leadingAnchor,constant: 5),
      titlekoiLabel.trailingAnchor.constraint(equalTo: bgBlackMemberView.trailingAnchor,constant: -10),
      titlekoiLabel.bottomAnchor.constraint(equalTo: bgBlackMemberView.bottomAnchor,constant: -10)
    ])
  }
   required init?(coder: NSCoder) {
     fatalError("init(coder:) has not been implemented")
   }
  
}
