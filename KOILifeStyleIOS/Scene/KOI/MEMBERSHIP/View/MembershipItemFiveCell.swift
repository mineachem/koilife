//
//  MembershipItemFiveCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MembershipItemFiveCell: UITableViewCell {
  
    lazy var titlekoiLabel: UILabel = {
       let label = UILabel()
        label.textColor = .white
       let attributedString = NSMutableAttributedString(string:
       """
        KOI THE
          Membership and Reward Programs
          
        KOI THE Membership Program consist of 3 tiers,all of which give you the opportunity to enjoy more benefits the more you drink. Double Indulgence!
          
        Join today and start you discovery right away at any of our KOI outlets in Cambodia.
                                  
                                  1 Cup = 1 Point

        Reward Points are given for every dollar spent. Theses points can be exchanged for exclusive merchandises.The more you spend, the bigger, better,more stylish, and incredible your REWARD!

                                  $1 = 1 Point
       """)
       let paragraphStyle = NSMutableParagraphStyle()
       paragraphStyle.lineSpacing = 2
       // *** Apply attribute to string ***
       
       
       attributedString.addAttribute(.font, value:UIFont(name: "Montserrat-Medium", size: 14)!, range:NSRange(location: 0, length: attributedString.length))
       attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
       label.attributedText = attributedString
       label.numberOfLines = 0
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
     }()

     
     override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
       super.init(style: style, reuseIdentifier: reuseIdentifier)
       selectionStyle = .none
       setupView()
     }
     fileprivate func setupView(){
        backgroundColor = .clear
       addSubview(titlekoiLabel)
       NSLayoutConstraint.activate([
         titlekoiLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
         titlekoiLabel.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
         titlekoiLabel.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
         titlekoiLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
       ])
     }
     required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
     }
     
}
