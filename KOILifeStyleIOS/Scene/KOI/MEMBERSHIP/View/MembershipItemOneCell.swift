//
//  MembershipItemOneCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import WebKit

class MembershipItemOneCell: UITableViewCell {
  
    lazy var webView: WKWebView = {
        
        let view = WKWebView()
        view.scrollView.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        view.scrollView.showsVerticalScrollIndicator = false
        view.isOpaque = false
        return view
        
    }()
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    backgroundColor = .clear
    addSubview(webView)
    setupView()
  }
  fileprivate func setupView(){
    
//    addSubview(webView)
    NSLayoutConstraint.activate([
      webView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
      webView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
      webView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
      webView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
    
  }
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

extension MembershipItemOneCell: UIScrollViewDelegate{
    public func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}
