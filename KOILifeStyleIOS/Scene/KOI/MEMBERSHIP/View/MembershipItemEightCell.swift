//
//  MembershipItemEightCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/26/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class MembershipItemEightCell: UITableViewCell {
      lazy var bgGoldMemberView: UIView = {
        let subView = UIView()
        subView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        subView.layer.cornerRadius = 10
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
      }()
      
        lazy var titlekoiLabel: UILabel = {
          let label = UILabel()
          let attributedString = NSMutableAttributedString(string:
          """
              Gold Membership (300 Points or above)
              Benefits of Gold Membership

              1.Special Birthday Presents

                  . Obtain One Medius-Sized drink (our treat of course)
              
              2.Get TWO Free small-sized monthly

              3.Get an early and extened access to promotions and events! (Applicable for Silver and Gold Memberships ONLY)

              4.Get special privileges to join our EXCLUSIVE promotions tailored specifically for Silver and Gold Memberships ONLY

              5.Enjoy the DOUBLE POINTS day rewarded on special days set by us

              6.Get privilege to be invited to our exclusive
              
             Criteria of Gold Membership Card

            1.You must have a registered Silver Membership Card
            2.Save your KOI Membership Point from 300 points or above (1 Cup = 1 Point)
           
          """)
          let paragraphStyle = NSMutableParagraphStyle()
          paragraphStyle.lineSpacing = 2
          // *** Apply attribute to string ***
          
          
          attributedString.addAttribute(.font, value:UIFont(name: "Montserrat-Medium", size: 14)!, range:NSRange(location: 0, length: attributedString.length))
          attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
          label.attributedText = attributedString
          label.numberOfLines = 0
          label.translatesAutoresizingMaskIntoConstraints = false
          label.textColor = .white
          return label
        }()

      override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
         super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
       }
       
      fileprivate func setupView(){
        backgroundColor = .clear
        addSubview(bgGoldMemberView)
        NSLayoutConstraint.activate([
          bgGoldMemberView.topAnchor.constraint(equalTo: topAnchor,constant: 10),
          bgGoldMemberView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 15),
          bgGoldMemberView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -15),
          bgGoldMemberView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10)
        ])
        
        bgGoldMemberView.addSubview(titlekoiLabel)
        NSLayoutConstraint.activate([
          titlekoiLabel.topAnchor.constraint(equalTo: bgGoldMemberView.topAnchor,constant: 10),
          titlekoiLabel.leadingAnchor.constraint(equalTo: bgGoldMemberView.leadingAnchor,constant: 5),
          titlekoiLabel.trailingAnchor.constraint(equalTo: bgGoldMemberView.trailingAnchor,constant: -10),
          titlekoiLabel.bottomAnchor.constraint(equalTo: bgGoldMemberView.bottomAnchor,constant: -10)
        ])
      }
      
       required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
       }
}
