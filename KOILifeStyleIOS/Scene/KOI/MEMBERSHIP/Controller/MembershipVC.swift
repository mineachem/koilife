//
//  MembershipVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/13/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import WebKit

class MembershipVC: UIViewController, WKNavigationDelegate  {

   lazy var membershipView = MembershipView()
    lazy var titleNavigationBar: UINavigationBar = {
     let navigationBar = UINavigationBar()
     let navItem = UINavigationItem()
       let label = UILabel()
       label.text = "Membership Black Membership"
        label.textColor = .white
       navItem.titleView = label
     navigationBar.setBackgroundImage(UIImage(), for: .default)
     navigationBar.shadowImage = UIImage()
     let backBtn = UIButton(type: .system)
     backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_white").withRenderingMode(.alwaysOriginal), for: .normal)
     backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
     backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
     let leftBarButton = UIBarButtonItem()
     leftBarButton.customView = backBtn
     navItem.leftBarButtonItem = leftBarButton
     navigationBar.setItems([navItem], animated: false)
     navigationBar.translatesAutoresizingMaskIntoConstraints = false
     return navigationBar
     }()
    override func loadView() {
      super.loadView()
      view = membershipView
    }
    
      override func viewDidLoad() {
          super.viewDidLoad()
           setupNavigation()
         setupView()
        
      }
      
    fileprivate func setupNavigation(){
      
    view.addSubview(titleNavigationBar)
    NSLayoutConstraint.activate([
      titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
    ])
    }
    
  fileprivate func setupView(){
    membershipView.membertableView.delegate = self
    membershipView.membertableView.dataSource = self

  }
    @objc private func handleBackTapped(){
      self.dismiss(animated: true)
    }

}

extension MembershipVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.size.height - 44 - (view.safeAreaInsets.bottom * 2.5)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let tableView = scrollView as? UITableView {
            for cell in tableView.visibleCells {
                guard let cell = cell as? MembershipItemOneCell else { continue }
                cell.webView.setNeedsLayout()
            }
        }
    }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.row {
    case 0:
      let membershipOneCell =  tableView.dequeueReusableCell(withIdentifier: "MembershipItemOneCell", for: indexPath) as! MembershipItemOneCell
      membershipOneCell.selectionStyle = .none
      let htmlFile = Bundle.main.path(forResource: "MembershipRewardPrograms", ofType: "html")
      let htmlString = try? String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
      membershipOneCell.webView.loadHTMLString(htmlString!, baseURL: nil)
      return membershipOneCell
//    case 1:
//      let membershipTwoCell =  tableView.dequeueReusableCell(withIdentifier: "MembershipItemTwoCell", for: indexPath) as! MembershipItemTwoCell
//       membershipTwoCell.selectionStyle = .none
//      return membershipTwoCell
//    case 2:
//    let membershipThreeCell =  tableView.dequeueReusableCell(withIdentifier: "MembershipItemThreeCell", for: indexPath) as! MembershipItemThreeCell
//    membershipThreeCell.selectionStyle = .none
//    return membershipThreeCell
//    case 3:
//    let membershipFourCell =  tableView.dequeueReusableCell(withIdentifier: "MembershipItemFourCell", for: indexPath) as! MembershipItemFourCell
//    membershipFourCell.selectionStyle = .none
//    return membershipFourCell
//    case 4:
//     let membershipFiveCell =  tableView.dequeueReusableCell(withIdentifier: "MembershipItemFiveCell", for: indexPath) as! MembershipItemFiveCell
//     membershipFiveCell.selectionStyle = .none
//      return membershipFiveCell
//    case 5:
//     let membershipSixCell =  tableView.dequeueReusableCell(withIdentifier: "MembershipItemSixCell", for: indexPath) as! MembershipItemSixCell
//      membershipSixCell.selectionStyle = .none
//      return membershipSixCell
//    case 6:
//   let membershipSevenCell =  tableView.dequeueReusableCell(withIdentifier: "MembershipItemSevenCell", for: indexPath) as! MembershipItemSevenCell
//    membershipSevenCell.selectionStyle = .none
//    return membershipSevenCell
//    case 7:
//    let membershipEightCell =  tableView.dequeueReusableCell(withIdentifier: "MembershipItemEightCell", for: indexPath) as! MembershipItemEightCell
//    membershipEightCell.selectionStyle = .none
//    return membershipEightCell
    default:
      return UITableViewCell()
    }

  }
  
  
}
