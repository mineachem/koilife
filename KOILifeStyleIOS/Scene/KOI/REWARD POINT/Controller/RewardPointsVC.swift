//
//  RewardPointsVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/29/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class RewardPointsVC: UIViewController {
    
    var emptyView = EmptyInboxView()
    var refreshCtr = UIRefreshControl()
    lazy var loading: NVActivityIndicatorView = {
        let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
        loading.translatesAutoresizingMaskIntoConstraints = false
        return loading
    }()

  lazy var titleNavigationBar: UINavigationBar = {
     let navigationBar = UINavigationBar()
     let navItem = UINavigationItem()
     let titleNav = UILabel()
     titleNav.text = "REWARD POINTS"
     titleNav.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
     navItem.titleView = titleNav
     navigationBar.setBackgroundImage(UIImage(), for: .default)
     navigationBar.shadowImage = UIImage()
     let backBtn = UIButton(type: .system)
     backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
      backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
     backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
     let leftBarButton = UIBarButtonItem()
     leftBarButton.customView = backBtn
     navItem.leftBarButtonItem = leftBarButton
     navigationBar.setItems([navItem], animated: false)
     navigationBar.translatesAutoresizingMaskIntoConstraints = false
     return navigationBar
   }()
    
  var rewardPointTableView:UITableView!
  private let rewardpointIdentifier = "rewardpointCell"
   var resultsRewardPont = [ResultRewardPoint]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIndicator()
        emptyView.emptyDescription.text = "There is no reward points."
        view.backgroundColor = .white
        getdataFromService()
        setupNavigationBar()
        setupTableView()
        rewardPointTableView.backgroundView = loading
    }
  
    fileprivate func setupIndicator(){
        view.addSubview(loading)
        NSLayoutConstraint.activate([
          loading.widthAnchor.constraint(equalToConstant: 40),
          loading.heightAnchor.constraint(equalToConstant: 40),
          loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
          loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
     
   }
  
  fileprivate func getdataFromService(){
    self.showLoading()
    
    Alamofire.request(KoiService.share.rewardpointAllUrl, method: .get, encoding: JSONEncoding.default).responseJSON { (response) in
      switch response.result {
      case .success:
        let jsonData = response.data!
        let rewardpointResponse = try? JSONDecoder().decode(RewardPoint.self, from: jsonData)
        if rewardpointResponse?.response?.code == 200 {

          self.resultsRewardPont = rewardpointResponse!.results!
          
            if self.resultsRewardPont.count > 0 {
                DispatchQueue.main.async {
                  self.rewardPointTableView.reloadData()
                    self.rewardPointTableView.backgroundView = .none
                
             }
            } else{
                self.rewardPointTableView.backgroundView = self.emptyView
            }
        }else {
            self.rewardPointTableView.backgroundView = self.emptyView
        }
        self.hideLoading()
        self.refreshCtr.endRefreshing()
      case .failure(let failure):
        print(failure.localizedDescription)
        self.hideLoading()
        self.refreshCtr.endRefreshing()
        self.rewardPointTableView.backgroundView = self.emptyView
      }
    }
    
    
  }
  
  fileprivate func setupNavigationBar(){
    view.addSubview(titleNavigationBar)
    NSLayoutConstraint.activate([
      titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
    ])
  }
    
  fileprivate func setupTableView(){
    rewardPointTableView = UITableView()
    rewardPointTableView.delegate = self
    rewardPointTableView.dataSource = self
    refreshCtr.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
    // TableView
    rewardPointTableView.addSubview(refreshCtr)
    rewardPointTableView.separatorStyle = .none
    rewardPointTableView.showsVerticalScrollIndicator = false
    rewardPointTableView.showsHorizontalScrollIndicator = false
    rewardPointTableView.translatesAutoresizingMaskIntoConstraints = false
    rewardPointTableView.register(RewardPointTVCell.self, forCellReuseIdentifier: rewardpointIdentifier)
    
    view.addSubview(titleLabel)
    titleLabel.anchor(top: titleNavigationBar.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
    
    
    view.addSubview(rewardPointTableView)
    
    NSLayoutConstraint.activate([
      rewardPointTableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
      rewardPointTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      rewardPointTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      rewardPointTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
    ])
  }
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "COLLECT   \nAWESOME STUFFS"
        label.textColor = .black
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        return label
    }()
    func showLoading()
    {
        self.loading.startAnimating()
    }

    func hideLoading()
    {
         loading.stopAnimating()
    }
    
    @objc func refresh() {
        getdataFromService()
    }

  @objc private func handleBackTapped(){
     self.dismiss(animated: true)
   }
  
  
}

extension RewardPointsVC:UITableViewDelegate,UITableViewDataSource {
    
//  func numberOfSections(in tableView: UITableView) -> Int {
//    return 1
//  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return resultsRewardPont.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let rewardPointCell = tableView.dequeueReusableCell(withIdentifier: rewardpointIdentifier, for: indexPath) as! RewardPointTVCell
   rewardPointCell.rewardpontStickerImgView.loadImageUsingUrlString(urlString:"\(KoiService.share.displayImgUrl)\(resultsRewardPont[indexPath.row].thumbnail ?? "")")
    print("\(KoiService.share.displayImgUrl)\(resultsRewardPont[indexPath.row].thumbnail ?? "")")
    return rewardPointCell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let detailRewardpointVC = DetailRewardPointVC()
   // detailRewardpointVC.resultRewardPoint = resultsRewardPont[indexPath.row]
    detailRewardpointVC.modalPresentationStyle = .overCurrentContext
    detailRewardpointVC.modalTransitionStyle = .crossDissolve
    self.present(detailRewardpointVC, animated: true)
  }
  
}
