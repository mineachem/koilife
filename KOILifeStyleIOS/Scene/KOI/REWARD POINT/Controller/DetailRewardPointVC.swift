//
//  DetailRewardPointVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/12/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class DetailRewardPointVC: UIViewController {

  var resultsRewardPont:ResultRewardPoint?
  
  lazy var titleNavigationBar: UINavigationBar = {
  let navigationBar = UINavigationBar()
  let navItem = UINavigationItem()
    let label = UILabel()
    label.text = "Reward Points Detail"
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    navItem.titleView = label
  navigationBar.setBackgroundImage(UIImage(), for: .default)
  navigationBar.shadowImage = UIImage()
  let backBtn = UIButton(type: .system)
  backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
  backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
  backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
  let leftBarButton = UIBarButtonItem()
  leftBarButton.customView = backBtn
  navItem.leftBarButtonItem = leftBarButton
  navigationBar.setItems([navItem], animated: false)
  navigationBar.translatesAutoresizingMaskIntoConstraints = false
  return navigationBar
  }()
    
  
  lazy var detailRewardpointView = DetailRewardPointView()
  
  override func loadView() {
    super.loadView()
    self.view = detailRewardpointView
    
  }
    override func viewDidLoad() {
        super.viewDidLoad()

       setupNavigation()
       setupView()
    }
    

   fileprivate func setupNavigation(){
     
   view.addSubview(titleNavigationBar)
   NSLayoutConstraint.activate([
     titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
     titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
     titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
   ])
  }

  fileprivate func setupView(){
    detailRewardpointView.rewardPointTableView.delegate = self
    detailRewardpointView.rewardPointTableView.dataSource = self
  }
  
  @objc private func handleBackTapped(){
    self.dismiss(animated: true)
  }
  

}

extension DetailRewardPointVC:UITableViewDelegate,UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let rewardpointCell = tableView.dequeueReusableCell(withIdentifier: "RewardPointCell", for: indexPath) as! RewardPointCell
    rewardpointCell.resultsRewardPont = resultsRewardPont
    return rewardpointCell
    
  }
  
  
}
