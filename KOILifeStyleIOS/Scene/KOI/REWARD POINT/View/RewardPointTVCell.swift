//
//  RewardPointTVCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 11/29/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class RewardPointTVCell: UITableViewCell {
  

  
  lazy var rewardpontStickerImgView: CustomImageView = {
     let imageView = CustomImageView()
    imageView.contentMode = .scaleAspectFill
     imageView.layer.masksToBounds = true
    imageView.layer.cornerRadius = 10
     imageView.translatesAutoresizingMaskIntoConstraints = false
     return imageView
   }()
  
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    addSubview(rewardpontStickerImgView)
    
    NSLayoutConstraint.activate([
      rewardpontStickerImgView.topAnchor.constraint(equalTo: topAnchor,constant: 10),
      rewardpontStickerImgView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
      rewardpontStickerImgView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
      rewardpontStickerImgView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10),
      rewardpontStickerImgView.heightAnchor.constraint(equalToConstant: 180)
    ])
  }
  
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
