//
//  DetailRewardPointView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/12/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class DetailRewardPointView: UIView {

  lazy var rewardPointTableView: UITableView = {
    let tableView = UITableView()
    tableView.separatorStyle = .none
    tableView.showsHorizontalScrollIndicator = false
    tableView.showsVerticalScrollIndicator = false
    tableView.register(RewardPointCell.self, forCellReuseIdentifier: "RewardPointCell")
    tableView.translatesAutoresizingMaskIntoConstraints = false
    return tableView
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  fileprivate func setupUI(){
    addSubview(rewardPointTableView)
    NSLayoutConstraint.activate([
      rewardPointTableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
      rewardPointTableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
      rewardPointTableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
      rewardPointTableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
    ])

  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
}
