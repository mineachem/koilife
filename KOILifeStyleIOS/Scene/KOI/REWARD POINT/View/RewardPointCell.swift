//
//  RewardPointCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/31/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class RewardPointCell: UITableViewCell {

  var resultsRewardPont:ResultRewardPoint?{
    didSet{
      if let coverImage = resultsRewardPont?.image {
        let coverImg = URL(string: KoiService.share.REWARD_BASE_IMAGE)?.appendingPathComponent(coverImage)
          DispatchQueue.global().async {
                    guard let imageData = try? Data(contentsOf: coverImg!) else { return }
                      let image = UIImage(data: imageData)
                      DispatchQueue.main.async {
                          if image != nil {
                              self.rewardpontDetailImgView.image = image
                         
                          }else {
                              self.rewardpontDetailImgView.image = #imageLiteral(resourceName: "img-5")
                               }
                             }
                       }
      }
      
      if let titleReward = resultsRewardPont?.name {
        titleRewardPointLabel.text = titleReward
      }
      
      if let descriptions = resultsRewardPont?.resultDescription {
        descriptionRewardPointLabel.text = descriptions.htmlToString
      }
      
      if let points = resultsRewardPont?.point {
        pointBtn.setTitle("\(points) POINT", for: .normal)
      }
    }
  }
  
  lazy var rewardpontDetailImgView: UIImageView = {
      let iv = UIImageView()
      iv.translatesAutoresizingMaskIntoConstraints = false
      iv.contentMode = .scaleAspectFill
      iv.layer.masksToBounds = true
      iv.layer.cornerRadius = 10
      return iv
  }()
  
  lazy var titleRewardPointLabel: UILabel = {
    let label = UILabel()
    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.font = UIFont.systemFont(ofSize: 25, weight: .bold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var pointBtn: UIButton = {
    let button = UIButton()
    button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .medium)
    button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    button.layer.cornerRadius = 10
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var descriptionRewardPointLabel: UILabel = {
     let label = UILabel()
     label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    label.numberOfLines = 0
     label.textAlignment = .left
     label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
     setupUI()
  }
  
  fileprivate func setupUI(){
    backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    addSubview(rewardpontDetailImgView)
    
    NSLayoutConstraint.activate([
      rewardpontDetailImgView.topAnchor.constraint(equalTo: topAnchor,constant: 50),
      rewardpontDetailImgView.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
      rewardpontDetailImgView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
      rewardpontDetailImgView.heightAnchor.constraint(equalTo: heightAnchor,multiplier: 0.7)
    ])
    
    addSubview(titleRewardPointLabel)
    NSLayoutConstraint.activate([
      titleRewardPointLabel.topAnchor.constraint(equalTo: rewardpontDetailImgView.bottomAnchor,constant: 10),
      titleRewardPointLabel.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
      titleRewardPointLabel.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10)
    ])
    
    addSubview(pointBtn)
    
    NSLayoutConstraint.activate([
      pointBtn.topAnchor.constraint(equalTo: titleRewardPointLabel.bottomAnchor,constant: 10),
      pointBtn.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
    ])
    
    addSubview(descriptionRewardPointLabel)
    
    NSLayoutConstraint.activate([
      descriptionRewardPointLabel.topAnchor.constraint(equalTo: pointBtn.bottomAnchor,constant: 10),
      descriptionRewardPointLabel.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10),
      descriptionRewardPointLabel.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10),
      //descriptionRewardPointLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor,constant: -10)
    ])
  }
  
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
