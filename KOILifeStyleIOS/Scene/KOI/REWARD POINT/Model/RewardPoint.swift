//
//  RewardPoint.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/12/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation

// MARK: - RewardPoint
struct RewardPoint: Codable {
    let response: ResponseRewardPoint?
    let results: [ResultRewardPoint]?
}

// MARK: - Response
struct ResponseRewardPoint: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Result
struct ResultRewardPoint: Codable {
    let id: Int?
    let name: String?
    let point: Int?
    let thumbnail, image, resultDescription: String?
    let statusReward: StatusReward?
    let status: Bool?

    enum CodingKeys: String, CodingKey {
        case id, name, point, thumbnail, image
        case resultDescription = "description"
        case statusReward, status
    }
}

// MARK: - StatusReward
struct StatusReward: Codable {
    let id: Int?
    let name: String?
    let status: Bool?
}
