//
//  MapviewVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/9/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class OutletMapviewVC: UIViewController {
  
  var currentLocationMarker: GMSMarker?
  lazy var mapView = OutletMapView()
  
  lazy var outletCustomView: OutletDetailCustomView = {
    let outletCustomView = OutletDetailCustomView()
    outletCustomView.outletMapViewVC = self
    return outletCustomView
  }()
  
  var locationManager = CLLocationManager()
  var currentLocation: CLLocation?
  let zoomLevel: Float = 12.0
  var lat:Double?
  var lng:Double?
  let customMarkerWidth: Int = 50
  let customMarkerHeight: Int = 70
  var placesClient: GMSPlacesClient!
  // An array to hold the list of likely places.
  var likelyPlaces: [GMSPlace] = []

  // The currently selected place.
  var selectedPlace: GMSPlace?
  var koiNearByData = [ResultMapNearBy]()
  
 // var outlets = [OutletModel]()
lazy var whiteView: UIView = {
       let v = UIView()
       v.translatesAutoresizingMaskIntoConstraints = false
       v.backgroundColor = .white
       return v
   }()
    
  override func loadView() {
    super.loadView()
    self.view = mapView
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()

        setupGoogleMapView()
        setupEvent()
    }
    
  //MARK: SETUP VIEW
  
  
  fileprivate func setupGoogleMapView() {
     getCurrentLocation()
    mapView.addSubview(whiteView)
    whiteView.topAnchor.constraint(equalTo: mapView.topAnchor, constant: 0).isActive = true
    whiteView.leadingAnchor.constraint(equalTo: mapView.leadingAnchor).isActive = true
    whiteView.trailingAnchor.constraint(equalTo: mapView.trailingAnchor).isActive = true
    whiteView.heightAnchor.constraint(equalToConstant: 50).isActive = true
   
  }
  
  func getNearbyByLatLongMapFromService(){
    
   let markerView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
    markerView.image = #imageLiteral(resourceName: "ic_koi_marker")
    
    let param:[String:Any] = [
                          "lat": lat!,
                          "lng": lng!,
                          "countryCode": "kh"
                          ]
    
    IGNetworkRequest.shareInstance.requestPOSTURL(KoiService.share.outletnearbyUrl, params: param, success: { (responseKoiNearBy:OutletMapNearBy) in
      if responseKoiNearBy.response?.code == 200 {
         print("outet \(responseKoiNearBy)")
        self.koiNearByData = responseKoiNearBy.results!
        for koiMap in responseKoiNearBy.results! {
               let lat = Double(koiMap.lat!)
               let lng = Double(koiMap.lng!)
                   let positon = CLLocationCoordinate2D(latitude: lat! , longitude: lng!)
            self.currentLocationMarker = GMSMarker(position: positon)
            self.currentLocationMarker?.title = koiMap.name
            self.currentLocationMarker?.iconView = markerView
            self.currentLocationMarker?.map = self.mapView.mapOutletLocationView
            self.currentLocationMarker?.rotation = self.locationManager.location?.course ?? 0

             }
      }else {
        print(responseKoiNearBy.response?.message ?? "")
      }
    }) { (failure) in
      print(failure)
    }

  
  }

  
  
  fileprivate func setupEvent(){
    mapView.outletBtn.addTarget(self, action: #selector(handleOutletTapped), for: .touchUpInside)
    mapView.getCurrentLocationBtn.addTarget(self, action: #selector(handleGetCurrentLocationTapped), for: .touchUpInside)
  }
  
  
    @objc private func handleOutletTapped(){
    
      let outletVC = OutletsVC()
        outletVC.koiNearByData = self.koiNearByData
      DispatchQueue.main.async {
      outletVC.outletTableView.reloadData()
      }
  
      outletVC.modalTransitionStyle = .crossDissolve
      outletVC.modalPresentationStyle = .overCurrentContext
      self.present(outletVC, animated: true)
    }
  
  
  @objc func handleGetCurrentLocationTapped(sender:UIButton){

    getCurrentLocation()
   
    
  }
  
  func getCurrentLocation(){
    if CLLocationManager.locationServicesEnabled() {
      startMonitoringLocation()
     // addCurrentLocationMarker()
      if let location = locationManager.location {
        lat = location.coordinate.latitude
        lng = location.coordinate.longitude
         
        KoiService.mLng = lng ?? 0
        KoiService.mLate = lat ?? 0
        
        
        getNearbyByLatLongMapFromService()
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,longitude: location.coordinate.longitude , zoom: zoomLevel)
        self.mapView.mapOutletLocationView.animate(to: camera)
        self.mapView.mapOutletLocationView.settings.compassButton = true
        self.mapView.mapOutletLocationView.isMyLocationEnabled = true
      }
      
      let status = CLLocationManager.authorizationStatus()
      
      if(status == .denied || status == .restricted || !CLLocationManager.locationServicesEnabled()){
        // show alert to user telling them they need to allow location data to use some feature of your app
        return
      }
      
      // if haven't show location permission dialog before, show it to user
      if(status == .notDetermined){
        locationManager.requestWhenInUseAuthorization()
        
        // if you want the app to retrieve location data even in background, use requestAlwaysAuthorization
        // locationManager.requestAlwaysAuthorization()
        return
      }
      
      // at this point the authorization status is authorized
      // request location data once
      locationManager.startUpdatingHeading()
      
      // start monitoring location data and get notified whenever there is change in location data / every few seconds, until stopUpdatingLocation() is called
      // locationManager.startUpdatingLocation()
    }
  }
  
  func startMonitoringLocation() {
    if CLLocationManager.locationServicesEnabled() {
      //locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
      locationManager.activityType = CLActivityType.automotiveNavigation
      locationManager.distanceFilter = 100
      locationManager.headingFilter = 1
      //locationManager.requestAlwaysAuthorization()
      locationManager.requestWhenInUseAuthorization()
      locationManager.startMonitoringSignificantLocationChanges()
      locationManager.startUpdatingLocation()
      placesClient = GMSPlacesClient.shared()
      mapView.mapOutletLocationView.settings.compassButton = true
      mapView.mapOutletLocationView.isMyLocationEnabled = true
      
    }
  }
  
  func stopMonitoringLocation() {
    locationManager.stopMonitoringSignificantLocationChanges()
    locationManager.stopUpdatingLocation()
  }
  
  
//  fileprivate func addCurrentLocationMarker() {
//
//  }
  
  fileprivate func zoomToCoordinates(_ coordinates: CLLocationCoordinate2D) {
    let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: zoomLevel)
    
    mapView.mapOutletLocationView.camera = camera
  }
  

}


extension OutletMapviewVC:GMSMapViewDelegate {
  //MARK:GOOGLE MAP DELEGATE
  
  func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    guard let customMarkerView = marker.iconView as? CustomMarkerView else { return false }
    let img = customMarkerView.img!
    let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight),image: img,borderColor: UIColor.white,tag: customMarkerView.tag)
    marker.iconView = customMarker
    return false
  }
  
  func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
    guard let customMarkerView = marker.iconView as? CustomMarkerView else { return }
    let img = customMarkerView.img!
    let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight), image: img, borderColor: .white, tag: customMarkerView.tag)
    marker.iconView = customMarker
  }
  
  func showPartyMarkers(lat:Double,long:Double){
    mapView.mapOutletLocationView.clear()
    
//    for i in 0..<3 {
//      let randNum = Double(arc4random_uniform(30))/10000
//      let marker = GMSMarker()
//      //let customMa
//    }
  }
}

extension OutletMapviewVC:CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = locations.last
    
    let lat = (location?.coordinate.latitude)!
    let long = (location?.coordinate.longitude)!
    let camara = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: zoomLevel)
    self.mapView.mapOutletLocationView.animate(to: camara)
    
  }
  
  // Handle authorization for the location manager.
     func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
         switch status {
         case .restricted:
             print("Location access was restricted.")
         case .denied:
             print("User denied access to location.")
             // Display the map using the default location.
         case .notDetermined:
             print("Location status not determined.")
         case .authorizedAlways:
             fallthrough
         case .authorizedWhenInUse:
             print("Location status is OK.")
         }
     }

     // Handle location manager errors.
     func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
         locationManager.stopUpdatingLocation()
         print("Error: \(error)")
     }
}
