//
//  OutletDetailVC.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/13/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class OutletDetailVC: UIViewController {

  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    let label = UILabel()
    label.text = "DETAIL OUTLETS"
   label.font = UIFont.boldSystemFont(ofSize: 14)
   navItem.titleView = label
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
  
  lazy var detailOutletView = DetailOutletView()
  var lat:String? = ""
  var lng:String? = ""
  var call:String? = ""
    var images = [String]()
    var outlets: ResultMapNearBy? {
        didSet{
        setupView(outlet: outlets!)
            self.images.removeAll()
        let images = outlets?.images?.components(separatedBy: [","])
            if images?.count ?? 0 > 0 {
                for image in images! {
                    let url = "\(KoiService.share.BRANCH_BASE_IMAGE)\(image)"
                    self.images.append(url)
                    }
            }
    
            detailOutletView.cardOutletCollectionView.reloadData()
        }
        
    }
    
    
  

  override func loadView() {
    super.loadView()
    self.view = detailOutletView
    
  }
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigation()
        
        setupEvent()
    }
    
  fileprivate func setupNavigation(){
    
  view.addSubview(titleNavigationBar)
  NSLayoutConstraint.activate([
    titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
    titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
    titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
  ])
  }
  
    fileprivate func setupView(outlet: ResultMapNearBy){
    detailOutletView.cardOutletCollectionView.delegate = self
    detailOutletView.cardOutletCollectionView.dataSource = self
   
    
    detailOutletView.telOutletLabel.text =  "Tel: \(call ?? "")"
    detailOutletView.titleOutletLabel.text = outlet.name ?? ""
   
    detailOutletView.timeOutletLabel.text = "8:00AM - 9:30PM"
    //"\(String(describing: outlets?.startHour ?? "")) - \(String(describing: outlets?.stopHour ?? ""))"
    detailOutletView.pageControl.numberOfPages = images.count
    
    
  }
  
  fileprivate func setupEvent(){
    //detailOutletView.pageControl.addTarget(self, action: #selector(pageChanged), for: .valueChanged)
    detailOutletView.directionButton.addTarget(self, action: #selector(handleDirectionTapped), for: .touchUpInside)
    
    detailOutletView.callButton.addTarget(self, action: #selector(handleCallTapped), for: .touchUpInside)
  }

  
//  @objc private func pageChanged(){
//
//  }
  @objc private func handleDirectionTapped(){
    if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
      UIApplication.shared.open(URL(string:
        "comgooglemaps://?saddr=&daddr=\(String(describing: Double(lat!)!)),\(String(describing: Double(lng!)!))&directionsmode=driving")!)

        } else {
            NSLog("Can't use comgooglemaps://");
        }
  }

  @objc private func handleCallTapped(){
   let string = call!
    var phoneNumber = [Character]()
    for st in string{
        if let _ = Int(String(st)) {
            phoneNumber.append(st)
        }
    }
    let url: URL = URL(string: "TEL://\(String(phoneNumber))")!
    UIApplication.shared.open(url, options: [:], completionHandler: nil)
  }
  
  @objc private func handleBackTapped(){
    self.dismiss(animated: true)
  }

}


extension OutletDetailVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return images.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cardOutletCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardOutletCell", for: indexPath) as! CardOutletCell
    cardOutletCell.cardOutletImgView.loadImageUsingUrlString(urlString: images[indexPath.item])

    return cardOutletCell
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    self.detailOutletView.pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
  }
  
  
  
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    self.detailOutletView.pageControl.currentPage = indexPath.row
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: view.frame.width, height: view.frame.height)
  }
}
