//
//  OutletDetailCustomView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/18/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class OutletDetailCustomView: NSObject,UITableViewDelegate,UITableViewDataSource {
  
   let cellHight:CGFloat = 100
  private let outletIdentifier = "outletCell"
   var titleStringArr = ["male","female"]
   let blackView = UIView()
  
  lazy var tableview:UITableView = {
    let tableView = UITableView()
    tableView.backgroundColor = .white
    let subView = UIView()
    subView.backgroundColor = .white
    tableView.tableFooterView = subView
    return tableView
  }()
  
  var outletMapViewVC:OutletMapviewVC?
  
  func showOutletLocation(){
    if let window = UIApplication.shared.keyWindow {
      self.blackView.backgroundColor = UIColor(white: 0, alpha: 0)
      self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
      window.addSubview(blackView)
      window.addSubview(tableview)
     
      let height:CGFloat = CGFloat(titleStringArr.count) * cellHight
      let y = window.frame.height - height
      tableview.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
    
      self.blackView.frame = window.frame
      self.blackView.alpha = 0
      
      UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        self.blackView.alpha = 1
        self.tableview.frame = CGRect(x: 0, y: y, width: self.tableview.frame.width, height: self.tableview.frame.height)
      
      }, completion: nil)
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return titleStringArr.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let outletDetailCell = tableView.dequeueReusableCell(withIdentifier: outletIdentifier, for: indexPath)
    return outletDetailCell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
         self.blackView.alpha = 0
         
         if let window = UIApplication.shared.keyWindow {
           self.tableview.frame = CGRect(x: 0, y: window.frame.height, width: self.tableview.frame.width, height: self.tableview.frame.height)
         }
       }) { (completed) in

       }
  }
  
   @objc func handleDismiss(){
      UIView.animate(withDuration: 0.5) {
        self.blackView.alpha = 0
        if let window = UIApplication.shared.keyWindow {
          self.tableview.frame = CGRect(x: 0, y: window.frame.height, width: self.tableview.frame.width, height: self.tableview.frame.height)
        }
      }
    }

  
  override init() {
    super.init()
    tableview.delegate = self
    tableview.dataSource = self
    tableview.register(UITableViewCell.self, forCellReuseIdentifier: outletIdentifier)
  }
}
