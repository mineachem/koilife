//
//  CardOutletCell.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/13/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class CardOutletCell: UICollectionViewCell {
    
  lazy var bgCardOutletView: UIView = {
    let subView = UIView()
    subView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    subView.layer.cornerRadius = 10
    subView.translatesAutoresizingMaskIntoConstraints = false
    return subView
  }()
  
  lazy var cardOutletImgView: CustomImageView = {
    let imageView = CustomImageView()
    imageView.contentMode = .scaleAspectFit
    imageView.layer.masksToBounds = true
    imageView.layer.cornerRadius = 10
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  

  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  fileprivate func setupUI(){
    addSubview(bgCardOutletView)
    
    NSLayoutConstraint.activate([
        bgCardOutletView.topAnchor.constraint(equalTo: self.topAnchor),
      bgCardOutletView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
      bgCardOutletView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
      bgCardOutletView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
   
    ])

    bgCardOutletView.addSubview(cardOutletImgView)
    NSLayoutConstraint.activate([
      cardOutletImgView.topAnchor.constraint(equalTo: bgCardOutletView.topAnchor),
      cardOutletImgView.leadingAnchor.constraint(equalTo: bgCardOutletView.leadingAnchor, constant: 10),
      cardOutletImgView.trailingAnchor.constraint(equalTo: bgCardOutletView.trailingAnchor, constant: -10),
      cardOutletImgView.bottomAnchor.constraint(equalTo: bgCardOutletView.bottomAnchor)
    ])
  
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
