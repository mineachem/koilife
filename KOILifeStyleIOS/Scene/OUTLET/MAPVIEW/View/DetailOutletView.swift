//
//  DetailOutletView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/13/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class DetailOutletView: UIView {

  lazy var cardOutletCollectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal
    layout.minimumLineSpacing = 0
    layout.minimumInteritemSpacing = 0
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    collectionView.backgroundColor = .none
    collectionView.showsVerticalScrollIndicator = false
    collectionView.showsHorizontalScrollIndicator = false
    collectionView.register(CardOutletCell.self, forCellWithReuseIdentifier: "CardOutletCell")
    collectionView.isPagingEnabled = true
    collectionView.layer.cornerRadius = 10
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    return collectionView
  }()
 
  lazy var pageControl: UIPageControl = {
       let pageControl = UIPageControl()
       pageControl.currentPage = 0
       //pageControl.numberOfPages = 4
       pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
       pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
      pageControl.translatesAutoresizingMaskIntoConstraints = false
       return pageControl
     }()
  
  lazy var titleOutletLabel: UILabel = {
     let label = UILabel()
     label.text = "KOI IFL"
     label.textAlignment = .left
     label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var telOutletLabel: UILabel = {
    let label = UILabel()
    label.text = "Tel:"
    label.textAlignment = .left
    label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var timeOutletLabel: UILabel = {
     let label = UILabel()
     label.text = "8AM - 10AM"
     label.textAlignment = .left
     label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
     label.translatesAutoresizingMaskIntoConstraints = false
     return label
   }()
  
  lazy var descOutletLabel: UILabel = {
    let label = UILabel()
    label.text = "66 Preah Monivong Blvd,B1B1 Sangkat Wat Phnom ,Khan Daun Penh,Phnom Penh,Cambodia"
    label.textAlignment = .left
    label.numberOfLines = 2
    label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  lazy var directionButton: UIButton = {
        let button = UIButton(type: .system)
       button.setTitle("DIRECTION", for: .normal)
       button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
       button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
       button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
       button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
       button.layer.shadowOpacity = 1.0
       button.layer.shadowRadius = 0.0
       button.layer.masksToBounds = false
       button.layer.cornerRadius = 10.0
       button.translatesAutoresizingMaskIntoConstraints = false
      return button
    }()
  
  lazy var callButton: UIButton = {
      let button = UIButton(type: .system)
     button.setTitle("CALL", for: .normal)
     button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
     button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
     button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
     button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
     button.layer.shadowOpacity = 1.0
     button.layer.shadowRadius = 0.0
     button.layer.masksToBounds = false
     button.layer.cornerRadius = 10.0
     button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .white
    setupUI()
  }
  
  
  fileprivate func setupUI(){
    addSubview(cardOutletCollectionView)
    NSLayoutConstraint.activate([
        cardOutletCollectionView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 44),
      cardOutletCollectionView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 0),
      cardOutletCollectionView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: 0),
      cardOutletCollectionView.heightAnchor.constraint(equalTo: safeAreaLayoutGuide.heightAnchor,multiplier: 0.63)
    ])
    
    addSubview(pageControl)
    NSLayoutConstraint.activate([
      pageControl.topAnchor.constraint(equalTo: cardOutletCollectionView.bottomAnchor,constant: -40),
      pageControl.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      pageControl.heightAnchor.constraint(equalToConstant: 50)
    ])
    
   let contentTitleoutletHStack = UIStackView(arrangedSubviews: [titleOutletLabel])
    contentTitleoutletHStack.axis = .horizontal
    contentTitleoutletHStack.alignment = .fill
    contentTitleoutletHStack.distribution = .fill
    contentTitleoutletHStack.spacing = 5
    contentTitleoutletHStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentDescriptionOutletVStack = UIStackView(arrangedSubviews: [timeOutletLabel,telOutletLabel,descOutletLabel])
    contentDescriptionOutletVStack.axis = .vertical
    contentDescriptionOutletVStack.alignment = .fill
    contentDescriptionOutletVStack.distribution = .fill
    contentDescriptionOutletVStack.spacing = 0
    contentDescriptionOutletVStack.translatesAutoresizingMaskIntoConstraints = false
    
    let contentDetailOutletVStack = UIStackView(arrangedSubviews: [contentTitleoutletHStack,contentDescriptionOutletVStack])
    contentDetailOutletVStack.axis = .vertical
    contentDetailOutletVStack.alignment = .fill
    contentDetailOutletVStack.distribution = .fill
    contentDetailOutletVStack.spacing = 10
    contentDetailOutletVStack.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentDetailOutletVStack)
    
    NSLayoutConstraint.activate([
      contentDetailOutletVStack.topAnchor.constraint(equalTo: cardOutletCollectionView.bottomAnchor,constant: 10),
      contentDetailOutletVStack.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 20),
      contentDetailOutletVStack.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -20)
    ])
    
    
    let contentBtnOutletVStack = UIStackView(arrangedSubviews: [directionButton,callButton])
       contentBtnOutletVStack.axis = .vertical
       contentBtnOutletVStack.alignment = .fill
       contentBtnOutletVStack.distribution = .fill
       contentBtnOutletVStack.spacing = 10
      contentBtnOutletVStack.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(contentBtnOutletVStack)
    
    NSLayoutConstraint.activate([
      contentBtnOutletVStack.topAnchor.constraint(equalTo: contentDetailOutletVStack.bottomAnchor,constant: 30),
      contentBtnOutletVStack.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,constant: 30),
      contentBtnOutletVStack.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -30),
      directionButton.heightAnchor.constraint(equalToConstant: 35),
      callButton.heightAnchor.constraint(equalToConstant: 35)
      
    ])
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
