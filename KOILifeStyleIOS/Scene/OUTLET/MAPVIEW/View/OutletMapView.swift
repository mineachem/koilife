//
//  MapView.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/9/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import GoogleMaps
class OutletMapView: UIView {

   lazy var outletBtn: UIButton = {
     let button = UIButton(type: .system)
     button.setTitle("OUTLET LIST", for: .normal)
     //button.set
     button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
     button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
     button.layer.cornerRadius = 15
     button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
     button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
     button.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
     button.layer.shadowOpacity = 1.0
     button.layer.shadowRadius = 0.0
     button.layer.masksToBounds = false
     button.translatesAutoresizingMaskIntoConstraints = false
     return button
   }()
  
  lazy var getCurrentLocationBtn:UIButton = {
    let button = UIButton(type: .system)
    button.setTitleColor(.black, for: .normal)
    button.backgroundColor = .white
    button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    button.setImage(#imageLiteral(resourceName: "current-location").withRenderingMode(.alwaysOriginal), for: .normal)
    //button.imageEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    button.layer.cornerRadius = 15
    button.clipsToBounds = true
    button.contentHorizontalAlignment = .leading
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  lazy var bgOutletView: UIView = {
     let view = UIView()
    view.backgroundColor = .none
     view.translatesAutoresizingMaskIntoConstraints = false
     return view
   }()
  
   lazy var mapOutletLocationView:GMSMapView = {
    let map = GMSMapView(frame: .zero)
     map.translatesAutoresizingMaskIntoConstraints = false
     return map
   }()
  
   
    
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  fileprivate func setupUI(){
    backgroundColor = .white
      addSubview(mapOutletLocationView)
      mapOutletLocationView.addSubview(getCurrentLocationBtn)
      mapOutletLocationView.addSubview(bgOutletView)
      bgOutletView.addSubview(outletBtn)
    
       NSLayoutConstraint.activate([
       mapOutletLocationView.topAnchor.constraint(equalTo: topAnchor),
       mapOutletLocationView.leadingAnchor.constraint(equalTo: leadingAnchor),
       mapOutletLocationView.trailingAnchor.constraint(equalTo: trailingAnchor),
       mapOutletLocationView.bottomAnchor.constraint(equalTo:bottomAnchor)
       ])
    
    
       
       NSLayoutConstraint.activate([
         getCurrentLocationBtn.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
         getCurrentLocationBtn.bottomAnchor.constraint(equalTo: bgOutletView.topAnchor,constant: 20),
         getCurrentLocationBtn.widthAnchor.constraint(equalToConstant: 45),
         getCurrentLocationBtn.heightAnchor.constraint(equalToConstant: 45)
       ])
    
    
       
       NSLayoutConstraint.activate([
         bgOutletView.heightAnchor.constraint(equalToConstant: 100),
         bgOutletView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
         bgOutletView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
         bgOutletView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
       ])
    
    
    NSLayoutConstraint.activate([
      outletBtn.bottomAnchor.constraint(equalTo: bgOutletView.bottomAnchor,constant: -20),
      outletBtn.leadingAnchor.constraint(equalTo: bgOutletView.leadingAnchor,constant: 10),
      outletBtn.trailingAnchor.constraint(equalTo: bgOutletView.trailingAnchor,constant: -10),
      outletBtn.heightAnchor.constraint(equalToConstant: 35)
    ])
    
   
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
