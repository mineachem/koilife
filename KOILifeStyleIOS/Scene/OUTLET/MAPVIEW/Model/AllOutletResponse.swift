//
//  AllOutletResponse.swift
//  KOILifeStyleIOS
//
//  Created by Apple on 12/29/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
struct AllOutletResponse: Codable {
    var response: ResponseModel
    var results: [OutletModel]
    
}
struct OutletModel: Codable {
    var id: Int?
    var code: String?
    var name: String?
    var phone1: String?
    var phone2: String?
    var address: String?
    var companyCode: String?
    var email: String?
    var description: String?
    var lat: String?
    var lng: String?
    var countryCode: String?
    var startHour: String?
    var stopHour: String?
    var terminalId: String?
    var images: String?
    var status: Bool?
    var version: Int?
    var hq: Bool?
}
