//
//  OutletMapNearBy.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/14/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
// MARK: - OutletMapNearBy
struct OutletMapNearBy: Codable {
    let response: ResponseMapNearBy?
    let results: [ResultMapNearBy]?
}

// MARK: - Response
struct ResponseMapNearBy: Codable {
    let code: Int?
    let message: String?
}

// MARK: - Result
struct ResultMapNearBy: Codable {
    let address, name: String?
    let id: Int?
    let resultDescription: String?
    let code, phone1, lat, lng: String?
    let email, phone2: String?
    let companyCode: String?
    let distanceInKm: Double?
    let images: String?

    enum CodingKeys: String, CodingKey {
        case address, name, id
        case resultDescription = "description"
        case code, phone1, lat, lng, email, phone2
        case companyCode = "company_code"
        case distanceInKm = "distance_in_km"
        case images
    }
}
