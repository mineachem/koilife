//
//  BaseCollectionviewController.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import Foundation
import MJRefresh
import NVActivityIndicatorView

class BaseCollectionviewController: UICollectionViewController,UICollectionViewDelegateFlowLayout {
    
    let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
      var deletegate: LoadMoreDataProtocol?
       var page = -1
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        NetworkManager.isUnreachable { networkManagerInstance in
//            print("Not Internet")
//            ApiService.sharedInstance.hideLoading()
//            ApiService.sharedInstance.hideMyloading()
//            self.retryNetwork()
//
//        }
    }
    
   fileprivate func setupIndicator(){
      
         loading.translatesAutoresizingMaskIntoConstraints = false
          
         collectionView.addSubview(loading)
         NSLayoutConstraint.activate([
           loading.widthAnchor.constraint(equalToConstant: 40),
           loading.heightAnchor.constraint(equalToConstant: 40),
           loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
           loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
         ])
      
    }
    func showLoading()
    {
      loading.startAnimating()
    }

    func hideLoading()
    {
      loading.stopAnimating()
    }
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func retryNetwork()
//    {
//        NetworkManager.isUnreachable { networkManagerInstance in
//            print("Not Internet")
//            ApiService.sharedInstance.hideLoading()
//            //   self.collectionView.setEmpty(view: self.emptyView)
//
//            AppConstant.showAlertNoInternet(vc: self, titleStr: "", messageStr: "Connection error\nPlease check your connection\n and try again", actionTitle: "Retry", completionHandler: { (isClick) in
//                if isClick {
//                    NetworkManager.isReachable(completed: { (isReachable) in
//                        self.retryInternetDelegate?.RetryUI()
//                    })
//                    NetworkManager.isUnreachable(completed: { (isUnreachable) in
//                        self.retryNetwork()
//                    })
//                }
//            })
//        }
//    }
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.collectionView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {
            self.page += 1
            
            self.deletegate?.loadMoreData(page: self.page)
        })
        

    }
  
    
    
}
