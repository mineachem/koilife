//
//  BaseViewController.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/19/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController,NVActivityIndicatorViewable {

  lazy var titleNavigationBar: UINavigationBar = {
    let navigationBar = UINavigationBar()
    let navItem = UINavigationItem()
    
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
    let backBtn = UIButton(type: .system)
    backBtn.setImage(#imageLiteral(resourceName: "ic_back_arrow_black").withRenderingMode(.alwaysOriginal), for: .normal)
    backBtn.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 35)
    backBtn.addTarget(self, action: #selector(handleBackTapped), for: .touchUpInside)
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = backBtn
    navItem.leftBarButtonItem = leftBarButton
    navigationBar.setItems([navItem], animated: false)
    navigationBar.translatesAutoresizingMaskIntoConstraints = false
    return navigationBar
  }()
  
  let loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), padding: 0)
  
  override func loadView() {
    super.loadView()
    setupLoadView()
  }
  
  
   func setupLoadView(){}
  
  override func viewDidLoad() {
        super.viewDidLoad()
    setupIndicator()
    getDataFromService()
    setupNavigationBar()
    setupUI()
    setupEvent()
    }
    
  
  fileprivate func setupIndicator(){
    
       loading.translatesAutoresizingMaskIntoConstraints = false
        
       view.addSubview(loading)
       NSLayoutConstraint.activate([
         loading.widthAnchor.constraint(equalToConstant: 40),
         loading.heightAnchor.constraint(equalToConstant: 40),
         loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
       ])
    
  }
  
  func getDataFromService(){}
  
   func setupNavigationBar(){
    view.addSubview(titleNavigationBar)
    NSLayoutConstraint.activate([
      titleNavigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      titleNavigationBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      titleNavigationBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
    ])
  }
  
 
  
   func setupUI(){}
  
   func setupEvent(){}
  
  
  func showLoading()
   {
     loading.startAnimating()
   }

   func hideLoading()
   {
     
     loading.stopAnimating()
   }
  
  @objc private func handleBackTapped(){
      //self.view.window?.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
      view.endEditing(true)
      self.dismiss(animated: true)
      
    }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
  }
    
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }

}
