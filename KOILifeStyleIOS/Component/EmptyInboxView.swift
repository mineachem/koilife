//
//  EmptyInboxView.swift
//  KOILifeStyleIOS
//
//  Created by MOLIDA LOEUNG on 12/25/19.
//  Copyright © 2019 Minea. All rights reserved.
//

import UIKit

class EmptyInboxView: UIView{
    
    lazy var emptyIcon: UIImageView = {
        
        let image = UIImageView(image: UIImage(named: "ic_empty_inbox"))
        image.contentMode = .scaleAspectFit
        image.layer.masksToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
        
    }()
    
    lazy var emptyDescription: UILabel = {
        
        let label = UILabel()
        label.textColor = .lightGray
        label.textAlignment = .center
        label.font = UIFont(name: "Montserrat-Medium", size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    lazy var stack: UIStackView = {
        
        let stack = UIStackView(arrangedSubviews: [emptyIcon, emptyDescription])
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.alignment = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(stack)
        
        setupView()
    }
    
    func setupView(){
        
        NSLayoutConstraint.activate([
            
            emptyIcon.widthAnchor.constraint(equalToConstant: 100),
            emptyIcon.heightAnchor.constraint(equalToConstant: 100),
            emptyDescription.leadingAnchor.constraint(equalTo: leadingAnchor),
            emptyDescription.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            stack.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            stack.centerYAnchor.constraint(equalTo: centerYAnchor)
        
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
