//
//  LoadMoreDataProtocol.swift
//  KOILifeStyleIOS
//
//  Created by User on 12/20/19.
//  Copyright © 2019 Minea. All rights reserved.
//


import Foundation
protocol LoadMoreDataProtocol {
    func loadMoreData(page: Int)
}
